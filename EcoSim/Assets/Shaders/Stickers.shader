Shader "Ctrl-J/Stickers" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	
	SubShader {
		Tags {
			"Queue" = "Transparent-100"
			"IgnoreProjector"="True"
		}
		Cull Off
		ColorMask RGB
		
		Pass {
			Lighting Off
			Offset -25, -1
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			Color [_Color]
		}
	}	


	Fallback Off
}
