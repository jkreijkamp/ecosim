Shader "Ctrl-J/Select-Additive" {
	Properties {
        _MainTex ("Base (RGB) Trans. (Alpha)", 2D) = "white" { }
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	
	SubShader {
		Tags {
			"Queue" = "Transparent+100"
			"RenderType" = "Transparent"
			"IgnoreProjector"="True"
		}
		Cull Back
		ColorMask RGB
		
		Pass {
			Lighting Off
			ZTest Always
			Fog { Mode Off }
			Blend SrcAlpha OneMinusSrcAlpha
			Color [_Color]
			SetTexture [_MainTex]
			{
				combine previous * texture, texture
			} 
		}
	}	


	Fallback Off
}
