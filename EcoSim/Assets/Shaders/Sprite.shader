Shader "Ctrl-J/Sprite"
{
    Properties
    {
        _MainTex ("Base (RGB) Trans. (Alpha)", 2D) = "white" { }
		
    }

	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		Pass
		{
			ZWrite Off
			ZTest Always  
			Alphatest Off
			Fog { Mode Off }
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			Lighting Off
			SetTexture [_MainTex]
			{
				combine texture, texture
			} 
		}
	} 
}