Shader "Ctrl-J/Test" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Main Texture", 2D) = "white" {  }
	}
	

	SubShader {
		Tags {
			"Queue" = "Background"
			"IgnoreProjector"="True"
		}
		Cull Off
		ColorMask RGB
		
		Pass {
			Lighting Off
			BindChannels {
				Bind "Color", color
				Bind "Vertex", vertex
				Bind "TexCoord", texcoord
			}			
			AlphaTest GEqual [_Cutoff]
			ZWrite On
			
			SetTexture [_MainTex] {
				constantColor (1,1,1,1)						
				combine primary	 * texture double, texture
			}
		}
	}	


	Fallback Off
}
