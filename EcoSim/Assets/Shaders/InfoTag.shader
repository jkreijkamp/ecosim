Shader "Ctrl-J/InfoTag" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType" = "Opaque" }
		CGPROGRAM
		#pragma surface surf MyLighting addshadow
      
      
		struct Input {
			float2 uv_MainTex;
		};
		
		half4 _Color;
		sampler2D _MainTex;
		
		half4 LightingMyLighting(SurfaceOutput s, half3 lightDir, half atten) {
			half4 result;
			result.rgb = s.Albedo * _Color;
			result.a = 1.0;
			return result;
		}      
		
		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
		}
		ENDCG
	} 
	Fallback "Ctrl-J/Opaque-NoLighting"
}