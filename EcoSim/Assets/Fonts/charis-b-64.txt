info face="CharisSIL-Bold" size=64 bold=0 italic=0 charset="" unicode=0 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=2,2
common lineHeight=105 base=77 scaleW=1024 scaleH=512 pages=1 packed=0
page id=0 file="charis-b-64.png"
chars count=190
char id=124 x=2 y=2 width=6 height=68 xoffset=10 yoffset=26 xadvance=25 page=0 chnl=0 letter="|"
char id=197 x=10 y=2 width=44 height=64 xoffset=0 yoffset=13 xadvance=44 page=0 chnl=0 letter="Å"
char id=254 x=56 y=2 width=36 height=63 xoffset=1 yoffset=29 xadvance=39 page=0 chnl=0 letter="þ"
char id=106 x=94 y=2 width=22 height=63 xoffset=-6 yoffset=29 xadvance=21 page=0 chnl=0 letter="j"
char id=210 x=118 y=2 width=45 height=62 xoffset=2 yoffset=17 xadvance=48 page=0 chnl=0 letter="Ò"
char id=211 x=165 y=2 width=45 height=62 xoffset=2 yoffset=17 xadvance=48 page=0 chnl=0 letter="Ó"
char id=217 x=212 y=2 width=44 height=62 xoffset=1 yoffset=17 xadvance=47 page=0 chnl=0 letter="Ù"
char id=218 x=258 y=2 width=44 height=62 xoffset=1 yoffset=17 xadvance=47 page=0 chnl=0 letter="Ú"
char id=253 x=304 y=2 width=36 height=62 xoffset=0 yoffset=30 xadvance=36 page=0 chnl=0 letter="ý"
char id=255 x=342 y=2 width=36 height=62 xoffset=0 yoffset=30 xadvance=36 page=0 chnl=0 letter="ÿ"
char id=212 x=380 y=2 width=45 height=61 xoffset=2 yoffset=18 xadvance=48 page=0 chnl=0 letter="Ô"
char id=214 x=427 y=2 width=45 height=61 xoffset=2 yoffset=18 xadvance=48 page=0 chnl=0 letter="Ö"
char id=219 x=474 y=2 width=44 height=61 xoffset=1 yoffset=18 xadvance=47 page=0 chnl=0 letter="Û"
char id=220 x=520 y=2 width=44 height=61 xoffset=1 yoffset=18 xadvance=47 page=0 chnl=0 letter="Ü"
char id=166 x=566 y=2 width=4 height=61 xoffset=9 yoffset=29 xadvance=22 page=0 chnl=0 letter="¦"
char id=213 x=572 y=2 width=45 height=60 xoffset=2 yoffset=19 xadvance=48 page=0 chnl=0 letter="Õ"
char id=192 x=619 y=2 width=44 height=60 xoffset=0 yoffset=17 xadvance=44 page=0 chnl=0 letter="À"
char id=193 x=665 y=2 width=44 height=60 xoffset=0 yoffset=17 xadvance=44 page=0 chnl=0 letter="Á"
char id=221 x=711 y=2 width=42 height=60 xoffset=0 yoffset=17 xadvance=42 page=0 chnl=0 letter="Ý"
char id=200 x=755 y=2 width=36 height=60 xoffset=1 yoffset=17 xadvance=38 page=0 chnl=0 letter="È"
char id=201 x=793 y=2 width=36 height=60 xoffset=1 yoffset=17 xadvance=38 page=0 chnl=0 letter="É"
char id=204 x=831 y=2 width=21 height=60 xoffset=1 yoffset=17 xadvance=23 page=0 chnl=0 letter="Ì"
char id=205 x=854 y=2 width=21 height=60 xoffset=1 yoffset=17 xadvance=23 page=0 chnl=0 letter="Í"
char id=194 x=877 y=2 width=44 height=59 xoffset=0 yoffset=18 xadvance=44 page=0 chnl=0 letter="Â"
char id=196 x=923 y=2 width=44 height=59 xoffset=0 yoffset=18 xadvance=44 page=0 chnl=0 letter="Ä"
char id=199 x=969 y=2 width=37 height=59 xoffset=2 yoffset=33 xadvance=41 page=0 chnl=0 letter="Ç"
char id=202 x=2 y=72 width=36 height=59 xoffset=1 yoffset=18 xadvance=38 page=0 chnl=0 letter="Ê"
char id=203 x=40 y=72 width=36 height=59 xoffset=1 yoffset=18 xadvance=38 page=0 chnl=0 letter="Ë"
char id=206 x=78 y=72 width=23 height=59 xoffset=0 yoffset=18 xadvance=23 page=0 chnl=0 letter="Î"
char id=207 x=103 y=72 width=23 height=59 xoffset=0 yoffset=18 xadvance=23 page=0 chnl=0 letter="Ï"
char id=209 x=128 y=72 width=45 height=58 xoffset=1 yoffset=19 xadvance=48 page=0 chnl=0 letter="Ñ"
char id=195 x=175 y=72 width=44 height=58 xoffset=0 yoffset=19 xadvance=44 page=0 chnl=0 letter="Ã"
char id=167 x=221 y=72 width=28 height=57 xoffset=2 yoffset=30 xadvance=32 page=0 chnl=0 letter="§"
char id=81 x=251 y=72 width=45 height=56 xoffset=2 yoffset=33 xadvance=48 page=0 chnl=0 letter="Q"
char id=41 x=298 y=72 width=20 height=56 xoffset=2 yoffset=31 xadvance=25 page=0 chnl=0 letter=")"
char id=40 x=320 y=72 width=20 height=56 xoffset=3 yoffset=31 xadvance=25 page=0 chnl=0 letter="("
char id=64 x=342 y=72 width=54 height=55 xoffset=3 yoffset=33 xadvance=60 page=0 chnl=0 letter="@"
char id=36 x=398 y=72 width=32 height=55 xoffset=3 yoffset=29 xadvance=38 page=0 chnl=0 letter="$"
char id=93 x=432 y=72 width=19 height=55 xoffset=2 yoffset=31 xadvance=38 page=0 chnl=0 letter="]"
char id=91 x=453 y=72 width=18 height=55 xoffset=15 yoffset=31 xadvance=35 page=0 chnl=0 letter="["
char id=216 x=473 y=72 width=45 height=54 xoffset=2 yoffset=29 xadvance=48 page=0 chnl=0 letter="Ø"
char id=125 x=520 y=72 width=26 height=54 xoffset=4 yoffset=31 xadvance=32 page=0 chnl=0 letter="}"
char id=123 x=548 y=72 width=25 height=54 xoffset=2 yoffset=31 xadvance=32 page=0 chnl=0 letter="{"
char id=229 x=575 y=72 width=33 height=53 xoffset=2 yoffset=25 xadvance=35 page=0 chnl=0 letter="å"
char id=169 x=610 y=72 width=50 height=51 xoffset=2 yoffset=29 xadvance=54 page=0 chnl=0 letter="©"
char id=174 x=662 y=72 width=50 height=51 xoffset=2 yoffset=29 xadvance=54 page=0 chnl=0 letter="®"
char id=240 x=714 y=72 width=33 height=50 xoffset=2 yoffset=28 xadvance=37 page=0 chnl=0 letter="ð"
char id=223 x=749 y=72 width=41 height=49 xoffset=1 yoffset=29 xadvance=43 page=0 chnl=0 letter="ß"
char id=100 x=792 y=72 width=36 height=49 xoffset=2 yoffset=29 xadvance=39 page=0 chnl=0 letter="d"
char id=47 x=830 y=72 width=32 height=49 xoffset=0 yoffset=34 xadvance=32 page=0 chnl=0 letter="/"
char id=92 x=864 y=72 width=32 height=49 xoffset=0 yoffset=34 xadvance=32 page=0 chnl=0 letter="\"
char id=182 x=898 y=72 width=31 height=49 xoffset=1 yoffset=34 xadvance=33 page=0 chnl=0 letter="¶"
char id=190 x=931 y=72 width=54 height=48 xoffset=2 yoffset=31 xadvance=58 page=0 chnl=0 letter="¾"
char id=35 x=2 y=133 width=41 height=48 xoffset=3 yoffset=31 xadvance=47 page=0 chnl=0 letter="#"
char id=249 x=45 y=133 width=38 height=48 xoffset=1 yoffset=30 xadvance=40 page=0 chnl=0 letter="ù"
char id=250 x=85 y=133 width=38 height=48 xoffset=1 yoffset=30 xadvance=40 page=0 chnl=0 letter="ú"
char id=251 x=125 y=133 width=38 height=48 xoffset=1 yoffset=30 xadvance=40 page=0 chnl=0 letter="û"
char id=252 x=165 y=133 width=38 height=48 xoffset=1 yoffset=30 xadvance=40 page=0 chnl=0 letter="ü"
char id=104 x=205 y=133 width=38 height=48 xoffset=1 yoffset=29 xadvance=40 page=0 chnl=0 letter="h"
char id=107 x=245 y=133 width=37 height=48 xoffset=1 yoffset=29 xadvance=38 page=0 chnl=0 letter="k"
char id=98 x=284 y=133 width=36 height=48 xoffset=1 yoffset=29 xadvance=38 page=0 chnl=0 letter="b"
char id=242 x=322 y=133 width=34 height=48 xoffset=2 yoffset=30 xadvance=37 page=0 chnl=0 letter="ò"
char id=243 x=358 y=133 width=34 height=48 xoffset=2 yoffset=30 xadvance=37 page=0 chnl=0 letter="ó"
char id=244 x=394 y=133 width=34 height=48 xoffset=2 yoffset=30 xadvance=37 page=0 chnl=0 letter="ô"
char id=246 x=430 y=133 width=34 height=48 xoffset=2 yoffset=30 xadvance=37 page=0 chnl=0 letter="ö"
char id=224 x=466 y=133 width=33 height=48 xoffset=2 yoffset=30 xadvance=35 page=0 chnl=0 letter="à"
char id=225 x=501 y=133 width=33 height=48 xoffset=2 yoffset=30 xadvance=35 page=0 chnl=0 letter="á"
char id=226 x=536 y=133 width=33 height=48 xoffset=2 yoffset=30 xadvance=35 page=0 chnl=0 letter="â"
char id=228 x=571 y=133 width=33 height=48 xoffset=2 yoffset=30 xadvance=35 page=0 chnl=0 letter="ä"
char id=232 x=606 y=133 width=30 height=48 xoffset=2 yoffset=30 xadvance=34 page=0 chnl=0 letter="è"
char id=233 x=638 y=133 width=30 height=48 xoffset=2 yoffset=30 xadvance=34 page=0 chnl=0 letter="é"
char id=234 x=670 y=133 width=30 height=48 xoffset=2 yoffset=30 xadvance=34 page=0 chnl=0 letter="ê"
char id=235 x=702 y=133 width=30 height=48 xoffset=2 yoffset=30 xadvance=34 page=0 chnl=0 letter="ë"
char id=102 x=734 y=133 width=26 height=48 xoffset=1 yoffset=29 xadvance=22 page=0 chnl=0 letter="f"
char id=108 x=762 y=133 width=19 height=48 xoffset=1 yoffset=29 xadvance=21 page=0 chnl=0 letter="l"
char id=105 x=783 y=133 width=18 height=48 xoffset=2 yoffset=29 xadvance=21 page=0 chnl=0 letter="i"
char id=188 x=803 y=133 width=54 height=47 xoffset=2 yoffset=32 xadvance=58 page=0 chnl=0 letter="¼"
char id=112 x=859 y=133 width=36 height=47 xoffset=1 yoffset=45 xadvance=39 page=0 chnl=0 letter="p"
char id=121 x=897 y=133 width=36 height=47 xoffset=0 yoffset=45 xadvance=36 page=0 chnl=0 letter="y"
char id=113 x=935 y=133 width=35 height=47 xoffset=2 yoffset=45 xadvance=39 page=0 chnl=0 letter="q"
char id=181 x=972 y=133 width=35 height=47 xoffset=3 yoffset=45 xadvance=37 page=0 chnl=0 letter="µ"
char id=245 x=2 y=183 width=34 height=47 xoffset=2 yoffset=31 xadvance=37 page=0 chnl=0 letter="õ"
char id=103 x=38 y=183 width=33 height=47 xoffset=2 yoffset=45 xadvance=36 page=0 chnl=0 letter="g"
char id=227 x=73 y=183 width=33 height=47 xoffset=2 yoffset=31 xadvance=35 page=0 chnl=0 letter="ã"
char id=162 x=108 y=183 width=30 height=47 xoffset=4 yoffset=37 xadvance=38 page=0 chnl=0 letter="¢"
char id=231 x=140 y=183 width=29 height=47 xoffset=2 yoffset=45 xadvance=32 page=0 chnl=0 letter="ç"
char id=239 x=171 y=183 width=23 height=47 xoffset=-1 yoffset=30 xadvance=21 page=0 chnl=0 letter="ï"
char id=238 x=196 y=183 width=23 height=47 xoffset=-1 yoffset=30 xadvance=21 page=0 chnl=0 letter="î"
char id=237 x=221 y=183 width=20 height=47 xoffset=2 yoffset=30 xadvance=21 page=0 chnl=0 letter="í"
char id=236 x=243 y=183 width=20 height=47 xoffset=0 yoffset=30 xadvance=21 page=0 chnl=0 letter="ì"
char id=37 x=265 y=183 width=55 height=46 xoffset=1 yoffset=33 xadvance=57 page=0 chnl=0 letter="%"
char id=79 x=322 y=183 width=45 height=46 xoffset=2 yoffset=33 xadvance=48 page=0 chnl=0 letter="O"
char id=71 x=369 y=183 width=43 height=46 xoffset=2 yoffset=33 xadvance=46 page=0 chnl=0 letter="G"
char id=67 x=414 y=183 width=37 height=46 xoffset=2 yoffset=33 xadvance=41 page=0 chnl=0 letter="C"
char id=241 x=453 y=183 width=37 height=46 xoffset=1 yoffset=31 xadvance=40 page=0 chnl=0 letter="ñ"
char id=52 x=492 y=183 width=34 height=46 xoffset=2 yoffset=33 xadvance=38 page=0 chnl=0 letter="4"
char id=54 x=528 y=183 width=34 height=46 xoffset=2 yoffset=33 xadvance=38 page=0 chnl=0 letter="6"
char id=56 x=564 y=183 width=34 height=46 xoffset=2 yoffset=33 xadvance=38 page=0 chnl=0 letter="8"
char id=57 x=600 y=183 width=34 height=46 xoffset=2 yoffset=33 xadvance=38 page=0 chnl=0 letter="9"
char id=51 x=636 y=183 width=33 height=46 xoffset=2 yoffset=33 xadvance=38 page=0 chnl=0 letter="3"
char id=83 x=671 y=183 width=30 height=46 xoffset=2 yoffset=33 xadvance=34 page=0 chnl=0 letter="S"
char id=87 x=703 y=183 width=62 height=45 xoffset=0 yoffset=34 xadvance=62 page=0 chnl=0 letter="W"
char id=189 x=767 y=183 width=54 height=45 xoffset=2 yoffset=32 xadvance=58 page=0 chnl=0 letter="½"
char id=38 x=823 y=183 width=45 height=45 xoffset=2 yoffset=33 xadvance=47 page=0 chnl=0 letter="&"
char id=86 x=870 y=183 width=45 height=45 xoffset=0 yoffset=34 xadvance=45 page=0 chnl=0 letter="V"
char id=82 x=917 y=183 width=45 height=45 xoffset=1 yoffset=34 xadvance=45 page=0 chnl=0 letter="R"
char id=85 x=964 y=183 width=44 height=45 xoffset=1 yoffset=34 xadvance=47 page=0 chnl=0 letter="U"
char id=48 x=2 y=232 width=34 height=45 xoffset=2 yoffset=33 xadvance=38 page=0 chnl=0 letter="0"
char id=55 x=38 y=232 width=34 height=45 xoffset=2 yoffset=34 xadvance=38 page=0 chnl=0 letter="7"
char id=53 x=74 y=232 width=33 height=45 xoffset=2 yoffset=34 xadvance=38 page=0 chnl=0 letter="5"
char id=74 x=109 y=232 width=31 height=45 xoffset=1 yoffset=34 xadvance=33 page=0 chnl=0 letter="J"
char id=63 x=142 y=232 width=28 height=45 xoffset=2 yoffset=33 xadvance=33 page=0 chnl=0 letter="?"
char id=191 x=172 y=232 width=27 height=45 xoffset=4 yoffset=45 xadvance=33 page=0 chnl=0 letter="¿"
char id=33 x=201 y=232 width=12 height=45 xoffset=5 yoffset=33 xadvance=22 page=0 chnl=0 letter="!"
char id=161 x=215 y=232 width=12 height=45 xoffset=5 yoffset=45 xadvance=22 page=0 chnl=0 letter="¡"
char id=65 x=229 y=232 width=44 height=44 xoffset=0 yoffset=33 xadvance=44 page=0 chnl=0 letter="A"
char id=163 x=275 y=232 width=33 height=44 xoffset=2 yoffset=33 xadvance=38 page=0 chnl=0 letter="£"
char id=50 x=310 y=232 width=33 height=44 xoffset=2 yoffset=33 xadvance=38 page=0 chnl=0 letter="2"
char id=49 x=345 y=232 width=26 height=44 xoffset=4 yoffset=33 xadvance=38 page=0 chnl=0 letter="1"
char id=59 x=373 y=232 width=14 height=44 xoffset=2 yoffset=45 xadvance=20 page=0 chnl=0 letter=";"
char id=198 x=389 y=232 width=61 height=43 xoffset=0 yoffset=34 xadvance=62 page=0 chnl=0 letter="Æ"
char id=77 x=452 y=232 width=56 height=43 xoffset=1 yoffset=34 xadvance=58 page=0 chnl=0 letter="M"
char id=72 x=510 y=232 width=47 height=43 xoffset=1 yoffset=34 xadvance=49 page=0 chnl=0 letter="H"
char id=78 x=559 y=232 width=45 height=43 xoffset=1 yoffset=34 xadvance=48 page=0 chnl=0 letter="N"
char id=208 x=606 y=232 width=44 height=43 xoffset=0 yoffset=34 xadvance=46 page=0 chnl=0 letter="Ð"
char id=68 x=652 y=232 width=43 height=43 xoffset=1 yoffset=34 xadvance=46 page=0 chnl=0 letter="D"
char id=75 x=697 y=232 width=43 height=43 xoffset=1 yoffset=34 xadvance=44 page=0 chnl=0 letter="K"
char id=88 x=742 y=232 width=43 height=43 xoffset=0 yoffset=34 xadvance=43 page=0 chnl=0 letter="X"
char id=89 x=787 y=232 width=42 height=43 xoffset=0 yoffset=34 xadvance=42 page=0 chnl=0 letter="Y"
char id=165 x=831 y=232 width=41 height=43 xoffset=0 yoffset=34 xadvance=41 page=0 chnl=0 letter="¥"
char id=66 x=874 y=232 width=38 height=43 xoffset=1 yoffset=34 xadvance=41 page=0 chnl=0 letter="B"
char id=84 x=914 y=232 width=37 height=43 xoffset=1 yoffset=34 xadvance=39 page=0 chnl=0 letter="T"
char id=80 x=953 y=232 width=36 height=43 xoffset=1 yoffset=34 xadvance=39 page=0 chnl=0 letter="P"
char id=222 x=2 y=279 width=36 height=43 xoffset=1 yoffset=34 xadvance=39 page=0 chnl=0 letter="Þ"
char id=69 x=40 y=279 width=36 height=43 xoffset=1 yoffset=34 xadvance=38 page=0 chnl=0 letter="E"
char id=248 x=78 y=279 width=34 height=43 xoffset=2 yoffset=40 xadvance=37 page=0 chnl=0 letter="ø"
char id=76 x=114 y=279 width=34 height=43 xoffset=1 yoffset=34 xadvance=35 page=0 chnl=0 letter="L"
char id=70 x=150 y=279 width=34 height=43 xoffset=1 yoffset=34 xadvance=36 page=0 chnl=0 letter="F"
char id=90 x=186 y=279 width=33 height=43 xoffset=2 yoffset=34 xadvance=37 page=0 chnl=0 letter="Z"
char id=73 x=221 y=279 width=21 height=43 xoffset=1 yoffset=34 xadvance=23 page=0 chnl=0 letter="I"
char id=116 x=244 y=279 width=23 height=41 xoffset=1 yoffset=37 xadvance=24 page=0 chnl=0 letter="t"
char id=43 x=269 y=279 width=40 height=39 xoffset=12 yoffset=38 xadvance=64 page=0 chnl=0 letter="+"
char id=177 x=311 y=279 width=40 height=39 xoffset=12 yoffset=38 xadvance=64 page=0 chnl=0 letter="±"
char id=215 x=353 y=279 width=36 height=36 xoffset=14 yoffset=40 xadvance=64 page=0 chnl=0 letter="×"
char id=164 x=391 y=279 width=36 height=35 xoffset=2 yoffset=32 xadvance=39 page=0 chnl=0 letter="¤"
char id=230 x=429 y=279 width=46 height=34 xoffset=2 yoffset=44 xadvance=49 page=0 chnl=0 letter="æ"
char id=60 x=477 y=279 width=40 height=34 xoffset=12 yoffset=41 xadvance=64 page=0 chnl=0 letter="<"
char id=62 x=519 y=279 width=40 height=34 xoffset=12 yoffset=41 xadvance=64 page=0 chnl=0 letter=">"
char id=117 x=561 y=279 width=38 height=33 xoffset=1 yoffset=45 xadvance=40 page=0 chnl=0 letter="u"
char id=111 x=601 y=279 width=34 height=33 xoffset=2 yoffset=45 xadvance=37 page=0 chnl=0 letter="o"
char id=97 x=637 y=279 width=33 height=33 xoffset=2 yoffset=45 xadvance=35 page=0 chnl=0 letter="a"
char id=101 x=672 y=279 width=30 height=33 xoffset=2 yoffset=45 xadvance=34 page=0 chnl=0 letter="e"
char id=99 x=704 y=279 width=29 height=33 xoffset=2 yoffset=45 xadvance=32 page=0 chnl=0 letter="c"
char id=114 x=735 y=279 width=26 height=33 xoffset=1 yoffset=45 xadvance=28 page=0 chnl=0 letter="r"
char id=115 x=763 y=279 width=25 height=33 xoffset=2 yoffset=45 xadvance=28 page=0 chnl=0 letter="s"
char id=58 x=790 y=279 width=11 height=33 xoffset=4 yoffset=45 xadvance=20 page=0 chnl=0 letter=":"
char id=109 x=803 y=279 width=56 height=32 xoffset=1 yoffset=45 xadvance=58 page=0 chnl=0 letter="m"
char id=119 x=861 y=279 width=54 height=32 xoffset=0 yoffset=45 xadvance=54 page=0 chnl=0 letter="w"
char id=110 x=917 y=279 width=37 height=32 xoffset=1 yoffset=45 xadvance=40 page=0 chnl=0 letter="n"
char id=118 x=956 y=279 width=36 height=32 xoffset=0 yoffset=45 xadvance=36 page=0 chnl=0 letter="v"
char id=120 x=2 y=324 width=35 height=32 xoffset=0 yoffset=45 xadvance=35 page=0 chnl=0 letter="x"
char id=122 x=39 y=324 width=28 height=32 xoffset=2 yoffset=45 xadvance=31 page=0 chnl=0 letter="z"
char id=247 x=69 y=324 width=40 height=29 xoffset=12 yoffset=43 xadvance=64 page=0 chnl=0 letter="÷"
char id=179 x=111 y=324 width=22 height=28 xoffset=1 yoffset=26 xadvance=24 page=0 chnl=0 letter="³"
char id=178 x=135 y=324 width=22 height=27 xoffset=1 yoffset=26 xadvance=24 page=0 chnl=0 letter="²"
char id=185 x=159 y=324 width=17 height=27 xoffset=3 yoffset=26 xadvance=24 page=0 chnl=0 letter="¹"
char id=42 x=178 y=324 width=26 height=25 xoffset=3 yoffset=31 xadvance=32 page=0 chnl=0 letter="*"
char id=171 x=206 y=324 width=25 height=25 xoffset=2 yoffset=49 xadvance=29 page=0 chnl=0 letter="«"
char id=187 x=233 y=324 width=25 height=25 xoffset=2 yoffset=49 xadvance=29 page=0 chnl=0 letter="»"
char id=186 x=260 y=324 width=22 height=25 xoffset=1 yoffset=33 xadvance=24 page=0 chnl=0 letter="º"
char id=170 x=284 y=324 width=22 height=25 xoffset=1 yoffset=33 xadvance=23 page=0 chnl=0 letter="ª"
char id=44 x=308 y=324 width=14 height=22 xoffset=2 yoffset=67 xadvance=20 page=0 chnl=0 letter=","
char id=34 x=324 y=324 width=21 height=20 xoffset=2 yoffset=31 xadvance=25 page=0 chnl=0 letter="""
char id=39 x=347 y=324 width=9 height=20 xoffset=1 yoffset=31 xadvance=11 page=0 chnl=0 letter="'"
char id=61 x=358 y=324 width=40 height=16 xoffset=12 yoffset=50 xadvance=64 page=0 chnl=0 letter="="
char id=184 x=400 y=324 width=14 height=16 xoffset=2 yoffset=76 xadvance=16 page=0 chnl=0 letter="¸"
char id=172 x=416 y=324 width=36 height=15 xoffset=2 yoffset=50 xadvance=40 page=0 chnl=0 letter="¬"
char id=176 x=454 y=324 width=15 height=15 xoffset=3 yoffset=30 xadvance=20 page=0 chnl=0 letter="°"
char id=126 x=471 y=324 width=43 height=14 xoffset=5 yoffset=51 xadvance=53 page=0 chnl=0 letter="~"
char id=94 x=516 y=324 width=22 height=12 xoffset=0 yoffset=30 xadvance=22 page=0 chnl=0 letter="^"
char id=180 x=540 y=324 width=17 height=12 xoffset=7 yoffset=30 xadvance=24 page=0 chnl=0 letter="´"
char id=96 x=559 y=324 width=17 height=12 xoffset=0 yoffset=30 xadvance=24 page=0 chnl=0 letter="`"
char id=46 x=578 y=324 width=11 height=11 xoffset=4 yoffset=67 xadvance=20 page=0 chnl=0 letter="."
char id=168 x=591 y=324 width=23 height=10 xoffset=0 yoffset=30 xadvance=22 page=0 chnl=0 letter="¨"
char id=183 x=616 y=324 width=8 height=9 xoffset=2 yoffset=51 xadvance=12 page=0 chnl=0 letter="·"
char id=45 x=626 y=324 width=17 height=7 xoffset=2 yoffset=58 xadvance=21 page=0 chnl=0 letter="-"
char id=173 x=645 y=324 width=17 height=7 xoffset=2 yoffset=58 xadvance=21 page=0 chnl=0 letter="­"
char id=95 x=664 y=324 width=22 height=6 xoffset=0 yoffset=82 xadvance=22 page=0 chnl=0 letter="_"
char id=175 x=688 y=324 width=22 height=6 xoffset=0 yoffset=34 xadvance=22 page=0 chnl=0 letter="¯"
char id=32 x=776 y=324 width=0 height=0 xoffset=0 yoffset=197 xadvance=19 page=0 chnl=0 letter="space"
kernings count=192
kerning first=203 second=127 amount=-5
kerning first=203 second=128 amount=-5
kerning first=203 second=129 amount=-5
kerning first=203 second=130 amount=-5
kerning first=203 second=131 amount=-5
kerning first=203 second=132 amount=-5
kerning first=203 second=134 amount=-5
kerning first=203 second=135 amount=-5
kerning first=203 second=136 amount=-5
kerning first=203 second=137 amount=-5
kerning first=203 second=138 amount=-5
kerning first=203 second=139 amount=-5
kerning first=203 second=140 amount=-5
kerning first=203 second=141 amount=-5
kerning first=203 second=142 amount=-5
kerning first=203 second=143 amount=-5
kerning first=203 second=144 amount=-5
kerning first=203 second=145 amount=-5
kerning first=203 second=146 amount=-5
kerning first=203 second=147 amount=-5
kerning first=203 second=148 amount=-5
kerning first=203 second=149 amount=-5
kerning first=203 second=150 amount=-5
kerning first=203 second=151 amount=-5
kerning first=203 second=152 amount=-5
kerning first=203 second=153 amount=-5
kerning first=203 second=154 amount=-5
kerning first=203 second=156 amount=-5
kerning first=203 second=157 amount=-5
kerning first=203 second=158 amount=-5
kerning first=203 second=159 amount=-5
kerning first=81 second=252 amount=-7
kerning first=81 second=235 amount=-7
kerning first=81 second=102 amount=-1
kerning first=81 second=236 amount=-7
kerning first=81 second=52 amount=-1
kerning first=81 second=42 amount=-1
kerning first=64 second=56 amount=-1
kerning first=64 second=51 amount=-1
kerning first=64 second=50 amount=-1
kerning first=64 second=42 amount=-4
kerning first=64 second=34 amount=-1
kerning first=91 second=235 amount=-6
kerning first=91 second=236 amount=-6
kerning first=91 second=173 amount=-1
kerning first=47 second=52 amount=-2
kerning first=102 second=102 amount=-1
kerning first=121 second=44 amount=-5
kerning first=121 second=46 amount=-5
kerning first=79 second=102 amount=-4
kerning first=79 second=82 amount=-4
kerning first=87 second=121 amount=-1
kerning first=87 second=65 amount=-2
kerning first=87 second=59 amount=-1
kerning first=87 second=117 amount=-1
kerning first=87 second=111 amount=-1
kerning first=87 second=97 amount=-2
kerning first=87 second=101 amount=-1
kerning first=87 second=114 amount=-1
kerning first=87 second=58 amount=-1
kerning first=87 second=44 amount=-4
kerning first=87 second=46 amount=-4
kerning first=87 second=45 amount=-1
kerning first=86 second=105 amount=-1
kerning first=86 second=121 amount=-2
kerning first=86 second=65 amount=-5
kerning first=86 second=59 amount=-2
kerning first=86 second=117 amount=-2
kerning first=86 second=111 amount=-4
kerning first=86 second=97 amount=-5
kerning first=86 second=101 amount=-4
kerning first=86 second=114 amount=-2
kerning first=86 second=58 amount=-2
kerning first=86 second=44 amount=-6
kerning first=86 second=46 amount=-6
kerning first=86 second=45 amount=-4
kerning first=82 second=87 amount=-1
kerning first=82 second=86 amount=-1
kerning first=82 second=89 amount=-1
kerning first=82 second=84 amount=-1
kerning first=63 second=126 amount=-4
kerning first=33 second=182 amount=-4
kerning first=33 second=235 amount=-7
kerning first=33 second=102 amount=-2
kerning first=33 second=236 amount=-7
kerning first=65 second=121 amount=-1
kerning first=65 second=87 amount=-2
kerning first=65 second=86 amount=-5
kerning first=65 second=89 amount=-5
kerning first=65 second=84 amount=-5
kerning first=65 second=119 amount=-1
kerning first=65 second=118 amount=-1
kerning first=65 second=127 amount=-1
kerning first=65 second=128 amount=-1
kerning first=65 second=129 amount=-1
kerning first=65 second=130 amount=-1
kerning first=65 second=131 amount=-1
kerning first=65 second=132 amount=-1
kerning first=65 second=134 amount=-1
kerning first=65 second=135 amount=-1
kerning first=65 second=136 amount=-1
kerning first=65 second=137 amount=-1
kerning first=65 second=138 amount=-1
kerning first=65 second=139 amount=-1
kerning first=65 second=140 amount=-1
kerning first=65 second=141 amount=-1
kerning first=65 second=142 amount=-1
kerning first=65 second=143 amount=-1
kerning first=65 second=144 amount=-1
kerning first=65 second=145 amount=-1
kerning first=65 second=146 amount=-1
kerning first=65 second=147 amount=-1
kerning first=65 second=148 amount=-1
kerning first=65 second=149 amount=-1
kerning first=65 second=150 amount=-1
kerning first=65 second=151 amount=-1
kerning first=65 second=152 amount=-1
kerning first=65 second=153 amount=-1
kerning first=65 second=154 amount=-1
kerning first=65 second=156 amount=-1
kerning first=65 second=157 amount=-1
kerning first=65 second=158 amount=-1
kerning first=65 second=159 amount=-1
kerning first=65 second=32 amount=-4
kerning first=49 second=49 amount=-5
kerning first=89 second=105 amount=-2
kerning first=89 second=112 amount=-5
kerning first=89 second=113 amount=-6
kerning first=89 second=65 amount=-5
kerning first=89 second=59 amount=-4
kerning first=89 second=117 amount=-4
kerning first=89 second=111 amount=-6
kerning first=89 second=97 amount=-5
kerning first=89 second=101 amount=-6
kerning first=89 second=58 amount=-4
kerning first=89 second=118 amount=-4
kerning first=89 second=44 amount=-8
kerning first=89 second=46 amount=-8
kerning first=89 second=45 amount=-6
kerning first=89 second=32 amount=-1
kerning first=84 second=105 amount=-2
kerning first=84 second=121 amount=-4
kerning first=84 second=79 amount=-1
kerning first=84 second=65 amount=-5
kerning first=84 second=59 amount=-7
kerning first=84 second=117 amount=-2
kerning first=84 second=111 amount=-7
kerning first=84 second=97 amount=-7
kerning first=84 second=101 amount=-7
kerning first=84 second=99 amount=-7
kerning first=84 second=114 amount=-2
kerning first=84 second=115 amount=-7
kerning first=84 second=58 amount=-7
kerning first=84 second=119 amount=-4
kerning first=84 second=44 amount=-7
kerning first=84 second=46 amount=-7
kerning first=84 second=45 amount=-4
kerning first=84 second=32 amount=-1
kerning first=80 second=65 amount=-5
kerning first=80 second=62 amount=-4
kerning first=80 second=122 amount=-1
kerning first=80 second=44 amount=-8
kerning first=80 second=46 amount=-8
kerning first=80 second=32 amount=-1
kerning first=76 second=121 amount=-2
kerning first=76 second=87 amount=-5
kerning first=76 second=86 amount=-5
kerning first=76 second=89 amount=-5
kerning first=76 second=84 amount=-5
kerning first=76 second=32 amount=-2
kerning first=70 second=65 amount=-4
kerning first=70 second=44 amount=-7
kerning first=70 second=46 amount=-7
kerning first=90 second=173 amount=1
kerning first=62 second=35 amount=-4
kerning first=114 second=44 amount=-4
kerning first=114 second=46 amount=-4
kerning first=119 second=44 amount=-4
kerning first=119 second=46 amount=-4
kerning first=118 second=44 amount=-5
kerning first=118 second=46 amount=-5
kerning first=34 second=219 amount=-1
kerning first=34 second=182 amount=-5
kerning first=34 second=235 amount=-8
kerning first=34 second=236 amount=-8
kerning first=34 second=82 amount=-1
kerning first=34 second=163 amount=-4
kerning first=34 second=42 amount=-1
kerning first=32 second=92 amount=-4
kerning first=32 second=65 amount=-4
kerning first=32 second=89 amount=-1
kerning first=32 second=84 amount=-1
