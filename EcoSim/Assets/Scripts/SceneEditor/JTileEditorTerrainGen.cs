using UnityEngine;
using System.Collections.Generic;

public static class JTileEditorTerrainGen {
	
	private class DetailMap {
		public int[,] map;
		public DetailPrototype proto;
		public bool isEmpty;
	}
	
	public static int randomSeed = 0;
	
	private static int RESOLUTION = 32;
	private static int CENTRE = 15;
	private static float SCALE = 20f;
	
	private static DetailMap[] detailMaps;
	
	public static void Setup(TerrainData data) {
		DetailPrototype[] details = data.detailPrototypes;
		detailMaps = new DetailMap[details.Length];
		for (int i = 0; i < details.Length; i++) {
			DetailMap d = new DetailMap();
			d.map = new int[RESOLUTION, RESOLUTION];
			d.proto = details[i];
			detailMaps[i] = d;
		}
	}
	
	public static GameObject[] Generate(Terrain terrain, JXTerrainTile centre, JXTerrainTile surrounds) {
		List<GameObject> objects = new List<GameObject>();
		Transform parent = terrain.transform;
		List<GameObject> childrenToDestroy = new List<GameObject>();
		foreach (Transform t in parent) childrenToDestroy.Add(t.gameObject);
		foreach (GameObject go in childrenToDestroy) GameObject.DestroyImmediate(go);
		childrenToDestroy.Clear();
		childrenToDestroy = null;
		TerrainData data = terrain.terrainData;
		float[,,] alpha = new float[RESOLUTION, RESOLUTION, 4];
		List<TreeInstance> treeList = new List<TreeInstance>();
		
		foreach (DetailMap d in detailMaps) {
			d.isEmpty = true;
			System.Array.Clear(d.map, 0, RESOLUTION * RESOLUTION);
		}
		int totalDM = 0;
		
		JXTEExtraTileLayerPrototype[] layerPrototypes = JElements.self.layerPrototypes;
		JXTerrainLayerHelper[] tileLayers = new JXTerrainLayerHelper[layerPrototypes.Length];

		for (int y = 0; y < RESOLUTION; y++) {
			for (int x = 0; x < RESOLUTION; x++) {
				System.Random rnd = new System.Random((randomSeed + x + 3333 * y) | x | y);
				JXTerrainTile tile = surrounds;
				bool noRandom = false;
				if ((x == CENTRE) && (y == CENTRE)) {
					tile = centre;
					noRandom = (randomSeed <= 1);
				}
				alpha[y, x, 0] = tile.am0;
				alpha[y, x, 1] = tile.am1;
				alpha[y, x, 2] = tile.am2;
				alpha[y, x, 3] = 1f - tile.am2 - tile.am1 - tile.am0;

				foreach (int tileLayerIndex in tile.extraLayers) {
					JXTerrainLayerHelper tlh = tileLayers[tileLayerIndex];
					if (tlh == null) {
						JXTEExtraTileLayerPrototype prototype = layerPrototypes[tileLayerIndex];
						tlh = new JXTerrainLayerHelper(tileLayerIndex, RESOLUTION, prototype.verticalOffset, prototype.useWaterHeights);
						tileLayers[tileLayerIndex] = tlh;
					}
					tlh.AddTileFakeHeights(x, y, 0f);
				}
				
				foreach (JXTerrainTileTreeData treeData in tile.trees) {
					TreeInstance ti = new TreeInstance();
					float tx = treeData.x;
					float ty = treeData.y;
					float rad = treeData.r;
					if ((rad > 0f) && (!noRandom)) {
						float angle = (float) rnd.NextDouble() * Mathf.PI * 2;
						rad = rad * (float) rnd.NextDouble();
						tx += Mathf.Sin(angle) * rad;
						ty += Mathf.Cos(angle) * rad;
					}
					// ti.position = TreePos(ref heightMap, x + Mathf.Clamp(tx, 0f, 0.999f), y + Mathf.Clamp(ty, 0f, 0.999f));
					tx = x + Mathf.Clamp(tx, 0f, 0.999f);
					ty = y + Mathf.Clamp(ty, 0f, 0.999f);
					ti.position = new Vector3(tx / RESOLUTION, 0f, ty / RESOLUTION);
					ti.prototypeIndex = treeData.prototypeIndex;
					if (noRandom) {
						ti.heightScale = 0.5f * (treeData.minHeight + treeData.maxHeight);
						ti.widthScale = ti.heightScale * 0.5f * (treeData.minWidthVariance + treeData.maxWidthVariance);
						Color c = treeData.colorTo;
						ti.color = c;
					}
					else {
						ti.heightScale = JUtil.RndRange(ref rnd, treeData.minHeight, treeData.maxHeight);
						ti.widthScale = ti.heightScale * JUtil.RndRange(ref rnd, treeData.minWidthVariance, treeData.maxWidthVariance);
						Color c = JUtil.RndRange(ref rnd, treeData.colorFrom, treeData.colorTo);
						ti.color = c;
					}
					ti.lightmapColor = Color.white;
					treeList.Add(ti);
				}
				GameObject[] prefabs = JElements.self.tileObjectPrefabs;
				foreach (JXTerrainTileObjectData objData in tile.objects) {
					GameObject go = (GameObject) GameObject.Instantiate(prefabs[objData.index]);
					go.layer = JLayers.L_GUI;
					Transform t = go.transform;
					t.parent = parent;
					float tx = objData.x;
					float ty = objData.y;
					float rad = objData.r;
					t.localRotation = Quaternion.Euler(0f, objData.angle, 0f);
					if ((rad > 0f) && (!noRandom)) {
						float angle = (float) rnd.NextDouble() * Mathf.PI * 2;
						rad = rad * (float) rnd.NextDouble();
						tx += Mathf.Sin(angle) * rad;
						ty += Mathf.Cos(angle) * rad;
					}
					tx = x + Mathf.Clamp(tx, 0f, 0.999f);
					ty = y + Mathf.Clamp(ty, 0f, 0.999f);
					t.localPosition = new Vector3(tx * SCALE, 0f, ty * SCALE);
					if (noRandom) {
						float heightScale = 0.5f * (objData.minHeight + objData.maxHeight);
						float widthScale = heightScale * 0.5f * (objData.minWidthVariance + objData.maxWidthVariance);
						t.localScale = new Vector3(widthScale, heightScale, widthScale);
					}
					else {
						float heightScale = JUtil.RndRange(ref rnd, objData.minHeight, objData.maxHeight);
						float widthScale = heightScale * JUtil.RndRange(ref rnd, objData.minWidthVariance, objData.maxWidthVariance);
						t.localScale = new Vector3(widthScale, heightScale, widthScale);
					}
					objects.Add(go);
				}
				for (int i = 0; i < tile.detailCounts.Length; i++) {
					int dc = tile.detailCounts[i];
					if (dc > 0) {
						DetailMap dMap = detailMaps[i];
						if (dMap.isEmpty) {
							dMap.isEmpty = false;
							totalDM++;
						}
						dMap.map[y, x] = dc;
					}
				}	
			}
		}
		data.SetAlphamaps(0, 0, alpha);
		data.SetDetailResolution(32, 32);
		data.treeInstances = treeList.ToArray();
		DetailPrototype[] detailPrototypes = new DetailPrototype[totalDM];
		int j = 0;
		foreach (DetailMap d in detailMaps) {
			if (!d.isEmpty) detailPrototypes[j++] = d.proto;
		}
		data.detailPrototypes = detailPrototypes;
		j = 0;
		foreach (DetailMap d in detailMaps) {
			if (!d.isEmpty) {
				data.SetDetailLayer(0, 0, j++, d.map);
			}
		}

		foreach (JXTerrainLayerHelper tlh in tileLayers) {
			if (tlh != null) {
				Mesh m = new Mesh();
				m.vertices = tlh.points.ToArray();
				m.uv = tlh.uvs.ToArray();
				m.triangles = tlh.indices.ToArray();
				m.Optimize();
				m.RecalculateNormals();
				m.RecalculateBounds();
				GameObject go = new GameObject("tilelayer " + tlh.id);
				go.layer = JLayers.L_GUI;
				go.transform.parent = parent;
				go.transform.localPosition = Vector3.zero;
				go.transform.localScale = Vector3.one;
				go.transform.localRotation = Quaternion.identity;
				MeshFilter mf = go.AddComponent<MeshFilter>();
				mf.sharedMesh = m;
				MeshRenderer mr = go.AddComponent<MeshRenderer>();
				mr.sharedMaterial = layerPrototypes[tlh.id].material;
			}
		}
		
		return objects.ToArray();
	}
}
