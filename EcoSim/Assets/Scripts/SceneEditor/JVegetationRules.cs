using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JVegetationRules : MonoBehaviour {
	
	private int id = JWinMgr.idGenerator++;
	private Rect windowPos = JWinMgr.AdjustWindowSize(new Rect(Random.Range(200, 400), Random.Range(100, 200), 760, 660));
	
	
	class RuleCopy {
		public JXVegetation.NewParam[] newParams;
		public EActionTypes[] acceptableMeasures;
		public JXVegetation.ParamChange[] changes;
		public JXVegetation.Step[] steps;
		
		public void Copy(JXVegetation dest) {
			dest.acceptableMeasures = (EActionTypes[]) acceptableMeasures.Clone();
			dest.newParams = new JXVegetation.NewParam[newParams.Length];
			for (int i = 0; i < newParams.Length; i++) {
				if (newParams[i] != null) {
					dest.newParams[i] = new JXVegetation.NewParam();
					dest.newParams[i].Assign(newParams[i]);
				}
			}

			dest.changes = new JXVegetation.ParamChange[changes.Length];
			for (int i = 0; i < changes.Length; i++) {
				if (changes[i] != null) {
					dest.changes[i] = new JXVegetation.ParamChange();
					dest.changes[i].Assign(changes[i]);
				}
			}

			dest.steps = new JXVegetation.Step[steps.Length];
			for (int i = 0; i < steps.Length; i++) {
				if (steps[i] != null) {
					dest.steps[i] = new JXVegetation.Step();
					dest.steps[i].Assign(steps[i]);
				}
			}
		}
		
		public void Assign(JXVegetation source) {
			acceptableMeasures = (EActionTypes[]) source.acceptableMeasures.Clone();
			newParams = new JXVegetation.NewParam[source.newParams.Length];
			for (int i = 0; i < source.newParams.Length; i++) {
				if (source.newParams[i] != null) {
					newParams[i] = new JXVegetation.NewParam();
					newParams[i].Assign(source.newParams[i]);
				}
			}

			changes = new JXVegetation.ParamChange[source.changes.Length];
			for (int i = 0; i < source.changes.Length; i++) {
				if (source.changes[i] != null) {
					changes[i] = new JXVegetation.ParamChange();
					changes[i].Assign(source.changes[i]);
				}
			}

			steps = new JXVegetation.Step[source.steps.Length];
			for (int i = 0; i < source.steps.Length; i++) {
				if (source.steps[i] != null) {
					steps[i] = new JXVegetation.Step();
					steps[i].Assign(source.steps[i]);
				}
			}
		}
	}
	
	
	class MinMaxStrings {
		public string min;
		public string max;
		public string delta;
	}
	
	class Info {
		public JXVegetation vegetation;
		public Dictionary<EParamTypes, string> newParamMinValues;
		public Dictionary<EParamTypes, string> newParamMaxValues;
		public Dictionary<JXVegetation.ParamCondition, MinMaxStrings> newParamConditions;
		public Dictionary<JXVegetation.ParamDelta, MinMaxStrings> newParamDeltas;
		public Dictionary<JXVegetation.Step, string>stepChanceStrings = new Dictionary<JXVegetation.Step, string>();
		public Dictionary<JXVegetation.ParamChange, string>paramChangeChanceStrings = new Dictionary<JXVegetation.ParamChange, string>();
		public Vector2 scrollPos;
		public string colourStr = null;
		public string[] vegetations;
	}
	
	class StaticInfo {
		public Dictionary<JXVegetation, JVegetationRules> editors = new Dictionary<JXVegetation, JVegetationRules>();
		public RuleCopy clipboard = null;
		public List<EActionTypes> actionTypes;
		public List<EParamTypes> paramTypes;
	}
	
	private Info info;
	private static StaticInfo sInfo;
	
	public static bool EditorActive(JXVegetation t) {
		return sInfo.editors.ContainsKey(t);
	}
	
	public static void RemoveEditor(JXVegetation t) {
		JVegetationRules editor;
		if (sInfo.editors.TryGetValue(t, out editor)) {
			sInfo.editors.Remove(t);
			Destroy(editor.gameObject);
		}
	}
	
	public static void RemoveAllEditors() {
		foreach (JVegetationRules editor in sInfo.editors.Values) {
			Destroy(editor.gameObject);
		}
		sInfo.editors.Clear();
	}
	
	public static JVegetationRules CreateEditor(JVegetationEditor vegEditor, JXVegetation vegetation) {
		if (sInfo.editors.ContainsKey(vegetation)) {
			JVegetationRules editor = sInfo.editors[vegetation];
			JWinMgr.MoveToFront(editor.id);
			return editor;
		}
		GameObject go = new GameObject("Vegetation Rules");
		go.transform.parent = vegEditor.transform;
		go.transform.localPosition = Vector3.zero;
		go.layer = JLayers.L_GUI;
		JVegetationRules rulesEditor = go.AddComponent<JVegetationRules>();
		rulesEditor.info = new Info();
		rulesEditor.info.vegetation = vegetation;
		List<string> vegetationStrings = new List<string>();
		foreach (JXVegetation veg in vegetation.succession.vegetation) {
			vegetationStrings.Add(veg.name);
		}
		rulesEditor.info.vegetations = vegetationStrings.ToArray();
		sInfo.editors.Add(vegetation, rulesEditor);
		return rulesEditor;
	}
	
	public static void SetupStaticData() {
		sInfo = new StaticInfo();
		sInfo.actionTypes = new List<EActionTypes>(System.Enum.GetValues(typeof(EActionTypes)) as EActionTypes[]);
		sInfo.paramTypes = new List<EParamTypes>(System.Enum.GetValues(typeof(EParamTypes)) as EParamTypes[]);
	}
		
	void Start() {
		UpdateStrings();
		JWinMgr.MoveToFront(id);
	}
	
	void UpdateStrings() {
		JXVegetation vegetation = info.vegetation;
		info.newParamMinValues = new Dictionary<EParamTypes, string>();
		info.newParamMaxValues = new Dictionary<EParamTypes, string>();
		foreach (JXVegetation.NewParam np in vegetation.newParams) {
			float minRange = np.minRange / 255f;
			Debug.Log("np.type = " + np.type.ToString());
			info.newParamMinValues.Add(np.type, minRange.ToString("0.00"));
			float maxRange = np.maxRange / 255f;
			info.newParamMaxValues.Add(np.type, maxRange.ToString("0.00"));
		}
		info.newParamConditions = new Dictionary<JXVegetation.ParamCondition, MinMaxStrings>();
		foreach (JXVegetation.Step step in vegetation.steps) {
			foreach (JXVegetation.ParamCondition pc in step.conditions) {
				MinMaxStrings mmstrings = new MinMaxStrings();
				float min = pc.minRange / 255f;
				float max = pc.maxRange / 255f;
				mmstrings.min = min.ToString("0.00");
				mmstrings.max = max.ToString("0.00");
				info.newParamConditions.Add(pc, mmstrings);
			}
		}
		info.newParamDeltas = new Dictionary<JXVegetation.ParamDelta, MinMaxStrings>();
		foreach (JXVegetation.ParamChange change in vegetation.changes) {
			foreach (JXVegetation.ParamDelta pd in change.deltas) {
				MinMaxStrings mmstrings = new MinMaxStrings();
				float min = pd.minRange / 255f;
				float max = pd.maxRange / 255f;
				float delta = pd.delta / 255f;
				mmstrings.min = min.ToString("0.00");
				mmstrings.max = max.ToString("0.00");
				mmstrings.delta = delta.ToString("0.00");
				info.newParamDeltas.Add(pd, mmstrings);
			}
		}
		info.colourStr = vegetation.colour.r.ToString("X2") + vegetation.colour.g.ToString("X2") + vegetation.colour.b.ToString("X2");
	}
	
	void OnGUI() {
		GUI.skin = JWinMgr.self.skin;
		windowPos = JWinMgr.Window(id, windowPos, HandleSuccession, "Successie Editor '" + info.vegetation.name + "'", JWinMgr.self.skin.window);
	}
	
	
	
	void HandleSuccession(int id) {
		JXVegetation vegetation = info.vegetation;
		// GUISkin skin = JXWindowManager.self.skin;
		if (GUI.Button(new Rect(windowPos.width - 37, 3, 24, 12), "X")) {
			RemoveEditor(vegetation);
		}
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Kopieer")) {
			if (sInfo.clipboard == null) {
				sInfo.clipboard = new RuleCopy();
			}
			sInfo.clipboard.Assign(vegetation);
		}
		if (sInfo.clipboard != null) {
			if (GUILayout.Button("Plak")) {
				sInfo.clipboard.Copy(vegetation);
				foreach (JXVegetation.Step s in vegetation.steps) {
					s.newVegetation = Mathf.Min(vegetation.succession.vegetation.Length - 1, s.newVegetation);
				}
				UpdateStrings();
			}
		}
		
		GUILayout.FlexibleSpace();
		Color bgColor = GUI.backgroundColor;
		GUI.backgroundColor = vegetation.colour;
		GUILayout.Box("", GUILayout.Width(16), GUILayout.Height(16));
		GUI.backgroundColor = bgColor;
		string newStr = GUILayout.TextField(info.colourStr, GUILayout.Width(60));
		if (newStr != info.colourStr) {
			int colourNr;
			if (int.TryParse(newStr, System.Globalization.NumberStyles.HexNumber, null, out colourNr)) {
				Color32 c = new Color32((byte) (colourNr >> 16), (byte) (colourNr >> 8), (byte) colourNr, 255);
				vegetation.colour = c;
			}
			info.colourStr = newStr;
		}
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		int count = 0;
		foreach (EActionTypes t in sInfo.actionTypes) {
			int intVal = (int) t;
			if ((intVal > 0) && (intVal < 0xf0) && ((intVal & 0x0f) != 0)) {
				if (((++count) % 4 == 1) && (count > 1)) {
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
					GUILayout.BeginHorizontal();
				}
				bool validMeasure = vegetation.IsValidMeasure(t);
				bool newValidMeasure = GUILayout.Toggle(validMeasure, t.ToString(), GUILayout.Width(160));
				if (newValidMeasure != validMeasure) {
					vegetation.MakeValidMeasure(t, newValidMeasure);
				}
			}
		}
		GUILayout.EndHorizontal();
		
		
		GUILayout.BeginHorizontal();
		count = 0;
		foreach (EParamTypes t in sInfo.paramTypes) {
			if (((int) t >= 0) && ((int) t < 80)) {
				JXVegetation.NewParam newParam = null;
				float minVal = 0f;
				float maxVal = 0f;
				bool isActive = false;
				foreach (JXVegetation.NewParam p in vegetation.newParams) {
					if (p.type == t) {
						newParam = p;
						minVal = p.minRange / 255f;
						maxVal = p.maxRange / 255f;
						isActive = true;
					}
				}
				
				if (((++count) % 3 == 1) && (count > 1)) {
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
					GUILayout.BeginHorizontal();
				}
				
				// GUILayout.Label(t.ToString(), GUILayout.Width(100));
				bool newIsActive = GUILayout.Toggle(isActive, t.ToString(), GUILayout.Width(170));
				if (isActive) {
					string minValStr = info.newParamMinValues[t];
					minValStr = GUILayout.TextField(minValStr, GUILayout.Width(28), GUILayout.Height(16));
					info.newParamMinValues[t] = minValStr;
					float.TryParse(minValStr, out minVal);
					string maxValStr = info.newParamMaxValues[t];
					maxValStr = GUILayout.TextField(maxValStr, GUILayout.Width(28), GUILayout.Height(16));
					info.newParamMaxValues[t] = maxValStr;
					float.TryParse(maxValStr, out maxVal);
				}
				else {
					GUILayout.Label("",GUILayout.Width(60));
				}
				if (newIsActive	 != isActive) UpdateStrings();
				
				if (newIsActive) {
					if (newParam == null) {
						newParam = new JXVegetation.NewParam(); 
						newParam.type = t;
						List<JXVegetation.NewParam> list = new List<JXVegetation.NewParam>(vegetation.newParams);
						list.Add(newParam);
						vegetation.newParams = list.ToArray();
						minVal = newParam.minRange / 255f;
						maxVal = newParam.maxRange / 255f;
						info.newParamMinValues.Add(newParam.type, minVal.ToString("0.00"));
						info.newParamMaxValues.Add(newParam.type, maxVal.ToString("0.00"));
					}
					newParam.minRange = (byte) (minVal * 255f);
					newParam.maxRange = (byte) (maxVal * 255f);
				}
				else {
					if (newParam != null) {
						List<JXVegetation.NewParam> list = new List<JXVegetation.NewParam>(vegetation.newParams);
						list.Remove(newParam);
						vegetation.newParams = list.ToArray();
					}
				}
				
				GUILayout.Space(20);
			}
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		info.scrollPos = GUILayout.BeginScrollView(info.scrollPos, false, true);
		GUILayout.BeginVertical();
		
		// Steps
		int stepsCount = vegetation.steps.Length;
		int stepsIndex = 0;
		int stepsSwap = -1;
		foreach (JXVegetation.Step step in vegetation.steps) {
			GUILayout.BeginHorizontal();
			GUILayout.BeginVertical();
			if (stepsIndex > 0) {
				if (GUILayout.Button("^", GUILayout.Width(20))) {
					stepsSwap = stepsIndex - 1;
				}
			}
			else {
				GUILayout.Space(20);
			}
			GUILayout.Label("", GUILayout.Width(20), GUILayout.Height(1));
			if (stepsIndex < stepsCount - 1) {
				if (GUILayout.Button("v", GUILayout.Width(20))) {
					stepsSwap = stepsIndex;
				}
			}
			GUILayout.EndVertical();
			HandleStep(step);
			GUILayout.EndHorizontal();
			stepsIndex++;
		}
		
		if (stepsSwap >= 0) {
			JXVegetation.Step tmp = vegetation.steps[stepsSwap];
			vegetation.steps[stepsSwap] = vegetation.steps[stepsSwap + 1];
			vegetation.steps[stepsSwap + 1] = tmp;
		}
		
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Voeg overgang toe")) {
			JXVegetation.Step step = new JXVegetation.Step();
			step.newVegetation = 0;
			step.chance = 1f;
			step.conditions = new JXVegetation.ParamCondition[0];
			step.action = EActionTypes.GeenActie;
			List<JXVegetation.Step> steps = new List<JXVegetation.Step>(vegetation.steps);
			steps.Add(step);
			vegetation.steps = steps.ToArray();
			vegetation.UpdateStableParam();
			UpdateStrings();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();

		// Parameter changes
		int changesCount = vegetation.changes.Length;
		int changesIndex = 0;
		int changesSwap = -1;
		foreach (JXVegetation.ParamChange change in vegetation.changes) {
			GUILayout.BeginHorizontal();
			GUILayout.BeginVertical();
			if (changesIndex > 0) {
				if (GUILayout.Button("^", GUILayout.Width(20))) {
					changesSwap = changesIndex - 1;
				}
			}
			else {
				GUILayout.Space(20);
			}
			GUILayout.Label("", GUILayout.Width(20), GUILayout.Height(1));
			if (changesIndex < changesCount - 1) {
				if (GUILayout.Button("v", GUILayout.Width(20))) {
					changesSwap = changesIndex;
				}
			}
			GUILayout.EndVertical();
			HandleChange(change);
			GUILayout.EndHorizontal();
			changesIndex++;
		}
		
		if (changesSwap >= 0) {
			JXVegetation.ParamChange tmp = vegetation.changes[changesSwap];
			vegetation.changes[changesSwap] = vegetation.changes[changesSwap + 1];
			vegetation.changes[changesSwap + 1] = tmp;
		}
		
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Voeg parameter verandering toe")) {
			JXVegetation.ParamChange change = new JXVegetation.ParamChange();
			change.chance = 1f;
			change.deltas = new JXVegetation.ParamDelta[0];
			change.action = EActionTypes.GeenActie;
			List<JXVegetation.ParamChange> changes = new List<JXVegetation.ParamChange>(vegetation.changes);
			changes.Add(change);
			vegetation.changes = changes.ToArray();
			vegetation.UpdateStableParam();
			UpdateStrings();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		
		GUILayout.EndVertical();
		
		GUILayout.FlexibleSpace();
		GUILayout.EndScrollView();
		
		GUILayout.EndVertical();
		GUI.DragWindow();
	}
	
	
	void HandleStep(JXVegetation.Step step) {
		JXVegetation vegetation = info.vegetation;
		GUISkin skin = JWinMgr.self.skin;
//		GUIStyle normal = skin.box;

		GUILayout.BeginVertical(skin.box);
		
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("-", GUILayout.Width(20))) {
			List<JXVegetation.Step> steps = new List<JXVegetation.Step>(vegetation.steps);
			steps.Remove(step);
			vegetation.steps = steps.ToArray();
			vegetation.UpdateStableParam();
		}
		int index = sInfo.actionTypes.IndexOf(step.action);
		if (index > 0) {
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				step.action = sInfo.actionTypes[index - 1];
				vegetation.UpdateStableParam();
				UpdateStrings();
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.Label(sInfo.actionTypes[index].ToString(), GUILayout.Width(180));
		if (index < sInfo.actionTypes.Count - 1) {
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				step.action = sInfo.actionTypes[index + 1];
				vegetation.UpdateStableParam();
				UpdateStrings();
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		string chanceStr = step.chance.ToString();
		if (info.stepChanceStrings.ContainsKey(step)) {
			chanceStr = info.stepChanceStrings[step];
		}
		GUILayout.Label("Kans (0..1): ");
		string newChanceStr = GUILayout.TextField(chanceStr, GUILayout.Width(40));
		if (newChanceStr != chanceStr) {
			info.stepChanceStrings[step] = newChanceStr;
			float.TryParse(newChanceStr, out step.chance);
		}
		
		GUILayout.Label("overgang:");
		
		if (step.newVegetation > 0) {
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				step.newVegetation--;
				UpdateStrings();
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.Label(info.vegetations[step.newVegetation], GUILayout.Width(140));
		if (step.newVegetation < info.vegetations.Length - 1) {
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				step.newVegetation++;
				UpdateStrings();
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		foreach (JXVegetation.ParamCondition pc in step.conditions) {
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("-", GUILayout.Width(20))) {
				List<JXVegetation.ParamCondition> pcs = new List<JXVegetation.ParamCondition>(step.conditions);
				pcs.Remove(pc);
				step.conditions = pcs.ToArray();
			}

			int pindex = sInfo.paramTypes.IndexOf(pc.type);
			if (pindex > 0) {
				if (GUILayout.Button("<", GUILayout.Width(20))) {
					pc.type = sInfo.paramTypes[pindex - 1];
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(20));
			}
			GUILayout.Label(sInfo.paramTypes[pindex].ToString(), GUILayout.Width(180));
			if (pindex < sInfo.paramTypes.Count - 1) {
				if (GUILayout.Button(">", GUILayout.Width(20))) {
					pc.type = sInfo.paramTypes[pindex + 1];
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(20));
			}
			
			GUILayout.Label("minimum: ");
			MinMaxStrings mmstrings = info.newParamConditions[pc];
			mmstrings.min = GUILayout.TextField(mmstrings.min, GUILayout.Width(40));
			GUILayout.Label("maximum: ");
			mmstrings.max = GUILayout.TextField(mmstrings.max, GUILayout.Width(40));
			
			float min;
			float max;
			if (float.TryParse(mmstrings.min, out min)) {
				pc.minRange = (byte) (min * 255f);
				if (pc.minRange	> pc.maxRange) pc.maxRange = pc.minRange;
			}
			if (float.TryParse(mmstrings.max, out max)) {
				pc.maxRange = (byte) (max * 255f);
				if (pc.minRange	> pc.maxRange) pc.minRange = pc.maxRange;
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}
		
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Voeg parameter conditie toe")) {
			List<JXVegetation.ParamCondition> pcs = new List<JXVegetation.ParamCondition>(step.conditions);
			JXVegetation.ParamCondition pc = new JXVegetation.ParamCondition();
			pc.type = EParamTypes.PH;
			pc.minRange = 0;
			pc.maxRange = 255;
			pcs.Add(pc);
			step.conditions = pcs.ToArray();
			UpdateStrings();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.EndVertical();		
	}

	void HandleChange(JXVegetation.ParamChange change) {
		GUISkin skin = JWinMgr.self.skin;
		JXVegetation vegetation = info.vegetation;
//		GUIStyle normal = skin.box;

		GUILayout.BeginVertical(skin.box);
		
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("-", GUILayout.Width(20))) {
			List<JXVegetation.ParamChange> changes = new List<JXVegetation.ParamChange>(vegetation.changes);
			changes.Remove(change);
			vegetation.changes = changes.ToArray();
			vegetation.UpdateStableParam();
		}
		int index = sInfo.actionTypes.IndexOf(change.action);
		if (index > 0) {
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				change.action = sInfo.actionTypes[index - 1];
				vegetation.UpdateStableParam();
				UpdateStrings();
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.Label(sInfo.actionTypes[index].ToString(), GUILayout.Width(180));
		if (index < sInfo.actionTypes.Count - 1) {
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				change.action = sInfo.actionTypes[index + 1];
				vegetation.UpdateStableParam();
				UpdateStrings();
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		string chanceStr = change.chance.ToString();
		if (info.paramChangeChanceStrings.ContainsKey(change)) {
			chanceStr = info.paramChangeChanceStrings[change];
		}
		GUILayout.Label("Kans (0..1): ");
		string newChanceStr = GUILayout.TextField(chanceStr, GUILayout.Width(40));
		if (newChanceStr != chanceStr) {
			info.paramChangeChanceStrings[change] = newChanceStr;
			float.TryParse(newChanceStr, out change.chance);
		}
				
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		foreach (JXVegetation.ParamDelta pd in change.deltas) {
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("-", GUILayout.Width(20))) {
				List<JXVegetation.ParamDelta> pds = new List<JXVegetation.ParamDelta>(change.deltas);
				pds.Remove(pd);
				change.deltas = pds.ToArray();
			}

			int pindex = sInfo.paramTypes.IndexOf(pd.type);
			if (pindex > 0) {
				if (GUILayout.Button("<", GUILayout.Width(20))) {
					pd.type = sInfo.paramTypes[pindex - 1];
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(20));
			}
			GUILayout.Label(sInfo.paramTypes[pindex].ToString(), GUILayout.Width(180));
			if (pindex < sInfo.paramTypes.Count - 1) {
				if (GUILayout.Button(">", GUILayout.Width(20))) {
					pd.type = sInfo.paramTypes[pindex + 1];
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(20));
			}
			
			GUILayout.Label("minimum: ");
			MinMaxStrings mmstrings = info.newParamDeltas[pd];
			mmstrings.min = GUILayout.TextField(mmstrings.min, GUILayout.Width(40));
			GUILayout.Label("maximum: ");
			mmstrings.max = GUILayout.TextField(mmstrings.max, GUILayout.Width(40));
			GUILayout.Label("verandering: ");
			mmstrings.delta = GUILayout.TextField(mmstrings.delta, GUILayout.Width(40));
			
			float min;
			float max;
			float delta;
			if (float.TryParse(mmstrings.min, out min)) {
				pd.minRange = (byte) (min * 255f);
				if (pd.minRange	> pd.maxRange) pd.maxRange = pd.minRange;
			}
			if (float.TryParse(mmstrings.max, out max)) {
				pd.maxRange = (byte) (max * 255f);
				if (pd.minRange	> pd.maxRange) pd.minRange = pd.maxRange;
			}
			if (float.TryParse(mmstrings.delta, out delta)) {
				delta = Mathf.Clamp(delta, -1f, 1f);
				pd.delta = (int) (delta * 255f);
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}
		
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Voeg parameter aanpassing toe")) {
			List<JXVegetation.ParamDelta> pds = new List<JXVegetation.ParamDelta>(change.deltas);
			JXVegetation.ParamDelta pd = new JXVegetation.ParamDelta();
			pd.type = EParamTypes.PH;
			pd.minRange = 0;
			pd.maxRange = 255;
			pd.delta = 0;
			pds.Add(pd);
			change.deltas = pds.ToArray();
			UpdateStrings();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.EndVertical();		
	}

}
