using UnityEngine;
using System.Collections;

public class JSettingsEditor : MonoBehaviour {

	private int id = JWinMgr.idGenerator++;
	private Rect pos = new Rect(200, 100, 300, 150);
	
	public JEditor editor;

	void OnGUI() {
		GUI.skin = JWinMgr.self.skin;
		pos = JWinMgr.Window(id, pos, HandleEditor, "Settings", JWinMgr.self.skin.window);
	}
	
	private string qualityStr;
	
	void Start() {
		qualityStr = QualitySettings.GetQualityLevel().ToString();
	}
	
	void HandleEditor(int id) {
		if (GUI.Button(new Rect(pos.width - 37, 3, 24, 12), "X")) {
			gameObject.SetActive(false);
		}
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Weergave: ");
		if (GUILayout.Button("<", GUILayout.Width(20))) {
			QualitySettings.DecreaseLevel();
			qualityStr = QualitySettings.GetQualityLevel().ToString();
		}
		GUILayout.Label(qualityStr, GUILayout.Width(120));
		if (GUILayout.Button(">", GUILayout.Width(20))) {
			QualitySettings.IncreaseLevel();
			qualityStr = QualitySettings.GetQualityLevel().ToString();
		}
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUI.DragWindow();
	}
}
