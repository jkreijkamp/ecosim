using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JAnimalEditor : MonoBehaviour {

	private int id = JWinMgr.idGenerator++;
	private Rect pos = new Rect(75, 100, 320, 200);

	public JEditor editor;

	public static bool isEditing = false;
	private int index = 0;

	private JTerrainData data;

	void ScenarioChanged() {
		if (editor.scenario != null) {
			data = editor.scenario.data;
		}
		else {
			data = null;
		}
	}


	void OnGUI() {
		GUI.skin = JWinMgr.self.skin;
		pos = JWinMgr.Window(id, pos, HandleEditor, "Dieren", JWinMgr.self.skin.window);
	}

	void Awake() {
	}

	void OnDisable() {
		isEditing = false;
	}

	void OnEnable() {
		ScenarioChanged();
		isEditing = true;
	}

	void HandleEditor(int id) {
//		GUISkin skin = JWinMgr.self.skin;
//		GUIStyle selected = skin.GetStyle("selectedbox");
//		GUIStyle normal = skin.box;
		if (GUI.Button(new Rect(pos.width - 37, 3, 24, 12), "X")) {
			gameObject.SetActive(false);
		}
		EAnimalTypes[] aTypes = EnumExtensions.animalTypeArray;
		
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		if (index > 0) {
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				index--;
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.Label(aTypes[index].ToString(), GUILayout.Width(200));
		if (index < aTypes.Length - 1) {
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				index++;
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUI.DragWindow();
	}


	void Update() {
		Vector3 mousePos = Input.mousePosition;
		if (JWinMgr.InUIRect(mousePos.x, Screen.height - mousePos.y) ||
			Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) {
			return;
		}
		if (Input.GetMouseButtonDown(0)) {		
			Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
			RaycastHit hit;
			if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER)) {
				Vector3 point = hit.point;
				int x = (int) (point.x / JTerrainData.HORIZONTAL_SCALE);
				int y = (int) (point.z / JTerrainData.HORIZONTAL_SCALE);
				int cx = x >> JTerrainData.CELL_SIZE2EXP;
				int cy = y >> JTerrainData.CELL_SIZE2EXP;
				x = x & JTerrainData.CELL_MASK;
				y = y & JTerrainData.CELL_MASK;
				JCellData cell = data.GetCell(cx, cy);
				cell.AddAnimal(GameObject.Find("ObjectHolder"), EnumExtensions.animalTypeArray[index], x, y);
			}
		}
		if (Input.GetMouseButtonDown(1) || Input.GetKeyDown(KeyCode.Delete) || Input.GetKeyDown(KeyCode.Backspace)) {
			Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
			RaycastHit[] hits = Physics.RaycastAll(screenRay, Mathf.Infinity, JLayers.M_ANIMALS);
				foreach (RaycastHit hit in hits) {
				JAnimal animal = hit.collider.gameObject.GetComponent<JAnimal>();
				if (animal != null) {
					animal.animalData.cell.DeleteAnimal(animal.animalData);
					Destroy(animal.gameObject);
				}
			}
		}
	}
}