using UnityEngine;
using System.Collections.Generic;

public class JTileEditor : MonoBehaviour {

	
	private int id = JWinMgr.idGenerator++;
	private Rect pos = JWinMgr.AdjustWindowSize(new Rect(Random.Range(200, 400), Random.Range(200, 400), 660, 460));
	
	private class Info {
		public JXVegetation vegetation;
		public JXTerrainTile tile;
		public int tileIndex;
		public float cameraAngle = 45f;
		public float cameraDistance = 30f;
		public float cameraHeight = 15f;
		public float lookHeight = -0.35f;
		public bool showOneOnly = true;
		public bool isChanged = false;

		public JXTerrainTile backup;
		
		public Vector2 scroll;
		public int deleteTreeIndex = -1;
		public int deleteObjectIndex = -1;
		public string[] detailCountStrings;
	};
	
	private class StaticInfo {
		public JEditor editor;
		public Terrain terrain;
		public Camera camera;
		public Transform root;
		public JXTerrainTile clipboard;
		public string[] treeNames;
		public string[] objectNames;
		public string[] detailNames;
		public JXTEExtraTileLayerPrototype[] layerPrototypes;
		public Dictionary<JXTerrainTile, JTileEditor> editors;
	};
	
	private Info info;
	private static StaticInfo sInfo;

	public static JTileEditor Create(Transform parent, JXTerrainTile tile) {
		return Create(parent, tile.vegetation, tile.index);
	}

	
	public static JTileEditor Create(Transform parent, JXVegetation vegetation, int tileIndex) {
		GameObject go = new GameObject("Tile Editor");
		go.transform.parent = parent;
		go.transform.localPosition = Vector3.zero;
		go.layer = JLayers.L_GUI;
		JTileEditor tileEditor = go.AddComponent<JTileEditor>();
		tileEditor.info = new Info();
		tileEditor.info.vegetation = vegetation;
		tileEditor.info.tile = vegetation.tiles[tileIndex];
		tileEditor.info.tileIndex = tileIndex;
		tileEditor.info.backup = new JXTerrainTile(tileEditor.info.tile);
		sInfo.editors.Add(tileEditor.info.tile, tileEditor);
		return tileEditor;
	}
	
	public static void SetupStaticData(JEditor editor, Terrain terrain, Terrain scenarioTerrain) {
		TerrainData data = scenarioTerrain.terrainData;

		sInfo = new StaticInfo();
		sInfo.editor = editor;
		sInfo.terrain = terrain;
		sInfo.editors = new Dictionary<JXTerrainTile, JTileEditor>();
		sInfo.treeNames = new string[data.treePrototypes.Length];
		sInfo.objectNames = new string[JElements.self.tileObjectPrefabs.Length];
		sInfo.layerPrototypes = JElements.self.layerPrototypes;
		int index = 0;
		foreach (TreePrototype tp in data.treePrototypes) {
			sInfo.treeNames[index++] = tp.prefab.name;
		}
		index = 0;
		foreach (GameObject go in JElements.self.tileObjectPrefabs) {
			sInfo.objectNames[index++] = go.name;
		}
		List<string> detailNames = new List<string>();
		foreach (DetailPrototype dp in data.detailPrototypes) {
			if (dp.prototype != null) {
				detailNames.Add(dp.prototype.name);
			}
			else {
				detailNames.Add(dp.prototypeTexture.name);
			}
		}
		sInfo.detailNames = detailNames.ToArray();
		sInfo.root = terrain.transform.parent;
		sInfo.camera = sInfo.root.FindChild("Camera").GetComponent<Camera>();
	}
	
	// Use this for initialization
	void Start () {
		info.detailCountStrings = new string[sInfo.detailNames.Length];
		for (int i = 0; i < info.detailCountStrings.Length; i++) {
			int count = 0;
			if (i < info.tile.detailCounts.Length) {
				count = info.tile.detailCounts[i];
			}
			info.detailCountStrings[i] = count.ToString();
		}
		if (info.tile.icon == null) {
			info.isChanged = true;
		}
	}
	
	public static bool EditorActive(JXTerrainTile t) {
		return sInfo.editors.ContainsKey(t);
	}
	
	public static void RemoveEditor(JXTerrainTile t) {
		JTileEditor editor;
		if (sInfo.editors.TryGetValue(t, out editor)) {
			sInfo.editors.Remove(t);
			Destroy(editor.gameObject);
		}
	}
	
	public static void RemoveAllEditors() {
		foreach (JTileEditor editor in sInfo.editors.Values) {
			Destroy(editor.gameObject);
		}
		sInfo.editors.Clear();
	}

	void DestroyTile() {
		sInfo.clipboard = new JXTerrainTile(info.tile);
		List<JXTerrainTile> tilesList = new List<JXTerrainTile>();
		tilesList.AddRange(info.vegetation.tiles);
		tilesList.RemoveAt(info.tileIndex);
		info.vegetation.tiles = tilesList.ToArray();
		sInfo.editor.scenario.UpdateSuccessionIndices();
		
		Destroy(gameObject);
	}
	
	void OnGUI() {
		GUI.skin = JWinMgr.self.skin;
		pos = JWinMgr.Window(id, pos, HandleTile, "TileEditor", JWinMgr.self.skin.window);
	}

	
	void HandleTile(int id) {
		JXTerrainTile tile = info.tile;
		GUISkin skin = JWinMgr.self.skin;
		if (GUI.Button(new Rect(pos.width - 37, 3, 24, 12), "X")) {
			RemoveEditor(info.tile);
		}
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		GUILayout.Label(info.tile.icon, GUILayout.Width(256), GUILayout.Height(256));
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		GUILayout.Label("herhaal", GUILayout.Width(110));
		bool newShowOneOnly = !GUILayout.Toggle(!info.showOneOnly, "", GUILayout.Width(40));
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("willekeur", GUILayout.Width(110));
		int newSeed = (int) GUILayout.HorizontalSlider(JTileEditorTerrainGen.randomSeed, 0, 1000, GUILayout.Width(80));
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("camera afstand", GUILayout.Width(110));
		float newCamDistance = GUILayout.HorizontalSlider(info.cameraDistance, 20, 100, GUILayout.Width(120));
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("camera hoogte", GUILayout.Width(110));
		float newCamHeight = GUILayout.HorizontalSlider(info.cameraHeight, 10, 50, GUILayout.Width(120));
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("camera hoek", GUILayout.Width(110));
		float newCamAngle = GUILayout.HorizontalSlider(info.cameraAngle, 0, 360, GUILayout.Width(120));
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("verticale hoek", GUILayout.Width(110));
		float newLookHeight = GUILayout.HorizontalSlider(info.lookHeight, -0.5f, 0.5f, GUILayout.Width(120));
		GUILayout.EndHorizontal();
		if ((newShowOneOnly != info.showOneOnly) || (newSeed != JTileEditorTerrainGen.randomSeed)) {
			info.showOneOnly = newShowOneOnly;
			JTileEditorTerrainGen.randomSeed = newSeed;
			info.isChanged = true;
		}
		if ((newCamDistance != info.cameraDistance) || (newCamHeight != info.cameraHeight) ||
		    (newCamAngle != info.cameraAngle) || (newLookHeight != info.lookHeight)) {
			info.cameraDistance = newCamDistance;
			info.cameraHeight = newCamHeight;
			info.cameraAngle = newCamAngle;
			info.lookHeight = newLookHeight;
			info.isChanged = true;
		}
		GUILayout.Space(8);
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Kopieer", GUILayout.Width(90))) {
			sInfo.clipboard = new JXTerrainTile(info.tile);
		}
		GUILayout.FlexibleSpace();
		if (GUILayout.Button("Plak", GUILayout.Width(90))) {
			tile.Assign(sInfo.clipboard);
			info.vegetation.tiles[info.tileIndex] = tile;
			sInfo.editor.scenario.UpdateSuccessionIndices();
			info.isChanged = true;
		}
		if (GUILayout.Button("Herstel", GUILayout.Width(90))) {
			tile.Assign(info.backup);
			info.vegetation.tiles[info.tileIndex] = tile;
			sInfo.editor.scenario.UpdateSuccessionIndices();
			info.isChanged = true;
		}
		GUILayout.EndHorizontal();
		GUILayout.Space(8);
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		if (GUILayout.Button("VERWIJDER!", GUILayout.Width(90))) {
			DestroyTile();
		}
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		info.scroll = GUILayout.BeginScrollView(info.scroll, false, true);
		GUILayout.BeginHorizontal();
		GUILayout.Label("Ondergrond", GUILayout.Width(120));
		float newS0 = GUILayout.HorizontalSlider(tile.am0, 0f, 1f, GUILayout.Width(60));
		float newS1 = GUILayout.HorizontalSlider(tile.am1, 0f, 1f, GUILayout.Width(60));
		float newS2 = GUILayout.HorizontalSlider(tile.am2, 0f, 1f, GUILayout.Width(60));
		float oldS3 = 1f - tile.am0 - tile.am1 - tile.am2;
		float newS3 = GUILayout.HorizontalSlider(oldS3, 0f, 1f, GUILayout.Width(60));
		if ((tile.am0 != newS0) || (tile.am1 != newS1) || (tile.am2 != newS2) || (oldS3 != newS3)) {
			float total = newS0 + newS1 + newS2 + newS3;
			float factor = 1f;
			if (total > 1f) factor = 1f / total;
			tile.am0 = newS0 * factor;
			tile.am1 = newS1 * factor;
			tile.am2 = newS2 * factor;
			info.isChanged = true;
		}
		GUILayout.EndHorizontal();
		
		// Trees
		GUILayout.BeginVertical(skin.box);
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("+", GUILayout.Width(20))) {
			JXTerrainTileTreeData[] trees = new JXTerrainTileTreeData[tile.trees.Length + 1];
			System.Array.Copy(tile.trees, trees, trees.Length - 1);
			JXTerrainTileTreeData treeData = new JXTerrainTileTreeData();
			trees[trees.Length - 1] = treeData;
			tile.trees = trees;
			info.isChanged = true;
		}
		GUILayout.Label("Bomen");
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		int index = 0;
		info.deleteTreeIndex = -1;
		foreach (JXTerrainTileTreeData tree in tile.trees) {
			info.isChanged |= HandleTree(index++, tree);
		}
		
		GUILayout.EndVertical();
		// ~Trees

		// Objects
		GUILayout.BeginVertical(skin.box);
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("+", GUILayout.Width(20))) {
			JXTerrainTileObjectData[] objects = new JXTerrainTileObjectData[tile.objects.Length + 1];
			System.Array.Copy(tile.objects, objects, objects.Length - 1);
			JXTerrainTileObjectData objData = new JXTerrainTileObjectData();
			objects[objects.Length - 1] = objData;
			tile.objects = objects;
			info.isChanged = true;
		}
		GUILayout.Label("Objecten");
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		index = 0;
		info.deleteObjectIndex = -1;
		foreach (JXTerrainTileObjectData obj in tile.objects) {
			info.isChanged |= HandleObject(index++, obj);
		}
		
		GUILayout.EndVertical();
		// ~Objects

		
		// Detail
		GUILayout.BeginHorizontal(skin.box);
		GUILayout.BeginVertical();
		GUILayout.Label("Detail");
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUILayout.BeginVertical();
		
		for (int i = 0; i < sInfo.detailNames.Length; i++) {
			info.isChanged |= HandleDetail(i);
		}
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
		// ~Detail

		// Layer Tile
		GUILayout.BeginVertical(skin.box);
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("+", GUILayout.Width(20))) {
			int[] newExtraLayers = new int[tile.extraLayers.Length + 1];
			System.Array.Copy(tile.extraLayers, newExtraLayers, tile.extraLayers.Length);
			newExtraLayers[tile.extraLayers.Length] = 0;
			tile.extraLayers = newExtraLayers;
			info.isChanged = true;
		}
		GUILayout.Label("Stikkers");
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		int deleteELIndex = -1;
		for (int i = 0; i < tile.extraLayers.Length; i++) {
			GUILayout.BeginHorizontal();
			// GUILayout.Label("Layer");
			if (GUILayout.Button("-", GUILayout.Width(20))) {
				deleteELIndex = i;
				info.isChanged = true;
			}
			if (tile.extraLayers[i] > 0) {
				if (GUILayout.Button("<", GUILayout.Width(20))) {
					tile.extraLayers[i]--;
					info.isChanged = true;
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(20));
			}
			GUILayout.Label(sInfo.layerPrototypes[tile.extraLayers[i]].material.name, GUILayout.Width(140));
			if (tile.extraLayers[i] < sInfo.layerPrototypes.Length - 1) {
				if (GUILayout.Button(">", GUILayout.Width(20))) {
					tile.extraLayers[i]++;
					info.isChanged = true;
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(20));
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}
		GUILayout.EndVertical(); // ~Layer Tile
		
		
		GUILayout.EndScrollView();
		GUILayout.EndVertical();
		GUI.DragWindow();
		
		if (deleteELIndex >= 0) {
			int[] newExtraLayers = new int[tile.extraLayers.Length - 1];
			for (int i = 0; i < newExtraLayers.Length; i++) {
				newExtraLayers[i] = tile.extraLayers[i + ((i >= deleteELIndex)?1:0)];
			}
			tile.extraLayers = newExtraLayers;
		}
		
		if (info.deleteTreeIndex >= 0) {
			JXTerrainTileTreeData[] trees = new JXTerrainTileTreeData[tile.trees.Length - 1];
			for (int i = 0; i < trees.Length; i++) {
				trees[i] = tile.trees[i + ((i >= info.deleteTreeIndex)?1:0)];
			}
			tile.trees = trees;
		}
		if (info.deleteObjectIndex >= 0) {
			JXTerrainTileObjectData[] objects = new JXTerrainTileObjectData[tile.objects.Length - 1];
			for (int i = 0; i < objects.Length; i++) {
				objects[i] = tile.objects[i + ((i >= info.deleteObjectIndex)?1:0)];
			}
			tile.objects = objects;
		}
		if (info.isChanged /* && JXTileEditorTerrain.CanRender() */) {
			info.isChanged = false;
			JTileIconGenerator.self.CalculateCameraPos(info.cameraAngle, info.cameraDistance, info.cameraHeight, info.lookHeight);
			JTileIconGenerator.self.UpdateTexture(ref tile.icon, tile, info.showOneOnly?(info.vegetation.tiles[0]):tile);
		}
	}

	bool HandleObject(int index, JXTerrainTileObjectData obj) {
		bool isChanged = false;
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("-", GUILayout.Width(20))) {
			info.deleteObjectIndex = index;
			isChanged = true;
		}
		if (obj.index > 0) {
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				obj.index--;
				isChanged = true;
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.Label(sInfo.objectNames[obj.index], GUILayout.Width(80));
		if (obj.index < sInfo.objectNames.Length - 1) {
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				obj.index++;
				isChanged = true;
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		float newX = GUILayout.HorizontalSlider(obj.x, 0f, 1f, GUILayout.Width(60));
		float newY = GUILayout.HorizontalSlider(obj.y, 0f, 1f, GUILayout.Width(60));
		float newR = GUILayout.HorizontalSlider(obj.r, 0f, 0.5f, GUILayout.Width(30));
		GUILayout.Space(8);
		float newAngle = GUILayout.HorizontalSlider(obj.angle, 0f, 360f, GUILayout.Width(30));
		GUILayout.Space(8);
		float oldHeight = .5f * (obj.minHeight + obj.maxHeight);
		float oldHeightVar = obj.maxHeight - obj.minHeight;
		float oldWidth = .5f * (obj.maxWidthVariance + obj.minWidthVariance);
		float oldWidthVar = obj.maxWidthVariance - obj.minWidthVariance;
		float newHeight = GUILayout.HorizontalSlider(oldHeight, 0.5f, 2f, GUILayout.Width(60));
		float newHeightVar = GUILayout.HorizontalSlider(oldHeightVar, 0f, 1f, GUILayout.Width(30));
		GUILayout.Space(8);
		float newWidth = GUILayout.HorizontalSlider(oldWidth, 0.25f, 2f, GUILayout.Width(60));
		float newWidthVar = GUILayout.HorizontalSlider(oldWidthVar, 0f, 1f, GUILayout.Width(30));
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		if ((newX != obj.x) || (newY != obj.y) || (newR != obj.r) || (newAngle != obj.angle)) {
			obj.x = newX;
			obj.y = newY;
			obj.r = newR;
			obj.angle = newAngle;
			isChanged = true;
		}
		if ((newHeight != oldHeight) || (newHeightVar != oldHeightVar)) {
			obj.minHeight = Mathf.Max(0f, newHeight - 0.5f * newHeightVar);
			obj.maxHeight = newHeight + 0.5f * newHeightVar;
			isChanged = true;
		}
		if ((newWidth != oldWidth) || (newWidthVar != oldWidthVar)) {
			obj.minWidthVariance = Mathf.Max(0f, newWidth - 0.5f * newWidthVar);
			obj.maxWidthVariance = newWidth + 0.5f * newWidthVar;
			isChanged = true;
		}
		return isChanged;
	}

	bool HandleTree(int index, JXTerrainTileTreeData tree) {
		bool isChanged = false;
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("-", GUILayout.Width(20))) {
			info.deleteTreeIndex = index;
			isChanged = true;
		}
		if (tree.prototypeIndex > 0) {
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				tree.prototypeIndex--;
				isChanged = true;
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.Label(sInfo.treeNames[tree.prototypeIndex], GUILayout.Width(80));
		if (tree.prototypeIndex < sInfo.treeNames.Length - 1) {
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				tree.prototypeIndex++;
				isChanged = true;
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		float newX = GUILayout.HorizontalSlider(tree.x, 0f, 1f, GUILayout.Width(60));
		float newY = GUILayout.HorizontalSlider(tree.y, 0f, 1f, GUILayout.Width(60));
		float newR = GUILayout.HorizontalSlider(tree.r, 0f, 0.5f, GUILayout.Width(30));
		GUILayout.Space(8);
		float oldHeight = .5f * (tree.minHeight + tree.maxHeight);
		float oldHeightVar = tree.maxHeight - tree.minHeight;
		float oldWidth = .5f * (tree.maxWidthVariance + tree.minWidthVariance);
		float oldWidthVar = tree.maxWidthVariance - tree.minWidthVariance;
		float newHeight = GUILayout.HorizontalSlider(oldHeight, 0.5f, 2f, GUILayout.Width(60));
		float newHeightVar = GUILayout.HorizontalSlider(oldHeightVar, 0f, 1f, GUILayout.Width(30));
		GUILayout.Space(8);
		float newWidth = GUILayout.HorizontalSlider(oldWidth, 0.25f, 2f, GUILayout.Width(60));
		float newWidthVar = GUILayout.HorizontalSlider(oldWidthVar, 0f, 1f, GUILayout.Width(30));
		GUILayout.Space(8);
		float oldColour = tree.colorTo.r;
		float newColour = GUILayout.HorizontalSlider(oldColour, 0.25f, 1f, GUILayout.Width(60));
		if (GUILayout.Button("", GUILayout.Width(16))) newColour = 0.75f;
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		if ((newX != tree.x) || (newY != tree.y) || (newR != tree.r)) {
			tree.x = newX;
			tree.y = newY;
			tree.r = newR;
			isChanged = true;
		}
		if ((newHeight != oldHeight) || (newHeightVar != oldHeightVar)) {
			tree.minHeight = Mathf.Max(0f, newHeight - 0.5f * newHeightVar);
			tree.maxHeight = newHeight + 0.5f * newHeightVar;
			isChanged = true;
		}
		if ((newWidth != oldWidth) || (newWidthVar != oldWidthVar)) {
			tree.minWidthVariance = Mathf.Max(0f, newWidth - 0.5f * newWidthVar);
			tree.maxWidthVariance = newWidth + 0.5f * newWidthVar;
			isChanged = true;
		}
		if (newColour != oldColour) {
			Color toColour = new Color(newColour, newColour, newColour, 1f);
			tree.colorTo = toColour;
			tree.colorFrom = 0.75f * toColour;
			isChanged = true;
		}
		return isChanged;
	}

	bool HandleDetail(int index) {
		JXTerrainTile tile = info.tile;
		bool isChanged = false;
		GUILayout.BeginHorizontal();
		GUILayout.Label(sInfo.detailNames[index], GUILayout.Width(120));
		string newCountStr = GUILayout.TextField(info.detailCountStrings[index], GUILayout.Width(60));
		if (newCountStr != info.detailCountStrings[index]) {
			int count = 0;
			if (int.TryParse(newCountStr, out count)) {
				if (count >= 0) {
					if (index >= tile.detailCounts.Length) {
						if (count > 0) {
							int[] newCounts = new int[index + 1];
							System.Array.Copy(tile.detailCounts, newCounts, tile.detailCounts.Length);
							newCounts[index] = count;
							tile.detailCounts = newCounts;
							isChanged = true;
						}
					}
					else {
						tile.detailCounts[index] = count;
						isChanged = true;
					}
				}
			}
		}
		info.detailCountStrings[index] = newCountStr;
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		return isChanged;
	}
	
}
