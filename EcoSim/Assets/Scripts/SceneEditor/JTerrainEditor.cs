using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class JTerrainEditor : MonoBehaviour {

	private int id = JWinMgr.idGenerator++;
	private Rect pos = new Rect(50, 50, 420, 400);
	
	private Color[] colors;
	private int activeColorIndex = 0;
	public JEditor editor;
	
	public Texture2D raiseHeightTex;
	public Texture2D setHeightTex;
	public Texture2D equaliseTex;
	public Texture2D relativeHeightTex;
	public Texture2D slootTex;
	public Texture2D parametersTex;
	public Texture2D setSuccessionTex;
	public Texture2D setSpeciesTex;
	public Texture2D setTileTex;
	public Texture2D imageToDataTex;
	public Texture2D questionTex;
	
	private JTerrainData data;

	void ScenarioChanged() {
		if (editor.scenario != null) {
			data = editor.scenario.data;
		}
		else {
			data = null;
		}
	}
	
	void OnGUI() {
		GUI.skin = JWinMgr.self.skin;
		pos = JWinMgr.Window(id, pos, HandleEditor, "Terrein", JWinMgr.self.skin.window);
	}
	
	void OnDisable() {
		if (JEditAreas.self) JEditAreas.self.DisableSelecting();
	}

	void OnEnable() {
		ScenarioChanged();
		activeTool = ETool.RaiseLower;
		JEditAreas.self.EnableSelecting(radius, strength, smooth);
	}
	
	// Use this for initialization
	void Start () {
		directoryName = JConfig.desktopPath;
	}
	
	enum ETool { RaiseLower, SetHeight, Equalise, RelativeHeight, Sloot, Parameters, SetSuccession, SetSpecies, SetTile, ImageToData };
	
	ETool activeTool = ETool.RaiseLower;
	Vector2 scroll = Vector2.zero;
	
	private int strength = 100;
	private string strengthStr = "100";
	
	private int radius = 0;
	private string radiusStr = "0";
	private bool smooth = true;
	private float height = 1.0f;
	private string heightStr = "1.0";
	private float deltaHeight = 1.0f;
	private string deltaHeightStr = "1.0";
	private bool workOnWater = false;
	private int slootDepth = 0;
	private int activeSpecies = 0;
	
	public delegate void ProcessImgPoint(int x, int y, Color c);
	private bool isBusyProcessingImg = false;
	
	private int activeTargetMap = -1;
	
	IEnumerator COProcessImage(ProcessImgPoint process) {
		isBusyProcessingImg = true;
		Color[] pixels = image.GetPixels();
		int width = image.width;
		int height = image.height;
		int dwidth = data.width;
		int dheight = data.height;
		
		int p = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int xx = x + imageOffsetX;
				int yy = y + imageOffsetY;
				if ((xx >= 0) && (xx < dwidth) && (yy >= 0) && (yy <dheight)) {
					process(xx, yy, pixels[p]);
				}
				p++;
			}
			if ((y % 8) == 0) yield return 0;
		}
		yield return 0;
		JCamera.ForceRender();
		yield return 0;
		isBusyProcessingImg = false;
	}
	
	void DImgSetHeight(int x, int y, Color c) {
		float height = (maxHeight - minHeight) * c.r + minHeight;
		data.SetHeight(x, y, height);
	}
	
	void DImgAddHeight(int x, int y, Color c) {
		float height = (maxHeight - minHeight) * c.r + minHeight;		
		data.SetHeight(x, y, data.GetHeight(x, y) + height);
	}

	void DImgSubHeight(int x, int y, Color c) {
		float height = (maxHeight - minHeight) * c.r + minHeight;		
		data.SetHeight(x, y, data.GetHeight(x, y) - height);
	}

	void DImgSetWaterHeight(int x, int y, Color c) {
		float height = (maxHeight - minHeight) * c.r + minHeight;
		data.SetWaterHeight(x, y, height);
	}
	
	void DImgAddWaterHeight(int x, int y, Color c) {
		float height = (maxHeight - minHeight) * c.r + minHeight;		
		data.SetWaterHeight(x, y, data.GetWaterHeight(x, y) + height);
	}

	void DImgSubWaterHeight(int x, int y, Color c) {
		float height = (maxHeight - minHeight) * c.r + minHeight;		
		data.SetWaterHeight(x, y, data.GetWaterHeight(x, y) - height);
	}
	
	void DImgRelHeight(int x, int y, Color c) {
		float height = (maxHeight - minHeight) * c.r + minHeight;		
		data.SetHeight(x, y, data.GetWaterHeight(x, y) + height);
	}
	
	void DImgRelWaterHeight(int x, int y, Color c) {
		float height = (maxHeight - minHeight) * c.r + minHeight;		
		data.SetWaterHeight(x, y, data.GetHeight(x, y) + height);
	}
	
	void DImgSloten(int x, int y, Color c) {
		int val = Mathf.Clamp((int) (c.r * 8.0), 0, 7);
		data.SetSpecial(x, y, (byte) val, (byte) 0x07);
	}
	
	void DImgSetParameters(int x, int y, Color c) {
		int val = (int) (255 * c.r);
		data.SetData(activeType, x, y, (byte) val);
	}
	
	void DImgSetVegetation(int x, int y, Color c) {
		if (c == colors[activeColorIndex]) {
			data.SetVegetation(x, y, vegetation, variantIndex);
		}
	}
	
	void DIncreaseHeight(int x, int y, int strength) {
		if (workOnWater) {
			float h = data.GetWaterHeight(x, y);
			data.SetWaterHeight(x, y, h + height * ((float) strength) / 100f);
			data.SetAdjustedWaterHeight(x, y, h + height * ((float) strength) / 100f);
		}
		else {
			float h = data.GetHeight(x, y);
			data.SetHeight(x, y, h + height * ((float) strength) / 100f);
		}
	}

	void DDecreaseHeight(int x, int y, int strength) {
		if (workOnWater) {
			float h = data.GetWaterHeight(x, y);
			data.SetWaterHeight(x, y, h - height * ((float) strength) / 100f);
			data.SetAdjustedWaterHeight(x, y, h - height * ((float) strength) / 100f);
		}
		else {
			float h = data.GetHeight(x, y);
			data.SetHeight(x, y, h - height * ((float) strength) / 100f);
		}
	}

	void DSetHeight(int x, int y, int strength) {
		if (workOnWater) {
			float h = data.GetWaterHeight(x, y);
			h = Mathf.Lerp(h, height, ((float) strength) / 100f);
			data.SetWaterHeight(x, y, h);
			data.SetAdjustedWaterHeight(x, y, h);
		}
		else {
			float h = data.GetHeight(x, y);
			h = Mathf.Lerp(h, height, ((float) strength) / 100f);
			data.SetHeight(x, y, h);
		}
	}
	
	int avgHeightCount = 0;

	void DCalculateAvgHeight(int x, int y, int strength) {
		if (workOnWater) {
			float h = data.GetWaterHeight(x, y);
			height += h * strength;
			avgHeightCount += strength;
		}
		else {
			float h = data.GetHeight(x, y);
			height += h * strength;
			avgHeightCount += strength;
		}
	}

	void DSetLandHeightRelToWater(int x, int y, int strength) {
		float h = data.GetWaterHeight(x, y) + deltaHeight;
		float h2 = data.GetHeight(x, y);
		h2 = Mathf.Lerp(h2, h, ((float) strength) / 100f);
		data.SetHeight(x, y, h2);
	}

	void DSetWaterHeightRelToLand(int x, int y, int strength) {
		float h = data.GetHeight(x, y) + deltaHeight;
		float h2 = data.GetWaterHeight(x, y);
		h2 = Mathf.Lerp(h2, h, ((float) strength) / 100f);
		data.SetWaterHeight(x, y, h2);
		data.SetAdjustedWaterHeight(x, y, h2);
	}

	void DSloot(int x, int y, int strength) {
		data.SetSpecial(x, y, (byte) slootDepth, 0x07);
		int variant;
		JXVegetation veg = data.GetVegetation(x, y, out variant);
		if ((slootDepth > 0) || (veg.tiles.Length < 2)) {
			data.SetVegetation(x, y, veg, 0);
		}
		else {
			int count = veg.tiles.Length;
			data.SetVegetation(x, y, veg, Random.Range(1, count));
		}
	}

	void DSetTile(int x, int y, int strength) {
		if (Random.Range(0, 100) < strength) {
			JXVegetation veg = editor.scenario.successions[successionIndex].vegetation[vegetationIndex];
			int tile = (variantIndex < 0)?(Random.Range(1, veg.tiles.Length)):variantIndex;
			if (tile >= veg.tiles.Length) tile = 0;
			int sloot = data.GetSpecial(x, y);
			if (sloot > 0) tile = 0;
			data.SetVegetation(x, y, veg, tile);
		}
	}
	
	void DSetParameter(int x, int y, int strength) {
		data.SetData(activeType, x, y, (byte) ((strength * 255 + 50) / 100));
	}

	void DSetSuccessionMap(int x, int y, int strength) {
		data.SetNeedCalculateSuccession(x, y);
	}

	void DSetTargetMap(int x, int y, int strength) {
		data.SetTarget(activeTargetMap, x, y);
	}
	
	void DSetSpeciesMap(int x, int y, int strength) {
		if (data.NeedCalculateSuccession(x, y)) {
			int v = Mathf.Clamp(strength, 0, 3);
			Debug.Log("setSpecies(" + x + ", " + y + "," + activeSpecies + ", " + v + ")");
			data.SetSpecies(x, y, activeSpecies, v);
		}
	}

	void SetupParameters() {
		JEditAreas.self.Clear();
		for (int cy = 0; cy < data.cHeight; cy++) {
			for (int cx = 0; cx < data.cWidth; cx++) {
				JCellData cell = data.GetCell(cx, cy);
				if (cell.HasParameterType(activeType)) {
					int xOff = cx * JTerrainData.CELL_SIZE;
					int yOff = cy * JTerrainData.CELL_SIZE;
					for (int y = 0; y < JTerrainData.CELL_SIZE; y++) {
						for (int x = 0; x < JTerrainData.CELL_SIZE; x++) {
							JEditAreas.self.Add(x + xOff, y + yOff, cell.GetData(activeType, x, y) * 100 / 255);
						}
					}
				}
			}
		}
	}
	
	void SetupSuccessionSelection() {
		JEditAreas.self.Clear();
		for (int cy = 0; cy < data.cHeight; cy++) {
			for (int cx = 0; cx < data.cWidth; cx++) {
				JCellData cell = data.GetCell(cx, cy);
				if (cell.NeedCalculateSuccession()) {
					int xOff = cx * JTerrainData.CELL_SIZE;
					int yOff = cy * JTerrainData.CELL_SIZE;
					for (int y = 0; y < JTerrainData.CELL_SIZE; y++) {
						for (int x = 0; x < JTerrainData.CELL_SIZE; x++) {
							if (cell.NeedCalculateSuccession(x, y)) {
								JEditAreas.self.Add(x + xOff, y + yOff, 100);
							}
						}
					}
				}
			}
		}
	}

	void SetupTargetSelection() {
		int index = activeTargetMap;
		JEditAreas.self.Clear();
		for (int cy = 0; cy < data.cHeight; cy++) {
			for (int cx = 0; cx < data.cWidth; cx++) {
				JCellData cell = data.GetCell(cx, cy);
				byte[] map = cell.GetTargetMap(index);
				if (map != null) {
					int p = 0;
					int xOff = cx * JTerrainData.CELL_SIZE;
					int yOff = cy * JTerrainData.CELL_SIZE;
					for (int y = 0; y < JTerrainData.CELL_SIZE; y++) {
						for (int x = 0; x < JTerrainData.CELL_SIZE; x += 8) {
							int v = map[p++];
							if (v != 0) {
								for (int q = 0; q < 8; q++) {
									if ((v & (1 << q)) != 0) {
										JEditAreas.self.Add(x + q + xOff, y + yOff, 100);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	
	void SetupSpeciesSelection() {
		if (activeSpecies >= data.scenario.species.Length) return;
		JEditAreas.self.Clear();
		if (data.scenario.species.Length == 0) return;
		for (int cy = 0; cy < data.cHeight; cy++) {
			for (int cx = 0; cx < data.cWidth; cx++) {
				JCellData cell = data.GetCell(cx, cy);
				if (cell.CountSpecies(activeSpecies) > 0) {
					int xOff = cx * JTerrainData.CELL_SIZE;
					int yOff = cy * JTerrainData.CELL_SIZE;
					for (int y = 0; y < JTerrainData.CELL_SIZE; y++) {
						for (int x = 0; x < JTerrainData.CELL_SIZE; x++) {
							int v = cell.GetSpeciesValue(x, y, activeSpecies);
							if (v > 0) {
								JEditAreas.self.Add(x + xOff, y + yOff, v);
							}
						}
					}
				}
			}
		}
	}
	
	void UpdateSuccessionMap() {
		for (int cy = 0; cy < data.cHeight; cy++) {
			for (int cx = 0; cx < data.cWidth; cx++) {
				JCellData cell = data.GetCell(cx, cy);
				if (cell.NeedCalculateSuccession()) {
					cell.ClearNeedCalculateSuccession();
				}
			}
		}
		JEditAreas.self.ProcessPoints(DSetSuccessionMap);
	}
	
	void UpdateTargetMap() {
		for (int cy = 0; cy < data.cHeight; cy++) {
			for (int cx = 0; cx < data.cWidth; cx++) {
				JCellData cell = data.GetCell(cx, cy);
				cell.ClearTargetMap(activeTargetMap);
			}
		}
		JEditAreas.self.ProcessPoints(DSetTargetMap);
	}

	void UpdateSpeciesMap() {
		for (int cy = 0; cy < data.cHeight; cy++) {
			for (int cx = 0; cx < data.cWidth; cx++) {
				JCellData cell = data.GetCell(cx, cy);
				cell.ClearSpecies(activeSpecies);
			}
		}
		JEditAreas.self.ProcessPoints(DSetSpeciesMap);
	}
	
	EParamTypes activeType = EParamTypes.PH;
	int successionIndex = 0;
	int vegetationIndex = 0;
	int variantIndex = -1;
	JXVegetation vegetation;
	
	void HandleSetTile() {
		GUILayout.BeginHorizontal();
		GUILayout.Label("Sterkte", GUILayout.Width(80));
		int newStrength = (int) GUILayout.HorizontalSlider(strength, 1, 100, GUILayout.Width(100f));
		if (newStrength != strength) strengthStr = newStrength.ToString();
		strengthStr = GUILayout.TextField(strengthStr, GUILayout.Width(40f));
		int.TryParse(strengthStr, out newStrength);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Radius", GUILayout.Width(80));
		int newRadius = (int) GUILayout.HorizontalSlider(radius, 0, 20, GUILayout.Width(100f));
		if (newRadius != radius) radiusStr = newRadius.ToString();
		radiusStr = GUILayout.TextField(radiusStr, GUILayout.Width(40f));
		int.TryParse(radiusStr, out newRadius);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Uitgevlakt", GUILayout.Width(80));
		bool newSmooth =  GUILayout.Toggle(smooth, "", GUILayout.Width(100f));
		if ((newStrength != strength) || (radius != newRadius) || (smooth != newSmooth)) {
			strength = newStrength;
			radius = newRadius;
			smooth = newSmooth;
			JEditAreas.self.EnableSelecting(radius, ((float) strength) / 100f, smooth);
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		JScenario scenario = editor.scenario;
		if (scenario != null) {
			GUILayout.BeginHorizontal();
			JXSuccession succession = scenario.successions[successionIndex];
			if (successionIndex > 0) {
				if (GUILayout.Button("<", GUILayout.Width(20))) {
					successionIndex--;
					vegetationIndex = 0;
					if (variantIndex > 1) variantIndex = 1;
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(20));
			}
			GUILayout.Label(succession.name, GUILayout.Width(300));
			if (successionIndex < scenario.successions.Length - 1) {
				if (GUILayout.Button(">", GUILayout.Width(20))) {
					successionIndex++;
					vegetationIndex = 0;
					if (variantIndex > 1) variantIndex = 1;
				}
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			if (succession.vegetation.Length > 0) {
				GUILayout.BeginHorizontal();
				vegetation = succession.vegetation[vegetationIndex];
				if (vegetationIndex > 0) {
					if (GUILayout.Button("<", GUILayout.Width(20))) {
						vegetationIndex--;
					if (variantIndex > 1) variantIndex = 1;
					}
				}
				else {
					GUILayout.Label("", GUILayout.Width(20));
				}
				GUILayout.Label(vegetation.name, GUILayout.Width(300));
				if (vegetationIndex < succession.vegetation.Length - 1) {
					if (GUILayout.Button(">", GUILayout.Width(20))) {
						vegetationIndex++;
					if (variantIndex > 1) variantIndex = 1;
					}
				}
				else {
					GUILayout.Label("", GUILayout.Width(20));
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
				GUIStyle selected = JWinMgr.self.skin.GetStyle("selectedbox");
				GUIStyle normal = JWinMgr.self.skin.box;
				int counter = 0;
				for (int i = -1; i < vegetation.tiles.Length; i++) {
					if (counter % 5 == 0) {
						GUILayout.FlexibleSpace();
						GUILayout.EndHorizontal();
						GUILayout.BeginHorizontal();
					}
					if (GUILayout.Button((i < 0)?questionTex:(vegetation.tiles[i].GetIcon()), (i == variantIndex)?selected:normal , GUILayout.Width(64), GUILayout.Height(64))) {
						variantIndex = i;
					}
					counter++;
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
			}
		}
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Deselecteer")) {
			JEditAreas.self.Clear();
		}
		if (GUILayout.Button("Zet tegels")) {
			JEditAreas.self.ProcessPoints(DSetTile);
			JCamera.ForceRender();
			JEditAreas.self.Redraw();
		}
		if (variantIndex >= 0) {
			if (variantIndex >= scenario.successions[successionIndex].vegetation[vegetationIndex].tiles.Length) {
				variantIndex = -1;
			}
			JXTerrainTile tile = scenario.successions[successionIndex].vegetation[vegetationIndex].tiles[variantIndex];
			bool isActive = JTileEditor.EditorActive(tile);
			if (!isActive && GUILayout.Button("Open tegeleditor")) {
				JTileEditor.Create(transform.parent, tile);
			}
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		if ((image != null) && (colors != null)) {
			GUILayout.BeginHorizontal();
			if (activeColorIndex > 0) {
				if (GUILayout.Button("<", GUILayout.Width(20))) {
					activeColorIndex--;
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(20));
			}
			Color bgColor = GUI.backgroundColor;
			GUI.backgroundColor = colors[activeColorIndex];
			GUILayout.Box("", GUILayout.Width(16), GUILayout.Height(16));
			GUI.backgroundColor = bgColor;
			
			if (activeColorIndex < colors.Length - 1) {
				if (GUILayout.Button(">", GUILayout.Width(20))) {
					activeColorIndex++;
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(20));
			}
			if (GUILayout.Button("Zet vegetatie op pixels uit png met gekozen kleur")) {
				StartCoroutine(COProcessImage(DImgSetVegetation));
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}
	}
	
	void HandleSloten() {
		GUILayout.BeginHorizontal();
		GUILayout.Label("Diepte", GUILayout.Width(80));
		slootDepth = (int) GUILayout.HorizontalSlider(slootDepth, 0, 7, GUILayout.Width(100f));
		GUILayout.Label(slootDepth.ToString(), GUILayout.Width(40f));
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Deselecteer")) {
			JEditAreas.self.Clear();
		}
		if (GUILayout.Button("Maak sloot")) {
			JEditAreas.self.ProcessPoints(DSloot);
			JCamera.ForceRender();
			JEditAreas.self.Redraw();
		}
		if (GUILayout.Button("Herbereken water")) {
			data.UpdateAdjustedWaterMap();
			JCamera.ForceRender();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Neem waarden uit png")) {
			StartCoroutine(COProcessImage(DImgSloten));
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();		
	}
	
	int IndexOfParam(EParamTypes param) {
		EParamTypes[] a = EnumExtensions.paramTypeArray;
		for (int i = 0; i < a.Length; i++) {
			if (a[i] == param) return i;
		}
		return 0;
	}

	void HandleParameters() {
		GUILayout.BeginHorizontal();
		GUILayout.Label("Sterkte", GUILayout.Width(80));
		int newStrength = (int) GUILayout.HorizontalSlider(strength, 1, 100, GUILayout.Width(100f));
		if (newStrength != strength) strengthStr = newStrength.ToString();
		strengthStr = GUILayout.TextField(strengthStr, GUILayout.Width(40f));
		int.TryParse(strengthStr, out newStrength);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Radius", GUILayout.Width(80));
		int newRadius = (int) GUILayout.HorizontalSlider(radius, 0, 20, GUILayout.Width(100f));
		if (newRadius != radius) radiusStr = newRadius.ToString();
		radiusStr = GUILayout.TextField(radiusStr, GUILayout.Width(40f));
		int.TryParse(radiusStr, out newRadius);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		if ((newStrength != strength) || (radius != newRadius)) {
			strength = newStrength;
			radius = newRadius;
			JEditAreas.self.EnableSelecting(radius, ((float) strength) / 100f, false, true, true);
		}
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("<", GUILayout.Width(20))) {
			int l = EnumExtensions.paramTypeArray.Length;
			activeType = EnumExtensions.paramTypeArray[((IndexOfParam(activeType) + l - 1) % l)];
			SetupParameters();
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.Label(activeType.ToString(), GUILayout.Width(300));
		if (GUILayout.Button(">", GUILayout.Width(20))) {
			int l = EnumExtensions.paramTypeArray.Length;
			activeType = EnumExtensions.paramTypeArray[((IndexOfParam(activeType) + 1) % l)];
			SetupParameters();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Reset")) {
			SetupParameters();
		}
		if (GUILayout.Button("Maak selectie definitief")) {
			editor.scenario.data.ClearParamType(activeType);
			JEditAreas.self.ProcessPoints(DSetParameter);
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Zet successie default waarden")) {
			for (int cy = 0; cy < data.cHeight; cy++) {
				for (int cx = 0; cx < data.cWidth; cx++) {
					JCellData cell = data.GetCell(cx, cy);
					cell.RemoveUnusedParameters();
					if (cell.NeedCalculateSuccession()) {
						for (int y = 0; y < JTerrainData.CELL_SIZE; y++) {
							for (int x = 0; x < JTerrainData.CELL_SIZE; x++) {
								if (cell.NeedCalculateSuccession(x, y)) {
									cell.SetupDefaultParameters(x, y);
								}
							}
						}
					}
				}
			}
			SetupParameters();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Neem waarden uit png")) {
			StartCoroutine(COProcessImage(DImgSetParameters));
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();		
	}
	
	

	void HandleSuccession() {
		GUILayout.BeginHorizontal();
		GUILayout.Label("Radius", GUILayout.Width(80));
		int newRadius = (int) GUILayout.HorizontalSlider(radius, 0, 20, GUILayout.Width(100f));
		if (newRadius != radius) radiusStr = newRadius.ToString();
		radiusStr = GUILayout.TextField(radiusStr, GUILayout.Width(40f));
		int.TryParse(radiusStr, out newRadius);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		if (radius != newRadius) {
			radius = newRadius;
			JEditAreas.self.EnableSelecting(radius, 1f, false);
		}
		GUILayout.BeginHorizontal();
		if (activeTargetMap >= 0) {
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				activeTargetMap--;
				if (activeTargetMap == -1) {
					SetupSuccessionSelection();
				}
				else {
					SetupTargetSelection();
				}
				JEditAreas.self.limitToNeedSuccession = (activeTargetMap >= 0);

			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.Label((activeTargetMap < 0)?"Successie gebied":("Doel gebied " + activeTargetMap));
		if (activeTargetMap < JCellData.TARGET_MAP_COUNT - 1) {
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				activeTargetMap++;
				SetupTargetSelection();
				JEditAreas.self.limitToNeedSuccession = true;
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Reset")) {
			if (activeTargetMap == -1) {
				SetupSuccessionSelection();
			}
			else {
				SetupTargetSelection();
			}
		}
		if (GUILayout.Button("Maak selectie definitief")) {
			if (activeTargetMap == -1) {
				UpdateSuccessionMap();
			}
			else {
				UpdateTargetMap();
			}
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
	}

	void HandleSpecies() {
		if (data.scenario.species.Length <= 0) {
			GUILayout.BeginHorizontal();
			GUILayout.Label("Maak eerst soorten aan in scenario editor!");
			GUILayout.EndHorizontal();
			return;
		}
		GUILayout.BeginHorizontal();
		if (activeSpecies > 0) {
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				activeSpecies--;
				SetupSpeciesSelection();
			}
		}
		else GUILayout.Label("", GUILayout.Width(20));
		
		GUILayout.Label(data.scenario.species[activeSpecies].name, GUILayout.Width(100));
		
		if (activeSpecies < data.scenario.species.Length - 1) {
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				activeSpecies++;
				SetupSpeciesSelection();
			}
		}
		else GUILayout.Label("", GUILayout.Width(20));
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label("Populatiegrootte", GUILayout.Width(140));
		int newStrength = (int) GUILayout.HorizontalSlider(strength, 1, 3, GUILayout.Width(100f));
		if (newStrength != strength) strengthStr = newStrength.ToString();
		strengthStr = GUILayout.TextField(strengthStr, GUILayout.Width(40f));
		int.TryParse(strengthStr, out newStrength);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();

		
		GUILayout.BeginHorizontal();
		GUILayout.Label("Radius", GUILayout.Width(140));
		int newRadius = (int) GUILayout.HorizontalSlider(radius, 0, 20, GUILayout.Width(100f));
		if (newRadius != radius) radiusStr = newRadius.ToString();
		radiusStr = GUILayout.TextField(radiusStr, GUILayout.Width(40f));
		int.TryParse(radiusStr, out newRadius);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		if ((radius != newRadius) || (strength != newStrength)){
			radius = newRadius;
			strength = newStrength;
			JEditAreas.self.EnableSelecting(radius, strength / 100f, false, true, true);
		}
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Reset")) {
			SetupSpeciesSelection();
		}
		if (GUILayout.Button("Maak selectie definitief")) {
			UpdateSpeciesMap();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
	}
	
	
	
	void HandleHeight() {
		GUILayout.BeginHorizontal();
		GUILayout.Label("Sterkte", GUILayout.Width(80));
		int newStrength = (int) GUILayout.HorizontalSlider(strength, 1, 100, GUILayout.Width(100f));
		if (newStrength != strength) strengthStr = newStrength.ToString();
		strengthStr = GUILayout.TextField(strengthStr, GUILayout.Width(40f));
		int.TryParse(strengthStr, out newStrength);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Radius", GUILayout.Width(80));
		int newRadius = (int) GUILayout.HorizontalSlider(radius, 0, 20, GUILayout.Width(100f));
		if (newRadius != radius) radiusStr = newRadius.ToString();
		radiusStr = GUILayout.TextField(radiusStr, GUILayout.Width(40f));
		int.TryParse(radiusStr, out newRadius);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Uitgevlakt", GUILayout.Width(80));
		bool newSmooth =  GUILayout.Toggle(smooth, "", GUILayout.Width(100f));
		if ((newStrength != strength) || (radius != newRadius) || (smooth != newSmooth)) {
			strength = newStrength;
			radius = newRadius;
			smooth = newSmooth;
			JEditAreas.self.EnableSelecting(radius, ((float) strength) / 100f, smooth);
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		if ((activeTool == ETool.RaiseLower) || (activeTool == ETool.SetHeight)) {
			GUILayout.BeginHorizontal();
			GUILayout.Label("Hoogte (m)", GUILayout.Width(80));
			float minHeight = (activeTool == ETool.SetHeight)?0f:0.1f;
			float maxHeight = (activeTool == ETool.SetHeight)?400f:10f;
			float newHeight = (float) GUILayout.HorizontalSlider(height, minHeight, maxHeight, GUILayout.Width(100f));
			if (newHeight != height) heightStr = newHeight.ToString("0.00");
			heightStr = GUILayout.TextField(heightStr, GUILayout.Width(40f));
			float.TryParse(heightStr, out newHeight);
			height = newHeight;
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}
		if ((activeTool == ETool.RelativeHeight)) {
			GUILayout.BeginHorizontal();
			GUILayout.Label("Delta (m)", GUILayout.Width(80));
			float newDeltaHeight = (float) GUILayout.HorizontalSlider(deltaHeight, -10f, 10f, GUILayout.Width(100f));
			if (newDeltaHeight != deltaHeight) deltaHeightStr = newDeltaHeight.ToString("0.00");
			deltaHeightStr = GUILayout.TextField(deltaHeightStr, GUILayout.Width(40f));
			float.TryParse(deltaHeightStr, out newDeltaHeight);
			deltaHeight = newDeltaHeight;
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}
		if ((activeTool != ETool.RelativeHeight)) {
			GUILayout.BeginHorizontal();
			GUILayout.Label("WaterNiveau", GUILayout.Width(80));
			workOnWater =  GUILayout.Toggle(workOnWater, "", GUILayout.Width(100f));
			
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Deselecteer")) {
			JEditAreas.self.Clear();
		}
		if (activeTool == ETool.RaiseLower) {
			if (GUILayout.Button("Verhogen")) {
				JEditAreas.self.ProcessPoints(DIncreaseHeight);
				JCamera.ForceRender();
				JEditAreas.self.Redraw();
			}
			if (GUILayout.Button("Verlagen")) {
				JEditAreas.self.ProcessPoints(DDecreaseHeight);
				JCamera.ForceRender();
				JEditAreas.self.Redraw();
			}
		}
		else if (activeTool == ETool.SetHeight) {
			if (GUILayout.Button("Bereken Gem. Hoogte")) {
				height = 0;
				avgHeightCount = 0;
				JEditAreas.self.ProcessPoints(DCalculateAvgHeight);
				height = height / avgHeightCount;
				heightStr = height.ToString("0.00");
			}
			if (GUILayout.Button("Zet hoogte")) {
				JEditAreas.self.ProcessPoints(DSetHeight);
				JCamera.ForceRender();
				JEditAreas.self.Redraw();
			}
		}
		else if (activeTool == ETool.Equalise) {
			if (GUILayout.Button("Egaliseer")) {
				height = 0;
				avgHeightCount = 0;
				JEditAreas.self.ProcessPoints(DCalculateAvgHeight);
				height = height / avgHeightCount;
				heightStr = height.ToString("0.00");
				JEditAreas.self.ProcessPoints(DSetHeight);
				JCamera.ForceRender();
				JEditAreas.self.Redraw();
			}
		}
		else if (activeTool == ETool.RelativeHeight) {
			if (GUILayout.Button("LH = WH + Delta")) {
				JEditAreas.self.ProcessPoints(DSetLandHeightRelToWater);
				JCamera.ForceRender();
				JEditAreas.self.Redraw();
			}
			if (GUILayout.Button("WH = LH + Delta")) {
				JEditAreas.self.ProcessPoints(DSetWaterHeightRelToLand);
				JCamera.ForceRender();
				JEditAreas.self.Redraw();
			}
		}
		if (GUILayout.Button("Herbereken water")) {
			data.UpdateAdjustedWaterMap();
			JCamera.ForceRender();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		if ((image != null) && (activeTool != ETool.Equalise)) {
			GUILayout.BeginHorizontal();
			GUILayout.Label("Verwerk ingeladen png:");
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			GUILayout.Label("Min en max hoogten");
			minHeightStr = GUILayout.TextField(minHeightStr, GUILayout.Width(40));
			maxHeightStr = GUILayout.TextField(maxHeightStr, GUILayout.Width(40));
			float.TryParse(minHeightStr, out minHeight);
			float.TryParse(maxHeightStr, out maxHeight);
			switch (activeTool) {
			case ETool.RaiseLower :
				if (workOnWater) {
					if (GUILayout.Button("Verhogen Water")) {
						StartCoroutine(COProcessImage(DImgAddWaterHeight));
					}
					if (GUILayout.Button("Verlagen Water")) {
						StartCoroutine(COProcessImage(DImgSubWaterHeight));
					}
				}
				else {
					if (GUILayout.Button("Verhogen Land")) {
						StartCoroutine(COProcessImage(DImgAddHeight));
					}
					if (GUILayout.Button("Verlagen Land")) {
						StartCoroutine(COProcessImage(DImgSubHeight));
					}
				}
				break;
			case ETool.SetHeight :
				if (workOnWater) {
					if (GUILayout.Button("Zet waterhoogte")) {
						StartCoroutine(COProcessImage(DImgSetWaterHeight));
					}
				}
				else {
					if (GUILayout.Button("Zet landhoogte")) {
						StartCoroutine(COProcessImage(DImgSetHeight));
					}
				}
				break;
			case ETool.RelativeHeight :
				if (GUILayout.Button("LH = WH + Delta")) {
					StartCoroutine(COProcessImage(DImgRelHeight));
				}
				if (GUILayout.Button("WH = LH + Delta")) {
					StartCoroutine(COProcessImage(DImgRelWaterHeight));
				}
				break;
				
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}
	}
	
	float minHeight = 0.0f;
	float maxHeight = 100.0f;
	string minHeightStr = "0.0";
	string maxHeightStr = "100.0";
	string directoryName = null;
	string textureName = "hoogtemap.png";
	string errorMsg = "";
	int imageOffsetX = 0;
	int imageOffsetY = 0;
	string imageOffsetXStr = "0";
	string imageOffsetYStr = "0";
	
	Texture2D image = null;
	
	void HandleImageToData() {
		if (!image) {
			GUILayout.BeginHorizontal();
			GUILayout.Label("Map", GUILayout.Width(80));
			directoryName = GUILayout.TextField(directoryName);
			if (GUILayout.Button("Bureaublad", GUILayout.Width(80))) {
				directoryName = JConfig.desktopPath;
			}
			// GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			GUILayout.Label("png bestand", GUILayout.Width(80));
			textureName = GUILayout.TextField(textureName);
			string path = directoryName + Path.DirectorySeparatorChar + textureName;
			if (!File.Exists(path) || Directory.Exists(path)) {
				GUILayout.Label("", GUILayout.Width(80));
			}
			else if (GUILayout.Button("Laad", GUILayout.Width(80))) {
				byte[] data = null;
				try {
					data = System.IO.File.ReadAllBytes(path);
				}
				catch (System.Exception) {
					errorMsg = "Bestand niet gevonden";
					return;
				}
				Texture2D tex = new Texture2D(4, 4, TextureFormat.ARGB32, false);
				tex.filterMode = FilterMode.Point;
				if (!tex.LoadImage(data)) {
					errorMsg = "Ongeldig bestand";
				}
				else {
					if (image != null) {
						Destroy(image);
					}
					image = tex;
					errorMsg = "";
					tex = null;
					StartCoroutine(COFindColors(image));
				}
				if (tex != null) {
					Destroy(tex);
				}
			}
			// GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			GUILayout.Label(errorMsg);
			GUILayout.EndHorizontal();
		}
		else {
			GUILayout.BeginHorizontal();
			GUILayout.Label(image, GUILayout.Width(128), GUILayout.Height(128));
			GUILayout.BeginVertical();
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("kies bestand", GUILayout.Width(90))) {
				Destroy(image);
				image = null;
				colors = null;
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			GUILayout.Label("X Offset", GUILayout.Width(90));
			imageOffsetXStr = GUILayout.TextField(imageOffsetXStr);
			int.TryParse(imageOffsetXStr, out imageOffsetX);
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			GUILayout.Label("Y Offset", GUILayout.Width(90));
			imageOffsetYStr = GUILayout.TextField(imageOffsetYStr);
			int.TryParse(imageOffsetYStr, out imageOffsetY);
			GUILayout.EndHorizontal();
			GUILayout.EndVertical();
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			string explain = "Veld afmetingen zijn " + data.width + "x" + data.height + "\n";
			explain += "Png afmetingen zijn " + image.width + "x" + image.height + "\n";
			if ((imageOffsetX + image.width < 0) || (imageOffsetY + image.height < 0) ||
				(imageOffsetX >= data.width) || (imageOffsetY >= data.height)) {
				explain += "X, Y offset laat plaatje buiten veld vallen!\n";
			}
			else {
				int minX = Mathf.Clamp(imageOffsetX, 0, data.width - 1);
				int minY = Mathf.Clamp(imageOffsetY, 0, data.height - 1);
				int maxX = Mathf.Clamp(imageOffsetX + image.width - 1, 0, data.width - 1);
				int maxY = Mathf.Clamp(imageOffsetY + image.height - 1, 0, data.height - 1);
				explain += "plaatje beslaat cell " + minX + "x" + minY + " tot en met " + maxX + "x" + maxY + ".\n"; 
			}
			explain += "kies tab uit tabbar om plaatje te gebruiken (bijv. zet hoogte).";
			GUILayout.Label(explain);
			GUILayout.EndHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.BeginHorizontal();
			GUILayout.Label(findColorsProgress);
			GUILayout.EndHorizontal();
		}
	}
	
	private string findColorsProgress;
	
	IEnumerator COFindColors(Texture2D tex) {
		colors = null;
		Color[] pixels = tex.GetPixels();
		List<Color> colorList = new List<Color>();
		int len = pixels.Length;
		int p = len - 1;
		Color lastColor = pixels[p--];
		colorList.Add(lastColor);
		while (tex == image) {
			for (int i = 0; (i < 2048) && (p >= 0); i++) {
				Color nextColor = pixels[p--];
				if (nextColor != lastColor) {
					if (!colorList.Contains(nextColor)) {
						colorList.Add(nextColor);
					}
					lastColor = nextColor;
				}
			}
			if (p < 0) {
				colors = colorList.ToArray();
				findColorsProgress ="Verwerken kleuren voltooid.";
				activeColorIndex = 0;
				yield break;
			}
			findColorsProgress = "Verkwerken kleuren" + (100 - (100 * p / len)) + "%";
			yield return 0;
		}
	}
	
	void HandleEditor(int id) {
		if (GUI.Button(new Rect(pos.width - 37, 3, 24, 12), "X")) {
			gameObject.SetActive(false);
		}
		GUIStyle tab = JWinMgr.self.skin.GetStyle("tab");
		GUIStyle activeTab = JWinMgr.self.skin.GetStyle("activetab");
		GUILayout.BeginVertical();
		if (isBusyProcessingImg) {
			GUILayout.Label("Bezig met verwerken...");
			GUILayout.EndVertical();
			return;
		}
		GUILayout.BeginHorizontal();
		if (GUILayout.Button(raiseHeightTex, (activeTool == ETool.RaiseLower)?activeTab:tab)) {
			activeTool = ETool.RaiseLower;
			height = 1.0f; heightStr = "1.0";
			JEditAreas.self.EnableSelecting(radius, ((float) strength) / 100f, smooth);
		}
		if (GUILayout.Button(setHeightTex, (activeTool == ETool.SetHeight)?activeTab:tab)) {
			activeTool = ETool.SetHeight;
			JEditAreas.self.EnableSelecting(radius, ((float) strength) / 100f, smooth);
		}
		if (GUILayout.Button(equaliseTex, (activeTool == ETool.Equalise)?activeTab:tab)) {
			activeTool = ETool.Equalise;
			JEditAreas.self.EnableSelecting(radius, ((float) strength) / 100f, smooth);
		}
		if (GUILayout.Button(relativeHeightTex, (activeTool == ETool.RelativeHeight)?activeTab:tab)) {
			activeTool = ETool.RelativeHeight;
			JEditAreas.self.EnableSelecting(radius, ((float) strength) / 100f, smooth);
		}
		if (GUILayout.Button(slootTex, (activeTool == ETool.Sloot)?activeTab:tab)) {
			activeTool = ETool.Sloot;
			radius = 0;
			strength = 100;
			JEditAreas.self.Clear();
			JEditAreas.self.EnableSelecting(0, 1f, false);
		}
		if (GUILayout.Button(parametersTex, (activeTool == ETool.Parameters)?activeTab:tab)) {
			activeTool = ETool.Parameters;
			SetupParameters();
			JEditAreas.self.EnableSelecting(radius, 0.5f, false, true, true);
		}
		if (GUILayout.Button(setSuccessionTex, (activeTool == ETool.SetSuccession)?activeTab:tab)) {
			activeTool = ETool.SetSuccession;
			activeTargetMap = -1;
			SetupSuccessionSelection();
			JEditAreas.self.EnableSelecting(radius, 1f, false);
		}
		if (GUILayout.Button(setSpeciesTex, (activeTool == ETool.SetSpecies)?activeTab:tab)) {
			activeTool = ETool.SetSpecies;
			strength = 3;
			strengthStr = "3";
			activeSpecies = 0;
			SetupSpeciesSelection();
			JEditAreas.self.EnableSelecting(radius, strength / 100f, false, true, true);
		}
		if (GUILayout.Button(setTileTex, (activeTool == ETool.SetTile)?activeTab:tab)) {
			activeTool = ETool.SetTile;
			JEditAreas.self.EnableSelecting(0, ((float) strength) / 100f, smooth);
		}
		if (GUILayout.Button(imageToDataTex, (activeTool == ETool.ImageToData)?activeTab:tab)) {
			activeTool = ETool.ImageToData;
			JEditAreas.self.DisableSelecting();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		scroll = GUILayout.BeginScrollView(scroll, JWinMgr.self.skin.GetStyle("box"));
		GUILayout.BeginVertical();
		switch (activeTool) {
		case ETool.RaiseLower : HandleHeight(); break;
		case ETool.SetHeight : HandleHeight(); break;
		case ETool.Equalise : HandleHeight(); break;
		case ETool.RelativeHeight : HandleHeight(); break;
		case ETool.Sloot : HandleSloten(); break;
		case ETool.Parameters : HandleParameters(); break;
		case ETool.SetSuccession : HandleSuccession(); break;
		case ETool.SetSpecies : HandleSpecies(); break;
		case ETool.SetTile : HandleSetTile(); break;
		case ETool.ImageToData : HandleImageToData(); break;
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUILayout.EndScrollView();
		GUILayout.EndVertical();
		GUI.DragWindow();
	}

	void Update() {
		if (isBusyProcessingImg) return;
		Vector3 mousePos = Input.mousePosition;
		if (JWinMgr.InUIRect(mousePos.x, Screen.height - mousePos.y) ||
			Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) {
			return;
		}
		if ((activeTool == ETool.SetHeight) &&  Input.GetMouseButtonDown(1)) {
			Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
			RaycastHit hit;
			if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER)) {
				Vector3 point = hit.point;
				int x = (int) (point.x / JTerrainData.HORIZONTAL_SCALE);
				int y = (int) (point.z / JTerrainData.HORIZONTAL_SCALE);
				if (workOnWater) {
					height = data.GetAdjustedWaterHeight(x, y);
				}
				else {
					height = data.GetHeight(x, y);
				}
				heightStr = height.ToString();
			}
		}
		else if ((activeTool == ETool.SetTile) &&  Input.GetMouseButtonDown(1)) {
			Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
			RaycastHit hit;
			if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER)) {
				Vector3 point = hit.point;
				int x = (int) (point.x / JTerrainData.HORIZONTAL_SCALE);
				int y = (int) (point.z / JTerrainData.HORIZONTAL_SCALE);
				JXVegetation veg = data.GetVegetation(x, y, out variantIndex);
				successionIndex = veg.succession.index;
				vegetationIndex = veg.index;
			}
		}
	}

}
