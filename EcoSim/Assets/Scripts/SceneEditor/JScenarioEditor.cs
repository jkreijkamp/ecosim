using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;

public class JScenarioEditor : MonoBehaviour {
	
	private int id = JWinMgr.idGenerator++;
	private Rect pos = new Rect(100, 100, 750, 600);
	
	public JEditor editor;
	
	private JScenario scenario;
	
	public GameObject nestMarkerPrefab;
	
	private const string SCENARIO_NAME_KEY = "ScenarioName";
	
	private enum ETool { LoadSave, Budget, Actions, Research, Species, OldVariables, OldProgress, GameRules };
	private ETool activeTool;
	
	
	void OnGUI() {
		GUI.skin = JWinMgr.self.skin;
		pos = JWinMgr.Window(id, pos, HandleEditor, "Scenario", JWinMgr.self.skin.window);
	}
	
	private struct Info {
		public string scenarioName;
		public bool startNew;
		public bool makeSmallScenario;
		public int newWidth;
		public int newHeight;
		public int msStartCellX;
		public int msStartCellY;
		public int msCellCountX;
		public int msCellCountY;
		public bool newUseVeg;
		public bool confirmReplace;
		public bool isBusy;
		public string writeToScenario;
	};
	
	private static Info info;
	
	private GUIStyle box;
	void Start() {
		info.scenarioName = JConfig.GetValue(SCENARIO_NAME_KEY, "Scenario");
		info.confirmReplace = false;
		info.newWidth = 1024;
		info.newHeight = 1024;
		info.newUseVeg = true;
		scenario = editor.scenario;
		box = JWinMgr.self.skin.box;
	}
	
	void OnEnable() {
		StartCoroutine(COBlinkCurrentNestGO());
	}
	
	void OnDisable() {
		StopCoroutine("COBlinkCurrentNestGO");
	}
	
	IEnumerator CONew() {
		JConfig.ClearMessage();
		info.isBusy = true;
		yield return 0;
		try {
			if (editor.scenario != null) {
				JRenderTerrain.RemoveAll(editor.scenario);
				editor.scenario.Unload();
			}
			JXSuccession[] successions = null;
			if ((scenario != null) && (info.newUseVeg)) {
				successions = scenario.successions;
			}
			scenario = new JScenario("Scenario", info.newWidth, info.newHeight);
			if (successions != null) {
				scenario.successions = successions;
				scenario.UpdateSuccessionIndices();
			}
			editor.scenario = scenario;
			editor.BroadcastMessage("ScenarioChanged", SendMessageOptions.DontRequireReceiver);
		}
		catch (System.Exception e) {
			info.isBusy = false;
			JConfig.SetMessage(e);
			throw e;
		}
		
		yield return 0;
		info.isBusy = false;
		info.startNew = false;
	}
	
	void CopyImages(string oldPath, string newPath) {
		oldPath += "images/";
		newPath += "images/";
		if (!Directory.Exists(oldPath)) return;
		//Now Create all of the directories
		foreach (string dirPath in Directory.GetDirectories(oldPath, "*", SearchOption.AllDirectories)) {
			try {
    			Directory.CreateDirectory(dirPath.Replace(oldPath, newPath));
			}
			catch (System.Exception e) {
				Debug.LogWarning(e.Message);
			}
		}

		//Copy all .jpg files
		foreach (string filePath in Directory.GetFiles(oldPath, "*.jpg", SearchOption.AllDirectories)) {
			try {
				Debug.Log("Trying to copy '" + filePath + "' to '" + filePath.Replace(oldPath, newPath) + "'");
	    		File.Copy(filePath, filePath.Replace(oldPath, newPath));
			}
			catch (System.Exception e) {
				Debug.LogWarning(e.Message);
			}
		}

		//Copy all .png files
		foreach (string filePath in Directory.GetFiles(oldPath, "*.png", SearchOption.AllDirectories)) {
			try {
				Debug.Log("Trying to copy '" + filePath + "' to '" + filePath.Replace(oldPath, newPath) + "'");
	    		File.Copy(filePath, filePath.Replace(oldPath, newPath), true);
			}
			catch (System.Exception e) {
				Debug.LogWarning(e.Message);
			}
		}
	}

	void CopyExtraAssets(string oldPath, string newPath) {
		//Copy all .unity3d files
		foreach (string filePath in Directory.GetFiles(oldPath, "*.unity3d")) {
			try {
				Debug.Log("Trying to copy '" + filePath + "' to '" + filePath.Replace(oldPath, newPath) + "'");
	    		File.Copy(filePath, filePath.Replace(oldPath, newPath));
			}
			catch (System.Exception e) {
				Debug.LogWarning(e.Message);
			}
		}
	}
	
	IEnumerator COSaveResized(string scenarioName) {
		info.isBusy = true;
		JConfig.ClearMessage();
		yield return 0;
		if (editor.scenario == null) {
			JConfig.message = "Geen scenario om weg te schrijven";
			info.isBusy = false;
			yield break;
		}
		try {
			string oldPath = scenario.scenarioPath;
			scenario.data.Resize(info.msStartCellX, info.msStartCellY, info.msCellCountX, info.msCellCountY);
			scenario.data.UpdateAdjustedWaterMap();
			JRenderMap.SetupForScenario(scenario);
			scenario.SaveScenario(scenarioName);
			CopyImages(oldPath, scenario.scenarioPath);
			JConfig.message = "Scenario weggeschreven";
			JConfig.SetValue(SCENARIO_NAME_KEY, scenarioName);
		}
		catch (System.Exception e) {
			JConfig.SetMessage(e);
		}
		yield return 0;
		yield return new WaitForSeconds(5.0f);
		info.isBusy = false;
		info.makeSmallScenario = false;
		Application.LoadLevel("Edit");
	}
	
	IEnumerator COSave(string scenarioName) {
		info.isBusy = true;
		JConfig.ClearMessage();
		yield return 0;
		if (editor.scenario == null) {
			JConfig.message = "Geen scenario om weg te schrijven";
			info.isBusy = false;
			yield break;
		}
		try {
			scenario.data.UpdateAdjustedWaterMap();
			string oldPath = scenario.scenarioPath;
			scenario.SaveScenario(scenarioName);
			if (oldPath != scenario.scenarioPath) {
				CopyImages(oldPath, scenario.scenarioPath);
				CopyExtraAssets(oldPath, scenario.scenarioPath);
			}
		}
		catch (System.Exception e) {
			info.isBusy = false;
			JConfig.SetMessage(e);
			throw e;
		}
		yield return 0;
		JConfig.message = "Scenario weggeschreven";
		JConfig.SetValue(SCENARIO_NAME_KEY, scenarioName);
		info.isBusy = false;
	}
	
	IEnumerator COLoad(string scenarioName) {
		if (info.isBusy) yield break;
		info.isBusy = true;
		JConfig.ClearMessage();
		yield return 0;
		if (JScenario.IsScenario(scenarioName)) {
			try {
				if (editor.scenario != null) {
					JRenderTerrain.RemoveAll(editor.scenario);
					editor.scenario.Unload();
				}
				scenario = JScenario.Load(scenarioName, null, false, false);
			}
			catch (System.Exception e) {
				info.isBusy = false;
				JConfig.SetMessage(e);
				throw e;
			}
			yield return StartCoroutine(scenario.COLoadExtraAssets());
			editor.scenario = scenario;
			editor.BroadcastMessage("ScenarioChanged", SendMessageOptions.DontRequireReceiver);
			JConfig.message = "Scenario ingelezen";
			JConfig.SetValue(SCENARIO_NAME_KEY, scenarioName);
		}
		else if (JScenario.IsObsoleteScenario(scenarioName)) {
			JConfig.message = "Formaat niet meer ondersteund.";
		}
		else {
			JConfig.message = "Scenario niet gevonden";
		}
		
		yield return 0;
		info.isBusy = false;
	}
	
	void ScenarioChanged() {
		JCamera.SetScenario(scenario, true);
		info.msStartCellX = 0;
		info.msStartCellY = 0;
		info.msCellCountX = 256 / JTerrainData.CELL_SIZE;
		info.msCellCountY = 256 / JTerrainData.CELL_SIZE;
	}
	
	IEnumerator COControleerVegetatie(bool fix) {
		List<string> messages = new List<string>();
		JTerrainData data = scenario.data;
		JXSuccession[] successionData = scenario.successions;
		JScenario scenarioLocalRef = scenario;
		JConfig.message = "Even geduld...";
		if (successionData.Length == 0) {
			messages.Add("geen successies gedefineerd (kan niet automatisch gerepareerd worden)!");
		}
		else {
			if (successionData[0].vegetation.Length == 0) {
				messages.Add("eerste successie type (" + successionData[0].name +
					") moet minstens 1 vegetatie hebben! (kan niet automatisch gerepareerd worden).");
			}
			else if (successionData[0].vegetation[0].tiles.Length == 0) {
				messages.Add("eerste vegetatie in eerste successie type (" + successionData[0].name +
					" -> " + successionData[0].vegetation[0].name +
					") moet minstens 1 variant hebben! (kan niet automatisch gerepareerd worden).");
			}
		}
		int count = 0;
		for (int cy = 0; cy < data.cHeight; cy++) {
			for (int cx = 0; cx < data.cWidth; cx++) {
				if (scenarioLocalRef != scenario) {
					JConfig.message = "Controleer vegetatie onderbroken";
					yield break;
				}
				JCellData cellData = data.GetCell(cx, cy);
				if (!cellData.isLoaded) cellData.Load();
				for (int y = 0; y < JTerrainData.CELL_SIZE; y++) {
					for (int x = 0; x < JTerrainData.CELL_SIZE; x++) {
						int successionIndex = cellData.GetSuccessionIndex(x, y);
						if ((successionIndex < 0) || (successionIndex >= successionData.Length)) {
							string msg = cellData.GetCellName() + ": successie index ongeldig : " + successionIndex;
							if (!messages.Contains(msg)) messages.Add(msg);
							if (fix) {
								cellData.SetVegetation(x, y, successionData[0].vegetation[0].tiles[0]);
							}
							count++;
						}
						else {
							JXSuccession succession = successionData[successionIndex];
							int vegetationIndex = cellData.GetVegetationIndex(x, y);
							if ((vegetationIndex < 0) || (vegetationIndex >= succession.vegetation.Length)) {
								string msg = cellData.GetCellName() + ": " + succession.name + " vegetatie index ongeldig : " + successionIndex;
								if (!messages.Contains(msg)) messages.Add(msg);
								if (fix) {
									if ((succession.vegetation.Length > 0) && (succession.vegetation[0].tiles.Length > 0)) {
										cellData.SetVegetation(x, y, succession.vegetation[0].tiles[0]);
									}
									else {
										cellData.SetVegetation(x, y, successionData[0].vegetation[0].tiles[0]);
									}
								}
								count++;							
							}
							else {
								JXVegetation vegetation = succession.vegetation[vegetationIndex];
								int tileIndex = cellData.GetTileIndex(x, y);
								if ((tileIndex < 0) || (tileIndex >= vegetation.tiles.Length)) {
									string msg = cellData.GetCellName() + ": " + succession.name + " " + vegetation.name + " variant index ongeldig : " + tileIndex;
									if (!messages.Contains(msg)) messages.Add(msg);
									if (fix) {
										if (vegetation.tiles.Length > 0) {
											cellData.SetVegetation(x, y, vegetation.tiles[vegetation.tiles.Length - 1]);
										}
										else {
											cellData.SetVegetation(x, y, successionData[0].vegetation[0].tiles[0]);
										}
									}
									count++;
								}
							}
						}
						cellData.NormalizeSoilParams(x, y);
					}
				}
				yield return 0;
			}
		}
		string resultMsg = fix?("gerepareerd: " + count + " probleemtegels.\n"):("gevonden: " + count + " probleemtegels.\n");
		foreach (string msg in messages) {
			resultMsg += msg + "\n";
		}
		JConfig.message = resultMsg;
	}
	
	void WriteBitmap16(string path, string name, int width, int height, ushort[] data) {
			FileStream stream = new FileStream (path + Path.DirectorySeparatorChar + name + ".dat", FileMode.Create);
			try {
				BinaryWriter writer = new BinaryWriter (stream);
				writer.Write((int) 0x48000001);
				writer.Write("BitMap16");
				writer.Write(width);
				writer.Write(height);
				for (int i = 0; i < data.Length; i++) {
					writer.Write(data[i]);
				}				
				writer.Close ();
			} finally {
				stream.Close ();
			}		
	}

	void WriteBitmap8(string path, string name, int width, int height, byte[] data) {
			FileStream stream = new FileStream (path + Path.DirectorySeparatorChar + name + ".dat", FileMode.Create);
			try {
				BinaryWriter writer = new BinaryWriter (stream);
				writer.Write((int) 0x48000001);
				writer.Write("BitMap8");
				writer.Write(width);
				writer.Write(height);
				for (int i = 0; i < data.Length; i++) {
					writer.Write(data[i]);
				}				
				writer.Close ();
			} finally {
				stream.Close ();
			}		
	}

	void WriteBitmap2(string path, string name, int width, int height, byte[] data) {
			FileStream stream = new FileStream (path + Path.DirectorySeparatorChar + name + ".dat", FileMode.Create);
			try {
				BinaryWriter writer = new BinaryWriter (stream);
				writer.Write((int) 0x48000001);
				writer.Write("BitMap2");
				writer.Write(width);
				writer.Write(height);
				for (int i = 0; i < data.Length; i+=4) {
					int v1 = Mathf.Min(3, data[i]);
					int v2 = Mathf.Min(3, data[i + 1]);
					int v3 = Mathf.Min(3, data[i + 2]);
					int v4 = Mathf.Min(3, data[i + 3]);
					int v = v1 | (v2 << 2) | (v3 << 4) | (v4 << 6);
					writer.Write((byte) v);
				}				
				writer.Close ();
			} finally {
				stream.Close ();
			}		
	}
	
	void ExportData(string path, EParamTypes paramT, int width, int height) {
		byte[] paramD = new byte[width * height];
		int p = 0;
		JTerrainData data = scenario.data;
		bool hasData = false;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				byte v = data.GetData(paramT, x, y);
				paramD[p] = v;
				if (v != 0) hasData = true;
				p++;
			}
		}
		if (hasData) {
			WriteBitmap8(path, paramT.ToString().ToLower(), width, height, paramD);
		}
	}
	
	void ExportScene() {
		string path = JConfig.scenarioPath + "Export";
		Directory.CreateDirectory(path);
		JVegetationFile.Save(path, scenario, 2);
		// vegetation definitions is named vegetation.xml in Ecosim 2
		File.Move (path + System.IO.Path.DirectorySeparatorChar + "elements.xml",
		           path + System.IO.Path.DirectorySeparatorChar + "vegetation.xml");
		int height = scenario.data.height;
		int width = scenario.data.width;

		// Test
		/*JVegetationFile.Save(path, scenario, 2);
		File.Move (path + System.IO.Path.DirectorySeparatorChar + "elements.xml",
		           path + System.IO.Path.DirectorySeparatorChar + "vegetation_new.xml");*/

		// Create data maps for the terrain data
		ushort[] heightD = new ushort[width * height];
		ushort[] waterD = new ushort[width * height];
		ushort[] vegD = new ushort[width * height];
		byte[] special = new byte[width * height];
		int p = 0;
		JTerrainData data = scenario.data;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				heightD[p] = (ushort) Mathf.Clamp((int) (data.GetHeight(x, y) * 100), 0, 65535);
				waterD[p] = (ushort) Mathf.Clamp((int) (data.GetWaterHeight(x, y) * 100), 0, 65535);
				int variant;
				JXVegetation veg = data.GetVegetation(x, y, out variant);
				int val = (veg.succession.index << 9) | (veg.index << 4)  | variant;
				vegD[p] = (ushort) val;
				special[p] = (byte) (data.GetSpecial(x, y) & 0x07);
				p++;
			}
		}
		WriteBitmap16(path, "heightmap", width, height, heightD);
		WriteBitmap16(path, "waterheightmap", width, height, waterD);
		WriteBitmap16(path, "vegetation", width, height, vegD);
		WriteBitmap2(path, "canals", width, height, special);

		foreach (EParamTypes pt in System.Enum.GetValues(typeof(EParamTypes))) {
			if (((int) pt >= 0) && ((int) pt < 80)) {
				ExportData(path, pt, width, height);
			}
		}

		// Export the buildings
		XmlTextWriter writer = new XmlTextWriter(path + Path.DirectorySeparatorChar + "buildings.xml", System.Text.Encoding.UTF8);
		writer.WriteStartDocument();
		writer.WriteStartElement("buildings");
		int id = 0;
		foreach (JCellData cell in scenario.data.cells) {
			cell.ExportBuildings(writer, ref id);
		}
		writer.WriteEndElement();
		writer.WriteEndDocument();
		writer.Close();

		// Export the roads
		File.Copy (JConfig.scenarioPath + info.scenarioName + Path.AltDirectorySeparatorChar + "roads.dat", path + Path.DirectorySeparatorChar + "roads.dat");

		// Export actions
		ExportActions(path);
		// Export species rules
		ExportSpeciesInit(path);
		// Create initialprogress.xml
		ExportInitialProgress(path);
	}
	void ExportActions(string path)
	{
		// Export the actions
		XmlTextWriter actionsWriter = new XmlTextWriter(path + Path.AltDirectorySeparatorChar + "actions.xml", System.Text.Encoding.UTF8);
		actionsWriter.WriteStartDocument();
		actionsWriter.WriteStartElement("actions");
		
		// Get all used actions in the game
		EActionTypes[] allActionTypes = (EActionTypes[]) System.Enum.GetValues(typeof(EActionTypes));
		int currActionId = 1;

		// Export first succession action
		actionsWriter.WriteStartElement("succession");
		actionsWriter.WriteAttributeString("id", currActionId.ToString());
		actionsWriter.WriteAttributeString("skipnormalsuccession", (!scenario.normalSuccessionActive).ToString().ToLower());
		actionsWriter.WriteEndElement(); // ~succession
		currActionId++;

		// Loop through all actions, we do it this way so that we add the actions in the correct order
		for (int i = 0; i < allActionTypes.Length; i++) {
			if (allActionTypes[i].ToString().StartsWith("Groep_")) continue;
			if (allActionTypes[i] == EActionTypes.UNDEFINED || allActionTypes[i] == EActionTypes.GeenActie) continue;

			bool enabledAction = false;
			foreach (KeyValuePair<EActionTypes, JScenario.ActionInfo> ai in scenario.actions) {
				if (ai.Key == allActionTypes[i]) {
					// New area action
					actionsWriter.WriteStartElement("area");
					
					actionsWriter.WriteAttributeString("id", currActionId.ToString());
					actionsWriter.WriteAttributeString("description", ai.Value.name);
					actionsWriter.WriteAttributeString("areaname", allActionTypes[i].ToString().ToLower());
					actionsWriter.WriteAttributeString("gridicon", "1");
					actionsWriter.WriteAttributeString("invalidicon", "0");
					
					actionsWriter.WriteStartElement("icon");
					actionsWriter.WriteAttributeString("name", ai.Value.name);
					actionsWriter.WriteAttributeString("description", ai.Value.name);
					actionsWriter.WriteAttributeString("help", ai.Value.descr);
					actionsWriter.WriteAttributeString("cost", ai.Value.costPerTile.ToString());
					actionsWriter.WriteAttributeString("icon", "1");
					actionsWriter.WriteEndElement(); // ~icon
					
					actionsWriter.WriteEndElement(); // ~area
					currActionId++;
					
					enabledAction = true;
					break;
				}
			}
			if (!enabledAction) {
				// We need to add the action also
				actionsWriter.WriteStartElement("area");
				
				actionsWriter.WriteAttributeString("id", currActionId.ToString());
				actionsWriter.WriteAttributeString("description", allActionTypes[i].ToString());
				actionsWriter.WriteAttributeString("areaname", allActionTypes[i].ToString().ToLower());
				actionsWriter.WriteAttributeString("gridicon", "1");
				actionsWriter.WriteAttributeString("invalidicon", "0");
				
				actionsWriter.WriteStartElement("icon");
				actionsWriter.WriteAttributeString("name", allActionTypes[i].ToString());
				actionsWriter.WriteAttributeString("description", "Description");
				actionsWriter.WriteAttributeString("help", "Help");
				actionsWriter.WriteAttributeString("cost", "100");
				actionsWriter.WriteAttributeString("icon", "1");
				actionsWriter.WriteEndElement(); // ~icon
				
				actionsWriter.WriteEndElement(); // ~area
				currActionId++;
			}
		}
		
		// Get all research actions
		foreach (EResearchTypes rt in EnumExtensions.researchTypeArray) {
			JScenario.ResearchInfo ri = scenario.GetResearchInfo (rt);
			if (ri != null) {
				// New inventarisation action
				actionsWriter.WriteStartElement("inventarisation");
				
				actionsWriter.WriteAttributeString("id", currActionId.ToString());
				actionsWriter.WriteAttributeString("description", ri.name);
				actionsWriter.WriteAttributeString("areaname", rt.ToString().ToLower());
				actionsWriter.WriteAttributeString("gridicon", "1");
				actionsWriter.WriteAttributeString("invalidicon", "0");
				
				actionsWriter.WriteStartElement("icon");
				actionsWriter.WriteAttributeString("name", ri.name);
				actionsWriter.WriteAttributeString("description", ri.name);
				actionsWriter.WriteAttributeString("help", ri.descr);
				actionsWriter.WriteAttributeString("cost", ri.costPerTile.ToString());
				actionsWriter.WriteAttributeString("icon", "0");
				actionsWriter.WriteEndElement(); // ~icon
				
				// Value types
				string[] resultNames = new string[] { "Absent","Rare","Frequent","Common" };
				for (int i = 0; i < resultNames.Length; i++) {
					actionsWriter.WriteStartElement("valuetype");
					actionsWriter.WriteAttributeString("index", i.ToString());
					actionsWriter.WriteAttributeString("name", resultNames[i]);
					actionsWriter.WriteAttributeString("icon", "0");
					actionsWriter.WriteEndElement(); // ~valuetype
				}
				
				actionsWriter.WriteEndElement(); // ~inventarisation
				currActionId++;
			}
		}
		
		// Create Measures category
		actionsWriter.WriteStartElement ("uicat");
		actionsWriter.WriteAttributeString("category", "measures");
		
		foreach (EActionGroups eg in EnumExtensions.actionGroupArray) {
			bool createdGroup = false;
			foreach (EActionTypes ea in EnumExtensions.GetActionTypesForGroup(eg)) {
				JScenario.ActionInfo ai = scenario.GetActionInfo(ea);
				if (ai != null) {
					// Add group
					if (!createdGroup) {
						createdGroup = true;
						actionsWriter.WriteStartElement ("group");
						actionsWriter.WriteAttributeString("icon", "0");
					}
					// Add measure
					actionsWriter.WriteStartElement ("ui");
					actionsWriter.WriteAttributeString("name", ai.name);
					actionsWriter.WriteEndElement(); // ~ui
				}
			}
			if (createdGroup) actionsWriter.WriteEndElement(); // ~group
		}
		
		actionsWriter.WriteEndElement(); // ~uicat measures
		
		// Create Research category
		actionsWriter.WriteStartElement ("uicat");
		actionsWriter.WriteAttributeString("category", "research");
		
		// We have no 'start-group' so we should always start with a group
		actionsWriter.WriteStartElement ("group");
		actionsWriter.WriteAttributeString("icon", "0");
		foreach (EResearchTypes rt in EnumExtensions.researchTypeArray) {
			if (rt == EResearchTypes.GeenResearch || rt == EResearchTypes.UNDEFINED) continue;
			// Check if we're in a group
			if (rt.ToString().StartsWith("Groep_")) {
				// End the previous group and start a new one
				actionsWriter.WriteEndElement(); // ~group
				actionsWriter.WriteStartElement ("group");
				actionsWriter.WriteAttributeString("icon", "0");
			} else {
				// Add to group if it exists
				JScenario.ResearchInfo ri = scenario.GetResearchInfo (rt);
				if (ri != null) {
					// Add research
					actionsWriter.WriteStartElement ("ui");
					actionsWriter.WriteAttributeString("name", ri.name);
					actionsWriter.WriteEndElement(); // ~ui
				}
			}
		}
		// End the last group
		actionsWriter.WriteEndElement(); // ~group
		actionsWriter.WriteEndElement(); // ~uicat research
		
		actionsWriter.WriteEndElement(); // ~actions
		actionsWriter.WriteEndDocument();
		actionsWriter.Close();
	}
	void ExportSpeciesInit(string path) // TODO: WIP
	{
		string ruleInit = @"r = new Rule();
	r.chance = %chance%;
	r.delta = %delta%;
	r.canSpawn = %canSpawn%;
	r.vegetations = new List<VegetationType>();
	r.conditions = new List<ParameterCondition>();";

		string vegetationInit = @"vt = this.GetVegetationType(%successionIndex%, %vegetationIndex%);";

		string paramConditionInit = @"pc = new ParameterCondition();
	pc.type = %type%;
	pc.min = %min%f;
	pc.max = %max%f;";	

		path = path + Path.DirectorySeparatorChar + "SpeciesRules";
		Directory.CreateDirectory(path);
		foreach (JSpecies species in scenario.species) {
			using (TextWriter w = File.CreateText (string.Format("{0}{1}{2}.txt", path, Path.DirectorySeparatorChar, species.name))) {
				w.WriteLine("void Init() {");
				w.WriteLine("\t// Init the local data");
				w.WriteLine("\tthis.spawnRadius = {0};", species.spawnRadius);
				w.WriteLine("\tthis.spawnCount = {0};", species.spawnCount);
				w.WriteLine("\tthis.spawnMultiplier = {0};\n", species.spawnMultiplier);
				w.WriteLine("\t// Rules for '{0}'", species.name);
				w.WriteLine("\tRule r = null;");
				w.WriteLine("\tVegetationType vt = null;");
				w.WriteLine("\tParameterCondition pc = null; \n");
				// Create the rules
				int ruleCount = 0;
				w.WriteLine("\t/** Add the rules **/");
				foreach (JSpecies.Rule rule in species.rules) {
					// Init the rule
					w.WriteLine("\t// Rule #{0}/{1}", ++ruleCount, species.rules.Length);
					string ruleInitStr = ruleInit;
					ruleInitStr = ruleInitStr.Replace ("%chance%", rule.chance + "f");
					ruleInitStr = ruleInitStr.Replace ("%delta%", "" + rule.delta);
					ruleInitStr = ruleInitStr.Replace ("%canSpawn%", rule.canSpawn.ToString().ToLower());
					w.WriteLine("\t" + ruleInitStr + "\n");
					if (rule.vegetations.Length > 0) {
						w.WriteLine("\t/** Add the vegetation rules **/");
						int vegCount = 0;
						foreach (JSpecies.Vegetation vegetation in rule.vegetations) {
							// Init vegetation
							w.WriteLine("\t// Vegetation #{0}/{1}", ++vegCount, rule.vegetations.Length);
							string vegetationInitStr = vegetationInit;
							vegetationInitStr = vegetationInitStr.Replace("%successionIndex%", "" + vegetation.successionIndex );
							vegetationInitStr = vegetationInitStr.Replace("%vegetationIndex%", "" + vegetation.vegetationIndex );
							w.WriteLine("\t" + vegetationInitStr);
							// Log error if the type was not found
							string successionName = "";
							string vegetationName = "";
							foreach (JXSuccession s in scenario.successions) {
								if (s.index == vegetation.successionIndex) {
									successionName = s.name;
									foreach (JXVegetation v in s.vegetation) {
										if (v.index == vegetation.vegetationIndex) {
											vegetationName = v.name;
										}
									}
									break;
								}
							}
							w.WriteLine(string.Format("\tif (vt == null) LogError(\"VegetationType not found ({0}::{1})\");", successionName, vegetationName));
							w.WriteLine("\tr.vegetations.Add (vt);\n");
						}
					}
					if (rule.conditions.Length > 0) {
						w.WriteLine("\t/** Add the parameter conditions **/");
						int paramCount = 0;
						foreach (JSpecies.Parameter parameter in rule.conditions) {
							// Init parameter condition
							w.WriteLine("\t// Parameter #{0}/{1}", ++paramCount, rule.conditions.Length);
							string paramConditionInitStr = paramConditionInit;
							paramConditionInitStr = paramConditionInitStr.Replace ("%type%", "\"" + parameter.type.ToString() + "\"");
							paramConditionInitStr = paramConditionInitStr.Replace ("%min%", parameter.min.ToString());
							paramConditionInitStr = paramConditionInitStr.Replace ("%max%", parameter.max.ToString());
							w.WriteLine("\t" + paramConditionInitStr);
							w.WriteLine("\tr.conditions.Add (pc);\n");
						}
					}
					w.WriteLine("\tthis.rules.Add (r);\n");
				}
				w.WriteLine("}");
			}
		}
	}
	void ExportInitialProgress(string path)
	{
		XmlTextWriter writer = new XmlTextWriter(path + Path.AltDirectorySeparatorChar + "initialprogress.xml", System.Text.Encoding.UTF8);
		writer.WriteStartDocument();
		writer.WriteStartElement("progress");

		writer.WriteAttributeString("budget", scenario.startBudget.ToString());
		writer.WriteAttributeString("startyear", scenario.startYear.ToString());
		writer.WriteAttributeString("allowresearch", "true");
		writer.WriteAttributeString("allowmeasures", "true");
		writer.WriteAttributeString("gameended", "false");
		writer.WriteAttributeString("messageindex", "0");

		// Add all data
		string[] extraData = new string[]{ "heightmap", "waterheightmap", "vegetation", "canals" };
		for (int i = 0; i < extraData.Length; i++) {
			// Add element
			writer.WriteStartElement("data");
			writer.WriteAttributeString("name", extraData[i]);
			writer.WriteAttributeString("year", "0");
			writer.WriteEndElement(); // ~data
		}

		int width = scenario.data.width;
		int height = scenario.data.height;
		foreach (EParamTypes pt in System.Enum.GetValues(typeof(EParamTypes))) {
			if (((int) pt >= 0) && ((int) pt < 80)) {
				byte[] paramD = new byte[width * height];
				int p = 0;
				JTerrainData data = scenario.data;
				bool hasData = false;
				for (int y = 0; y < height; y++) {
					for (int x = 0; x < width; x++) {
						byte v = data.GetData(pt, x, y);
						paramD[p] = v;
						if (v != 0) hasData = true;
						p++;
					}
				}
				if (hasData) {
					// Add element
					writer.WriteStartElement("data");
					writer.WriteAttributeString("name", pt.ToString());
					writer.WriteAttributeString("year", "0");
					writer.WriteEndElement(); // ~data
				}
			}
		}

		// Add all variables

		// Add all actions
		EActionTypes[] allActionTypes = (EActionTypes[]) System.Enum.GetValues(typeof(EActionTypes));
		int currActionId = 1;
		
		// Loop through all actions, we do it this way so that we add the actions in the correct order
		for (int i = 0; i < allActionTypes.Length; i++) {
			if (allActionTypes[i].ToString().StartsWith("Groep_")) continue;
			if (allActionTypes[i] == EActionTypes.UNDEFINED || allActionTypes[i] == EActionTypes.GeenActie) continue;

			writer.WriteStartElement("action");
			writer.WriteAttributeString("id", currActionId.ToString());
			// Check if it's in the actions array, if so, it's "enabled"
			bool actionEnabled = false;
			foreach (KeyValuePair<EActionTypes, JScenario.ActionInfo> ai in scenario.actions) {
				if (ai.Key == allActionTypes[i]) {
					// Got an enabled action
					actionEnabled = true;
					break;
				}
			}
			writer.WriteAttributeString("enabled", actionEnabled.ToString().ToLower());
			writer.WriteEndElement(); //~action
			currActionId++;
		}
		
		// Get all research actions
		foreach (EResearchTypes rt in EnumExtensions.researchTypeArray) {
			JScenario.ResearchInfo ri = scenario.GetResearchInfo (rt);
			if (ri != null) {
				writer.WriteStartElement("action");
				writer.WriteAttributeString("id", currActionId.ToString());
				writer.WriteAttributeString("enabled", "true");
				writer.WriteEndElement();
				currActionId++;
			}
		}

		writer.WriteEndElement(); // ~progress
		writer.WriteEndDocument();
		writer.Close();
	}
	
	void HandleLoadSave() {
		GUILayout.BeginVertical();
		if (info.isBusy) {
			GUILayout.BeginHorizontal(); // new
			GUILayout.Label("EVEN GEDULD!", GUILayout.Width(100));			
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal(); // ~new
		}
		else if (info.makeSmallScenario) {
			GUILayout.BeginHorizontal(); // small
			GUILayout.Label("MAAK KLEIN SCENARIO");			
			GUILayout.EndHorizontal(); // ~small
			GUILayout.BeginHorizontal(); // small
			GUILayout.Label("Nieuwe scenario naam:");
			info.scenarioName = GUILayout.TextField(info.scenarioName, GUILayout.Width(200));
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal(); // ~small
			GUILayout.BeginHorizontal(); // small
			GUILayout.Label("start cell : ");
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				info.msStartCellX = Mathf.Max(0, info.msStartCellX - 1);
			}
			GUILayout.Label(info.msStartCellX.ToString(), GUILayout.Width(40));
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				info.msStartCellX = Mathf.Min(scenario.data.cWidth - info.msCellCountX, info.msStartCellX + 1);
			}
			GUILayout.Label("/");
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				info.msStartCellY = Mathf.Max(0, info.msStartCellY - 1);
			}
			GUILayout.Label(info.msStartCellY.ToString(), GUILayout.Width(40));
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				info.msStartCellY = Mathf.Min(scenario.data.cHeight - info.msCellCountY, info.msStartCellY + 1);
			}
			
			GUILayout.Label(" grootte in cellen : ");

			if (GUILayout.Button("<", GUILayout.Width(20))) {
				info.msCellCountX = Mathf.Max(4, info.msStartCellX - 4);
			}
			GUILayout.Label(info.msCellCountX.ToString(), GUILayout.Width(40));
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				info.msCellCountX = Mathf.Min(scenario.data.cWidth, info.msCellCountX + 4);
				info.msStartCellX = Mathf.Min(scenario.data.cWidth - info.msCellCountX, info.msStartCellX);
			}
			GUILayout.Label("/");
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				info.msCellCountY = Mathf.Max(4, info.msStartCellY - 4);
			}
			GUILayout.Label(info.msCellCountY.ToString(), GUILayout.Width(40));
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				info.msCellCountY = Mathf.Min(scenario.data.cHeight, info.msCellCountY + 4);
				info.msStartCellY = Mathf.Min(scenario.data.cHeight - info.msCellCountY, info.msStartCellY);
			}
			GUILayout.Label(" (" + (info.msCellCountX * JTerrainData.CELL_SIZE) + "x" + (info.msCellCountY * JTerrainData.CELL_SIZE) + " tegels)");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal(); // ~small
			GUILayout.BeginHorizontal(); // small
			if (info.scenarioName != scenario.name) {
				if (GUILayout.Button("Creeer", GUILayout.Width(100))) {
					StartCoroutine(COSaveResized(info.scenarioName));
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(100));
				lastError = "Scenarionaam moet anders zijn dan origineel";
			}
			if (GUILayout.Button("Annuleer", GUILayout.Width(100))) {
				info.makeSmallScenario = false;
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal(); // ~small
		}
		else if (info.startNew) {
			GUILayout.BeginHorizontal(); // new
			GUILayout.Label("NIEUW", GUILayout.Width(100));			
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal(); // ~new
			GUILayout.BeginHorizontal(); // new
			GUILayout.Label("Breedte", GUILayout.Width(100));			
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				info.newWidth = Mathf.Max(256, info.newWidth - 256);
			}
			GUILayout.Label(info.newWidth.ToString(), GUILayout.Width(80));
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				info.newWidth = Mathf.Min(2048, info.newWidth + 256);
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal(); // ~new
			GUILayout.BeginHorizontal(); // new
			GUILayout.Label("Hoogte", GUILayout.Width(100));
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				info.newHeight = Mathf.Max(256, info.newHeight - 256);
			}
			GUILayout.Label(info.newHeight.ToString(), GUILayout.Width(80));
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				info.newHeight = Mathf.Min(2048, info.newHeight + 256);
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal(); // ~new
			GUILayout.BeginHorizontal(); // new
			GUILayout.Label("Vegetatie", GUILayout.Width(100));
			if (scenario != null) {
				info.newUseVeg = GUILayout.Toggle(info.newUseVeg, "Gebruik geladen vegetatie tegels");
			}
			else {
				GUILayout.Label("Om bestaande vegetatie te gebruiken, laad eerst scenario");
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal(); // ~new
			GUILayout.BeginHorizontal(); // new
			if (GUILayout.Button("Nieuw", GUILayout.Width(100))) {
				StartCoroutine(CONew());
			}
			if (GUILayout.Button("Annuleer", GUILayout.Width(100))) {
				info.startNew = false;
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal(); // ~new
			
		}
		else  {
			GUILayout.BeginHorizontal(); // 1st row
			
			info.scenarioName = GUILayout.TextField(info.scenarioName, GUILayout.Width(160));
			if (GUILayout.Button("Nieuw", GUILayout.Width(80))) {
				info.startNew = true;
			}
			if (GUILayout.Button("Laad", GUILayout.Width(80))) {
				StartCoroutine(COLoad(info.scenarioName));
			}
			if (scenario != null) {
				if (GUILayout.Button("Bewaar", GUILayout.Width(80))) {
					StartCoroutine(COSave(info.scenarioName));
				}
				if (GUILayout.Button("Maak klein scenario", GUILayout.Width(160))) {
					info.makeSmallScenario = true;
				}
				if (JConfig.SHOW_ECOSIM2_EXPORT) {
					if (GUILayout.Button("Export E2.0")) {
						ExportScene();
					}
				}
			}
			
			
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal(); // ~1st row

		}
		GUILayout.BeginHorizontal();
		if ((scenario != null) && !JDoSuccession.IsDoingSuccession && !info.makeSmallScenario){
			if (GUILayout.Button("Doe successie")) {
				JDoSuccession.StartSuccession(scenario);
			}
			if (GUILayout.Button("Controleer vegetatie")) {
				StartCoroutine(COControleerVegetatie(false));
			}
			if (GUILayout.Button("repareer vegetatie")) {
				StartCoroutine(COControleerVegetatie(true));
			}
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if ((scenario != null) && !JDoSuccession.IsDoingSuccession && !JRenderMap.IsGeneratingTiles){
			if (GUILayout.Button("Maak Map textures")) {
				JRenderMap.GenerateTiles();
			}
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Ga naar Spel modus")) {
			Application.LoadLevel("Game");
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label(JConfig.message);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
	}
	
	EParamTypes FindPreviousOrNext(EParamTypes p, int dir) {
		int index = 0;
		for (int i = 0; i < EnumExtensions.paramTypeArray.Length; i++) {
			if (p == EnumExtensions.paramTypeArray[i]) {
				index = i;
				break;
			}
		}
		index += dir;
		if (index < 0) index = EnumExtensions.paramTypeArray.Length - 1;
		if (index >= EnumExtensions.paramTypeArray.Length) index = 0;
		return EnumExtensions.paramTypeArray[index];
	}
	
	void HandleBudget() {
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Beknopte Omschrijving:", GUILayout.Width(100));
		scenario.shortDesc = GUILayout.TextField(scenario.shortDesc);
		// GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Omschrijving:", GUILayout.Width(100));
		scenario.description = GUILayout.TextField(scenario.description);
		// GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Start jaar:", GUILayout.Width(100));
		JTextField.IntField("StartYear", ref scenario.startYear);
		// GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Budget:", GUILayout.Width(100));
		JTextField.LongField("StartBudget", ref scenario.startBudget);
		// GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Standaard successie:", GUILayout.Width(100));
		scenario.normalSuccessionActive = GUILayout.Toggle(scenario.normalSuccessionActive, "");
		GUILayout.Label("(uit: tegels maken geen successie zonder maatregelen op tegel)");
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
//		GUILayout.BeginHorizontal();
//		GUILayout.Label("Tijd in jaren:", GUILayout.Width(100));
//		JTextField.IntField("GameDuration", ref scenario.duration);
//		// GUILayout.FlexibleSpace();
//		GUILayout.EndHorizontal();
		
		foreach (EParamTypes p in EnumExtensions.paramTypeArray) {
			if (((int) p >= 80) && ((int) p < 96)) {
				GUILayout.BeginHorizontal();
				GUILayout.Label(p.ToString(), GUILayout.Width(100));
				JScenario.CombinedData cd = null;
				int offset = 0;
				if (scenario.combinedData.ContainsKey(p)) {
					cd = scenario.combinedData[p];
					offset = cd.offset;
				}
				else {
					cd = new JScenario.CombinedData();
					cd.offset = 0;
					cd.parameters = new EParamTypes[0];
					cd.multipliers = new float[0];
					scenario.combinedData.Add(p, cd);
				}
				string newOffsetStr = GUILayout.TextField(offset.ToString(), GUILayout.Width(50));
				int newOffset = offset;
				int.TryParse(newOffsetStr, out newOffset);
				cd.offset = newOffset;
				int len = cd.parameters.Length;
				for (int i = 0; i < len; i++) {
					GUILayout.Label(" + ");
					string newMultiplierStr = GUILayout.TextField(cd.multipliers[i].ToString("0.00"), GUILayout.Width(50));
					float newMultiplier = cd.multipliers[i];
					float.TryParse(newMultiplierStr, out newMultiplier);
					cd.multipliers[i] = newMultiplier;
					if (GUILayout.Button("<")) {
						cd.parameters[i] = FindPreviousOrNext(cd.parameters[i], -1);
					}
					GUILayout.Label(cd.parameters[i].ToString());
					if (GUILayout.Button(">")) {
						cd.parameters[i] = FindPreviousOrNext(cd.parameters[i], +1);
					}
					if (GUILayout.Button("-")) {
						List<EParamTypes> pList = new List<EParamTypes>(cd.parameters);
						pList.RemoveAt(i);
						cd.parameters = pList.ToArray();
						List<float> fList = new List<float>(cd.multipliers);
						fList.RemoveAt(i);
						cd.multipliers = fList.ToArray();
						return;
					}
				}
				if (GUILayout.Button("+")) {
					List<EParamTypes> pList = new List<EParamTypes>(cd.parameters);
					pList.Add(EParamTypes.PH);
					cd.parameters = pList.ToArray();
					List<float> fList = new List<float>(cd.multipliers);
					fList.Add(1.0f);
					cd.multipliers = fList.ToArray();
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
			}
		}
		
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
	}
	
	void HandleActions() {
		GUILayout.BeginVertical();
		
		foreach (EActionGroups eg in EnumExtensions.actionGroupArray) {
			if (eg != EActionGroups.UNDEFINED) {
				GUILayout.BeginHorizontal();
				GUILayout.Label("Groep " + eg.ToString().Substring(6));
				GUILayout.EndHorizontal();
				foreach (EActionTypes ea in EnumExtensions.GetActionTypesForGroup(eg)) {
					GUILayout.BeginHorizontal();
					JScenario.ActionInfo ai = scenario.GetActionInfo(ea);
					bool check = GUILayout.Toggle((ai != null), "", GUILayout.Width(20));
					GUILayout.Label(ea.ToString(), GUILayout.Width(160));
					if (ai != null) {
						ai.name = GUILayout.TextField(ai.name, GUILayout.Width(150));
						ai.descr = GUILayout.TextField(ai.descr, GUILayout.Width(240));
						JTextField.IntField(ai, ref ai.costPerTile, GUILayout.Width(50));
						if (eg == EActionGroups.Groep_Objecten) {
							if (GUILayout.Button("Edit", GUILayout.Width(30))) {
								JActionObjectEditor.Create(transform.parent, ea);
							}
						}
						if (GUILayout.Button("<size=8>Alle veg.</size>", GUILayout.Width(50))) {
							foreach (JXSuccession suc in scenario.successions) {
								foreach (JXVegetation veg in suc.vegetation) {
									bool isAlreadyAcceptable = false;
									foreach (EActionTypes at in veg.acceptableMeasures) {
										if (at == ea) isAlreadyAcceptable = true;
									}
									if (!isAlreadyAcceptable) {
										List<EActionTypes> list = new List<EActionTypes>(veg.acceptableMeasures);
										list.Add(ea);
										veg.acceptableMeasures = list.ToArray();
									}
								}
							}
						}
					}
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
					if (check != (ai != null)) {
						if (check) {
							ai = new JScenario.ActionInfo();
							ai.action = ea;
							ai.name = ea.ToString();
							ai.descr = "Uitleg";
							ai.costPerTile = 100;
							scenario.AddActionInfo(ai);
						}
						else {
							scenario.RemoveActionInfo(ea);
						}
					}
				}
			}
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
	}

	void HandleResearch() {
		GUILayout.BeginVertical();
		foreach (EResearchTypes ra in System.Enum.GetValues(typeof(EResearchTypes))) {
			if ((ra != EResearchTypes.GeenResearch) && (ra != EResearchTypes.UNDEFINED) && (((int) ra & 0x0f) != 0x00)) {
				GUILayout.BeginHorizontal();
				JScenario.ResearchInfo ri = scenario.GetResearchInfo(ra);
				bool check = GUILayout.Toggle((ri != null), "", GUILayout.Width(20));
				GUILayout.Label(ra.ToString());
				if (ri != null) {
					ri.name = GUILayout.TextField(ri.name, GUILayout.Width(150));
					ri.descr = GUILayout.TextField(ri.descr, GUILayout.Width(280));
					JTextField.IntField(ri, ref ri.costPerTile, GUILayout.Width(50));
				}
				// GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
				if (check != (ri != null)) {
					if (check) {
						ri = new JScenario.ResearchInfo();
						ri.research = ra;
						ri.name = ra.ToString();
						ri.descr = "Uitleg";
						ri.costPerTile = 100;
						scenario.AddResearchInfo(ri);
					}
					else {
						scenario.RemoveResearchInfo(ra);
					}
				}
			}
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
	}
	
	class SpeciesStrings {
		public SpeciesStrings(JSpecies s) {
			spawnCountStr = s.spawnCount.ToString();
			spawnMultiplierStr = s.spawnMultiplier.ToString();
			spawnRadiusStr = s.spawnRadius.ToString();
		}
		public string spawnCountStr;
		public string spawnRadiusStr;
		public string spawnMultiplierStr;
	}
	
	class AnimSpeciesStrings {
		public AnimSpeciesStrings(JAnimalSpecies s) {
			walkDistFStr = s.femaleWalkDistance.ToString();
			walkDistMStr = s.maleWalkDistance.ToString();
			wanderFStr = s.femaleWandering.ToString();
			wanderMStr = s.maleWandering.ToString();
			offspringStr = s.offspringPerPair.ToString();
			deathStr = s.naturalDeath.ToString();
			maxNestStr = s.growthCapPerNest.ToString();
			popDividerStr = s.populationDivider.ToString();
		}
		
		public string walkDistFStr;
		public string walkDistMStr;
		public string wanderFStr;
		public string wanderMStr;
		public string offspringStr;
		public string deathStr;
		public string maxNestStr;
		public string popDividerStr;
	}
	
	Dictionary<JSpecies, SpeciesStrings> expandedSpecies = new Dictionary<JSpecies, SpeciesStrings>();
	Dictionary<JAnimalSpecies, AnimSpeciesStrings> expandedAnimSpecies = new Dictionary<JAnimalSpecies, AnimSpeciesStrings>();
	public string nestFStr;
	public string nestMStr;
	
	JAnimalSpecies activeSpecies = null;
	int activeSpeciesNestIndex = -1;
	
	void HandleSpecies() {
		GUISkin skin = JWinMgr.self.skin;
		GUIStyle normal = skin.box;
		
		GUILayout.Label("Planten");
		for (int i = 0; i < scenario.species.Length; i++) {
			GUILayout.BeginVertical(skin.box);
			GUILayout.BeginHorizontal();
			JSpecies species = scenario.species[i];
			bool isClosed = !expandedSpecies.ContainsKey(species);
			if (GUILayout.Button(isClosed?editor.expandTex:editor.collapseTex, normal, GUILayout.Width(20), GUILayout.Height(20))) {
				isClosed = !isClosed;
				if (isClosed) {
					expandedSpecies.Remove(species);
				}
				else {
					expandedSpecies.Add(species, new SpeciesStrings(species));
				}
			}
			species.name = GUILayout.TextField(species.name);
			GUILayout.Label("Soort #" + (i+1), GUILayout.Width(100));
			GUILayout.EndHorizontal();
			if (!isClosed) {
				SpeciesStrings sstr = expandedSpecies[species];
				GUILayout.BeginHorizontal();
				GUILayout.Label("# zaai pogingen");
				sstr.spawnCountStr = GUILayout.TextField(sstr.spawnCountStr, GUILayout.Width(40));
				GUILayout.Label("multiplier");
				sstr.spawnMultiplierStr = GUILayout.TextField(sstr.spawnMultiplierStr, GUILayout.Width(40));
				GUILayout.Label("dispersie");
				sstr.spawnRadiusStr = GUILayout.TextField(sstr.spawnRadiusStr, GUILayout.Width(40));
				GUILayout.FlexibleSpace();
				int.TryParse(sstr.spawnCountStr, out species.spawnCount);
				int.TryParse(sstr.spawnMultiplierStr, out species.spawnMultiplier);
				int.TryParse(sstr.spawnRadiusStr, out species.spawnRadius);
				
				GUILayout.EndHorizontal();
				for (int j = 0; j < species.rules.Length; j++) {
					JSpecies.Rule rule = species.rules[j];
					GUILayout.BeginVertical(skin.box);
					GUILayout.BeginHorizontal();
					if (GUILayout.Button("Verwijder regel")) {
						List<JSpecies.Rule> newList = new List<JSpecies.Rule>(species.rules);
						newList.RemoveAt(j);
						species.rules = newList.ToArray();
						break;
					}
					GUILayout.Label("Verandering", GUILayout.Width(60));
					rule.delta = Mathf.RoundToInt(GUILayout.HorizontalSlider(rule.delta, -3f, 3f, GUILayout.Width(40)));
					GUILayout.Label(rule.delta.ToString());
					
					rule.canSpawn = GUILayout.Toggle(rule.canSpawn, "Kan seedlings krijgen");
//					string chanceStr = GUILayout.TextField(rule.chance.ToString(), GUILayout.Width(30));
//					float.TryParse(chanceStr, out rule.chance);
					GUILayout.Label("Kans", GUILayout.Width(60));
					rule.chance = GUILayout.HorizontalSlider(rule.chance, 0.0f, 1.0f, GUILayout.Width(40));
					GUILayout.Label(rule.chance.ToString("0.00"));
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
					for (int k = 0; k < rule.vegetations.Length; k++) {
						
						JSpecies.Vegetation veg = rule.vegetations[k];
						GUILayout.BeginHorizontal();
						if (GUILayout.Button("-", GUILayout.Width(20))) {
							List<JSpecies.Vegetation> vl = new List<JSpecies.Vegetation>(rule.vegetations);
							vl.RemoveAt(k);
							rule.vegetations = vl.ToArray();
							break;
						}
						int successionIndex = veg.successionIndex;
						int vegetationIndex = veg.vegetationIndex;
						if (successionIndex >= 0) {
							if (GUILayout.Button("<", GUILayout.Width(20))) {
								veg.successionIndex--;
								veg.vegetationIndex = -1;
							}
						}
						else GUILayout.Label("", GUILayout.Width(20));
						
						GUILayout.Label((successionIndex < 0)?"Alle successies":(scenario.successions[successionIndex].name), GUILayout.Width(120));
						if (successionIndex < scenario.successions.Length - 1) {
							if (GUILayout.Button(">", GUILayout.Width(20))) {
								veg.successionIndex++;
								veg.vegetationIndex = -1;
							}
						}
						else GUILayout.Label("", GUILayout.Width(20));
						
						if (successionIndex >= 0) {				
							if (vegetationIndex >= 0) {
								if (GUILayout.Button("<", GUILayout.Width(20))) {
									veg.vegetationIndex--;
								}
							}
							else GUILayout.Label("", GUILayout.Width(20));
							GUILayout.Label((vegetationIndex<0)?"Alle vegetaties":(scenario.successions[successionIndex].vegetation[vegetationIndex].name), GUILayout.Width(120));
							if (vegetationIndex < scenario.successions[successionIndex].vegetation.Length - 1) {
								if (GUILayout.Button(">", GUILayout.Width(20))) {
									veg.vegetationIndex++;
								}
							}
							else GUILayout.Label("", GUILayout.Width(20));
						}
						GUILayout.EndHorizontal();
					}
					GUILayout.BeginHorizontal();
					if (GUILayout.Button("+ vegetatie")) {
						int l = rule.vegetations.Length;
						JSpecies.Vegetation[] vegArray = new JSpecies.Vegetation[l + 1];
						System.Array.Copy(rule.vegetations, vegArray, l);
						vegArray[l] = new JSpecies.Vegetation(-1, -1);
						rule.vegetations = vegArray;
					}
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
					for (int k = 0; k < rule.conditions.Length; k++) {
						JSpecies.Parameter cnd = rule.conditions[k];
						GUILayout.BeginHorizontal();
						if (GUILayout.Button("-", GUILayout.Width(20))) {
							List<JSpecies.Parameter> pl = new List<JSpecies.Parameter>(rule.conditions);
							pl.RemoveAt(k);
							rule.conditions = pl.ToArray();
							break;
						}
						if (GUILayout.Button("<", GUILayout.Width(20))) {
							int index = EnumExtensions.IndexOfParamType(cnd.type) - 1;
							if (index < 0) index = EnumExtensions.paramTypeArray.Length - 1;
							cnd.type = EnumExtensions.paramTypeArray[index];
						}
						else GUILayout.Label("", GUILayout.Width(20));
						GUILayout.Label(cnd.type.ToString(), GUILayout.Width(120));
						if (GUILayout.Button(">", GUILayout.Width(20))) {
							int index = (EnumExtensions.IndexOfParamType(cnd.type) + 1) % EnumExtensions.paramTypeArray.Length;
							cnd.type = EnumExtensions.paramTypeArray[index];
						}
						else GUILayout.Label("", GUILayout.Width(20));
						GUILayout.FlexibleSpace();
						cnd.min = GUILayout.HorizontalSlider(cnd.min, 0.0f, 1.0f, GUILayout.Width(60));
						GUILayout.Label(cnd.min.ToString("0.00"), GUILayout.Width(20));
						cnd.max = GUILayout.HorizontalSlider(Mathf.Max(cnd.min, cnd.max), 0.0f, 1.0f, GUILayout.Width(60));
						GUILayout.Label(cnd.max.ToString("0.00"), GUILayout.Width(20));
						GUILayout.EndHorizontal();
					}
					GUILayout.BeginHorizontal();
					if (GUILayout.Button("+ conditie")) {
						int l = rule.conditions.Length;
						JSpecies.Parameter[] cndArray = new JSpecies.Parameter[l + 1];
						System.Array.Copy(rule.conditions, cndArray, l);
						cndArray[l] = new JSpecies.Parameter();
						rule.conditions = cndArray;
					}
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
					GUILayout.EndVertical();
				}
				GUILayout.BeginHorizontal();
				if (GUILayout.Button("+ regel")) {
					int l = species.rules.Length;
					JSpecies.Rule[] rulesArray = new JSpecies.Rule[l + 1];
					System.Array.Copy(species.rules, rulesArray, l);
					rulesArray[l] = new JSpecies.Rule();
					species.rules = rulesArray;
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
			}
			GUILayout.EndVertical();
		}
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("+", GUILayout.Width(20))) {
			JSpecies[] ns = new JSpecies[scenario.species.Length + 1];
			System.Array.Copy(scenario.species, ns, scenario.species.Length);
			JSpecies species = new JSpecies();
			species.name = "Nieuwe soort";
			species.index = scenario.species.Length;
			ns[scenario.species.Length] = species;
			species.rules = new  JSpecies.Rule[0];
			scenario.species = ns;
		}
		if ((scenario.species.Length > 0) && GUILayout.Button("-", GUILayout.Width(20))) {
			JSpecies[] ns = new JSpecies[scenario.species.Length - 1];
			System.Array.Copy(scenario.species, ns, scenario.species.Length - 1);
			scenario.species = ns;
		}
		GUILayout.EndHorizontal();
		GUILayout.Label("Dieren");
		for (int i = 0; i < scenario.animalSpecies.Length; i++) {
			GUILayout.BeginVertical(skin.box);
			GUILayout.BeginHorizontal();
			JAnimalSpecies species = scenario.animalSpecies[i];
			bool isClosed = !expandedAnimSpecies.ContainsKey(species);
			if (GUILayout.Button(isClosed?editor.expandTex:editor.collapseTex, normal, GUILayout.Width(20), GUILayout.Height(20))) {
				isClosed = !isClosed;
				if (isClosed) {
					expandedAnimSpecies.Remove(species);
					if (species == activeSpecies) HideNestMarkers();
				}
				else {
					expandedAnimSpecies.Add(species, new AnimSpeciesStrings(species));
				}
			}
			species.name = GUILayout.TextField(species.name);
			GUILayout.Label("Soort #" + (i+1), GUILayout.Width(100));
			GUILayout.EndHorizontal();
			if (!isClosed) {
				AnimSpeciesStrings strings = expandedAnimSpecies[species];
				GUILayout.BeginHorizontal();
				GUILayout.Label("Voedsel parameter", GUILayout.Width(100));
				int foodIndex = EnumExtensions.IndexOfParamType(species.food);
				if (GUILayout.Button("<", GUILayout.Width(20))) {
					foodIndex = (foodIndex + EnumExtensions.paramTypeArray.Length - 1) % EnumExtensions.paramTypeArray.Length;
					species.food = EnumExtensions.paramTypeArray[foodIndex];
				}
				GUILayout.Label(species.food.ToString(), GUILayout.Width(100));
				if (GUILayout.Button(">", GUILayout.Width(20))) {
					foodIndex = (foodIndex + 1) % EnumExtensions.paramTypeArray.Length;
					species.food = EnumExtensions.paramTypeArray[foodIndex];
				}
				GUILayout.Space(20);
				GUILayout.Label("Voedsel overschrijf parameter", GUILayout.Width(160));
				int foodOIndex = EnumExtensions.IndexOfParamType(species.foodOverrule);
				if (GUILayout.Button("<", GUILayout.Width(20))) {
					foodOIndex = (foodOIndex + EnumExtensions.paramTypeArray.Length - 1) % EnumExtensions.paramTypeArray.Length;
					species.foodOverrule = EnumExtensions.paramTypeArray[foodOIndex];
				}
				GUILayout.Label(species.foodOverrule.ToString(), GUILayout.Width(100));
				if (GUILayout.Button(">", GUILayout.Width(20))) {
					foodOIndex = (foodOIndex + 1) % EnumExtensions.paramTypeArray.Length;
					species.foodOverrule = EnumExtensions.paramTypeArray[foodOIndex];
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
				GUILayout.Label("Gevaar parameter", GUILayout.Width(100));
				int dangerIndex = EnumExtensions.IndexOfParamType(species.danger);
				if (GUILayout.Button("<", GUILayout.Width(20))) {
					dangerIndex = (dangerIndex + EnumExtensions.paramTypeArray.Length - 1) % EnumExtensions.paramTypeArray.Length;
					species.danger = EnumExtensions.paramTypeArray[dangerIndex];
				}
				GUILayout.Label(species.danger.ToString(), GUILayout.Width(100));
				if (GUILayout.Button(">", GUILayout.Width(20))) {
					dangerIndex = (dangerIndex + 1) % EnumExtensions.paramTypeArray.Length;
					species.danger = EnumExtensions.paramTypeArray[dangerIndex];
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
				GUILayout.Label("Loop afstand", GUILayout.Width(100));
				strings.walkDistFStr = GUILayout.TextField(strings.walkDistFStr, GUILayout.Width(40));
				GUILayout.Label("V", GUILayout.Width(40));
				strings.walkDistMStr = GUILayout.TextField(strings.walkDistMStr, GUILayout.Width(40));
				GUILayout.Label("M");
				GUILayout.FlexibleSpace();
				int.TryParse(strings.walkDistFStr, out species.femaleWalkDistance);
				int.TryParse(strings.walkDistMStr, out species.maleWalkDistance);
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
				GUILayout.Label("Dwalen", GUILayout.Width(100));
				strings.wanderFStr = GUILayout.TextField(strings.wanderFStr, GUILayout.Width(40));
				GUILayout.Label("V", GUILayout.Width(40));
				strings.wanderMStr = GUILayout.TextField(strings.wanderMStr, GUILayout.Width(40));
				GUILayout.Label("M");
				GUILayout.FlexibleSpace();
				float.TryParse(strings.wanderFStr, out species.femaleWandering);
				float.TryParse(strings.wanderMStr, out species.maleWandering);
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
				if ((activeSpecies != species) && (GUILayout.Button("Edit nest lokaties"))) {
					CreateNestMarkers(species);
				}
				if ((species == activeSpecies) && (activeSpeciesNestIndex >= 0)) {
					GUILayout.Space(20);
					if (GUILayout.Button("<", GUILayout.Width(20))) {
						ReenableActiveNest();
						activeSpeciesNestIndex = (activeSpeciesNestIndex + nests.Count - 1) % nests.Count;
						SetupActiveNestStrings();
					}
					Coordinate pos = species.nests[activeSpeciesNestIndex].position;
					GUILayout.Label("nest #" + (activeSpeciesNestIndex + 1) + "[" + pos.x + "," + pos.y + "]");
					if (GUILayout.Button(">", GUILayout.Width(20))) { 
						ReenableActiveNest();						
						activeSpeciesNestIndex = (activeSpeciesNestIndex + 1) % nests.Count;
						SetupActiveNestStrings();
					}
					GUILayout.Space(20);
					GUILayout.Label("V");
					nestFStr = GUILayout.TextField(nestFStr, GUILayout.Width(40));
					GUILayout.Label("M");
					nestMStr = GUILayout.TextField(nestMStr, GUILayout.Width(40));
					int.TryParse(nestFStr, out species.nests[activeSpeciesNestIndex].females);
					int.TryParse(nestMStr, out species.nests[activeSpeciesNestIndex].males);
					if (GUILayout.Button("Verwijder")) {
						List<JAnimalSpecies.Nest> nests = new List<JAnimalSpecies.Nest>(species.nests);
						nests.RemoveAt(activeSpeciesNestIndex);
						species.nests = nests.ToArray();
						HideNestMarkers();
						CreateNestMarkers(species);
					}
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
			}
			GUILayout.EndVertical();
		}
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("+", GUILayout.Width(20))) {
			List<JAnimalSpecies> sList = new List<JAnimalSpecies>(scenario.animalSpecies);
			JAnimalSpecies aspec = new JAnimalSpecies();
			aspec.danger = EParamTypes.Extra2;
			aspec.food = EParamTypes.Extra1;
			aspec.name = "Dierensoort";
			aspec.nests = new JAnimalSpecies.Nest[0];
			aspec.index = sList.Count;
			sList.Add(aspec);
			scenario.animalSpecies = sList.ToArray();
			HideNestMarkers();
		}
		if ((scenario.animalSpecies.Length > 0) && GUILayout.Button("-", GUILayout.Width(20))) {
			List<JAnimalSpecies> sList = new List<JAnimalSpecies>(scenario.animalSpecies);
			sList.RemoveAt(sList.Count - 1);
			scenario.animalSpecies = sList.ToArray();
			HideNestMarkers();
		}
		GUILayout.EndHorizontal();
	}
	
	private string newVarName = "variable1";
	void HandleVariables() {
		GUILayout.BeginVertical();
		foreach (KeyValuePair<string, long> kvp in scenario.oldContext.vars) {
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("-", GUILayout.Width(20))) {
				scenario.oldContext.vars.Remove(kvp.Key);
				break;
			}
			GUILayout.Label(kvp.Key, GUILayout.Width(160));
			long val = kvp.Value;
			JTextField.LongField("Var" + kvp.Key, ref val, GUILayout.Width(50));
			if (val != kvp.Value) {
				scenario.oldContext.SetValue(kvp.Key, val);
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();	
		}
		GUILayout.BeginHorizontal();
		newVarName = GUILayout.TextField(newVarName, GUILayout.Width(160));
		if (GUILayout.Button("Voeg variable toe...")) {
			scenario.oldContext.vars.Add(newVarName, 0);
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
	}
	
	public string lastError = "";
	
	void HandleGoalAction(JScenarioGoal.Action action) {
		GUILayout.Label("artikel");
		JTextField.IntField(action, ref action.articleId, GUILayout.Width(30));
		GUILayout.Label("hint");
		string hintStr = (action.hintStr == null)?"":action.hintStr;
		hintStr = GUILayout.TextField(hintStr, GUILayout.Width(472));
		if (hintStr == "") action.hintStr = null;
		else action.hintStr = hintStr;
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Ken waarde toe:");
		string varName = (action.counterName==null)?"":action.counterName;
		varName = GUILayout.TextField(varName, GUILayout.Width(100)).Trim();
		if (varName != "") {
			action.counterName = varName;
			string expression = (action.expression == null)?"":(action.expression);
			GUILayout.Label(" = ");
			expression = GUILayout.TextField(expression, GUILayout.Width(430));
			if (GUILayout.Button("Test")) {
				try {
					JExpression.ClearCache();
					long val = JExpression.ParseExpression(expression, scenario.oldContext, true);
					lastError = "Expressie valide, test waarde " + val;
				}
				catch (System.Exception e) {
					lastError = e.Message;
					throw e;
				}				
			}
			action.expression = expression;
		}
		else {
			action.counterName = null;
			action.expression = null;
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		action.removeGoal = GUILayout.Toggle(action.removeGoal, "Verwijder regel");
		action.endGame = GUILayout.Toggle(action.endGame, "Beeindig spel");
	}
	
	List<JScenarioGoal> expandGoal = new List<JScenarioGoal>();
	void HandleProgress() {
		GUILayout.BeginVertical();
		int count = 0;
		foreach (JScenarioGoal goal in scenario.goals) {
			GUILayout.BeginVertical(box);
			GUILayout.BeginHorizontal();
			bool isClosed = !expandGoal.Contains(goal);
			if (GUILayout.Button(isClosed?editor.expandTex:editor.collapseTex, box, GUILayout.Width(20), GUILayout.Height(20))) {
				isClosed = !isClosed;
				if (isClosed) {
					expandGoal.Remove(goal);
				}
				else {
					expandGoal.Add(goal);
				}
			}
			if (GUILayout.Button("-", GUILayout.Width(20))) {
				scenario.goals.Remove(goal);
				break;
			}
			if (count > 0) {
				if (GUILayout.Button("^", GUILayout.Width(20))) {
					scenario.goals[count] = scenario.goals[count - 1];
					scenario.goals[count - 1] = goal;
					break;
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(20));
			}
			GUILayout.Label("#" + (++count), GUILayout.Width(60));
			string goalName = goal.name;
			if ((goalName == null) || (goalName == "")) goalName = "Regel " + count;
			goal.name = GUILayout.TextField(goalName, GUILayout.Width(491));
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
			if (!isClosed) {
				GUILayout.BeginHorizontal();
				bool beforeSuc = GUILayout.Toggle((goal.checkMask & JScenarioGoal.M_BEFORE_SUCCESSION) != 0, "Voor successie");
				bool afterSuc = GUILayout.Toggle((goal.checkMask & JScenarioGoal.M_AFTER_SUCCESSION) != 0, "Na successie");
				bool afterMeas = GUILayout.Toggle((goal.checkMask & JScenarioGoal.M_AFTER_MEASURE) != 0, "Na maatregel");
				bool afterRes = GUILayout.Toggle((goal.checkMask & JScenarioGoal.M_AFTER_RESEARCH) != 0, "Na onderzoek");
				bool afterEnc = GUILayout.Toggle((goal.checkMask & JScenarioGoal.M_AFTER_ENCYCLOPEDIA) != 0, "Na raadplegen");
				bool afterLoad = GUILayout.Toggle((goal.checkMask & JScenarioGoal.M_AFTER_LOAD) != 0, "Na laden");
				goal.checkMask = 0;
				if (beforeSuc) goal.checkMask |= JScenarioGoal.M_BEFORE_SUCCESSION;
				if (afterSuc) goal.checkMask |= JScenarioGoal.M_AFTER_SUCCESSION;
				if (afterMeas) goal.checkMask |= JScenarioGoal.M_AFTER_MEASURE;
				if (afterRes) goal.checkMask |= JScenarioGoal.M_AFTER_RESEARCH;
				if (afterEnc) goal.checkMask |= JScenarioGoal.M_AFTER_ENCYCLOPEDIA;
				if (afterLoad) goal.checkMask |= JScenarioGoal.M_AFTER_LOAD;
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
				for (int i = 0; i < goal.expressions.Length; i++) {
					GUILayout.BeginHorizontal();
					if (i > 0) {
						if (GUILayout.Button("-", GUILayout.Width(20))) {
							List<string> l = new List<string>(goal.expressions);
							l.RemoveAt(i);
							if (l.Count == 0) l.Add("");
							goal.expressions = l.ToArray();
							break;
						}
						if (GUILayout.Button("^", GUILayout.Width(20))) {
							string s = goal.expressions[i - 1];
							goal.expressions[i - 1] = goal.expressions[i];
							goal.expressions[i] = s;
							break;
						}
					}
					else {
						if (GUILayout.Button("+", GUILayout.Width(20))) {
							List<string> l = new List<string>(goal.expressions);
							l.Add("");
							goal.expressions = l.ToArray();
							break;
						}
						GUILayout.Label("", GUILayout.Width(20));
					}
					goal.expressions[i] = GUILayout.TextField(goal.expressions[i], GUILayout.Width(580));
					if (GUILayout.Button("Test")) {
						try {
							JExpression.ClearCache();
							long val = JExpression.ParseExpression(goal.expressions[i], scenario.oldContext, true);
							lastError = "Expressie valide, test waarde " + val;
						}
						catch (System.Exception e) {
							lastError = e.Message;
							throw e;
						}
					}
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
				}
				GUILayout.BeginHorizontal();
				GUILayout.Label("Succes:", GUILayout.Width(40));
				if (goal.success == null) {
					if(GUILayout.Button("Maak successactie")) {
						goal.success = new JScenarioGoal.Action();
					}
				}
				else {
					if (GUILayout.Button("-", GUILayout.Width(20))) {
						goal.success = null;
						break;
					}
					HandleGoalAction(goal.success);
				}
				
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
				GUILayout.Label("Faal:", GUILayout.Width(40));
				if (goal.failure == null) {
					if(GUILayout.Button("Maak faalactie")) {
						goal.failure = new JScenarioGoal.Action();
					}
				}
				else {
					if (GUILayout.Button("-", GUILayout.Width(20))) {
						goal.failure = null;
						break;
					}
					HandleGoalAction(goal.failure);
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
			}
			GUILayout.EndVertical();
		}
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Voeg regel toe...")) {
			JScenarioGoal goal = new JScenarioGoal();
			scenario.goals.Add(goal);
			expandGoal.Add(goal);
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label(lastError);
		GUILayout.EndVertical();
	}
		
	Dictionary<JGameRule,string> warnings = new Dictionary<JGameRule, string>();
	Dictionary<JGameRule,Vector2> expandGameRules = new Dictionary<JGameRule, Vector2>();
	string mainCompileWarning = null;
	
	void CompileScripts() {
		scenario.context.Reset();
		warnings.Clear();
		mainCompileWarning = scenario.CompileScript();
		foreach (JGameRule rule in scenario.gameRules) {
			string errstr = rule.CompileScript(scenario.context);
			if (errstr != null) {
				warnings.Add(rule, errstr);
			}
		}
	}
	
	
	bool isMainClosed = true;
	Vector2 mainScroll;
	string importExportName = "regels.txt";
	
	void ExportRules() {
		StreamWriter writer = new StreamWriter(JConfig.ecoDataPath + importExportName);
		writer.WriteLine("---- definitions");
		writer.WriteLine();
		writer.WriteLine(scenario.GetScript().Trim());
		writer.WriteLine();
		int count = 0;
		foreach (JGameRule rule in scenario.gameRules) {
			writer.WriteLine("---- rule " + (++count) + " mask " + rule.ruleMask + " : " + rule.ruleName);
			writer.WriteLine();
			writer.WriteLine(rule.GetScript().Trim());
			writer.WriteLine();
		}
		writer.Close();
	}
	
	void ImportRules() {
		StreamReader reader = new StreamReader(JConfig.ecoDataPath + importExportName);
		string currentScript = "";
		string ruleName = "";
		int mask = 0;
		int count = -1;
		scenario.gameRules.Clear();
		while (true) {
			string line = reader.ReadLine();
			if ((line == null) || (line.StartsWith("----"))) {
				if (count == 0) {
					scenario.mainScript = currentScript;
				}
				else if (count > 0) {
					JGameRule rule = new JGameRule(ruleName, mask, currentScript, false);
					scenario.gameRules.Add(rule);
				}
				if (line == null) break;
				currentScript = "";
				count = -1;
				line = line.Substring(4).Trim();
				if (line == "definitions") {
					count = 0;
				}
				else if (line.StartsWith("rule ")) {	
					int nameStart = line.IndexOf(':');
					if (nameStart <= 0) {
						Debug.LogError("bad line " + line);
						break;
					}
					ruleName = line.Substring(nameStart + 1).Trim();
					string[] args = line.Split(' ');
					if (args.Length < 4) {
						Debug.LogError("bad line " + line);
						break;
					}
					count = int.Parse(args[1]);
					mask = int.Parse(args[3]);
				}
				else {
					Debug.LogError("bad line " + line);
				}
			}
			else {
				currentScript += line + "\n";
			}
		}
		reader.Close();
		CompileScripts();
	}
	
	void HandleGameRules() {
		GUILayout.BeginVertical();
		int count = 0;
		
		GUILayout.BeginVertical(box);
		GUILayout.BeginHorizontal();
		if (GUILayout.Button(isMainClosed?editor.expandTex:editor.collapseTex, box, GUILayout.Width(20), GUILayout.Height(20))) {
			isMainClosed = !isMainClosed;
		}
		if (mainCompileWarning != null) GUILayout.Label(editor.warningTex);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		if (!isMainClosed) {
			mainScroll = GUILayout.BeginScrollView(mainScroll, GUILayout.Height(200));
			string newScript = GUILayout.TextArea(scenario.GetScript(), GUILayout.MinHeight(200), GUILayout.ExpandHeight(true));
			if (newScript != scenario.GetScript()) {
				scenario.SetScript(newScript);
				CompileScripts();
			}
			GUILayout.EndScrollView();
			if (mainCompileWarning != null) GUILayout.Label(mainCompileWarning);
		}
		GUILayout.EndVertical();
		
		
		foreach (JGameRule rule in scenario.gameRules) {
			GUILayout.BeginVertical(box);
			GUILayout.BeginHorizontal();
			bool isClosed = !expandGameRules.ContainsKey(rule);
			if (GUILayout.Button(isClosed?editor.expandTex:editor.collapseTex, box, GUILayout.Width(20), GUILayout.Height(20))) {
				isClosed = !isClosed;
				if (isClosed) {
					expandGameRules.Remove(rule);
				}
				else {
					expandGameRules.Add(rule, Vector2.zero);
				}
			}
			if (warnings.ContainsKey(rule)) GUILayout.Label(editor.warningTex);
			if (GUILayout.Button("-", GUILayout.Width(20))) {
				scenario.gameRules.Remove(rule);
				break;
			}
			if (count > 0) {
				if (GUILayout.Button("^", GUILayout.Width(20))) {
					scenario.gameRules[count] = scenario.gameRules[count - 1];
					scenario.gameRules[count - 1] = rule;
					break;
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(20));
			}
			GUILayout.Label("#" + (++count), GUILayout.Width(60));
			string ruleName = rule.ruleName;
			if ((ruleName == null) || (ruleName == "")) ruleName = "Regel " + count;
			rule.ruleName = GUILayout.TextField(ruleName, GUILayout.Width(491));
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
			if (!isClosed) {
				GUILayout.BeginHorizontal();
				bool beforeSuc = GUILayout.Toggle((rule.ruleMask & JGameRule.M_BEFORE_SUCCESSION) != 0, "Voor successie");
				bool afterSuc = GUILayout.Toggle((rule.ruleMask & JGameRule.M_AFTER_SUCCESSION) != 0, "Na successie");
				bool afterMeas = GUILayout.Toggle((rule.ruleMask & JGameRule.M_AFTER_MEASURE) != 0, "Na maatregel");
				bool afterRes = GUILayout.Toggle((rule.ruleMask & JGameRule.M_AFTER_RESEARCH) != 0, "Na onderzoek");
				bool afterEnc = GUILayout.Toggle((rule.ruleMask & JGameRule.M_AFTER_ENCYCLOPEDIA) != 0, "Na raadplegen");
				bool afterLoad = GUILayout.Toggle((rule.ruleMask & JGameRule.M_AFTER_LOAD) != 0, "Na laden");
				rule.ruleMask = 0;
				if (beforeSuc) rule.ruleMask |= JGameRule.M_BEFORE_SUCCESSION;
				if (afterSuc) rule.ruleMask |= JGameRule.M_AFTER_SUCCESSION;
				if (afterMeas) rule.ruleMask |= JGameRule.M_AFTER_MEASURE;
				if (afterRes) rule.ruleMask |= JGameRule.M_AFTER_RESEARCH;
				if (afterEnc) rule.ruleMask |= JGameRule.M_AFTER_ENCYCLOPEDIA;
				if (afterLoad) rule.ruleMask |= JGameRule.M_AFTER_LOAD;
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
				expandGameRules[rule] = GUILayout.BeginScrollView(expandGameRules[rule], GUILayout.Height(150));
				string newScript = GUILayout.TextArea(rule.GetScript(), GUILayout.MinHeight(150), GUILayout.ExpandHeight(true));
				if (newScript != rule.GetScript()) {
					rule.SetScript(newScript);
					CompileScripts();
				}
				GUILayout.EndScrollView();
				if (warnings.ContainsKey(rule)) GUILayout.Label(warnings[rule]);
			}
			GUILayout.EndVertical();
		}
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Voeg regel toe...")) {
			JGameRule rule = new JGameRule("Nieuwe regel", JGameRule.M_AFTER_SUCCESSION, "hint(\"boer\", \"Dag allemaal\", true);\nverwijderregel();", false); 
			scenario.gameRules.Add(rule);
			expandGameRules.Add(rule, Vector2.zero);
		}
		GUILayout.Space(10);
		importExportName = GUILayout.TextField(importExportName, GUILayout.Width(100));
		if (GUILayout.Button("Exporteer regels...")) {
			ExportRules();
		}
		if (GUILayout.Button("Importeer regels...")) {
			ImportRules();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label(lastError);
		GUILayout.EndVertical();
	}
	
	
	Vector2 scroll;
	
	class NestGO {
		public GameObject go;
		public int nestIndex;
	}
	
	List<NestGO> nests;
	
	void HideNestMarkers() {
		if (nests != null) {
			foreach (NestGO ngo in nests) Destroy(ngo.go);
			nests = null;
		}
		activeSpecies = null;
		activeSpeciesNestIndex = -1;
	}
	
	void CreateNestMarkers(JAnimalSpecies species) {
		HideNestMarkers();
		activeSpecies = species;
		nests = new List<NestGO>();
		int index = 0;
		foreach (JAnimalSpecies.Nest nest in species.nests) {
			Vector3 pos = new Vector3(nest.position.x * JTerrainData.HORIZONTAL_SCALE, scenario.data.GetHeight(nest.position.x, nest.position.y), nest.position.y * JTerrainData.HORIZONTAL_SCALE);
			GameObject go = (GameObject) Instantiate(nestMarkerPrefab, pos, Quaternion.identity);
			go.layer = JLayers.L_MAPSELECTION;
			go.transform.localScale = new Vector3(10, 10, 10);
			NestGO ngo = new NestGO();
			ngo.go = go;
			ngo.nestIndex = index++;
			nests.Add(ngo);
		}
		if (nests.Count > 0) {
			activeSpeciesNestIndex = 0;
			SetupActiveNestStrings();
		}
	}
	
	void ReenableActiveNest() {
		if ((activeSpecies != null) && (nests != null) && (activeSpeciesNestIndex >= 0)) {
			NestGO ngo = nests[activeSpeciesNestIndex];
			ngo.go.renderer.enabled = true;
		}
	}
	
	IEnumerator COBlinkCurrentNestGO() {
		while (true) {
			if ((activeSpecies != null) && (nests != null) && (activeSpeciesNestIndex >= 0)) {
				NestGO ngo = nests[activeSpeciesNestIndex];
				ngo.go.renderer.enabled = !ngo.go.renderer.enabled;
			}
			yield return new WaitForSeconds(0.1f);
		}
	}
	
	void SetupActiveNestStrings() {
		if (activeSpeciesNestIndex >= 0) {
			JAnimalSpecies.Nest nest = activeSpecies.nests[activeSpeciesNestIndex];
			nestFStr = nest.females.ToString();
			nestMStr = nest.males.ToString();
		}
	}
	
	void Update() {
		Vector3 mousePos = Input.mousePosition;
		if (JWinMgr.InUIRect(mousePos.x, Screen.height - mousePos.y) ||
			Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) {
			return;
		}
		if ((activeTool == ETool.Species) && (nests != null)) {
			if (Input.GetMouseButtonDown(0)) {
				Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
				RaycastHit hit;
				if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER | JLayers.M_MAPSELECTION)) {
					Vector3 point = hit.point;
					int x = (int) (point.x / JTerrainData.HORIZONTAL_SCALE);
					int y = (int) (point.z / JTerrainData.HORIZONTAL_SCALE);
					if (hit.collider.gameObject.layer == JLayers.L_MAPSELECTION) {
						int index = 0;
						foreach (NestGO ngo in nests) {
							if (ngo.go == hit.collider.gameObject) {
								CreateNestMarkers(activeSpecies);
								activeSpeciesNestIndex = index;
								SetupActiveNestStrings();
								return;
							}
							index++;
						}
					}
					else {
						int index = 0;
						foreach (JAnimalSpecies.Nest n in activeSpecies.nests) {
							if ((n.position.x == x) && (n.position.y == y)) {
								CreateNestMarkers(activeSpecies);
								activeSpeciesNestIndex = index;
								return;
							}
							index++;
						}
						JAnimalSpecies.Nest nest = new JAnimalSpecies.Nest();
						nest.position = new Coordinate(x, y);
						nest.males = 5;
						nest.females = 5;
						nestFStr = nest.females.ToString();
						nestMStr = nest.males.ToString();
						List<JAnimalSpecies.Nest> nn = new List<JAnimalSpecies.Nest>(activeSpecies.nests);
						nn.Add(nest);
						activeSpecies.nests = nn.ToArray();
						CreateNestMarkers(activeSpecies);
						activeSpeciesNestIndex = activeSpecies.nests.Length - 1;
						SetupActiveNestStrings();
					}
				}				
			}
		}
	}
	
	void HandleEditor(int id) {
		if (GUI.Button(new Rect(pos.width - 37, 3, 24, 12), "X")) {
			gameObject.SetActive(false);
		}
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		if (scenario != null) {
			GUIStyle tab = JWinMgr.self.skin.GetStyle("tab");
			GUIStyle activeTab = JWinMgr.self.skin.GetStyle("activetab");
			if (GUILayout.Button("Scenario", (activeTool == ETool.LoadSave)?activeTab:tab)) {
				activeTool = ETool.LoadSave;
				HideNestMarkers();
			}
			if (GUILayout.Button("Budget", (activeTool == ETool.Budget)?activeTab:tab)) {
				activeTool = ETool.Budget;
				HideNestMarkers();
			}
			if (GUILayout.Button("Maatregelen", (activeTool == ETool.Actions)?activeTab:tab)) {
				activeTool = ETool.Actions;
				HideNestMarkers();
			}
			if (GUILayout.Button("Onderzoek", (activeTool == ETool.Research)?activeTab:tab)) {
				activeTool = ETool.Research;
				HideNestMarkers();
			}
			if (GUILayout.Button("Soorten", (activeTool == ETool.Species)?activeTab:tab)) {
				activeTool = ETool.Species;
				HideNestMarkers();
			}
//			if (GUILayout.Button("Variabelen (Oud)", (activeTool == ETool.OldVariables)?activeTab:tab)) {
//				activeTool = ETool.OldVariables;
//				HideNestMarkers();
//			}
//			if (GUILayout.Button("Voortgang (Oud)", (activeTool == ETool.OldProgress)?activeTab:tab)) {
//				activeTool = ETool.OldProgress;
//				HideNestMarkers();
//			}
			if (GUILayout.Button("Game regels", (activeTool == ETool.GameRules)?activeTab:tab)) {
				HideNestMarkers();
				CompileScripts();
				activeTool = ETool.GameRules;
			}
		}
		else {
			GUILayout.Label("Scenario");
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		scroll = GUILayout.BeginScrollView(scroll, JWinMgr.self.skin.GetStyle("box"));
		switch (activeTool) {
		case ETool.LoadSave : HandleLoadSave(); break;
		case ETool.Budget : HandleBudget(); break;
		case ETool.Actions : HandleActions(); break;
		case ETool.Research : HandleResearch(); break;
		case ETool.Species : HandleSpecies(); break;
		case ETool.OldVariables : HandleVariables(); break;
		case ETool.OldProgress : HandleProgress(); break;
		case ETool.GameRules : HandleGameRules(); break;
		default : break;
		}
		GUILayout.EndScrollView();
		GUILayout.EndVertical();
		GUI.DragWindow();
	}
	
}
