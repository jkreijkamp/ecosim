using UnityEngine;
using System.Collections;

public class JBuildingHandle : MonoBehaviour {

	public JCellData.BuildingData data;
	public JCellData cell;
	public bool isSelected = false;
	
	private Material lineMaterial; 
	
	public static JBuildingHandle Create(JScenario scenario, GameObject prefab, Vector3 pos, Vector3 scale, float angle) {
		GameObject go = (GameObject) Instantiate(prefab);
		go.transform.parent = JRenderTerrain.self.objectHolder.transform;
		go.transform.localPosition = pos;
		go.transform.localRotation = Quaternion.Euler(0f, angle, 0f);
		go.transform.localScale = scale;
		go.layer = JLayers.L_BUILDINGS;
		go.tag = "BuildingHandle";
		Collider col = go.GetComponent<Collider>();
		if (col) {
			GameObject.DestroyImmediate(col);
		}
		JBuildingHandle handle = go.AddComponent<JBuildingHandle>();
		MeshFilter filter = go.GetComponent<MeshFilter>();
		BoxCollider col2 = go.AddComponent<BoxCollider>();
		filter.sharedMesh.RecalculateBounds();
		Bounds b = filter.sharedMesh.bounds;
		col2.center = b.center;
		col2.size = b.size;
		JCellData cell;
		JCellData.BuildingData data = scenario.data.AddBuilding(out cell, pos, Quaternion.Euler(0f, angle, 0f), scale, JElements.GetBuildingWrapper(prefab.name));
		cell.AddRenderedObject(go);
		handle.data = data;
		handle.cell = cell;
		JRoadEditor.CreateConnectorHandles(handle);
		return handle;
	}
	
	public void Delete() {
		cell.DeleteBuilding(data);
		Destroy(gameObject);
	}
	
	void OnDestroy() {
		JRoadEditor.DeleteConnectorHandles(this);
	}
	
	public void Move(JScenario scenario, Vector3 pos) {
		JRoadEditor.DeleteConnectorHandles(this);		
		transform.localPosition = pos;
		cell.DeleteBuilding(data);
		data = scenario.data.AddBuilding(out cell, pos, transform.localRotation, transform.localScale, data.building);
		JRoadEditor.CreateConnectorHandles(this);
	}
	
	public void Rotate(float angle, float snap) {
		JRoadEditor.DeleteConnectorHandles(this);		
		float currentAngle = transform.localRotation.eulerAngles.y;
		currentAngle += angle;
		if (snap > 0.0f) {
			currentAngle = Mathf.Round(currentAngle / snap) * snap;
		}
		transform.localRotation = Quaternion.Euler(0f, currentAngle, 0f);
		data.rotation = transform.localRotation;
		JRoadEditor.CreateConnectorHandles(this);
	}

	void Start() { 
		lineMaterial = new Material("Shader \"Lines/Colored Blended\" { SubShader { Pass { Blend SrcAlpha OneMinusSrcAlpha BindChannels { Bind \"Color\",color } ZWrite On Cull Front Fog { Mode Off } } } }"); 
		
		lineMaterial.hideFlags = HideFlags.HideAndDontSave; 
		lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave; 
	} 
	
	void OnRenderObject() {    
		if (!JBuildingEditor.isEditing) return;
		lineMaterial.SetPass(0); 
		
		Vector3 min = collider.bounds.min;
		Vector3 max = collider.bounds.max;
		
		// GL.PushMatrix(); 
		// GL.MultMatrix(transform.localToWorldMatrix); 
		GL.Begin(GL.LINES); 
		GL.Color(isSelected?Color.red:Color.blue);
		
		
		GL.Vertex(new Vector3(min.x, min.y, min.z));
		GL.Vertex(new Vector3(max.x, min.y, min.z));
		
		GL.Vertex(new Vector3(min.x, min.y, min.z));
		GL.Vertex(new Vector3(min.x, max.y, min.z));

		GL.Vertex(new Vector3(min.x, min.y, min.z));
		GL.Vertex(new Vector3(min.x, min.y, max.z));

		GL.Vertex(new Vector3(max.x, min.y, min.z));
		GL.Vertex(new Vector3(max.x, max.y, min.z));

		GL.Vertex(new Vector3(max.x, min.y, min.z));
		GL.Vertex(new Vector3(max.x, min.y, max.z));

		GL.Vertex(new Vector3(min.x, max.y, min.z));
		GL.Vertex(new Vector3(max.x, max.y, min.z));
		
		GL.Vertex(new Vector3(min.x, max.y, min.z));
		GL.Vertex(new Vector3(min.x, max.y, max.z));

		GL.Vertex(new Vector3(max.x, max.y, min.z));
		GL.Vertex(new Vector3(max.x, max.y, max.z));

		GL.Vertex(new Vector3(max.x, min.y, max.z));
		GL.Vertex(new Vector3(max.x, max.y, max.z));
		
		GL.Vertex(new Vector3(min.x, max.y, max.z));
		GL.Vertex(new Vector3(max.x, max.y, max.z));

		GL.Vertex(new Vector3(min.x, min.y, max.z));
		GL.Vertex(new Vector3(min.x, max.y, max.z));
		
		GL.Vertex(new Vector3(min.x, min.y, max.z));
		GL.Vertex(new Vector3(max.x, min.y, max.z));
		
		GL.End(); 
	}	
}
