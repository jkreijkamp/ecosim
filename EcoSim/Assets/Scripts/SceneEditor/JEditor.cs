using UnityEngine;
using System.Collections;

public class JEditor : MonoBehaviour {
	
	public JScenario scenario;
	public Terrain terrain;
	
	public Texture2D expandTex;
	public Texture2D collapseTex;
	public Texture2D warningTex;
	
	[System.Serializable]
	public class Tool {
		public Texture2D tool;
		public GameObject toolGO;
	}
	
	public Tool[] tools;
	
	void OnGUI() {
		GUI.skin = JWinMgr.self.skin;
		int pos = 0;
		foreach (Tool t in tools) {
			if (GUI.Button(new Rect(pos, 0, 32, 32), t.tool)) {
				t.toolGO.SetActive(!t.toolGO.activeSelf);
			}
			pos += 32;
			if (scenario == null) break; // show only scenario editor when no scenario is loaded
		}
		JWinMgr.AddRect(new Rect(0, 0, pos, 32));
	}
	
	void Start() {
		Terrain sceneT = GameObject.FindGameObjectWithTag("Terrain").GetComponent<Terrain>();
		JTileEditor.SetupStaticData(this, terrain, sceneT);
		JVegetationRules.SetupStaticData();
	}
}
