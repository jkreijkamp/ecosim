using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class JArticleEditor : MonoBehaviour {

	private int id = JWinMgr.idGenerator++;
	private Rect pos = new Rect(200, 100, 850, 560);
	
	public JEditor editor;
	public Texture2D destTex;
	
	enum EArticleType { Article, Encyclopedia };
	EArticleType aType = EArticleType.Article;
	
	private string[] imageList;

	void OnGUI() {
		GUI.skin = JWinMgr.self.skin;
		pos = JWinMgr.Window(id, pos, HandleEditor, "Artikelen", JWinMgr.self.skin.window);
	}
	
	private RenderFontToTexture rft;
	
	void OnEnable() {
		destTex = new Texture2D(512, 512, TextureFormat.RGB24, false);
		rft = RenderFontToTexture.self;
		ScenarioChanged();
		tab = JWinMgr.self.skin.GetStyle("tab");
		activeTab = JWinMgr.self.skin.GetStyle("activetab");
	}
	
	GUIStyle tab;
	GUIStyle activeTab;
	
	
	void UpdateImageList() {
		if (Directory.Exists(editor.scenario.scenarioPath + "/images/")) {
			imageList = Directory.GetFiles(editor.scenario.scenarioPath + "/images/", "*.*");
			for (int i = 0; i < imageList.Length; i++) {
				string f = imageList[i];
				int lastFS = Mathf.Max(f.LastIndexOf(Path.DirectorySeparatorChar), f.LastIndexOf(Path.AltDirectorySeparatorChar));
				if (lastFS > 0) {
					f = f.Substring(lastFS + 1);
				}
				// f = f.Substring(0, f.Length - 4);
				imageList[i] = f;
			}
		}
		else {
			imageList = new string[0];
		}
	}

	void ScenarioChanged() {
		if (editor.scenario != null) {
			rft.SetupRenderToTexture(editor.scenario.scenarioPath + "/images/", rft.bgTexList[0]);
			rft.RenderToTexture(destTex, 512);
			index = 0;
			UpdateImageList();
			aType = EArticleType.Article;
			if (editor.scenario.articles.Length > 0) {
				updateTimer = Time.time;
			}
		}
		else {
			imageList = new string[0];
		}
	}
	
	private float updateTimer = 0f;
	
	int index = 0;
	
	void HandleEditor(int id) {
		JScenario scenario = editor.scenario;
		if (GUI.Button(new Rect(pos.width - 37, 3, 24, 12), "X")) {
			gameObject.SetActive(false);
		}
		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Artikelen", (aType == EArticleType.Article)?activeTab:tab)) {
			aType = EArticleType.Article;
			index = 0;
			updateTimer = Time.time;
		}
		if (GUILayout.Button("Bibliotheek", (aType == EArticleType.Encyclopedia)?activeTab:tab)) {
			aType = EArticleType.Encyclopedia;
			index = 0;
			updateTimer = Time.time;
		}
		bool isArticle = (aType == EArticleType.Article);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		JEncyclopediaEntry eentry = null;
		int len = isArticle?(scenario.articles.Length):(scenario.encyclopedia.Length);
		if ((len > 0) && (!isArticle)) eentry = scenario.encyclopedia[index];
		if (index > 0) {
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				index--;
				updateTimer = Time.time + 0.1f;
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		if (len > 0) {
			if (isArticle) {
				GUILayout.Label("Artikel nummer " + index, GUILayout.Width(160));
			}
			else {
				string itemName = eentry.item;
				string newName = GUILayout.TextField(itemName, GUILayout.Width(100));
				if (newName != itemName) {
					eentry.item = newName;
					System.Array.Sort(scenario.encyclopedia);
					index = System.Array.IndexOf<JEncyclopediaEntry>(scenario.encyclopedia, eentry);
				}
			}
			if (GUILayout.Button("-", GUILayout.Width(20))) {
				if (isArticle) {
					List<string> l = new List<string>(scenario.articles);
					l.RemoveAt(index);				
					scenario.articles = l.ToArray();
					if (index > 0) index--;
					updateTimer = Time.time;
				}
				else {
					List<JEncyclopediaEntry> list = new List<JEncyclopediaEntry>(scenario.encyclopedia);
					list.RemoveAt(index);
					scenario.encyclopedia = list.ToArray();
					index--;
					updateTimer = Time.time;
					return;
				}
			}
		}
		else {
			if (isArticle) {
				GUILayout.Label("Geen artikelen gedefinieerd.");
			}
			else {
				GUILayout.Label("Geen encyclopedie definities.");
			}
		}
		if (index < len - 1) {
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				index++;
				updateTimer = Time.time + 0.1f;
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		if (GUILayout.Button(isArticle?"Nieuw artikel":"Nieuwe definitie")) {
			if (isArticle) {
				List<string> l = new List<string>(scenario.articles);
				l.Add("[TITLE]Nieuw artikel\n[INTRO]Een nieuw kranten artikel");
				scenario.articles = l.ToArray();
				index = scenario.articles.Length - 1;
				updateTimer = Time.time;
			}
			else {
				List<JEncyclopediaEntry> l = new List<JEncyclopediaEntry>(scenario.encyclopedia);
				JEncyclopediaEntry e = new JEncyclopediaEntry();
				e.item = "Nieuwe Definitie";
				l.Add(e);
				scenario.encyclopedia = l.ToArray();
				System.Array.Sort(scenario.encyclopedia);
				index = System.Array.IndexOf<JEncyclopediaEntry>(scenario.encyclopedia, e);
				
			}
			updateTimer = Time.time;
		}
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		if ((!isArticle) && (eentry != null)) {
			GUILayout.BeginHorizontal();
			GUILayout.Label("URL");
			string oldUrl = eentry.externalURL;
			if (oldUrl == null) oldUrl = "";
			string newUrl = GUILayout.TextField(oldUrl, GUILayout.Width(120)).Trim();
			if (newUrl != oldUrl) {
				if (newUrl == "") eentry.externalURL = null;
				else eentry.externalURL = newUrl;
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}
		GUILayout.BeginHorizontal();
		if (len > 0) {
			string text = isArticle?(scenario.articles[index]):(eentry.text);
			string newText = GUILayout.TextArea(text, GUILayout.Width(300), GUILayout.Height(300));
			if (newText != text) {
				updateTimer = Time.time + 0.5f;
				if (isArticle) {
					scenario.articles[index] = newText;
				}
				else {
					eentry.text = newText;
				}
			}
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		if (GUILayout.Button("Ververs lijst")) {
			UpdateImageList();
		}
		GUILayout.Label(editor.scenario.scenarioPath + "/images");
		imagesScroll = GUILayout.BeginScrollView(imagesScroll, false, true);
		foreach (string s in imageList) {
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("-", GUILayout.Width(20))) {
				Debug.Log("Delete '" + editor.scenario.scenarioPath + "/images/" + s + ".jpg'");
				File.Delete(editor.scenario.scenarioPath + "images/" + s + ".jpg");
				UpdateImageList();
				break;
			}
			GUILayout.Label(s);
			GUILayout.EndHorizontal();
		}
		GUILayout.EndScrollView();
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUILayout.Label(destTex, GUILayout.Width(512), GUILayout.Height(512));
		GUILayout.EndHorizontal();
		
		GUI.DragWindow();
	}
	
	Vector2 imagesScroll = new Vector2(0,0);
			
	void Update() {
		if ((updateTimer > 0f) && (updateTimer < Time.time)) {
			updateTimer = 0f;
			if (aType == EArticleType.Article) {
				rft.RenderNewsArticle(editor.scenario.articles[index], editor.scenario, destTex);
			}
			else {
				rft.RenderNewsArticle("[enc]\n" + editor.scenario.encyclopedia[index].text, editor.scenario, destTex);
			}
		}
	}
}
