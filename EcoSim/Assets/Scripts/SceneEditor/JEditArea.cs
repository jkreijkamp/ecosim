using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JEditArea : MonoBehaviour {

	private Mesh mesh;
	
	private byte[] map;
	private JCellData cell;
	private int cx;
	private int cy;
	private int usedCount = 0;
	private bool isChanged = false;
	
	private const int CELL_SIZE = JTerrainData.CELL_SIZE;

	public void ProcessPoints(JEditAreas.ProcessPoint handler) {
		int p = 0;
		int xOffset = cx * CELL_SIZE;
		int yOffset = cy * CELL_SIZE;
		for (int y = 0; y < CELL_SIZE; y++) {
			for (int x = 0; x < CELL_SIZE; x++) {
				int strength = map[p++];
				if (strength > 0) {
					handler(xOffset + x, yOffset + y, strength);
				}
			}
		}
	}
	
	public void SetOffset(JTerrainData data, int cx, int cy) {
		this.cx = cx;
		this.cy = cy;
		cell = data.GetCell(cx, cy);
	}
		
	void Awake() {
		mesh = new Mesh();
		GetComponent<MeshFilter>().sharedMesh = mesh;
		map = new byte[CELL_SIZE * CELL_SIZE];
	}

	public void GenerateMesh() {
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> indices = new List<int>();
		float uvStep = 1f / 10f;
		
		int count = 0;
		int i = 0;
		for (int y = 0; y < CELL_SIZE; y++) {
			for (int x = 0; x < CELL_SIZE; x++) {
				int val = map[i++];
				// val = (val + 9) / 10;
				if (val > 0) {
					int uvX = ((val - 1) % 10);
					int uvY = 9 - ((val - 1) / 10);
					uv.Add(new Vector2(uvStep * uvX, uvStep * uvY));
					uv.Add(new Vector2(uvStep * (uvX + 1), uvStep * uvY));
					uv.Add(new Vector2(uvStep * uvX, uvStep * (uvY + 1)));
					uv.Add(new Vector2(uvStep * (uvX + 1), uvStep * (uvY + 1)));
					vertices.Add(new Vector3(x, cell.GetCornerHeight(x, y), y));
					vertices.Add(new Vector3(x + 1, cell.GetCornerHeight(x + 1, y), y));
					vertices.Add(new Vector3(x, cell.GetCornerHeight(x, y + 1), y + 1));
					vertices.Add(new Vector3(x + 1, cell.GetCornerHeight(x + 1, y + 1), y + 1));
					indices.Add(count + 1);
					indices.Add(count);
					indices.Add(count + 2);
					indices.Add(count + 1);
					indices.Add(count + 2);
					indices.Add(count + 3);
					count += 4;
				}
			}
		}
		mesh.Clear();
		mesh.vertices = vertices.ToArray();
		mesh.uv = uv.ToArray();
		mesh.triangles = indices.ToArray();
		mesh.Optimize();
		mesh.RecalculateNormals();
	}

	public bool Set(int x, int y, int v) {
		int i = x + CELL_SIZE * y;
		int v0 = (int) map[i];
		if ((v > 0) && (v0 == 0)) {
			usedCount++;
		}
		else if ((v == 0) && (v0 > 0)) {
			usedCount--;
		}
		map[i] = (byte) v;
		if (usedCount == 0) {
			Destroy(gameObject);
			return false;
		}
		isChanged = true;
		return true;
	}
	
	
	public void Add(int x, int y, int v) {
		int i = x + CELL_SIZE * y;
		int v0 = (int) map[i];
		int vnew = Mathf.Clamp(v0 + v, 0, 100);
		if ((vnew > 0) && (v0 == 0)) {
			usedCount++;
		}
		map[i] = (byte) vnew;
		isChanged = true;
	}
	
	public bool Sub(int x, int y, int v) {
		int i = x + CELL_SIZE * y;
		int v0 = (int) map[i];
		int vnew = Mathf.Clamp(v0 - v, 0, 100);
		if ((vnew == 0) && (v0 > 0)) {
			usedCount--;
		}
		map[i] = (byte) vnew;
		if (usedCount == 0) {
			Destroy(gameObject);
			return false;
		}
		isChanged = true;
		return true;
	}
	
	void Update() {
		if (isChanged) {
			GenerateMesh();
			isChanged = false;
		}
	}
}
