using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JRoadEditor : MonoBehaviour {

	private int id = JWinMgr.idGenerator++;
	private Rect pos = new Rect(75, 50, 460, 540);

	public JEditor editor;
	
	public GameObject roadMarkerPrefab;
	public GameObject selectedMarkerPrefab;
	public GameObject connectorPrefab;
	
	public Texture2D[] icons;
	
	public int activeRoadIndex = 0;
	
	public class RoadSegment {
		public GameObject handle;
		public RoadNode node;
		public int index;
	}
	
	public class RoadNode {
		public List<RoadSegment> segments;
		public JRoad road;
	}
	
	private static JRoadEditor self;
	
	public Dictionary<GameObject, RoadSegment> nodes = new Dictionary<GameObject, RoadSegment>();
	public List<RoadNode> roads = new List<RoadNode>();

	
	public RoadSegment currentNode = null;
	
	private JTerrainData data;

	void ScenarioChanged() {
		if (editor.scenario != null) {
			data = editor.scenario.data;
			RemoveAllNodes();
			nodes = new Dictionary<GameObject, RoadSegment>();
			roads = new List<RoadNode>();
			MakeAllNodes();
		}
		else {
			data = null;
		}
	}
	
	void MakeAllNodes() {
		int count = data.roads.GetRoadCount();
		for (int i = 0; i < count; i++) {
			RoadNode n = new RoadNode();
			n.road = data.roads.GetRoadFromIndex(i);
			n.segments = new List<RoadSegment>();
			roads.Add(n);
			CreateRoadHandles(n, -1);
		}
	}
	
	void RemoveAllNodes() {
		foreach (RoadSegment node in nodes.Values) {
			Destroy(node.handle);
		}
		currentNode = null;
	}
	
	void RemoveRoadHandles(RoadNode r) {
		foreach (RoadSegment s in r.segments) {
			nodes.Remove(s.handle);
			Destroy(s.handle);
		}
		r.segments.Clear();
	}
	
	// selectedIndex if >= 0, set the node at index as selected node
	void CreateRoadHandles(RoadNode r, int selectedIndex) {
		RemoveRoadHandles(r);
		for (int i = 0; i < r.road.GetNodeCount(); i++) {
			Vector3 pos = r.road.GetNodePosition(i);
			RoadSegment rn = new RoadSegment();
			GameObject go = (GameObject) Instantiate((i == selectedIndex)?selectedMarkerPrefab:roadMarkerPrefab, pos, Quaternion.identity);
			rn.handle = go;
			rn.node = r;
			rn.index = i;
			nodes.Add(go, rn);
			r.segments.Add(rn);
			if (i == selectedIndex) currentNode = rn;
		}
	}
	
	public static void CreateConnectorHandles(JBuildingHandle building) {
		if (!self || !self.enabled) return;
		self.CreateConHandles(building);
	}
	
	public static void DeleteConnectorHandles(JBuildingHandle building) {
		if (!self || !self.enabled) return;
		self.DeleteConHandles(building);
	}
	
	void CreateConHandles(JBuildingHandle building) {
		foreach (Transform t in building.transform) {
			GameObject go = (GameObject) Instantiate(connectorPrefab);
			go.transform.parent = t;
			go.transform.localRotation = Quaternion.identity;
			go.transform.localPosition = Vector3.zero;
			go.layer = JLayers.L_ROADHANDLE;
		}
	}
	
	void DeleteConHandles(JBuildingHandle building) {
		foreach (Transform t in building.transform) {
			foreach (Transform t2 in t) {
				if (t2.gameObject.layer == JLayers.L_ROADHANDLE) Destroy(t2.gameObject);
			}
		}
	}
	
	void CreateAllConHandles() {
		GameObject[] gos = GameObject.FindGameObjectsWithTag("BuildingHandle");
		foreach (GameObject go in gos) {
			JBuildingHandle handle = go.GetComponent<JBuildingHandle>();
			if (handle) {
				CreateConHandles(handle);
			}
		}
	}

	void DeleteAllConHandles() {
		GameObject[] gos = GameObject.FindGameObjectsWithTag("BuildingHandle");
		foreach (GameObject go in gos) {
			JBuildingHandle handle = go.GetComponent<JBuildingHandle>();
			if (handle) {
				DeleteConHandles(handle);
			}
		}
	}
	
	void DeleteSelectedRoad() {
		if (currentNode != null) {
			RoadNode node = currentNode.node;
			RemoveRoadHandles(node);
			data.roads.DeleteRoad(node.road);
			roads.Remove(node);
			currentNode = null;
		}
	}
	
	void DeleteSelectedNode() {
		if (currentNode != null) {
			RoadNode node = currentNode.node;
			RemoveRoadHandles(node);
			if (node.road.GetNodeCount() <= 1) {
				data.roads.DeleteRoad(node.road);
				roads.Remove(node);
				currentNode = null;
			}
			else {
				int index = currentNode.index;
				node.road.DeleteNode(index);
				index = Mathf.Min(index, node.road.GetNodeCount() - 1);
				currentNode = null;
				CreateRoadHandles(node, index);
			}
		}
	}
	
	void Deselect() {
		if (currentNode != null) {
			GameObject go = (GameObject) Instantiate(roadMarkerPrefab, currentNode.handle.transform.position, currentNode.handle.transform.rotation);
			nodes.Remove(currentNode.handle);
			Destroy(currentNode.handle);
			currentNode.handle = go;
			nodes.Add(go, currentNode);
			currentNode = null;
		}
	}
	
	void Select(GameObject node) {
		if (currentNode != null) {
			if (currentNode.handle == node) return;
			Deselect();
		}
		if (!nodes.ContainsKey(node)) return;
		RoadSegment rn = nodes[node];
		nodes.Remove(node);
		GameObject go = (GameObject) Instantiate(selectedMarkerPrefab, node.transform.position, node.transform.rotation);
		rn.handle = go;
		nodes.Add(go, rn);
		Destroy(node);
		currentNode = rn;
	}
	
	void Create(Vector3 pos) {
		RoadNode currentRoad = null;
		if (currentNode != null) {
			currentRoad = currentNode.node;
			Deselect();
		}
		if (currentRoad == null) {
			GameObject newRoad = data.roads.CreateRoad(activeRoadIndex);
			currentRoad = new RoadNode();
			currentRoad.road = newRoad.GetComponent<JRoad>();
			currentRoad.segments = new List<RoadSegment>();
			roads.Add(currentRoad);
		}
		RoadSegment rn = new RoadSegment();
		GameObject go = (GameObject) Instantiate(selectedMarkerPrefab, pos, Quaternion.identity);
		rn.handle = go;
		rn.node = currentRoad;
		rn.index = currentRoad.road.AddNode(pos);
		nodes.Add(go, rn);
		currentNode = rn;
		currentRoad.segments.Add(rn);
	}
	
	void MoveTo(Vector3 pos, Vector3 connectorDir) {
		if (currentNode != null) {
			currentNode.handle.transform.position = pos;
			currentNode.node.road.MoveNodeTo(currentNode.index, pos, connectorDir);
		}
	}
	
	void OnGUI() {
		GUI.skin = JWinMgr.self.skin;
		pos = JWinMgr.Window(id, pos, HandleEditor, "Wegen", JWinMgr.self.skin.window);
	}
	
	void OnDisable() {
		RemoveAllNodes();
		DeleteAllConHandles();
	}

	void OnEnable() {
		ScenarioChanged();
		CreateAllConHandles();
	}
	
	void Awake() {
		self = this;
	}
	
	
	void HandleEditor(int id) {
		GUISkin skin = JWinMgr.self.skin;
		GUIStyle selected = skin.GetStyle("selectedbox");
		GUIStyle normal = skin.box;
		if (GUI.Button(new Rect(pos.width - 37, 3, 24, 12), "X")) {
			gameObject.SetActive(false);
		}
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		GameObject[] roadPrefabs = JElements.self.roadPrefabs;
		for (int i = 0; i < roadPrefabs.Length; i++) {
			if ((i > 0) && ((i % 5) == 0)) {
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
			}
			if (GUILayout.Button(icons[i], (i == activeRoadIndex)?selected:normal, GUILayout.Width(80), GUILayout.Height(80))) {
				activeRoadIndex = i;
				Deselect();
			}
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (currentNode != null) {
			if (GUILayout.Button("Verwijder punt")) {
				DeleteSelectedNode();
			}
			if (GUILayout.Button("Verwijder weg")) {
				DeleteSelectedRoad();
			}
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUI.DragWindow();
	}

	void Update() {
		Vector3 mousePos = Input.mousePosition;
		if (JWinMgr.InUIRect(mousePos.x, Screen.height - mousePos.y) ||
			Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) {
			return;
		}
		if (Input.GetMouseButtonDown(0)) {
			Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
			RaycastHit hit;
			if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER | JLayers.M_ROADS)) {
				Vector3 point = hit.point;
//				int x = (int) (point.x / JTerrainData.HORIZONTAL_SCALE);
//				int y = (int) (point.z / JTerrainData.HORIZONTAL_SCALE);
				if (hit.collider.gameObject.layer == JLayers.L_ROADS) {
					Select(hit.collider.gameObject);
				}
				else {
					Create(point);
				}
			}
		}
		else if (Input.GetMouseButton(0)) {
			if (currentNode != null) {
				Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
				RaycastHit hit;
				if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER | JLayers.M_ROADHANDLE)) {
					Vector3 point = hit.point;
					if (hit.collider.gameObject.layer == JLayers.L_ROADHANDLE) {
						Transform t = hit.transform;
						MoveTo(t.position, t.forward);
					}
					else {
						MoveTo(point, Vector3.zero);
					}
				}
			}
		}
		else if (Input.GetMouseButtonDown(1)) {
			Deselect();
		}
		if (Input.GetKeyDown(KeyCode.Delete) || Input.GetKeyDown(KeyCode.Backspace)) {
			DeleteSelectedNode();
		}
	}
}
