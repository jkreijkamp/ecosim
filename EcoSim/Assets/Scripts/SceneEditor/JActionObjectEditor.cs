using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JActionObjectEditor : MonoBehaviour {

	
	private int id = JWinMgr.idGenerator++;
	private Rect pos = JWinMgr.AdjustWindowSize(new Rect(95, 75, 270, 160));

	static Dictionary<EActionTypes, JActionObjectEditor> editors = new Dictionary<EActionTypes, JActionObjectEditor>();
	EActionTypes action;
	
	private GameObject parent;
	
	void Awake() {
		StartCoroutine(COBlink());
		parent = GameObject.FindWithTag("Scenario");
	}
	
	void ScenarioChanged() {
		Deselect();
		RemoveAllEditors();
	}
	
	JScenario scenario;
	JScenario.ActionGroup actionGroup;
	string[] prefabKeys;
	
		
	public static JActionObjectEditor Create(Transform parent, EActionTypes action) {
		if (editors.ContainsKey(action)) {
			editors[action].Deselect();
			editors[action].RemoveAll();
			editors.Remove(action);
		}
		GameObject go = new GameObject("ActionObjectEditor");
		go.transform.parent = parent;
		go.transform.localPosition = Vector3.zero;
		go.layer = JLayers.L_GUI;
		JActionObjectEditor actionEditor = go.AddComponent<JActionObjectEditor>();
		editors.Add(action, actionEditor);
		actionEditor.action = action;
		JEditor editor = GameObject.Find("Editor").GetComponent<JEditor>();
		JScenario scenario = editor .scenario;
		JScenario.ActionInfo info = scenario.actions[action];
		if (info.actionGroup == null) {
			info.actionGroup = new JScenario.ActionGroup();
			info.actionGroup.action = action;
			info.actionGroup.objects = new JScenario.ActionObject[0];
		}
		actionEditor.actionGroup = info.actionGroup;
//		actionEditor.parameterIndex = EnumExtensions.IndexOfParamType(info.actionGroup.influenceParam);
		actionEditor.scenario = scenario;
		actionEditor.prefabKeys = new string[scenario.extraPrefabs.Count];
//		actionEditor.onStr = info.actionGroup.activeValue.ToString();
//		actionEditor.offStr = info.actionGroup.inactiveValue.ToString();
		scenario.extraPrefabs.Keys.CopyTo(actionEditor.prefabKeys, 0);
		actionEditor.DisplayAll();
		return actionEditor;
	}

	public static void RemoveEditor(EActionTypes t) {
		JActionObjectEditor editor;
		if (editors.TryGetValue(t, out editor)) {
			editors.Remove(t);
			Destroy(editor.gameObject);
		}
	}
	
	public void OnDestroy() {
		RemoveAll();
	}
	
	public static void RemoveAllEditors() {
		foreach (JActionObjectEditor editor in editors.Values) {
			Destroy(editor.gameObject);
		}
		editors.Clear();
	}
	
	void OnGUI() {
		GUI.skin = JWinMgr.self.skin;
		pos = JWinMgr.Window(id, pos, HandleAction, "Actie " + action.ToString() + ": " + scenario.actions[action].name, JWinMgr.self.skin.window);
	}
	
//	int parameterIndex = 0;
	int prefabIndex = 0;
	int prefabRotation = 0;
//	string offStr = "0";
//	string onStr = "255";
	
	enum EMode { Inactive, Place, Edit };
	EMode mode = EMode.Inactive;
	
	void HandleAction(int id) {
//		GUISkin skin = JWinMgr.self.skin;
		if (GUI.Button(new Rect(pos.width - 37, 3, 24, 12), "X")) {
			RemoveEditor(action);
		}
		GUILayout.BeginVertical();
		int count = prefabKeys.Length;
		if (count == 0) {
			GUILayout.Label("Geen objecten beschikbaar, voeg een unity3d assetbundle toe in scenario met naam 'ExtraAssets.unity3d'");
		}
		else {
//			GUILayout.BeginHorizontal();
//			if (GUILayout.Button("<", GUILayout.Width(20))) {
//				parameterIndex = (parameterIndex == 0)?(EnumExtensions.paramTypeArray.Length - 1):(parameterIndex - 1);
//			}
//			GUILayout.Label(EnumExtensions.paramTypeArray[parameterIndex].ToString(), GUILayout.Width(100));
//			if (GUILayout.Button(">", GUILayout.Width(20))) {
//				parameterIndex = (parameterIndex + 1) % EnumExtensions.paramTypeArray.Length;
//			}
//			offStr = GUILayout.TextField(offStr, GUILayout.Width(25));
//			GUILayout.Label((actionGroup.inactiveValue * 100 / 255).ToString() + "%", GUILayout.Width(29));
//			onStr = GUILayout.TextField(onStr, GUILayout.Width(25));
//			GUILayout.Label((actionGroup.activeValue * 100 / 255).ToString() + "%", GUILayout.Width(29));
//			int.TryParse(onStr, out actionGroup.activeValue);
//			int.TryParse(offStr, out actionGroup.inactiveValue);
//			actionGroup.influenceParam = EnumExtensions.paramTypeArray[parameterIndex];
//			GUILayout.FlexibleSpace();
//			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				// Deselect();
				prefabIndex = (prefabIndex + count - 1) % count;
			}
			GUILayout.Label(prefabKeys[prefabIndex], GUILayout.Width(100));
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				// Deselect();
				prefabIndex = (prefabIndex + 1) % count;
			}
			if (indexOfObject >= 0) {
				if (GUILayout.Button("Pas aan")) {
					selectedObject.prefabKey = prefabKeys[prefabIndex];
					int saveIndex = indexOfObject;
					RemoveAll();
					DisplayAll();
					indexOfObject = saveIndex;
					selectedObject = actionGroup.objects[indexOfObject];
					EnableSelecting();
				}
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			prefabRotation = 5 * (int) GUILayout.HorizontalSlider(prefabRotation / 5.0f, 0f, 360f / 5f, GUILayout.Width(140));
			if ((selectedObject != null) && (((int) selectedObject.rotation) != prefabRotation)) {
				selectedObject.instance.transform.rotation = Quaternion.Euler(0, prefabRotation, 0);
				selectedObject.rotation = (float) prefabRotation;
			}
			GUILayout.Label("Rotatie " + prefabRotation);
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			if (GUILayout.Button((mode == EMode.Place)?"<b>Plaats object</b>":"Plaats object", GUILayout.Width(100))) {
				foreach (JActionObjectEditor e in editors.Values) {
					if (e != this) {
						e.Deselect();
						e.mode = EMode.Inactive;
					}
				}
				Deselect();
				mode = EMode.Place;
			}
			if (GUILayout.Button((mode == EMode.Edit)?"<b>Invloedsgebied</b>":"Invloedsgebied", GUILayout.Width(100))) {
				foreach (JActionObjectEditor e in editors.Values) {
					if (e != this) {
						e.Deselect();
						e.mode = EMode.Inactive;
					}
				}
				mode = EMode.Edit;
				EnableSelecting();
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("<", GUILayout.Width(20)) && (indexOfObject > 0)) {
				int newIndex = indexOfObject - 1;
				Deselect();
				selectedObject = actionGroup.objects[newIndex];
				prefabRotation = (int) selectedObject.rotation;
				indexOfObject = newIndex;
				EnableSelecting();
			}
			if (indexOfObject >= 0) {
				JScenario.ActionObject obj = actionGroup.objects[indexOfObject];
				GUILayout.Label("Object# " + indexOfObject);
				Vector3 objpos = obj.position;
				int x =  (int) (objpos.x / JTerrainData.HORIZONTAL_SCALE);
				int y = (int) (objpos.z / JTerrainData.HORIZONTAL_SCALE);
				GUILayout.Label("# " + indexOfObject + " " + x + "/" + y + " " +
					obj.prefabKey, GUILayout.Width(170));
			}
			if (GUILayout.Button(">", GUILayout.Width(20)) && (indexOfObject < actionGroup.objects.Length - 1)) {
				int newIndex = indexOfObject + 1;
				Deselect();
				selectedObject = actionGroup.objects[newIndex];
				prefabRotation = (int) selectedObject.rotation;
				indexOfObject = newIndex;
				EnableSelecting();
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			if (mode == EMode.Edit) {
				GUILayout.BeginHorizontal();
				int newStrength = (int) GUILayout.HorizontalSlider(strength, 1, 100, GUILayout.Width(100));
				if (newStrength != strength) {
					strength = newStrength;
					JEditAreas.self.SetIntStrength(strength);
				}
				GUILayout.Label(strength.ToString());
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
			}
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUI.DragWindow();		
	}
	
	int strength = 100;
	JScenario.ActionObject selectedObject;
	int indexOfObject = -1;
	
	void EnableSelecting() {
		if (mode != EMode.Edit) return;
		if (selectedObject != null) {
			JEditAreas.self.EnableSelecting(0, 1f, false, true, true);
			JEditAreas.self.SetIntStrength(strength);
			JEditAreas.self.Clear();
			Debug.Log("Adding " + selectedObject.influence.Length + " point to area select");
			foreach (InfluenceCoordinate c in selectedObject.influence) {
				JEditAreas.self.Add(c.x, c.y, c.v);
			}
		}
	}
	
	void DisableSelecting() {
		JEditAreas.self.DisableSelecting();
	}
	
	List<InfluenceCoordinate> coordinates;
	
	void DSetPoint(int x, int y, int strength) {
		coordinates.Add(new InfluenceCoordinate(x, y, strength));
	}

	void Deselect() {
		if (selectedObject != null) {
			if (mode == EMode.Edit) {
				coordinates = new List<InfluenceCoordinate>();
				JEditAreas.self.ProcessPoints(DSetPoint);
				selectedObject.influence = coordinates.ToArray();
//				Debug.Log("added " + coordinates.Count + " points to influence");
			}
			if (selectedObject.instance) {
				Renderer[] rList = selectedObject.instance.GetComponentsInChildren<Renderer>();
				foreach (Renderer r in rList) {
					if (r != null) r.enabled = true;
				}
			}
			selectedObject = null;
			indexOfObject = -1;
		}
		DisableSelecting();
	}
	
	IEnumerator COBlink() {
		while (this) {
			if (selectedObject != null) {
				Renderer[] rList = selectedObject.instance.GetComponentsInChildren<Renderer>();
				foreach (Renderer r in rList) {
					if (r != null) r.enabled = !r.enabled;
				}
			}
			yield return new WaitForSeconds(0.125f);
		}
	}
	
	bool preventDrag = false;
	
	void Update() {
		if (mode == EMode.Inactive) return;
		if ((mode == EMode.Place) || (selectedObject == null))  {
			UpdatePlace();
		}
		else if (Input.GetMouseButtonDown(1)) {
			Deselect();
			selectedObject = null;
			indexOfObject = -1;
		}
	}
	
	void DisplayAll() {
		Deselect();
		foreach (JScenario.ActionObject obj in actionGroup.objects) {
			if (obj.instance) Destroy(obj.instance);
			if (!scenario.extraPrefabs.ContainsKey(obj.prefabKey)) {
				Debug.LogWarning("Prefab '" + obj.prefabKey + "' not found.");
				obj.prefabKey = prefabKeys[0];
			}
			obj.instance = (GameObject) Instantiate(scenario.extraPrefabs[obj.prefabKey],
				obj.position, Quaternion.Euler(0f, obj.rotation, 0f));
			obj.instance.transform.parent = parent.transform;
			obj.instance.layer = JLayers.L_ACTIONOBJECTS;
		}
	}
	
	void RemoveAll() {
		Deselect();
		foreach (JScenario.ActionObject obj in actionGroup.objects) {
			if (obj.instance) {
				Destroy(obj.instance);
				obj.instance = null;
			}
		}
	}
	
	void UpdatePlace() {
		Vector3 mousePos = Input.mousePosition;
		if (JWinMgr.InUIRect(mousePos.x, Screen.height - mousePos.y) ||
			Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) {
			return;
		}
		if (Input.GetMouseButtonDown(0)) {
			Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
			RaycastHit hit;
			if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER | JLayers.M_ACTIONOBJECTS)) {
				Vector3 point = hit.point;
				if (hit.collider.gameObject.layer == JLayers.L_ACTIONOBJECTS) {
					int index = 0;
					foreach (JScenario.ActionObject obj in actionGroup.objects) {					
						if (obj.instance == hit.collider.gameObject) {
							Deselect();
							selectedObject = obj;
							indexOfObject = index;
							prefabRotation = (int) selectedObject.rotation;
							EnableSelecting();
							break;
						}
						index++;
					}
				}
				else if (mode == EMode.Place) {
					preventDrag = true;
					JScenario.ActionObject obj = new JScenario.ActionObject();
					obj.prefabKey = prefabKeys[prefabIndex];
					obj.position = point;
					obj.rotation = prefabRotation;
					obj.influence = new InfluenceCoordinate[0];
					
					obj.instance = (GameObject) Instantiate(scenario.extraPrefabs[obj.prefabKey],
						point, Quaternion.Euler(0f, prefabRotation, 0f));
					obj.instance.transform.parent = parent.transform;
					obj.instance.layer = JLayers.L_ACTIONOBJECTS;
					List<JScenario.ActionObject> objList = new List<JScenario.ActionObject>(actionGroup.objects);
					objList.Add(obj);
					actionGroup.objects = objList.ToArray();
					Deselect();
					selectedObject = obj;
					indexOfObject = actionGroup.objects.Length - 1;
				}
				else {
					preventDrag = true;
				}
			}
		}
		else if (Input.GetMouseButtonUp(0)) {
			preventDrag = false;
		}
		else if (Input.GetMouseButton(0) && !preventDrag) {
			if (selectedObject != null) { // object selected
				Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
				RaycastHit hit;
				if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER)) {
					Vector3 point = hit.point;
					selectedObject.instance.transform.position = point;
					selectedObject.position = point;
				}
			}
		}
		else if (Input.GetMouseButtonDown(1)) {
			Deselect();
			selectedObject = null;
			indexOfObject = -1;
		}
		if ((selectedObject != null) && Input.GetKeyDown(KeyCode.Comma)) {
			prefabRotation = (prefabRotation + 355) % 360;
			selectedObject.instance.transform.rotation = Quaternion.Euler(0, prefabRotation, 0);
		}
		if ((selectedObject != null) && Input.GetKeyDown(KeyCode.Period)) {
			prefabRotation = (prefabRotation + 5) % 360;
			selectedObject.instance.transform.rotation = Quaternion.Euler(0, prefabRotation, 0);
		}
		if (Input.GetKeyDown(KeyCode.Delete) || Input.GetKeyDown(KeyCode.Backspace)) {
			if (selectedObject != null) {
				DisableSelecting();
				List<JScenario.ActionObject> objList = new List<JScenario.ActionObject>(actionGroup.objects);
				objList.Remove(selectedObject);
				actionGroup.objects = objList.ToArray();
				Destroy(selectedObject.instance);
				int newIndex = (indexOfObject < actionGroup.objects.Length)?indexOfObject:(indexOfObject - 1);
				selectedObject = null;
				RemoveAll();
				indexOfObject = newIndex;
				DisplayAll();
				if (indexOfObject >= 0) {
					selectedObject = actionGroup.objects[newIndex];
					prefabRotation = (int) selectedObject.rotation;
				}
				EnableSelecting();
			}
		}
	}
}