using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JTileIconGenerator : MonoBehaviour {
	
	public static JTileIconGenerator self;
	public Camera tileCamera;
	public Terrain terrain;

	private List<JXTerrainTile> queue = new List<JXTerrainTile>();
	
	void Awake() {
		self = this;
	}
	
	void Start() {
		Terrain scenarioTerrain = GameObject.FindGameObjectWithTag("Terrain").GetComponent<Terrain>();
		TerrainData data = scenarioTerrain.terrainData;
		TerrainData eData = terrain.terrainData;
		eData.treePrototypes = data.treePrototypes;
		eData.splatPrototypes = data.splatPrototypes;
		JTileEditorTerrainGen.Setup(data);
	}
	
	public static void NeedsIcon(JXTerrainTile tile) {
		if (!self) return;
		if (!self.queue.Contains(tile)) {
			self.queue.Add(tile);
		}
	}
	
	public void Update() {
		if (queue.Count > 0) {
			JXTerrainTile tile = queue[0];
			if (tile.icon == null) {
				CalculateDefaultCameraPos();
				UpdateTexture(ref tile.icon, tile, tile.vegetation.tiles[0]);
			}
			queue.RemoveAt(0);
		}
	}
		
	public void CalculateDefaultCameraPos() {
		CalculateCameraPos(45f, 30f, 15f, -0.35f);
	}
	
	public void CalculateCameraPos(float cameraAngle, float cameraDistance, float cameraHeight, float lookHeight) {
		Vector3 pos = Vector3.zero;
		pos.x = Mathf.Sin(Mathf.Deg2Rad * cameraAngle) * cameraDistance;
		pos.z = Mathf.Cos(Mathf.Deg2Rad * cameraAngle) * cameraDistance;
		Vector3 dir = (-pos).normalized;
		pos.y = cameraHeight;
		dir.y = lookHeight;
		tileCamera.transform.localPosition = pos;
		tileCamera.transform.localRotation = Quaternion.LookRotation(dir, Vector3.up);
	}
		
	public void UpdateTexture(ref Texture2D resultTex, JXTerrainTile centre, JXTerrainTile others) {
		JTileEditorTerrainGen.Generate(terrain, centre, others);
		if ((resultTex == null) || (resultTex.format != TextureFormat.RGB24)) {
			if (resultTex != null) {
				Destroy(resultTex);
			}
			resultTex = new Texture2D(256, 256, TextureFormat.RGB24, false);
		}

		RenderTexture rt =  RenderTexture.GetTemporary(resultTex.width, resultTex.height, 24, RenderTextureFormat.ARGB32);
		rt.useMipMap = false;
		rt.wrapMode = TextureWrapMode.Clamp;
		tileCamera.targetTexture = rt;
		tileCamera.Render();
		RenderTexture.active = rt;
		resultTex.ReadPixels(new Rect(0, 0, resultTex.width, resultTex.height), 0, 0, false);
		RenderTexture.active = null;
		RenderTexture.ReleaseTemporary(rt);
		resultTex.Apply();
		Resources.UnloadUnusedAssets();
		System.GC.Collect();
	}
	
}
