using UnityEngine;
using System.Collections;

public class JShowMousePosition : MonoBehaviour {
	
	public JEditor editor;
	public Transform farCameraT;
	public Transform nearCameraT;
	
	
	void OnGUI() {
		string msg = null;
		if (editor.scenario == null) {
		}
		else if (JCamera.IsNear) {
		}
		else {
			Vector3 pos = farCameraT.camera.ScreenToWorldPoint(Input.mousePosition);
			int x =  (int) (pos.x / JTerrainData.HORIZONTAL_SCALE);
			int y = (int) (pos.z / JTerrainData.HORIZONTAL_SCALE);
			int cx = x / JTerrainData.CELL_SIZE;
			int cy = y / JTerrainData.CELL_SIZE;
			JTerrainData data = editor.scenario.data;
			if ((cx < 0) || (cy < 0) || (cx >= data.cWidth) || (cy >= data.cHeight)) return; 
			int ix = x % JTerrainData.CELL_SIZE;
			int iy = y % JTerrainData.CELL_SIZE;
			msg = cx.ToString() + "/" + cy.ToString() + " (" + ix.ToString() + "/" + iy.ToString() + ")\n<size=10>" + x + "/" + y + "</size>";
		}
		
		if (msg != null) {
			GUI.Label(new Rect(Screen.width - 100, Screen.height - 40, 100, 40), msg);
		}
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
