using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class JEditAreas : MonoBehaviour {

//	public interface IWalkThroughPoints {
//		void HandlePoint(int x, int y, int strength);
//	}
	
	public delegate void ProcessPoint(int x, int y, int strength);
	
	public class PointData {
		public int x;
		public int y;
		public int strength;
	}
	
	public GameObject areaPrefab;
	
	public JEditor editor;
	
	public JTerrainData data;
	
	public static JEditAreas self;
	private int radius = 0;
	private int strength = 100;
	private bool smooth = false;
	private bool absolute = false;
	public bool limitToNeedSuccession = false;
	private int xc;
	private int yc;
	
	private Mesh mesh;
	public JEditArea[,] areas;
	
	public void ProcessPoints(ProcessPoint handler) {
		foreach (JEditArea area in areas) {
			if (area != null) {
				area.ProcessPoints(handler);
			}
		}
	}
	
	void GenerateMesh() {
		if (data == null) return;
		int xMin = Mathf.Max(0, xc - radius);
		int xMax = Mathf.Min(data.width - 1, xc + radius);
		int yMin = Mathf.Max(0, yc - radius);
		int yMax = Mathf.Min(data.height - 1, yc + radius);
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> indices = new List<int>();
		float uvStep = 1f / 10f;
		
		int count = 0;
		int strength = this.strength;
		bool smooth = (radius == 0)?false:this.smooth;
		
		for (int y = yMin; y <= yMax; y++) {
			for (int x = xMin; x <= xMax; x++) {
				if (!limitToNeedSuccession || data.NeedCalculateSuccession(x, y)) {
					int dist2 = (xc - x) * (xc - x) + (yc - y) * (yc - y);
					if (dist2 <= radius * radius) {					
						if (smooth && (radius > 0)) {
							float s = 1f - ((float) dist2 / (float) (radius * radius));
							strength = (int) (s * this.strength);
						}
						if ((strength > 0) && (count < 64000)) {
							int uvX = ((strength - 1) % 10);
							int uvY = 9 - ((strength - 1) / 10);
							uv.Add(new Vector2(uvStep * uvX, uvStep * uvY));
							uv.Add(new Vector2(uvStep * (uvX + 1), uvStep * uvY));
							uv.Add(new Vector2(uvStep * uvX, uvStep * (uvY + 1)));
							uv.Add(new Vector2(uvStep * (uvX + 1), uvStep * (uvY + 1)));
							vertices.Add(new Vector3(x, data.GetCornerHeight(x, y), y));
							vertices.Add(new Vector3(x + 1, data.GetCornerHeight(x + 1, y), y));
							vertices.Add(new Vector3(x, data.GetCornerHeight(x, y + 1), y + 1));
							vertices.Add(new Vector3(x + 1, data.GetCornerHeight(x + 1, y + 1), y + 1));
							indices.Add(count + 1);
							indices.Add(count);
							indices.Add(count + 2);
							indices.Add(count + 1);
							indices.Add(count + 2);
							indices.Add(count + 3);
							count += 4;
						}
					}
				}
			}
		}
		mesh.Clear();
		if (count == 0) return;
		mesh.vertices = vertices.ToArray();
		mesh.uv = uv.ToArray();
		mesh.triangles = indices.ToArray();
		mesh.RecalculateNormals();
	}

	void AddOrSubstract(bool isAdding) {
		if (data == null) return;
		int xMin = Mathf.Max(0, xc - radius);
		int xMax = Mathf.Min(data.width - 1, xc + radius);
		int yMin = Mathf.Max(0, yc - radius);
		int yMax = Mathf.Min(data.height - 1, yc + radius);
		bool smooth = (radius == 0)?false:this.smooth;
		for (int y = yMin; y <= yMax; y++) {
			for (int x = xMin; x <= xMax; x++) {
				if (!limitToNeedSuccession || data.NeedCalculateSuccession(x, y)) {
					int dist2 = (xc - x) * (xc - x) + (yc - y) * (yc - y);
					if (dist2 <= radius * radius) {
						int strength = this.strength;
						if (smooth && (radius > 0)) {
							float s = 1f - ((float) dist2 / (float) (radius * radius));
							strength = ((int) (10 * s) * this.strength) / 10;
						}
						if (strength > 0) {
							if (isAdding) {
								if (absolute) {
									Set(x, y, strength);
								}
								else {
									Add(x, y, strength);
								}
							}
							else {
								if (absolute) {
									Set(x, y, 0);
								}
								else {
									Subtract(x, y, strength);
								}
							}
						}
					}
				}
			}
		}
	}
	
	
	void Awake() {
		self = this;
		mesh = new Mesh();
		mesh.MarkDynamic();
		GetComponent<MeshFilter>().sharedMesh = mesh;
	}
	
	void Start() {
		gameObject.SetActive(false);
	}
		
	void OnEnable() {
		ScenarioChanged();
	}
	
	void ScenarioChanged() {
		if (editor.scenario != null) { 
			data = editor.scenario.data;
			areas = new JEditArea[data.cHeight, data.cWidth];
		}
		else {
			data = null;
			areas = null;
		}
		mesh.Clear();
		xc = -1;
		foreach (Transform t in transform) {
			Destroy(t.gameObject);
		}
	}
	
	public void Clear() {
		foreach (Transform t in transform) {
			Destroy(t.gameObject);
		}
		areas = new JEditArea[data.cHeight, data.cWidth];
	}
	
	public void Redraw() {
		foreach (JEditArea area in areas) {
			if (area != null) area.GenerateMesh();
		}
		GenerateMesh();
	}
	public void EnableSelecting(int radius, float strength, bool smooth) {
		EnableSelecting(radius, strength, smooth, false, false);
	}
	
	public void EnableSelecting(int radius, float strength, bool smooth, bool needSuccession, bool absolute) {
		limitToNeedSuccession = needSuccession;
		gameObject.SetActive(true);
		this.radius = radius;
		this.strength = Mathf.Clamp(Mathf.RoundToInt(strength * 100), 0, 100);
		this.smooth = smooth;
		this.absolute = absolute;
	}
	
	public void SetIntStrength(int strength) {
		this.strength = strength;
	}
	
	public void DisableSelecting() {
		if (gameObject) gameObject.SetActive(false);
	}
	
	public void Set(int x, int y, int v) {
		if (limitToNeedSuccession && !data.NeedCalculateSuccession(x, y)) return;
		int cx = x >> JTerrainData.CELL_SIZE2EXP;
		int cy = y >> JTerrainData.CELL_SIZE2EXP;
		int xx = x & JTerrainData.CELL_MASK;
		int yy = y & JTerrainData.CELL_MASK;
		JEditArea area = areas[cy, cx];
		if (area == null) {
			GameObject go = (GameObject) Instantiate(areaPrefab);
			go.transform.parent = transform;
			go.transform.localPosition = new Vector3(cx * JTerrainData.CELL_SIZE, 0f, cy * JTerrainData.CELL_SIZE);
			go.transform.localScale = Vector3.one;
			area = go.GetComponent<JEditArea>();
			area.SetOffset(data, cx, cy);
			areas[cy, cx] = area;
		}
		area.Set(xx, yy, v);
	}
	
	public void Add(int x, int y, int v) {
		if (limitToNeedSuccession && !data.NeedCalculateSuccession(x, y)) return;
		int cx = x >> JTerrainData.CELL_SIZE2EXP;
		int cy = y >> JTerrainData.CELL_SIZE2EXP;
		int xx = x & JTerrainData.CELL_MASK;
		int yy = y & JTerrainData.CELL_MASK;
		JEditArea area = areas[cy, cx];
		if (area == null) {
			GameObject go = (GameObject) Instantiate(areaPrefab);
			go.transform.parent = transform;
			go.transform.localPosition = new Vector3(cx * JTerrainData.CELL_SIZE, 0f, cy * JTerrainData.CELL_SIZE);
			go.transform.localScale = Vector3.one;
			area = go.GetComponent<JEditArea>();
			area.SetOffset(data, cx, cy);
			areas[cy, cx] = area;
		}
		area.Add(xx, yy, v);
	}
	
	public void Subtract(int x, int y, int v) {
		int cx = x >> JTerrainData.CELL_SIZE2EXP;
		int cy = y >> JTerrainData.CELL_SIZE2EXP;
		int xx = x & JTerrainData.CELL_MASK;
		int yy = y & JTerrainData.CELL_MASK;
		JEditArea area = areas[cy, cx];
		if (area != null) {
			if (!area.Sub(xx, yy, v)) {
				areas[cy, cx] = null;
			}
		}
	}
	
	public PointData[] GetPoints() {
		return new PointData[0];
	}
	
	public string debugStr = "";
	
	void OnGUIXXX() {
		GUI.Label(new Rect(Screen.width - 200, 0, 200, 800), debugStr);
	}
	
	void Update() {
		if (!JCamera.IsNear) return;
		Vector3 mousePos = Input.mousePosition;
		if (JWinMgr.InUIRect(mousePos.x, Screen.height - mousePos.y) ||
			Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) {
			mesh.Clear();
			xc = -1;
			return;
		}
		Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
		RaycastHit hit;
		if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER)) {
			Vector3 point = hit.point;
			int newXC = (int) (point.x / JTerrainData.HORIZONTAL_SCALE);
			int newYC = (int) (point.z / JTerrainData.HORIZONTAL_SCALE);
			if ((xc != newXC) || (yc != newYC)) {
				xc = newXC;
				yc = newYC;
				GenerateMesh();
				// debugStr = editor.scenario.data.GetDebugHeight(xc, yc);
			}
			if (Input.GetMouseButtonDown(0)) {
				AddOrSubstract(!(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)));
			}
		}
	}
}
