using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JWinMgr : MonoBehaviour {

	static List<Rect> uiRects = new List<Rect>();
	
	public GUISkin skin;
	
	public static int idGenerator = 1;
	public static JWinMgr self;
	private static bool mouseStartedInWindow = false;
	private int moveToFront = -1;
	
	void Awake() {
		self = this;
	}
	
	void Update() {
		if (Input.GetMouseButtonUp(0)) {
			mouseStartedInWindow = false;
		}
		else if (Input.GetMouseButtonDown(0)) {
			Vector3 mousePos = Input.mousePosition;

			mouseStartedInWindow = InUIRect(mousePos.x, Screen.height - mousePos.y);
		}
	}
	
	public static Rect AdjustWindowSize(Rect winPos) {
		winPos.width += 32;
		winPos.height += 48;
		return winPos;
	}
	
	public static Rect Window(int id, Rect rect, GUI.WindowFunction func, string title, GUIStyle style) {
//		if (JXCameraControl.self.lockControls) return rect;
		
		rect = GUI.Window(id, rect, func, title, style);
		if (self.moveToFront == id) {
			GUI.BringWindowToFront(id);
			self.moveToFront = -1;
		}
		uiRects.Add(rect);
		return rect;
	}
	
	public static void MoveToFront(int id) {
		self.moveToFront = id;
	}
	
	public static bool InUIRect(float x, float y) {
		if (mouseStartedInWindow) return true;
		Vector2 point = new Vector2(x, y);
		foreach (Rect r in uiRects) {
			if (r.Contains(point)) return true;
		}
		return false;
	}
	
	public static void AddRect(Rect r) {
		uiRects.Add(r);
	}

	public static void ForceClearRects() {
		if (uiRects.Count > 0) {
			Debug.LogWarning("We had to clear!!");
			uiRects.Clear();
		}
	}
	
	void LateUpdate() {
		uiRects.Clear();
	}
	
	void OnDestroy() {
		uiRects.Clear();
	}
}
