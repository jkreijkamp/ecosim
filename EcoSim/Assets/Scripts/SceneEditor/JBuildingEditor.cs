using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JBuildingEditor : MonoBehaviour {

	private int id = JWinMgr.idGenerator++;
	private Rect pos = new Rect(75, 100, 320, 200);

	public JEditor editor;
	
	public static bool isEditing = false;
	
	private JTerrainData data;
	private JBuildingHandle selectedBuilding;
	private float adjustHeight = 0f;

	void ScenarioChanged() {
		if (editor.scenario != null) {
			data = editor.scenario.data;
		}
		else {
			data = null;
		}
	}
	
	[System.Serializable]
	public class BuildingMapping {
		public string category;
		public string[] startsWith;
		[System.NonSerialized]
		public List<GameObject> prefabs;
	}
	
	public BuildingMapping[] mapping;
	
	private int categoryIndex = 0;
	private int buildingIndex = 0;
	
	void OnGUI() {
		GUI.skin = JWinMgr.self.skin;
		pos = JWinMgr.Window(id, pos, HandleEditor, "Bebouwing", JWinMgr.self.skin.window);
	}
	
	void Awake() {
		foreach (BuildingMapping m in mapping) {
			m.prefabs = new List<GameObject>();
		}
		foreach (GameObject go in JElements.self.buildingPrefabs) {
			string bname = go.name.ToLower();
			foreach (BuildingMapping m in mapping) {
				foreach (string sw in m.startsWith) {
					if (bname.StartsWith(sw.ToLower())) {
						m.prefabs.Add(go);
					}
				}
			}
		}
	}
	
	void OnDisable() {
		isEditing = false;
	}

	void OnEnable() {
		ScenarioChanged();
		isEditing = true;
	}	
	
	public void SelectBuilding(JBuildingHandle building) {
		if (selectedBuilding) {
			selectedBuilding.isSelected = false;
		}
		selectedBuilding = building;
		building.isSelected = true;
		Vector3 currentPos = selectedBuilding.transform.position;
		Vector3 groundPos = FindGroundPoint(currentPos);
		adjustHeight = currentPos.y - groundPos.y;
	}
	
	public void DeselectBuilding() {
		if (selectedBuilding) {
			selectedBuilding.isSelected = false;
		}
		selectedBuilding = null;
		adjustHeight = 0f;
	}
	
	void HandleEditor(int id) {
//		GUISkin skin = JWinMgr.self.skin;
//		GUIStyle selected = skin.GetStyle("selectedbox");
//		GUIStyle normal = skin.box;
		if (GUI.Button(new Rect(pos.width - 37, 3, 24, 12), "X")) {
			gameObject.SetActive(false);
		}
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		if (categoryIndex > 0) {
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				categoryIndex--;
				buildingIndex = 0;
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.Label(mapping[categoryIndex].category, GUILayout.Width(200));
		if (categoryIndex < mapping.Length - 1) {
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				categoryIndex++;
				buildingIndex = 0;
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (buildingIndex > 0) {
			if (GUILayout.Button("<", GUILayout.Width(20))) {
				buildingIndex--;
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.Label(mapping[categoryIndex].prefabs[buildingIndex].name, GUILayout.Width(200));
		if (buildingIndex < mapping[categoryIndex].prefabs.Count - 1) {
			if (GUILayout.Button(">", GUILayout.Width(20))) {
				buildingIndex++;
			}
		}
		else {
			GUILayout.Label("", GUILayout.Width(20));
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (selectedBuilding != null) {
			float newAdjustedHeight = GUILayout.HorizontalSlider(adjustHeight, -5f, 15f, GUILayout.Width(200));
			if (newAdjustedHeight != adjustHeight) {
				adjustHeight = newAdjustedHeight;
				Vector3 currentPos = selectedBuilding.transform.position;
				Vector3 groundPos = FindGroundPoint(currentPos);
				groundPos.y += adjustHeight;
				selectedBuilding.Move(data.scenario, groundPos);
			}
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUI.DragWindow();
	}
	
	Vector3 FindGroundPoint(Vector3 pos) {
		Ray ray = new Ray(pos + 1001f * Vector3.up, Vector3.down);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, 10000f, JLayers.M_TERRAIN)) {
			pos = hit.point;
		}
		return pos;
	}
	
	bool preventDrag = false;
	void Update() {
		Vector3 mousePos = Input.mousePosition;
		if (JWinMgr.InUIRect(mousePos.x, Screen.height - mousePos.y) ||
			Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) {
			return;
		}
		if (Input.GetMouseButtonDown(0)) {
			Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
			RaycastHit hit;
			if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER | JLayers.M_BUILDINGS)) {
				Vector3 point = hit.point;
				if (hit.collider.gameObject.layer == JLayers.L_BUILDINGS) {
					JBuildingHandle handle = hit.collider.gameObject.GetComponent<JBuildingHandle>();
					if ((handle != null) && (handle != selectedBuilding)) {
						SelectBuilding(handle);
						preventDrag = true;
					}
				}
				else {
					JBuildingHandle handle = JBuildingHandle.Create(data.scenario, mapping[categoryIndex].prefabs[buildingIndex], point, Vector3.one, 0f);
					SelectBuilding(handle);
					preventDrag = true;
				}
//				int x = (int) (point.x / JTerrainData.HORIZONTAL_SCALE);
//				int y = (int) (point.z / JTerrainData.HORIZONTAL_SCALE);
			}
		}
		else if (Input.GetMouseButtonUp(0)) {
			preventDrag = false;
		}
		else if (Input.GetMouseButton(0) && !preventDrag) {
			if (selectedBuilding != null) { // object selected
				Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
				RaycastHit hit;
				if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER)) {
					Vector3 point = hit.point;
					selectedBuilding.Move(data.scenario, point);
				}
			}
		}
		else if (Input.GetMouseButtonDown(1)) {
			DeselectBuilding();
		}
		if (selectedBuilding && Input.GetKeyDown(KeyCode.Comma)) {
			selectedBuilding.Rotate(-5f, 5f);
		}
		if (selectedBuilding && Input.GetKeyDown(KeyCode.Period)) {
			selectedBuilding.Rotate(5f, 5f);
		}
		if (Input.GetKeyDown(KeyCode.Delete) || Input.GetKeyDown(KeyCode.Backspace)) {
			if (selectedBuilding != null) {
				selectedBuilding.Delete();
				selectedBuilding = null;
			}
		}
	}
}
