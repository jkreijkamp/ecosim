using UnityEngine;
using System.Collections.Generic;

public class JVegetationEditor : MonoBehaviour {
		
	public JEditor editor;
	public Terrain terrain;
	
	private int id = JWinMgr.idGenerator++;
	private Rect pos = new Rect(100, 100, 600, 600);
	
	private JXSuccession[] successions;	
	
	private struct Info {
		public Vector2 scrollPos;
		public List<JXSuccession> expanded;
		public JXSuccession isDeletingSuccession;
		public JXVegetation isDeletingVegetation;
		
	};
	
	private Info info;

	void ScenarioChanged() {
		if (editor.scenario != null) {
			successions = editor.scenario.successions;
		}
		else {
			successions = null;
		}
	}
	
	void Start() {
		info.expanded = new List<JXSuccession>();
		info.isDeletingSuccession = null;
		info.isDeletingVegetation = null;		
	}

	void OnEnable() {
		if (editor.scenario != null) {
			successions = editor.scenario.successions;
		}
		else {
			successions = null;
		}
		if (successions == null) return;
		JWinMgr.MoveToFront(id);
	}
	
	void OnGUI() {
		if (successions == null) return;
		GUI.skin = JWinMgr.self.skin;
		pos = JWinMgr.Window(id, pos, HandleEditor, "Vegetatie tegels", JWinMgr.self.skin.window);
	}
	
	void HandleEditor(int id) {
		if (GUI.Button(new Rect(pos.width - 37, 3, 24, 12), "X")) {
			gameObject.SetActive(false);
		}
		info.scrollPos = GUILayout.BeginScrollView(info.scrollPos, false, true);
		GUILayout.BeginVertical();
		foreach (JXSuccession s in successions) {
			GUILayout.BeginHorizontal();
			HandleSuccession(s);
			GUILayout.EndHorizontal();
		}
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Voeg Successie toe", GUILayout.Width(140))) {
			JXSuccession s = new JXSuccession();
			s.name = "Unnamed";
			s.index = successions.Length;
			JXSuccession[] list = new JXSuccession[successions.Length + 1];
			System.Array.Copy(successions, list, list.Length - 1);
			s.index = list.Length - 1;
			list[list.Length - 1] = s;
			successions = list;
			editor.scenario.successions = successions;
			editor.scenario.UpdateSuccessionIndices();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUILayout.EndScrollView();
		GUI.DragWindow();
	}

	void HandleSuccession(JXSuccession s) {
		GUISkin skin = JWinMgr.self.skin;
		GUIStyle normal = skin.box;

		GUILayout.BeginVertical(skin.box);
		GUILayout.BeginHorizontal();
		bool isClosed = !info.expanded.Contains(s);
		if (GUILayout.Button(isClosed?editor.expandTex:editor.collapseTex, normal, GUILayout.Width(20), GUILayout.Height(20))) {
			isClosed = !isClosed;
			if (isClosed) {
				info.expanded.Remove(s);
			}
			else {
				info.expanded.Add(s);
			}
		}
		GUILayout.Label("Successie naam:", GUILayout.Width(120));
		GUILayout.Label(s.index.ToString(), GUILayout.Width(30));
		s.name = GUILayout.TextField(s.name, GUILayout.Width(120));
		GUILayout.FlexibleSpace();
		if (info.isDeletingSuccession == s) {
			if (GUILayout.Button("Ja", GUILayout.Width(50))) {
				JXSuccession[] list = new JXSuccession[successions.Length - 1];
				int index = 0;
				foreach (JXSuccession ss in successions) {
					if (ss != s) {
						list[index++] = ss;
					}
				}
				successions = list;
				editor.scenario.successions = successions;
				editor.scenario.UpdateSuccessionIndices();
				info.isDeletingSuccession = null;
			}
			if (GUILayout.Button("Nee", GUILayout.Width(50))) {
				info.isDeletingSuccession = null;
			}
		}
		else if (GUILayout.Button("Verwijderen", GUILayout.Width(100))) {
			info.isDeletingSuccession = s;
		}
		GUILayout.EndHorizontal();
		if (!isClosed) {
			GUILayout.BeginVertical();
			int index = 0;
			foreach (JXVegetation tt in s.vegetation) {
				HandleVegetation(s, index++, tt);
			}
			GUILayout.EndVertical();
			if (GUILayout.Button("Nieuw vegetatietype", GUILayout.Width(140))) {
				JXVegetation[] vegetation = new JXVegetation[s.vegetation.Length + 1];
				System.Array.Copy(s.vegetation, vegetation, s.vegetation.Length);
				JXVegetation newT = new JXVegetation();
				JXTerrainTile[] tiles = new JXTerrainTile[1];
				tiles[0] = new JXTerrainTile();
				newT.tiles = tiles;
				newT.name = "Type " + vegetation.Length;
				newT.succession = s;
				vegetation[vegetation.Length - 1] = newT;
				s.vegetation = vegetation;
				editor.scenario.UpdateSuccessionIndices();
			}
		}
		
		GUILayout.EndVertical();		
	}

	void HandleVegetation(JXSuccession succession, int index, JXVegetation vegetation) {
		GUISkin skin = JWinMgr.self.skin;
		GUIStyle selected = skin.GetStyle("selectedbox");
		GUIStyle normal = skin.box;
		
		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical(GUILayout.Width(200)); // group 1
		GUILayout.Space(4);
		GUILayout.BeginHorizontal(); // name
		vegetation.name = GUILayout.TextField(vegetation.name, GUILayout.Width(160));
		GUILayout.Label(vegetation.index.ToString(), GUILayout.Width(30));
		GUILayout.EndHorizontal(); // end name

		GUILayout.BeginHorizontal(); // delete
		GUILayout.FlexibleSpace();
		if (GUILayout.Button("Regels", GUILayout.Width(80))) {
			JVegetationRules.CreateEditor(this, vegetation);
		}
		if (info.isDeletingVegetation == vegetation) {
			if (GUILayout.Button("Ja", GUILayout.Width(40))) {
				JXVegetation[] list = new JXVegetation[succession.vegetation.Length - 1];
				int index2 = 0;
				foreach (JXVegetation veg in succession.vegetation) {
					if (veg != vegetation) {
						list[index2++] = veg;
					}
				}
				succession.vegetation = list;
				editor.scenario.UpdateSuccessionIndices();
				info.isDeletingVegetation = null;
			}
			if (GUILayout.Button("Nee", GUILayout.Width(40))) {
				info.isDeletingVegetation = null;
			}
		}
		else if (GUILayout.Button("Verwijderen", GUILayout.Width(80))) {
			info.isDeletingVegetation = vegetation;
		}
		GUILayout.Label("", GUILayout.Width(30));
		GUILayout.EndHorizontal(); // end delete
		
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical(); // end group 1
		GUILayout.BeginVertical(); // icons
		GUILayout.BeginHorizontal(); // icon row
		int tindex = 0;
		foreach (JXTerrainTile tile in vegetation.tiles) {
			bool isActive = JTileEditor.EditorActive(tile);
			if (GUILayout.Button(tile.GetIcon(), isActive?selected:normal, GUILayout.Width(50), GUILayout.Height(50))) {
				if (isActive) {
					JTileEditor.RemoveEditor(tile);
				}
				else {
					JTileEditor.Create(transform.parent, vegetation, tindex);
				}
			}
			tindex++;
			if (tindex % 6 == 0) {
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal(); // end icon row
				GUILayout.BeginHorizontal(); // new icon row
			}
		}
		if ((tindex < 16) && GUILayout.Button("+", normal, GUILayout.Width(50), GUILayout.Height(50))) {
			JXTerrainTile[] newTiles = new JXTerrainTile[vegetation.tiles.Length + 1];
			System.Array.Copy(vegetation.tiles, newTiles, newTiles.Length - 1);
			newTiles[newTiles.Length - 1] = new JXTerrainTile();
			vegetation.tiles = newTiles;
			editor.scenario.UpdateSuccessionIndices();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal(); // end icon row
		GUILayout.FlexibleSpace();
		GUILayout.Space(4);
		GUILayout.EndVertical(); // end icons
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
	}
	
}
