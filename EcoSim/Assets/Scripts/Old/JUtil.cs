using UnityEngine;

public static class JUtil {
	public static string V3ToStr(Vector3 v) {
		return v.x + "," + v.y + "," + v.z;
	}
	
	public static Vector3 StrToV3(string s) {
		string[] entries = s.Split(new char[] {','});
		float x = float.Parse(entries[0]);
		float y = float.Parse(entries[1]);
		float z = float.Parse(entries[2]);
		return new Vector3(x, y, z);
	}
	
	public static Vector3[] StrToV3A(string s) {
		string[] entries = s.Split(new char[] {'|'});
		Vector3[] result = new Vector3[entries.Length];
		for (int i = 0; i < entries.Length; i++) {
			result[i] = StrToV3(entries[i]);
		}
		return result;
	}
	
	public static string V3AToStr(Vector3[] a) {
		string result = "";
		foreach (Vector3 v in a) {
			if (result != "") result += '|';
			result += V3ToStr(v);
		}
		return result;
	}
	
	public static string BAToStr(byte[] a) {
		string result = "";
		foreach (byte v in a) {
			if (result != "") result += '|';
			result += v.ToString();
		}
		return result;
	}
	
	public static byte[] StrToBA(string s) {
		string[] entries = s.Split(new char[] {'|'});
		byte[] result = new byte[entries.Length];
		for (int i = 0; i < entries.Length; i++) {
			result[i] = (byte) int.Parse(entries[i]);
		}
		return result;
	}

	public static int RndRange(ref System.Random rnd, int min, int max) {
		return (rnd.Next() % (max - min)) + min;
	}
	

	public static float RndRange(ref System.Random rnd, float min, float max) {
		return min + (max - min) * (float) rnd.NextDouble();
	}

	public static Color RndRange(ref System.Random rnd, Color min, Color max) {
		return Color.Lerp(min, max, (float) rnd.NextDouble());
	}
}