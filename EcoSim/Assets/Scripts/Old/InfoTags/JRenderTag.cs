using UnityEngine;
using System.Collections;

public class JRenderTag : MonoBehaviour {
	
	public Camera renderCamera;
	public Material backgroundColour;
	public TextMesh title;
	public TextMesh content;
	
	public const int TEX_SIZE = 256;
	static JRenderTag self;
	
	void Awake() {
		self = this;
		renderCamera.enabled = false;
	}
	
	public static void RenderTag(Color bg, string title, string contents, ref Texture2D texture) {
		if ((texture != null) && ((texture.width != TEX_SIZE) || (texture.height != TEX_SIZE))) {
			DestroyImmediate(texture);
			texture = null;
		}
		if (texture == null) {
			texture = new Texture2D(TEX_SIZE, TEX_SIZE, TextureFormat.RGB24, false);
		}
		self.title.text = title;
		self.title.font.material.color = Color.black;
		self.content.text = contents;
		self.content.font.material.color = Color.black;
		self.backgroundColour.color = bg;
		RenderTexture rt = RenderTexture.GetTemporary(TEX_SIZE, TEX_SIZE);
		self.renderCamera.targetTexture = rt;
		self.renderCamera.Render();
		RenderTexture oldRT = RenderTexture.active;
		RenderTexture.active = rt;
		texture.ReadPixels(new Rect(0, 0, TEX_SIZE, TEX_SIZE), 0, 0);
		texture.Apply();
		RenderTexture.active = oldRT;
		RenderTexture.ReleaseTemporary(rt);
		rt = null;
	}
}
