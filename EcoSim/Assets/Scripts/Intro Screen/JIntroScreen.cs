using UnityEngine;
using System.Collections;

public class JIntroScreen : MonoBehaviour {

	private bool isStarted = false;
	public GameObject spinner;
	public Texture2D banner;
	public Texture2D info;
	public Texture2D click;
	public Texture2D click2;
	
	void Awake() {
		Debug.LogWarning("Build version: " + JConfig.BUILD_VERSION_STRING);
	}
	
	void OnGUI() {
		int hheight = Screen.height / 2;
		int hwidth = Screen.width / 2;
		GUI.Label(new Rect(hwidth - 310, hheight + 33 * -3 - 154, 620, 153), banner, GUIStyle.none);
		GUI.Label(new Rect(hwidth - 310, hheight + 33 * -3, 620, 32), click, GUIStyle.none);
		GUI.Label(new Rect(hwidth - info.width / 2, hheight + 33 * 0, info.width, info.height), info, GUIStyle.none);
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!isStarted && Input.GetMouseButtonDown(0)) {
			isStarted = true;
			click = click2;
			StartCoroutine(COLoadGame());
		}
	}
	
	IEnumerator COLoadGame() {
		spinner.SetActive(true);
		yield return 0;
		Application.LoadLevelAsync("Game");
	}
}
