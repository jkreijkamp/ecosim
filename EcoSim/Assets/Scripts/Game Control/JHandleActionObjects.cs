using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JHandleActionObjects : MonoBehaviour {
	
	static JHandleActionObjects self;
	
	// Use this for initialization
	void Awake() {
		self = this;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isEditing) return;
		if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) {
			return;
		}
		Vector3 mousePos = Input.mousePosition;
		if (Input.GetMouseButtonDown(0)) {
			Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
			RaycastHit hit;
			if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER | JLayers.M_ACTIONOBJECTS)) {
				if (hit.collider.gameObject.layer == JLayers.L_ACTIONOBJECTS) {
					foreach (JScenario.ActionObject obj in actionGroup.objects) {
						if (obj.instance == hit.collider.gameObject) {
							if (!(obj.isEnabled) && !(obj.isChanged)) {
								obj.isChanged = true;
								list.Remove(obj);
								selectedList.Add(obj);
								Renderer[] rList = obj.instance.GetComponentsInChildren<Renderer>();
								foreach (Renderer r in rList) {
									if (r != null) r.enabled = true;
								}
							}
							else {
								if (selectedList.Contains(obj)) {
									selectedList.Remove(obj);
									if (!obj.isEnabled) list.Add(obj);
									obj.isChanged = false;
								}
							}
						}
					}
				}
			}
		}
	}
	
	public static int Count() {
		return self.selectedList.Count;
	}
	
	public static void StartEdit(EActionTypes action) {
		self.StartCoroutine(self.COFlashObjects(action));
	}
	
	public static void Rollback() {
		self.isEditing = false;
		UpdateObjects();
	}
	
	public static void Commit() {
		self.isEditing = false;
		foreach (JScenario.ActionObject obj in self.selectedList) {
			obj.isChanged = true;
		}
		UpdateObjects();
	}
	
	List<JScenario.ActionObject> list = new List<JScenario.ActionObject>();
	List<JScenario.ActionObject> selectedList = new List<JScenario.ActionObject>();
	JScenario.ActionGroup actionGroup;
	private bool isEditing = false;

	IEnumerator COFlashObjects(EActionTypes action) {
		isEditing = true;
		JScenario scenario = JGameControl.scenario;
		JScenario.ActionInfo info = scenario.actions[action];
		actionGroup = info.actionGroup;
		if (actionGroup != null) {
			list.Clear();
			selectedList.Clear();
			foreach (JScenario.ActionObject obj in actionGroup.objects) {
				bool enabl = obj.isEnabled;
				if (obj.isChanged) enabl = !enabl;
				if (!enabl) {
					obj.instance = (GameObject) Instantiate(scenario.extraPrefabs[obj.prefabKey],
						obj.position, Quaternion.Euler(0f, obj.rotation, 0f));
					obj.instance.transform.parent = self.transform;
					obj.instance.layer = JLayers.L_ACTIONOBJECTS;
					list.Add(obj);
				}
				else if (obj.isChanged) {
					selectedList.Add(obj);
				}
			}
			while (isEditing) {
				foreach (JScenario.ActionObject obj in list) {
					Renderer[] rList = obj.instance.GetComponentsInChildren<Renderer>();
					foreach (Renderer r in rList) {
						if (r != null) r.enabled = false;
					}
				}
				yield return new WaitForSeconds(0.25f);
				if (!isEditing) yield break;
				foreach (JScenario.ActionObject obj in list) {
					Renderer[] rList = obj.instance.GetComponentsInChildren<Renderer>();
					foreach (Renderer r in rList) {
						if (r != null) r.enabled = true;
					}
				}
				yield return new WaitForSeconds(0.25f);
			}
		}
	}
	
	public static void UpdateObjects() {
		JScenario scenario = JGameControl.scenario;
		foreach (JScenario.ActionInfo info in scenario.actions.Values) {
			JScenario.ActionGroup grp = info.actionGroup;
			if (grp != null) {
				foreach (JScenario.ActionObject obj in grp.objects) {
					if (obj.instance) Destroy(obj.instance);
					bool enabl = obj.isEnabled;
					if (obj.isChanged) enabl = !enabl;
					if (enabl) {
						obj.instance = (GameObject) Instantiate(scenario.extraPrefabs[obj.prefabKey],
							obj.position, Quaternion.Euler(0f, obj.rotation, 0f));
						obj.instance.transform.parent = self.transform;
						obj.instance.layer = JLayers.L_ACTIONOBJECTS;
					}
				}
			}
		}
	}
}
