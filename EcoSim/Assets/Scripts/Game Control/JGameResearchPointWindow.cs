using UnityEngine;
using System.Globalization;


public class JGameResearchPointWindow : JSimpleWindow {
	
	Texture2D icon;
	JScenario.ResearchInfo researchInfo;
//	int selectedTiles = 0;
	
	public bool IsValidTile(int x, int y) {
		JTerrainData data = JGameControl.scenario.data;
//		return JGameControl.scenario.data.GetVegetation(x, y).IsValidMeasure(actionInfo.action);
		return data.NeedCalculateSuccession(x, y);
	}
	
	public JGameResearchPointWindow(int x, int y, Texture2D icon, JScenario.ResearchInfo ri)
		: base(x, y) {
		this.icon = icon;
		this.researchInfo = ri;
		JResearchPointsMgr.SetAddPointMode(ri.research);
	}
	
	public override bool Render(int mx, int my, float dt) {
		// int pricePerTile = researchInfo.costPerTile;
		HandleDrag(mx, my, 329+32);
		MyGUI.CheckMouseOver(new Rect(xo, yo, 329+32, 165 + 32));
		bool overClose = (mx >= xo) && (my >= yo) && (mx < xo + 32) && (my < yo + 32);
		if (MyGUI.Button(new Rect(xo, yo, 32, 32), overClose?(JGameControl.self.closeIconActive):(JGameControl.self.closeIcon), overClose?JGameControl.styleOn:JGameControl.styleOff)) {
			JResearchPointsMgr.StopAddPointMode(false);
			return false;
		}
		MyGUI.Label(new Rect(xo + 33, yo, 32, 32), icon, skin.FindStyle("75"));
		MyGUI.Label(new Rect(xo + 33 + 32, yo, 330, 32), researchInfo.name, skin.FindStyle("ArialB16-75"));

		MyGUI.Label(new Rect(xo, yo + 33, 395, 32), "", skin.FindStyle("50"));

//		MyGUI.Label(new Rect(xo, yo + 66, 230, 32), "Geselecteerde tegels", skin.FindStyle("Arial16-50"));
//		MyGUI.Label(new Rect(xo + 231, yo + 66, 131, 32), selectedTiles.ToString(), skin.FindStyle("Arial16-50-Right"));
//		MyGUI.Label(new Rect(xo + 231 + 131 + 1, yo + 66, 32, 32), "", skin.FindStyle("50"));
//
//		MyGUI.Label(new Rect(xo, yo + 99, 230, 32), "kosten per tegel", skin.FindStyle("Arial16-50"));
//		MyGUI.Label(new Rect(xo + 231, yo + 99, 131, 32), pricePerTile.ToString("#,##0", CultureInfo.GetCultureInfo("nl-NL"))+",-",
//		          skin.FindStyle("Arial16-50-Right"));
//		MyGUI.Label(new Rect(xo + 231 + 131 + 1, yo + 99, 32, 32), "x", skin.FindStyle("Arial16-50"));
//
		MyGUI.Label(new Rect(xo, yo + 66, 230, 32), "Kosten", skin.FindStyle("Arial16-50"));
		MyGUI.Label(new Rect(xo + 231, yo + 66, 164, 32), (JResearchPointsMgr.TmpCosts).ToString("#,##0", CultureInfo.GetCultureInfo("nl-NL"))+",-",
		          skin.FindStyle("Arial16-50-Right"));
//		MyGUI.Label(new Rect(xo + 231 + 131 + 1, yo + 132, 32, 32), "=", skin.FindStyle("Arial16-50"));
//
		MyGUI.Label(new Rect(xo, yo + 99, 230, 32), "", skin.FindStyle("50"));
		
		Rect bRect = new Rect(xo + 231, yo + 99, 164, 32);
		bool hover = bRect.Contains(new Vector2(mx, my));
		if (MyGUI.Button(bRect, "Voer uit", skin.FindStyle(hover?"Arial16-W-Centre":"Arial16-75-Centre"))) {
			JResearchPointsMgr.StopAddPointMode(true);
			JGameControl.self.CalculateYearCosts();
			JGameControl.scenario.oldContext.SetValue("LASTRESEARCH", (long) researchInfo.research);
			JGameControl.scenario.context.Set("laatsteonderzoek", researchInfo.research.ToString().ToLower());
			JGameControl.scenario.context.Set("laatsteonderzoekaantal", JResearchPointsMgr.TmpCount);
			JGameControl.scenario.context.Set("laatsteonderzoeksgroep", "peilbuizen");
			JGameControl.scenario.ProcessGoals(JScenarioGoal.M_AFTER_RESEARCH);
			return false;
		}
		
		
		return true;
	}
}
