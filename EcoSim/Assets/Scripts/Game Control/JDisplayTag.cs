using UnityEngine;
using System.Collections;

public class JDisplayTag : MonoBehaviour {

	static Quaternion topRotation = Quaternion.Euler(30.0f, 0.0f, 0.0f);
	Transform myTransform;
	public JResearchPoint point;
	
	
	// Use this for initialization
	void Awake () {
		myTransform = transform;
		Camera cam = Camera.main;
		if (cam) {
			UpdateRotation(cam.transform.position, cam.transform.forward);
		}
	}
	
	public void UpdateRotation(Vector3 cameraPosition, Vector3 cameraFwd) {
		Vector3 myP = myTransform.position;
		if (cameraFwd.y < -0.9f) {
			myTransform.localRotation = topRotation;
		}
		else {
			Vector3 p = cameraPosition;
			p.y = (p.y +  3 * myP.y) / 4;
			myTransform.LookAt(p - 50 * cameraFwd);
		}
	}
	
	static int CountNL(string str) {
		return str.Split('\n').Length - 1;
	}
	
	void OnMouseEnter() {
		string str = "";
		foreach (JResearchPoint.Measurement m in point.measurements) {
			
			string subStr = m.researchType.ToString() + "Jaar " + m.year + "\n";
			if (m.msg == null) {
				subStr += "Data nog niet beschikbaar\n";
			}
			else {
//				foreach (JResearchPoint.Value v in m.values) {
//					subStr += EnumExtensions.ParamFloatToString(v.p, v.v) + "\n";
//					nl++;
//				}
				subStr += m.msg;
			}
			if (str != "") { subStr += "\n"; }
			str = subStr + str;
		}
		JResearchPointsMgr.SetMessage(point, str, CountNL(str), transform.position);
	}
	
	void OnMouseExit() {
		JResearchPointsMgr.ClearMessage(point);
	}
}
