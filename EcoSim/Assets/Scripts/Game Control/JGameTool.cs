using UnityEngine;
using System.Collections;

[System.Serializable]
public abstract class JGameTool : MonoBehaviour {

	protected float timeOut = 0;
	protected bool isShown = false;
	
	public void KeepAlive() {
		timeOut = 1f;
		isShown = true;
	}

	public void Hide() {
		isShown = false;
	}
	
	public abstract bool Render(GUISkin skin, int xo, int yo, int mx, int my, float dt);
}
