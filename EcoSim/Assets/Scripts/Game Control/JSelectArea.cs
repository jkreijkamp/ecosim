using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JSelectArea : MonoBehaviour {

	private Mesh mesh;
	public JAreaMapCell mapCell;
	private JCellData cell;
	public int cx;
	public int cy;
	private bool isChanged = false;
	private int mapIndex;
	
	void Awake() {
		mesh = new Mesh();
		GetComponent<MeshFilter>().sharedMesh = mesh;
	}
	
	public void SetupData(JTerrainData data, JAreaMapCell mapCell, int mapIndex, int cx, int cy) {
		cell = data.GetCell(cx, cy);
		this.mapCell = mapCell;
		this.mapIndex = mapIndex;
		this.cx = cx;
		this.cy = cy;
		
		mesh.Clear();
		isChanged = true;
	}

	public bool IsSelected(int x, int y) {
		return (mapCell.Get(x, y) == mapIndex);
	}
	
	public void Set(int x, int y) {
		if (mapCell.Get(x, y) != mapIndex) {
			mapCell.Set(x, y, mapIndex);
			isChanged = true;
		}
	}
	
	/**
	 * clears a tile, returns true <=> in cell are still tiles with mapIndex
	 */
	public bool Clear(int x, int y) {
		if (!(mapCell.Get(x, y) == 0)) {
			mapCell.Clear(x, y);
//			if (mapCell.GetCount(mapIndex) <= 0) {
//				Destroy(gameObject);
//				return false;
//			}
			isChanged = true;
		}
		return true;
	}
	
	void Update() {
		if (isChanged) {
			mapCell.GenerateMesh(cell, mesh);
			isChanged = false;
		}
	}
}
