using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

/**
 * Singleton MonoBehaviour class
 * is used to do succession on a scenario
 * succession is calculated in several threads.
 */
public class JDoSuccession : MonoBehaviour {
	
	public class Spawn {
		public Spawn(JSpecies species, int x, int y) { this.species = species; this.x = x; this.y = y; }
		public readonly JSpecies species;
		public readonly int x;
		public readonly int y;
	}
	
	public static bool IsDoingSuccession { get { return isDoingSuccession; } }
	
	private static JDoSuccession self;
	private const int CELL_SIZE = JTerrainData.CELL_SIZE;
	
	class Info {
		public Info(JScenario s) {
			scenario = s;
			progress = s.progress;
			data = s.data;
		}
		
		public readonly JScenario scenario;
		public readonly JScenarioProgress progress;
		public readonly JTerrainData data;
		
		public static JInventarisationResultMap[] resultMaps;
	}
	
	
	/**
	 * Data passed to DoCellSuccession when it's started as thread.
	 */
	class MyThreadData {
		public MyThreadData(Info i, int cx, int cy) { info = i; this.cx = cx; this.cy = cy; }
		public readonly Info info;
		public readonly int cx;
		public readonly int cy;
	}
	
	class MyThreadData2 {
		public MyThreadData2(JScenario scenario, JCellData cell) { this.scenario = scenario; this.cell = cell; }
		public readonly JScenario scenario;
		public readonly JCellData cell;
	}
	
	private volatile int activeThreads = 0;
	private List<Spawn> spawnList = new List<Spawn>();
	
	// Use this for initialization
	void Awake() {
		self = this;		
	}
	
	/**
	 * Calculates the handling of an action/measure.
	 * cell is the cell to process on
	 * action is the action to process
	 * successionMask is the bitmap used to determine which tiles needs handling
	 * mapCell is the map cell if the actiongroup action is part of.
	 * 
	 * Thread safe (if called with unique cells)
	 */
	int ProcessAction(JCellData cell, ref JCellData.AnimalData[,] animalData, EActionTypes action, byte[] successionMask, JAreaMapCell mapCell) {
		int actionIndex = ((int) action) & 0x0f; // index for right action in area map
		int count = 0; // keep track of nr of tiles to pay for
		int p = 0; // index into succession mask byte array
		int pp = 0x80; // mask for finding right bit
		int val = 0; // current byte in succession mask
		EAnimalTypes animalType = EAnimalTypes.UNDEFINED;
		switch (action) {
		case EActionTypes.BegrazenGallowaysExtJaarRond :
		case EActionTypes.BegrazenGallowaysExtNaBloei :
		case EActionTypes.BegrazenGallowaysIntJaarRond :
		case EActionTypes.BegrazenGallowaysIntNaBloei :
			animalType = EAnimalTypes.Galloway;
			break;
		case EActionTypes.BegrazenKoeienExtJaarRond :
		case EActionTypes.BegrazenKoeienExtNaBloei :
		case EActionTypes.BegrazenKoeienIntJaarRond :
		case EActionTypes.BegrazenKoeienIntNaBloei :
			animalType = EAnimalTypes.FrieseKoe;
			break;
		case EActionTypes.BegrazenKoniksExtJaarRond :
		case EActionTypes.BegrazenKoniksExtNaBloei :
		case EActionTypes.BegrazenKoniksIntJaarRond :
		case EActionTypes.BegrazenKoniksIntNaBloei :
			animalType = EAnimalTypes.KoniksPaard;
			break;
		case EActionTypes.BegrazenSchapenExtJaarRond :
		case EActionTypes.BegrazenSchapenExtNaBloei :
		case EActionTypes.BegrazenSchapenIntJaarRond :
		case EActionTypes.BegrazenSchapenIntNaBloei :
			animalType = EAnimalTypes.Schaap;
			break;
		default :
			break;
		}
		
		// walk over every tile in cell...
		for (int y = 0; y < CELL_SIZE; y++) {
			for (int x = 0; x < CELL_SIZE; x++) {
				if (pp == 0x80) {
					// we need to read in new byte from succession bitmap
					val = successionMask[p++];
					// and set mask to first bit
					pp = 0x01;
//					Debug.Log("@[" + x + "," + y + "] p = " + p);
				}
				else {
					// shift bit mask to look at next bit
					pp = pp << 1;
				}
				
				// check if tile supports succession and has the correct measure
				if (((val & pp) != 0) && (mapCell.Get(x, y) == actionIndex) && ((cell.GetSpecial(x, y) & 0x40) == 0)) {
					
					JXVegetation vegetation = cell.GetVegetation(x, y);
					if (vegetation.IsValidMeasure(action)) {
						// action is supported on tile
						count++; // so we need increase the nr of tiles to pay for
						bool clearAction = cell.CalculateSuccession(x, y, action);
						if (clearAction) {
							// measure needs to be cleared
							mapCell.Clear(x, y);
						}
						else if (animalType != EAnimalTypes.UNDEFINED) {
							int adx = x >> 2;
							int ady = y >> 2;
							if (animalData == null) {
								animalData = new JCellData.AnimalData[JTerrainData.CELL_SIZE >> 2, JTerrainData.CELL_SIZE >> 2];
							}
							if ((animalData[ady, adx] == null) || (cell.RndValue() < 0.2f)) {
								JCellData.AnimalData ad = new JCellData.AnimalData(cell, animalType, x, y);
								animalData[ady, adx] = ad;
							}
						}
						
					}
					else {
						// action isn't supported on tile anymore
						mapCell.Clear(x, y);
					}
				}
			}
		}
		return count;
	}
	
	/**
	 * Calculates the standard (no measures/actions) part of succession
	 * 
	 * Thread safe (if called with unique cells)
	 */
	int ProcessNormalSuccession(JCellData cell, byte[] successionMask) {
		int count = 0; // nr of cells we have calculated succession on
		int p = 0; // index into succession bitmap
		int pp = 0x80; // bit mask (start with 0x80 instead of 0x01 to force reading in new byte)
		int val = 0; // current byte of succession mask
		for (int y = 0; y < CELL_SIZE; y++) {
			for (int x = 0; x < CELL_SIZE; x++) {
				if (pp == 0x80) {
					// we need to read in new byte from succession bitmap
					val = successionMask[p++];
					// and set mask to first bit
					pp = 0x01;
				}
				else {
					// shift bit mask to look at next bit
					pp = pp << 1;
				}
				if ((val & pp) != 0) {
//					JXVegetation vegetation = cell.GetVegetation(x, y);
					if ((cell.GetSpecial(x, y) & 0xc0) == 0) {
						// not already done succession for this year (0x40) and not in stable state (0x80)
						count++;
						cell.CalculateSuccession(x, y, EActionTypes.GeenActie);
					}
				}
			}
		}
		return count;
	}
	
	int ProcessSpecies(JScenario scenario, JCellData cell, JSpecies species, byte[] successionMask) {
		int speciesIndex = species.index;
		int count = 0; // nr of cells we have calculated succession on
		int p = 0; // index into succession bitmap
		int pp = 0x80; // bit mask (start with 0x80 instead of 0x01 to force reading in new byte)
		int val = 0; // current byte of succession mask
		List<Spawn> localSpawnList = new List<Spawn>();
		for (int y = 0; y < CELL_SIZE; y++) {
			for (int x = 0; x < CELL_SIZE; x++) {
				if (pp == 0x80) {
					// we need to read in new byte from succession bitmap
					val = successionMask[p++];
					// and set mask to first bit
					pp = 0x01;
				}
				else {
					// shift bit mask to look at next bit
					pp = pp << 1;
				}
				if ((val & pp) != 0) {
					// we are on a tile that allows succession
					int speciesCount = cell.GetSpeciesValue(x, y, speciesIndex);
					if (speciesCount > 0) {
						// we have speciesCount of species on this tile
						JXVegetation vegetation = cell.GetVegetation(x, y);
						int increase = 0;
						foreach (JSpecies.Rule r in species.rules) {
							if (cell.RndRange(0.0f, 1.0f) <= r.chance) {
								bool foundVeg = (r.vegetations.Length == 0);
								foreach (JSpecies.Vegetation rVeg in r.vegetations) {
									if (rVeg.isCompatible(vegetation)) {
										foundVeg = true;
										break;
									}
								}
								if (foundVeg) {
									bool paramsMatches = true;
									foreach (JSpecies.Parameter rPar in r.conditions) {
										float curVal = ((float) cell.GetData(rPar.type, x, y)) / 255f;
										if ((curVal < rPar.min) || (curVal > rPar.max)) {
											paramsMatches = false;
											break;
										}
									}
									if (paramsMatches) {
										increase += r.delta;
									}
								}
							}
						}
						int newSpeciesCount = Mathf.Clamp(speciesCount + increase, 0, 3);
						count += (newSpeciesCount > 0)?1:0;
						if (newSpeciesCount != speciesCount) {
							cell.SetSpeciesValue(x, y, speciesIndex, newSpeciesCount);
						}
						if (newSpeciesCount > 0) {
							for (int i = species.spawnCount + newSpeciesCount * species.spawnMultiplier; i > 0; i--) {
								float angle = cell.RndRange(0f, 360f);
								float range = cell.RndRange(1f, species.spawnRadius);
								int rx = Mathf.RoundToInt(Mathf.Sin(angle) * range) + x + cell.xOffset;
								int ry = Mathf.RoundToInt(Mathf.Cos(angle) * range) + y + cell.yOffset;
								if ((rx >= 0) && (ry >= 0) && (rx < scenario.data.width) && (ry < scenario.data.height)) {
									Spawn s = new Spawn(species, rx, ry);
									localSpawnList.Add(s);
								}								
							}
						}
					}
				}
			}
		}
		if (localSpawnList.Count > 0) {
			lock(spawnList) {
				spawnList.AddRange(localSpawnList);
			}
		}
		return count;
	}
	
	void DoInventarisation(int cx, int cy, JCellData cell, JAreaMapCell mapCell) {
		for (int i = 1; i < Info.resultMaps.Length; i++) {
			if ((Info.resultMaps[i] != null) && (mapCell.GetCount(i) > 0)) {
				JAreaMapCell resultCell = Info.resultMaps[i].GetCell(cx, cy);
				if (i > 8) {
					// animal species
					for (int y = 0; y < CELL_SIZE; y++) {
						for (int x = 0; x < CELL_SIZE; x++) {
							if (mapCell.Get(x, y) == i) {
								resultCell.Set(x, y, cell.GetAnimalSpeciesValue(x, y, i - 9) + 1);
							}
						}
					}
				}
				else {
					// plant species
					for (int y = 0; y < CELL_SIZE; y++) {
						for (int x = 0; x < CELL_SIZE; x++) {
							if (mapCell.Get(x, y) == i) {
								resultCell.Set(x, y, cell.GetSpeciesValue(x, y, i - 1) + 1);
							}
						}
					}
				}
			}
		}
	}
	
	void DoUpdateWaterMap(System.Object threadData) {
		MyThreadData2 td2 = (MyThreadData2) threadData;
		JCellData cell = td2.cell;
		JTerrainData data = td2.scenario.data;
		try {
			int offsetX = cell.xOffset;
			int offsetY = cell.yOffset;
			byte[] successionMask = cell.GetSuccessionBitmap();
			int p = 0; // index into succession bitmap
			int pp = 0x80; // bit mask (start with 0x80 instead of 0x01 to force reading in new byte)
			int val = 0; // current byte of succession mask
			for (int y = 0; y < CELL_SIZE; y++) {
				for (int x = 0; x < CELL_SIZE; x++) {
					if (pp == 0x80) {
						// we need to read in new byte from succession bitmap
						val = successionMask[p++];
						// and set mask to first bit
						pp = 0x01;
					}
					else {
						// shift bit mask to look at next bit
						pp = pp << 1;
					}
					if ((val & pp) != 0) {
						data.UpdateAdjustedWaterMap(x + offsetX, y + offsetY);
					}
				}
			}
		}
		catch (System.Exception e) {
			Debug.LogException(e);
		}
		lock (this) {
			activeThreads--;
		}
	}
	
	/**
	 * DoCellSuccession calculates succession on a cell, including measures/actions and handle research
	 * The DoCellSuccession is normally started as new thread, so it gets its data passed as a
	 * System.Object and is casted to MyThreadData.
	 * 
	 * Thread safe (if called with unique cells)
	 */
	void DoCellSuccession(System.Object infoO) {
		long costForCell = 0;
		MyThreadData pos = (MyThreadData) infoO;
		Info info = pos.info;
		try {
			JCellData cell = info.data.GetCell(pos.cx, pos.cy);
			cell.ClearSpecialWithMask(0xbf); // clear bit that indicates succession is done for this year
			byte[] successionMask = cell.GetSuccessionBitmap();
			
			if (info.progress != null) {
				// we start by doing the inventarisations, if we need to do them that is...
				if (info.progress.HasInventarisationMap()) {
					JInventarisationMap inventarisationMap = info.progress.GetInventarisationMap();
					JAreaMapCell mapCell = inventarisationMap.GetCell(pos.cx, pos.cy);
					DoInventarisation(pos.cx, pos.cy, cell, mapCell);
				}
				// we'll walk over all action groups 
				JCellData.AnimalData[,] animalData = null;
				if (info.progress.allowMeasures) {
					foreach (EActionGroups ag in System.Enum.GetValues(typeof(EActionGroups))) {
						// check if we have actions/measures for given group
						if (info.progress.HasMeasureMap(ag)) {
							JMeasureMap map = info.progress.GetMeasureMap(ag);
							JAreaMapCell mapCell = map.GetCell(pos.cx, pos.cy);
							// now check if we have measures of this group for the cell we're working on
							if (!mapCell.IsEmpty()) {
								// now go over all actions/measures in this action group
								foreach (EActionTypes at in EnumExtensions.GetActionTypesForGroup(ag)) {
									int mapIndex = ((int) at) & 0x0f; // only lower 4 bits are used for index in map
									if (mapCell.GetCount(mapIndex) > 0) {
										// we have tiles with the measure/action, we need to do something now :-)
										int count = ProcessAction(cell, ref animalData, at, successionMask, mapCell);
									
										// add cost to total cost for this succession year
										costForCell += count * info.scenario.GetActionInfo(at).costPerTile;
									}
								}
							}
						}
					}
				}
				if (animalData == null) {
					cell.SetNewSuccessionAnimals(null);
				}
				else {
					List<JCellData.AnimalData> animalList = new List<JCellData.AnimalData>();
					foreach (JCellData.AnimalData ad in animalData) {
						if (ad != null) {
							animalList.Add(ad);
						}
					}
					cell.SetNewSuccessionAnimals(animalList);
				}
			}
			if (info.scenario.normalSuccessionActive) {
				ProcessNormalSuccession(cell, successionMask);
			}
			if (info.progress != null) {
				foreach (JSpecies species in info.scenario.species) {
					ProcessSpecies(info.scenario, cell, species, successionMask);
				}
			}
		}
		catch (System.Exception e) {
			Debug.LogError(e.Message + " @ " + e.StackTrace);
		}
		lock (this) {
			if (info.progress != null) {
				info.progress.budget -= costForCell;
			}
			activeThreads--;
		}
	}
	
	private void SpawnSpecies(JScenario scenario) {
		JTerrainData data = scenario.data;
		foreach (Spawn spawn in spawnList) {
			int x = spawn.x;
			int y = spawn.y;
			JSpecies sp = spawn.species;
			if ((data.GetSpecies(x, y, sp.index) == 0) && (data.NeedCalculateSuccession(x, y))) {
				JXVegetation vegetation = data.GetVegetation(x, y);
				bool doSpawn = false;
				foreach (JSpecies.Rule r in sp.rules) {
					if ((r.canSpawn) && (r.chance >= Random.value)) {
						bool rightVeg = (r.vegetations.Length == 0);
						foreach (JSpecies.Vegetation v in r.vegetations) {
							if (v.isCompatible(vegetation)) {
								rightVeg = true;
								break;
							}
						}
						if (rightVeg) {
							doSpawn = true;
							foreach (JSpecies.Parameter p in r.conditions) {
								float v = ((float) data.GetData(p.type, x, y)) / 255f;
								if ((p.min > v) || (p.max < v)) {
									doSpawn = false;
									break;
								}
							}
						}
						if (doSpawn) break;
					}
				}
				if (doSpawn) {
					data.SetSpecies(x, y, sp.index, 1);
				}
			}
		}
		spawnList.Clear();
	}
	
	private static bool isDoingSuccession = false;
	
	public static bool StartSuccession(JScenario scenario) {
		if (isDoingSuccession || (!self)) return false;
		isDoingSuccession = true;
		self.StartCoroutine(self.COSuccession(scenario));
		return true;
	}
	/**
	 * Coroutine that is started when doing succession
	 * routine will run until succession calculations are finished
	 */
	IEnumerator COSuccession(JScenario scenario) {
		Info sinfo = new Info(scenario);
		bool hasProgress = (scenario.progress != null);
		yield return 0;
		
//		JTerrainData data = scenario.data;
		if (hasProgress) {
			foreach (JScenario.ActionInfo info in scenario.actions.Values) {
				JScenario.ActionGroup grp = info.actionGroup;
				if (grp != null) {
					EcoBaseFunction influenceFn = null;
					scenario.context.functions.TryGetValue(info.action.ToString() + "Invloed", out influenceFn);
					EcoBaseFunction objectFn = null;
					scenario.context.functions.TryGetValue(info.action.ToString() + "Item", out objectFn);
					int index = 0;
					foreach (JScenario.ActionObject obj in grp.objects) {
						
						if (obj.isChanged) {
							obj.isEnabled = !obj.isEnabled;
							if (obj.isEnabled) {
								// we placed the object, we have to pay for it...
								scenario.progress.budget -= info.costPerTile;
							}
							obj.isChanged = false;
							if (objectFn != null) {
								try {
									objectFn.Execute(scenario.context, new EcoNumberValue(index), EcoBoolValue.Cast(obj.isEnabled));
								}
								catch (System.Exception e) {
									Debug.LogException(e);
								}
							}
							if (influenceFn != null) {
								foreach (InfluenceCoordinate c in obj.influence) {
									try {
										influenceFn.Execute(scenario.context, new EcoTileVar(scenario.context, c.x, c.y, "__internal__"), new EcoNumberValue(index), new EcoNumberValue(c.v), EcoBoolValue.Cast(obj.isEnabled));
									}
									catch (System.Exception e) {
										Debug.LogException(e);
									}
								}
							}
						}
						index++;
					}
				}
			}
		}
		if (hasProgress) {
			// handle animal species
			foreach (JAnimalSpecies species in scenario.animalSpecies) {
				// first create distancemap
				JDistanceMap distanceMap = new JDistanceMap(scenario, species);
				
				// calc where animals walk and live...
				distanceMap.CalcOccurance();
			}
		}
		
		// make all inventarisation map results... (will be filled with data later...)
		if (hasProgress && (scenario.progress.HasInventarisationMap())) {
			Info.resultMaps = new JInventarisationResultMap[16];
			JInventarisationMap inventarisationMap = scenario.progress.GetInventarisationMap();
			for (int i = 1; i < 16; i++) {
				int invCount = inventarisationMap.Count(i);
				if (invCount > 0) {
					// map will be filled in DoCellSuccession
					Info.resultMaps[i] = new JInventarisationResultMap(scenario, (EResearchTypes) (0x20 + i), scenario.progress.year);
					scenario.progress.budget -= scenario.research[(EResearchTypes)(i + 0x20)].costPerTile * invCount;
				}
			}
		}
		for (int cy = 0; cy < sinfo.data.cHeight; cy++) {
			for (int cx = 0; cx < sinfo.data.cWidth; cx++) {
				long costForCell = JResearchPointsMgr.DoSuccessionAtCell(cx, cy);
				lock (this) {
					if (scenario.progress != null) {
						scenario.progress.budget -= costForCell;
					}
				}
				

				if (sinfo.data.CellNeedsCalculateSuccession(cx, cy)) {
					lock(this) {
						activeThreads++;
					}
					MyThreadData info = new MyThreadData(sinfo, cx, cy);
					ThreadPool.QueueUserWorkItem(new WaitCallback(self.DoCellSuccession), info);
				}
				yield return 0;
				while (activeThreads > 4) yield return 0;
			}
		}
		while (activeThreads > 0) yield return 0;
		foreach (JCellData cell in sinfo.data.cells) {
			if (cell.NeedCalculateSuccession()) {
				lock(this) {
					activeThreads++;
				}
				MyThreadData2 info = new MyThreadData2(scenario, cell);
				ThreadPool.QueueUserWorkItem(new WaitCallback(self.DoUpdateWaterMap), info);
				yield return 0;
				while (activeThreads > 4) yield return 0;
			}
		}
		while (activeThreads > 0) yield return 0;
		yield return 0;
		SpawnSpecies(scenario);
//		yield return 0;
//		GameObject placeHolder = GameObject.Find("ObjectHolder");
//		for (int cy = 0; cy < sinfo.data.cHeight; cy++) {
//			for (int cx = 0; cx < sinfo.data.cWidth; cx++) {
//				if (sinfo.data.CellNeedsCalculateSuccession(cx, cy)) {
//					JCellData cell = sinfo.data.GetCell(cx, cy);
//					if (cell.isVisible) {
//						cell.UpdateShownSuccessionAnimals(placeHolder);
//					}
//				}
//			}
//		}
		if (hasProgress && scenario.progress.HasInventarisationMap()) {
			scenario.progress.ClearInventarisationMap();
			foreach (JInventarisationResultMap resultMap in Info.resultMaps) {
				if (resultMap != null) {
					scenario.progress.AddInventarisationResultMap(resultMap);
				}
			}
			Info.resultMaps = null;
		}
		yield return 0;
		
//		JCamera.ForceRender();
//		yield return 0;
		isDoingSuccession = false;
	}
}
