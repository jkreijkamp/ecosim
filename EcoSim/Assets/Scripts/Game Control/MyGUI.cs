using UnityEngine;

public static class MyGUI {
	public static bool mouseIsOverGUI; 
	
	public static bool CheckMouseOver(Rect position) {
		position.width += 1;
		position.height += 1;
		bool overPosition = position.Contains(Event.current.mousePosition);
		mouseIsOverGUI |= overPosition;
		return overPosition;
	}
	
	public static bool Button(Rect position, string text) {
		CheckMouseOver(position);
		return GUI.Button(position, text);
	}
	
	public static bool Button(Rect position, Texture image) {
		CheckMouseOver(position);
		return GUI.Button(position, image);
	}

	public static bool Button(Rect position, string text, GUIStyle style) {
		CheckMouseOver(position);
		return GUI.Button(position, text, style);
	}
	
	public static bool Button(Rect position, Texture image, GUIStyle style) {
		CheckMouseOver(position);
		return GUI.Button(position, image, style);
	}
	
	
	public static void Label(Rect position, string text) {
		CheckMouseOver(position);
		GUI.Label(position, text);
	}
	
	public static void Label(Rect position, Texture image) {
		CheckMouseOver(position);
		GUI.Label(position, image);
	}

	public static void Label(Rect position, string text, GUIStyle style) {
		CheckMouseOver(position);
		GUI.Label(position, text, style);
	}
	
	public static void Label(Rect position, Texture image, GUIStyle style) {
		CheckMouseOver(position);
		GUI.Label(position, image, style);
	}
	
	public static string TextField(Rect position, string text, GUIStyle style) {
		CheckMouseOver(position);
		return GUI.TextField(position, text, style);
	}
	
	
}
