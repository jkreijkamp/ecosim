using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class JDisplayAreas : MonoBehaviour {
	
	public GameObject displayAreaPrefab;
	
	public JTerrainData data;
	
	public static JDisplayAreas self;

	private JDisplayArea[,] areas;
		
	public Material baseMaterial;
	
	private Material mat;
	
	void Awake() {
		self = this;
	}

	JDisplayArea MakeAreaGO(JAreaMapCell mapCell, int cx, int cy) {
		GameObject go = (GameObject) Instantiate(self.displayAreaPrefab);
		go.transform.parent = self.transform;
		go.transform.localPosition = new Vector3(cx * JTerrainData.CELL_SIZE, 0f, cy * JTerrainData.CELL_SIZE);
		go.transform.localScale = Vector3.one;
		JDisplayArea area = go.GetComponent<JDisplayArea>();
		area.SetupData(data, mapCell, cx, cy);
		area.SetMaterial(mat);
		self.areas[cy, cx] = area;
		return area;
	}
	
	public static void SetupDisplayAreas(Texture2D tex, JAreaMap map) {
		self.mat = new Material(self.baseMaterial);
		if (tex != null) {
			self.mat.mainTexture = tex;
		}
		self.data = JGameControl.scenario.data;
		// self.displayAreaPrefab = prefab;
		
		JTerrainData data = JGameControl.scenario.data;
		self.areas = new JDisplayArea[data.cWidth, data.cHeight];
		for (int cy = 0; cy < data.cHeight; cy++) {
			for (int cx = 0; cx < data.cWidth; cx++) {
				JAreaMapCell mapCell = map.GetCell(cx, cy);
				if (!mapCell.IsEmpty()) {
					self.MakeAreaGO(mapCell, cx,cy);
				}
			}
		}
		self.gameObject.SetActive(true);
	}
	
	public static void Cleanup() {
		foreach (Transform t in self.transform) {
			Destroy(t.gameObject);
		}
		self.areas = null;
		self.gameObject.SetActive(false);
	}
	
}
