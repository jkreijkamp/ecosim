using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JDisplayArea : MonoBehaviour {

	private Mesh mesh;
	
	void Awake() {
		mesh = new Mesh();
		GetComponent<MeshFilter>().sharedMesh = mesh;
	}
	
	public void SetupData(JTerrainData data, JAreaMapCell mapCell, int cx, int cy) {
		mapCell.GenerateMeshShowOnePlus(data.GetCell(cx, cy), mesh);
		Destroy(this); // we don't need the script anymore after rendering
	}
	
	public void SetMaterial(Material mat) {
		GetComponent<MeshRenderer>().sharedMaterial = mat;
	}	
}
