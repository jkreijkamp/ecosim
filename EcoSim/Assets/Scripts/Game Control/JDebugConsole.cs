using UnityEngine;
using System.Collections.Generic;

public class JDebugConsole : MonoBehaviour {
	
	bool isDebug = false;
	bool isConsole = false;
	bool isRules = false;
	bool isGroundData = false;
	bool isEcoScript = false;
	Rect winDebugR;
	Rect winConsoleR;
	Rect winRulesR;
	Rect winGroundDataR;
	Rect winEcoScriptR;
	
	public GUISkin skin;
	public Texture2D expandTex;
	public Texture2D collapseTex;
	public Texture2D warningTex;
	
	public JEditor editor;
	
	private GUIStyle box;
	
	void Start() {
		box = skin.box;
	}
	
	void OnGUI() {
		GUI.skin = skin;
		if (isDebug) {
			winDebugR = GUI.Window(1000, winDebugR, DebugWindow, "Debug");
		}
		if (isConsole) {
			winConsoleR = GUI.Window(1001, winConsoleR, ConsoleWindow, "Console");
		}
		if (isRules) {
			winRulesR = GUI.Window(1002, winRulesR, RulesWindow, "Regels");
		}
		if (isGroundData) {
			winGroundDataR = GUI.Window(1003, winGroundDataR, GroundDataWindow, "Grond waarden");
		}
		if (isEcoScript) {
			winEcoScriptR = GUI.Window(1004, winEcoScriptR, EcoScriptWindow, "EcoScript");
		}
	}
	
	Vector2 scroll;
	string expression = "";
	string msg = "";
	long resultVal = 0;
	string groundDataString = "";
	int lastX = -1;
	int lastY = -1;
	
	Vector2 ecoScroll;
	Vector2 ecoScroll2;
	string script = "";
	string block = "";
	string error = "";
	
	void EcoScriptWindow(int winId) {
		if (GUI.Button(new Rect(winEcoScriptR.width - 37, 3, 24, 12), "X")) {
			ToggleEcoScript();
		}
		MyGUI.CheckMouseOver(winEcoScriptR);
		GUILayout.BeginVertical();
		ecoScroll = GUILayout.BeginScrollView(ecoScroll, GUILayout.Height(250));
		script = GUILayout.TextArea(script, GUILayout.MinHeight(250));
		GUILayout.EndScrollView();
		ecoScroll2 = GUILayout.BeginScrollView(ecoScroll2);
		block = GUILayout.TextArea(block, GUILayout.MinHeight(300));
		GUILayout.EndScrollView();
		if (GUILayout.Button("Test", GUILayout.Width(100))) {
			EcoScript es = new EcoScript();
			EcoContext context = JGameControl.scenario.context;
			context.PopAll();
			EcoBlock b = null;
			try {
				es.Parse(context, script, true);
			}
			catch (EcoException e) {
				error = "Parsing script: " + e.Message;
				throw e;
			}
			try {
				b = es.ParseBlock(context, block);
			}
			catch (EcoException e) {
				error = "Parsing block: " + e.Message;
				throw e;
			}
			try {
				b.Execute(context);
				error = "Block executed";
			}
			catch (EcoException e) {
				error = "Execute block: " + e.Message;
				throw e;
			}
		}
		GUILayout.Label(error);
		GUILayout.EndVertical();
		GUI.DragWindow();
	}
	
	string isFocus = "";
	string isFocusText = null;
	Vector2 debugScroll;
	
	void DebugWindow(int winId) {
		if (GUI.Button(new Rect(winDebugR.width - 37, 3, 24, 12), "X")) {
			ToggleDebug();
		}
		MyGUI.CheckMouseOver(winDebugR);
		JScenario scenario = JGameControl.scenario;
		if ((scenario == null) || (scenario.progress == null)) {
			isDebug = false;
			return;
		}
		debugScroll = GUILayout.BeginScrollView(debugScroll);
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		GUILayout.Label("jaar", GUILayout.Width(90));
		JTextField.IntField("ProgressYear", ref scenario.progress.year, GUILayout.Width(90));
		GUILayout.FlexibleSpace();
		GUILayout.Label("budget", GUILayout.Width(90));
		JTextField.LongField("ProgressBudget", ref scenario.progress.budget, GUILayout.Width(90));
		GUILayout.EndHorizontal();

		EcoContext context = scenario.context;
		string newIsFocus = GUI.GetNameOfFocusedControl();
		if (newIsFocus != isFocus) {
			isFocusText = null;
			isFocus = newIsFocus;
		}
		foreach (KeyValuePair<string, EcoVar> kv in context.variables[0]) {
			string text = kv.Value.GetValue().StringRepresentation();
			if (text != null) {
				GUILayout.BeginHorizontal();
				GUILayout.Label(kv.Key, GUILayout.Width(90));
				if ((isFocus == kv.Key) && (isFocusText != null)) {
					text = isFocusText;
				}
				GUI.SetNextControlName(kv.Key);
				string newText = GUILayout.TextField(text, GUILayout.MinWidth(200));
				if (isFocus == kv.Key) {
					isFocusText = newText;
				}
				if (text != newText) {
					EcoScript es = new EcoScript();
					try {
						kv.Value.SetValue(null, null, es.ParseValue(newText));
					}
					catch (System.Exception) {
						GUILayout.Label(warningTex);
					}
				}
				GUILayout.EndHorizontal();
			}
		}
		
		if (msg != "") GUILayout.Label(msg);
		GUILayout.EndVertical();
		GUILayout.EndScrollView();
		GUI.DragWindow();
	}
	
	
	void DebugWindowOld(int winId) {
		if (GUI.Button(new Rect(winDebugR.width - 37, 3, 24, 12), "X")) {
			ToggleDebug();
		}
		MyGUI.CheckMouseOver(winDebugR);
		JScenario scenario = JGameControl.scenario;
		if ((scenario == null) || (scenario.progress == null)) {
			isDebug = false;
			return;
		}
		GUILayout.BeginVertical();
		scroll = GUILayout.BeginScrollView(scroll);
		GUILayout.BeginHorizontal();
		GUILayout.Label("jaar", GUILayout.Width(90));
		JTextField.IntField("ProgressYear", ref scenario.progress.year, GUILayout.Width(90));
		GUILayout.FlexibleSpace();
		GUILayout.Label("budget", GUILayout.Width(90));
		JTextField.LongField("ProgressBudget", ref scenario.progress.budget, GUILayout.Width(90));
		GUILayout.EndHorizontal();
		int count = 0;
		foreach (KeyValuePair<string, long> kvp in scenario.oldContext.vars) {
			if ((count % 2) == 0) {
				if (count > 0) GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
			}
			if (GUILayout.Button("-", GUILayout.Width(20))) {
				scenario.oldContext.vars.Remove(kvp.Key);
				break;
			}
			GUILayout.Label(kvp.Key, GUILayout.Width(90));
			long val = kvp.Value;
			JTextField.LongField("Var"+kvp.Key, ref val, GUILayout.Width(70));
			if (val != kvp.Value) scenario.oldContext.SetValue(kvp.Key, val);
			if ((count % 2) == 0) GUILayout.FlexibleSpace();
			count++;
		}
		if (count > 0) GUILayout.EndHorizontal();
		GUILayout.EndScrollView();
		expression = GUILayout.TextArea(expression, GUILayout.Width(380), GUILayout.Height(60));
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Test expressie")) {
			msg = "";
			try {
				resultVal = JExpression.ParseExpression(expression, scenario.oldContext, false);
			}
			catch (System.Exception e) {
				msg = e.Message;
			}
		}
		GUILayout.Label(resultVal.ToString(), GUILayout.Width(80));
		GUILayout.EndHorizontal();
		if (msg != "") GUILayout.Label(msg);
		GUILayout.EndVertical();
		GUI.DragWindow();
	}
	
	string lastError = "";
	
	void HandleGoalAction(JScenario scenario, JScenarioGoal.Action action) {
		GUILayout.Label("artikel");
		JTextField.IntField(action, ref action.articleId, GUILayout.Width(30));
		GUILayout.Label("hint");
		string hintStr = (action.hintStr == null)?"":action.hintStr;
		hintStr = GUILayout.TextField(hintStr, GUILayout.Width(472));
		if (hintStr == "") action.hintStr = null;
		else action.hintStr = hintStr;
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Ken waarde toe:");
		string varName = (action.counterName==null)?"":action.counterName;
		varName = GUILayout.TextField(varName, GUILayout.Width(100)).Trim();
		if (varName != "") {
			action.counterName = varName;
			string expression = (action.expression == null)?"":(action.expression);
			GUILayout.Label(" = ");
			expression = GUILayout.TextField(expression, GUILayout.Width(430));
			if (GUILayout.Button("Test")) {
				try {
					JExpression.ClearCache();
					long val = JExpression.ParseExpression(expression, scenario.oldContext, false);
					lastError = "Expressie valide, test waarde " + val;
				}
				catch (System.Exception e) {
					lastError = e.Message;
					throw e;
				}				
			}
			action.expression = expression;
		}
		else {
			action.counterName = null;
			action.expression = null;
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		action.removeGoal = GUILayout.Toggle(action.removeGoal, "Verwijder regel");
		action.endGame = GUILayout.Toggle(action.endGame, "Beeindig spel");
	}

	List<JScenarioGoal> expandGoal = new List<JScenarioGoal>();
	
	void HandleProgress(JScenario scenario) {
		GUILayout.BeginVertical();
		int count = 0;
		foreach (JScenarioGoal goal in scenario.goals) {
			GUILayout.BeginVertical(box);
			GUILayout.BeginHorizontal();
			bool isClosed = !expandGoal.Contains(goal);
			if (GUILayout.Button(isClosed?expandTex:collapseTex, box, GUILayout.Width(20), GUILayout.Height(20))) {
				isClosed = !isClosed;
				if (isClosed) {
					expandGoal.Remove(goal);
				}
				else {
					expandGoal.Add(goal);
				}
			}
			if (GUILayout.Button("-", GUILayout.Width(20))) {
				scenario.goals.Remove(goal);
				break;
			}
			if (count > 0) {
				if (GUILayout.Button("^", GUILayout.Width(20))) {
					scenario.goals[count] = scenario.goals[count - 1];
					scenario.goals[count - 1] = goal;
					break;
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(20));
			}
			GUILayout.Label("#" + (++count), GUILayout.Width(60));
			string goalName = goal.name;
			if ((goalName == null) || (goalName == "")) goalName = "Regel " + count;
			goal.name = GUILayout.TextField(goalName, GUILayout.Width(491));
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
			if (!isClosed) {
				GUILayout.BeginHorizontal();
				bool beforeSuc = GUILayout.Toggle((goal.checkMask & JScenarioGoal.M_BEFORE_SUCCESSION) != 0, "Voor successie");
				bool afterSuc = GUILayout.Toggle((goal.checkMask & JScenarioGoal.M_AFTER_SUCCESSION) != 0, "Na successie");
				bool afterMeas = GUILayout.Toggle((goal.checkMask & JScenarioGoal.M_AFTER_MEASURE) != 0, "Na maatregel");
				bool afterRes = GUILayout.Toggle((goal.checkMask & JScenarioGoal.M_AFTER_RESEARCH) != 0, "Na onderzoek");
				bool afterEnc = GUILayout.Toggle((goal.checkMask & JScenarioGoal.M_AFTER_ENCYCLOPEDIA) != 0, "Na raadplegen");
				bool afterLoad = GUILayout.Toggle((goal.checkMask & JScenarioGoal.M_AFTER_LOAD) != 0, "Na laden");
				goal.checkMask = 0;
				if (beforeSuc) goal.checkMask |= JScenarioGoal.M_BEFORE_SUCCESSION;
				if (afterSuc) goal.checkMask |= JScenarioGoal.M_AFTER_SUCCESSION;
				if (afterMeas) goal.checkMask |= JScenarioGoal.M_AFTER_MEASURE;
				if (afterRes) goal.checkMask |= JScenarioGoal.M_AFTER_RESEARCH;
				if (afterEnc) goal.checkMask |= JScenarioGoal.M_AFTER_ENCYCLOPEDIA;
				if (afterLoad) goal.checkMask |= JScenarioGoal.M_AFTER_LOAD;
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
				for (int i = 0; i < goal.expressions.Length; i++) {
					GUILayout.BeginHorizontal();
					if (i > 0) {
						if (GUILayout.Button("-", GUILayout.Width(20))) {
							List<string> l = new List<string>(goal.expressions);
							l.RemoveAt(i);
							if (l.Count == 0) l.Add("");
							goal.expressions = l.ToArray();
							break;
						}
						if (GUILayout.Button("^", GUILayout.Width(20))) {
							string s = goal.expressions[i - 1];
							goal.expressions[i - 1] = goal.expressions[i];
							goal.expressions[i] = s;
							break;
						}
					}
					else {
						if (GUILayout.Button("+", GUILayout.Width(20))) {
							List<string> l = new List<string>(goal.expressions);
							l.Add("");
							goal.expressions = l.ToArray();
							break;
						}
						GUILayout.Label("", GUILayout.Width(20));
					}
					goal.expressions[i] = GUILayout.TextField(goal.expressions[i], GUILayout.Width(580));
					if (GUILayout.Button("Test")) {
						try {
							JExpression.ClearCache();
							long val = JExpression.ParseExpression(goal.expressions[i], scenario.oldContext, false);
							lastError = "Expressie valide, test waarde " + val;
						}
						catch (System.Exception e) {
							lastError = e.Message;
							throw e;
						}
					}
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
				}
				GUILayout.BeginHorizontal();
				GUILayout.Label("Succes:", GUILayout.Width(40));
				if (goal.success == null) {
					if(GUILayout.Button("Maak successactie")) {
						goal.success = new JScenarioGoal.Action();
					}
				}
				else {
					if (GUILayout.Button("-", GUILayout.Width(20))) {
						goal.success = null;
						break;
					}
					HandleGoalAction(scenario, goal.success);
				}
				
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
				GUILayout.Label("Faal:", GUILayout.Width(40));
				if (goal.failure == null) {
					if(GUILayout.Button("Maak faalactie")) {
						goal.failure = new JScenarioGoal.Action();
					}
				}
				else {
					if (GUILayout.Button("-", GUILayout.Width(20))) {
						goal.failure = null;
						break;
					}
					HandleGoalAction(scenario, goal.failure);
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
			}
			GUILayout.EndVertical();
		}
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Voeg regel toe...")) {
			JScenarioGoal goal = new JScenarioGoal();
			scenario.goals.Add(goal);
			expandGoal.Add(goal);
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label(lastError);
		GUILayout.EndVertical();
	}
	
	Vector2 rulesScroll;

	
	Dictionary<JGameRule,string> warnings = new Dictionary<JGameRule, string>();
	Dictionary<JGameRule,Vector2> expandGameRules = new Dictionary<JGameRule, Vector2>();
	string mainCompileWarning = null;
	
	void CompileScripts(JScenario scenario, bool compileMain) {
		scenario.context.PopAll();
		warnings.Clear();
		mainCompileWarning = null;
		if (compileMain) {
			mainCompileWarning = scenario.CompileScript(true);
		}
		foreach (JGameRule rule in scenario.gameRules) {
			string errstr = rule.CompileScript(scenario.context);
			if (errstr != null) {
				warnings.Add(rule, errstr);
			}
		}
	}
	
	
	bool isMainClosed = true;
	Vector2 mainScroll;
	
	void HandleGameRules(JScenario scenario) {
		GUILayout.BeginVertical();
		int count = 0;
		
		GUILayout.BeginVertical(box);
		GUILayout.BeginHorizontal();
		if (GUILayout.Button(isMainClosed?expandTex:collapseTex, box, GUILayout.Width(20), GUILayout.Height(20))) {
			isMainClosed = !isMainClosed;
		}
		if (mainCompileWarning != null) GUILayout.Label(warningTex);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		if (!isMainClosed) {
			mainScroll = GUILayout.BeginScrollView(mainScroll, GUILayout.Height(300));
			string newScript = GUILayout.TextArea(scenario.GetScript(), GUILayout.MinHeight(300));
			if (newScript != scenario.GetScript()) {
				scenario.SetScript(newScript);
				CompileScripts(scenario, true);
			}
			GUILayout.EndScrollView();
			if (mainCompileWarning != null) GUILayout.Label(mainCompileWarning);
		}
		GUILayout.EndVertical();
		
		
		foreach (JGameRule rule in scenario.gameRules) {
			GUILayout.BeginVertical(box);
			GUILayout.BeginHorizontal();
			bool isClosed = !expandGameRules.ContainsKey(rule);
			if (GUILayout.Button(isClosed?expandTex:collapseTex, box, GUILayout.Width(20), GUILayout.Height(20))) {
				isClosed = !isClosed;
				if (isClosed) {
					expandGameRules.Remove(rule);
				}
				else {
					expandGameRules.Add(rule, Vector2.zero);
				}
			}
			if (warnings.ContainsKey(rule)) GUILayout.Label(warningTex);
			if (GUILayout.Button("-", GUILayout.Width(20))) {
				scenario.gameRules.Remove(rule);
				break;
			}
			if (count > 0) {
				if (GUILayout.Button("^", GUILayout.Width(20))) {
					scenario.gameRules[count] = scenario.gameRules[count - 1];
					scenario.gameRules[count - 1] = rule;
					break;
				}
			}
			else {
				GUILayout.Label("", GUILayout.Width(20));
			}
			rule.disabled = !GUILayout.Toggle(!rule.disabled, "");
			GUILayout.Label("#" + (++count), GUILayout.Width(60));
			string ruleName = rule.ruleName;
			if ((ruleName == null) || (ruleName == "")) ruleName = "Regel " + count;
			rule.ruleName = GUILayout.TextField(ruleName, GUILayout.Width(491));
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
			if (!isClosed) {
				GUILayout.BeginHorizontal();
				bool beforeSuc = GUILayout.Toggle((rule.ruleMask & JGameRule.M_BEFORE_SUCCESSION) != 0, "Voor successie");
				bool afterSuc = GUILayout.Toggle((rule.ruleMask & JGameRule.M_AFTER_SUCCESSION) != 0, "Na successie");
				bool afterMeas = GUILayout.Toggle((rule.ruleMask & JGameRule.M_AFTER_MEASURE) != 0, "Na maatregel");
				bool afterRes = GUILayout.Toggle((rule.ruleMask & JGameRule.M_AFTER_RESEARCH) != 0, "Na onderzoek");
				bool afterEnc = GUILayout.Toggle((rule.ruleMask & JGameRule.M_AFTER_ENCYCLOPEDIA) != 0, "Na raadplegen");
				bool afterLoad = GUILayout.Toggle((rule.ruleMask & JGameRule.M_AFTER_LOAD) != 0, "Na laden");
				rule.ruleMask = 0;
				if (beforeSuc) rule.ruleMask |= JGameRule.M_BEFORE_SUCCESSION;
				if (afterSuc) rule.ruleMask |= JGameRule.M_AFTER_SUCCESSION;
				if (afterMeas) rule.ruleMask |= JGameRule.M_AFTER_MEASURE;
				if (afterRes) rule.ruleMask |= JGameRule.M_AFTER_RESEARCH;
				if (afterEnc) rule.ruleMask |= JGameRule.M_AFTER_ENCYCLOPEDIA;
				if (afterLoad) rule.ruleMask |= JGameRule.M_AFTER_LOAD;
				GUILayout.FlexibleSpace();
				if (GUILayout.Button("voer script nu uit")) {
					scenario.context.PopAll();
					string execError = rule.Execute(scenario.context);
					scenario.context.PopAll();
					if (execError != null) {
						warnings.Add(rule, execError);
					}
				}
				GUILayout.EndHorizontal();
				expandGameRules[rule] = GUILayout.BeginScrollView(expandGameRules[rule], GUILayout.Height(300));
				string newScript = GUILayout.TextArea(rule.GetScript(), GUILayout.MinHeight(300));
				if (newScript != rule.GetScript()) {
					rule.SetScript(newScript);
					CompileScripts(scenario, false);
				}
				GUILayout.EndScrollView();
				if (warnings.ContainsKey(rule)) GUILayout.Label(warnings[rule]);
			}
			GUILayout.EndVertical();
		}
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Voeg regel toe...")) {
			JGameRule rule = new JGameRule("Nieuwe regel", JGameRule.M_AFTER_SUCCESSION, "hint(\"boer\", \"Dag allemaal\", true);\nverwijderregel();", false); 
			scenario.gameRules.Add(rule);
			expandGameRules.Add(rule, Vector2.zero);
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label(lastError);
		GUILayout.EndVertical();
	}
	
	
	void RulesWindow(int winId) {
		if (GUI.Button(new Rect(winRulesR.width - 37, 3, 24, 12), "X")) {
			ToggleRules();
		}
		MyGUI.CheckMouseOver(winRulesR);
		JScenario scenario = JGameControl.scenario;
		if ((scenario == null) || (scenario.progress == null)) {
			isDebug = false;
			return;
		}
		rulesScroll = GUILayout.BeginScrollView(rulesScroll);
		HandleGameRules(scenario);
		GUILayout.EndScrollView();
		GUI.DragWindow();
	}
	
	void ConsoleWindow(int winId) {
		if (GUI.Button(new Rect(winConsoleR.width - 37, 3, 24, 12), "X")) {
			ToggleConsole();
		}
		MyGUI.CheckMouseOver(winConsoleR);
		scroll = GUILayout.BeginScrollView(scroll);
		GUILayout.BeginVertical();
		foreach (string s in MyDebug.log) {
			GUILayout.Label(s, GUILayout.Width(380));
		}
		GUILayout.EndVertical();
		GUILayout.EndScrollView();
		if (GUILayout.Button("Wis log")) {
			MyDebug.Clear();
		}
		GUI.DragWindow();
	}


	void GroundDataWindow(int winId) {
		if (GUI.Button(new Rect(winGroundDataR.width - 37, 3, 24, 12), "X")) {
			ToggleGroundData();
		}
		MyGUI.CheckMouseOver(winGroundDataR);
		scroll = GUILayout.BeginScrollView(scroll);
		GUILayout.BeginVertical();
		GUILayout.Label(groundDataString);
		GUILayout.EndVertical();
		GUILayout.EndScrollView();
		GUI.DragWindow();
	}
	
	void ToggleDebug() {
		isDebug = !isDebug;
		if (isDebug) {
			winDebugR = new Rect(Screen.width - 500, 0, 450, 400);
		}
	}
	
	void ToggleConsole() {
		isConsole = !isConsole;
		if (isConsole) {
			winConsoleR = new Rect(Screen.width - 440, 0, 400, 400);
		}
	}

	void ToggleRules() {
		isRules = !isRules;
		if (isRules) {
			winRulesR = new Rect(Screen.width - 840, 0, 800, 600);
		}
	}

	void ToggleGroundData() {
		isGroundData = !isGroundData;
		if (isGroundData) {
			winGroundDataR = new Rect(Screen.width - 840, 0, 400, 400);
		}
		else {
			lastX = -1;
		}
	}

	void ToggleEcoScript() {
		isEcoScript = !isEcoScript;
		if (isEcoScript) {
			winEcoScriptR = new Rect(10, 10, 500, 600);
		}
		else {
			lastX = -1;
		}
	}
	
	// Update is called once per frame
	void Update() {
		if (!editor) {
			if (Input.GetKeyDown(KeyCode.D) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))) {
				ToggleDebug();
			}
			if (Input.GetKeyDown(KeyCode.C) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))) {
				ToggleConsole();
			}
			if (Input.GetKeyDown(KeyCode.R) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))) {
				ToggleRules();
			}
		}
		if (Input.GetKeyDown(KeyCode.E) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))) {
			ToggleEcoScript();
		}
		if (Input.GetKeyDown(KeyCode.W) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))) {
			ToggleGroundData();
		}
		if (isGroundData) {
			if (JCamera.IsNear) {
				JScenario scenario = (editor)?(editor.scenario):(JGameControl.scenario);
				if (scenario == null) {
					ToggleGroundData();
					return;
				}
				JTerrainData data = scenario.data;
				Ray screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER)) {
					Vector3 point = hit.point;
					int x = (int) (point.x / JTerrainData.HORIZONTAL_SCALE);
					int y = (int) (point.z / JTerrainData.HORIZONTAL_SCALE);
					
					if ((x != lastX) || (y != lastY)) {
						lastX = x;
						lastY = y;
						groundDataString = "Positie " + x + "/" + y + "\n";
						int variant;
						JXVegetation vegetation = data.GetVegetation(x, y, out variant);
						groundDataString += "Successiegroep : " + vegetation.succession.name + "\n";
						groundDataString += "vegetatie : " + vegetation.name + " variatie : " + variant + "\n";
						groundDataString += "Land hoogte : " + data.GetHeight(x, y) + "\n";
						groundDataString += "Water hoogte : " + data.GetAdjustedWaterHeight(x, y) + "\n";
						
						foreach (EParamTypes t in System.Enum.GetValues(typeof(EParamTypes))) {
							if (t != EParamTypes.UNDEFINED) {
								byte v = data.GetData(t, x, y);
								int percent = (int) v * 100 / 255;
								groundDataString += t.ToString() + " : " + v + " (" + percent + "%)\n";
							}	
						}
						for (int i = 0; i < scenario.species.Length; i++) {
							int species = data.GetSpecies(x, y, i);
							if (species > 0) {
								groundDataString += scenario.species[i].name + " : " + species + "\n";
							}
						}
						groundDataString += "Speciaal : " + data.GetSpecial(x, y);
					}
				}
			}
			else {
				ToggleGroundData();
			}
		}
	}
}
