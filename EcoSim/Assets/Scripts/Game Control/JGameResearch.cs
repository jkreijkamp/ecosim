using UnityEngine;
using System.Globalization;
using System.Collections.Generic;


[System.Serializable]
public class JGameResearch : JGameTool {
	
	[System.Serializable]
	public class ResearchGroup {
		public string groupName;
		public EResearchTypes[] research;
	}
	
	public ResearchGroup[] groups;
	
	class ResearchInfo {
		public EResearchTypes research;
		public Texture2D icon;
		public Texture2D activeIcon;
		public int price;
		public string name;
		public string description;
	}
	
	class ShowGroups {
		public Texture2D icon;
		public Texture2D activeIcon;
		public ResearchInfo[] research;
	}
	
	ShowGroups[] showGroups;

	GUIStyle textStyle;
	GUIStyle textStyleSelected;
	GUIStyle priceStyle;
	GUIStyle priceStyleSelected;
	
	void Start() {
		GUISkin skin = JGameControl.self.skin;
		textStyle = skin.FindStyle("Arial16-50");
		textStyleSelected = skin.FindStyle("Arial16-W");
		priceStyle = skin.FindStyle("Arial16-50-Right");
		priceStyleSelected = skin.FindStyle("Arial16-W-Right");
	}
	
	void GameIsLoaded() {
		JScenario scenario = JGameControl.scenario;
		if (scenario != null) {
			List<ShowGroups> sg = new List<ShowGroups>();
			foreach (ResearchGroup g in groups) {
				List<ResearchInfo> newM = new List<ResearchInfo>();
				foreach (EResearchTypes er in g.research) {
					JScenario.ResearchInfo ri = scenario.GetResearchInfo(er);
					if (ri != null) {
						ResearchInfo mi = new ResearchInfo();
						mi.research = er;
						mi.price = ri.costPerTile;
						mi.name = ri.name;
						mi.description = ri.descr;
						string iconName = er.ToString();
						Texture2D icon = scenario.LoadTexure("Icons", iconName + "_w");
						Texture2D activeIcon = scenario.LoadTexure("Icons", iconName + "_zw");
						if (icon == null) {
							// Debug.LogWarning("No icon for '" + iconName + "'");
							icon =  scenario.LoadTexure("Icons", g.groupName + "_w");
							activeIcon =  scenario.LoadTexure("Icons", g.groupName + "_zw");
						}
						mi.icon = icon;
						mi.activeIcon = activeIcon;
						
						newM.Add(mi);
					}
				}
				if (newM.Count > 0) {
					ShowGroups newSG = new ShowGroups();
					newSG.icon = scenario.LoadTexure("Icons", g.groupName + "_w");
					newSG.activeIcon = scenario.LoadTexure("Icons", g.groupName + "_zw");
					newSG.research = newM.ToArray();
					sg.Add(newSG);
				}
			}
			showGroups = sg.ToArray();
		}
	}
	
	int activeGroupIndex = -1;
	
	public override bool Render(GUISkin skin, int xo, int yo, int mx, int my, float dt) {
		if (!isShown) {
			activeGroupIndex = -1;
			return false;
		}
		timeOut -= dt;
		
		int titleWidth = Mathf.Max(33 * showGroups.Length, 230);
		
		MyGUI.Label(new Rect(xo, yo - 33, titleWidth, 32), "Onderzoek", skin.FindStyle("ArialB16-50"));		
		
		int x = xo;
		int y = yo;

		bool childIsAlive = false;
		
		int overGroupIndex = -1;
		for (int i = 0; i < showGroups.Length; i++) {
			ShowGroups g = showGroups[i];
			bool hover = (x <= mx) && (mx <= x + 32) && (y <= my) && (my <= y + 32);
			
			if (hover) overGroupIndex = i;
//			if (g.measure != null) {
//				if (hover) {
//					g.measure.KeepAlive();
//				}
//				hover = g.measure.Render(skin, x, y + 33, mx, my, dt);
//				if (hover) {
//					JXGameGUI.SetHelp(g.icon, g.help, false);
//				}
//			}
			MyGUI.Label(new Rect(x, y, 32, 32), hover?(g.activeIcon):(g.icon), hover?JGameControl.styleOn:JGameControl.styleOff);			
			childIsAlive |= hover;
			x += 33;
			
		}
		if (x - xo < titleWidth) {
			MyGUI.Label(new Rect(x, y, titleWidth - x + xo, 32), "", JGameControl.styleOff);			
		}
		if (overGroupIndex >= 0) activeGroupIndex = overGroupIndex;
		if (activeGroupIndex >= 0) {
			ShowGroups g = showGroups[activeGroupIndex];
			for (int i = 0; i < g.research.Length; i++) {
				ResearchInfo m = g.research[i];
				x = xo + 33 * activeGroupIndex;
				y = yo + 33 * (i + 1);
				bool hover = (x <= mx) && (mx <= x + 32 + 290 + 100 + 2) && (y <= my) && (my <= y + 32);
				bool isClicked = false;
				isClicked |= MyGUI.Button(new Rect(x, y, 32, 32), hover?(m.activeIcon):(m.icon), hover?JGameControl.styleOn:JGameControl.styleOff);
				isClicked |= MyGUI.Button(new Rect(x + 33, y, 290, 32), m.name, hover?textStyleSelected:textStyle);
				isClicked |= MyGUI.Button(new Rect(x + 33 + 290 + 1, y, 100, 32),
					m.price.ToString("#,##0", CultureInfo.GetCultureInfo("nl-NL"))+",-",
			        hover?priceStyleSelected:priceStyle);
				childIsAlive |= hover;
				if (hover && (m.description != "")) {
					JGameControl.SetHelp(m.icon, m.description, false);
				}
				if (isClicked) {
					if ((((int) m.research) & 0xf0) == 0x20) {
						JGameControl.SetWindow(new JGameResearchWindow(33, 33, m.icon, JGameControl.scenario.GetResearchInfo(m.research)));
					}
					else {
						JGameControl.SetWindow(new JGameResearchPointWindow(33, 33, m.icon, JGameControl.scenario.GetResearchInfo(m.research)));
					}
				}
			}
		}
		
		if (childIsAlive) KeepAlive();
		
		if (timeOut < 0) {
			isShown = false;
		}
		return true;
	}

}
