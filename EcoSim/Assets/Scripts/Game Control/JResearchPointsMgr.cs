using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JResearchPointsMgr : MonoBehaviour {
	public GameObject pointPrefab;
	public GameObject projector;
	
	private Transform cameraTransform;
	private long tmpCosts = 0;
	private int tmpCount = 0;
	
	private JScenario scenario;
		
	class RPCell {
		public RPCell(JCellData cell) {
			this.cell = cell;
			researchPoints = new List<JResearchPoint>();
		}
		
		public long CalculateSuccession() {
			long total = 0;
			foreach (JResearchPoint rp in researchPoints) {
				total += rp.DoSuccession(cell, rp.x - cell.xOffset, rp.y - cell.yOffset);
			}
			return total;
		}
		
		public void DestroyAllGOs() {
			if (visibleTags != null) {
				foreach (JDisplayTag tag in visibleTags) {
					GameObject.Destroy(tag.gameObject);
				}
				visibleTags = null;
			}
		}
		
		public void CreateGO(JResearchPoint rp) {
			int x = rp.x - cell.xOffset;
			int y = rp.y - cell.yOffset;
			float height = cell.getHiResHeight(x * 4 + 2, y * 4 + 2);
			Vector3 pos = new Vector3(((float) rp.x + 0.5f) * JTerrainData.HORIZONTAL_SCALE, height, ((float) rp.y + 0.5f) * JTerrainData.HORIZONTAL_SCALE);
			GameObject go = (GameObject) Instantiate(JResearchPointsMgr.self.pointPrefab, pos, Quaternion.identity);
			go.transform.parent = JResearchPointsMgr.self.transform;
			JDisplayTag dt = go.GetComponent<JDisplayTag>();
			dt.point = rp;
			visibleTags.Add(dt);
		}
		
		public void CreateAllGOs() {
			visibleTags = new List<JDisplayTag>();
			foreach (JResearchPoint rp in researchPoints) {
				CreateGO(rp);
			}
		}
		
		public void AddResearchPoint(JResearchPoint rp) {
			researchPoints.Add(rp);
			if (visibleTags != null) {
				CreateGO(rp);
			}
		}
		
		public void RemoveResearchPoint(JResearchPoint rp) {
			RemoveGO(rp);
			researchPoints.Remove(rp);
		}
		
		public void UpdateRotations(Vector3 pos, Vector3 fwd) {
			if (visibleTags == null) return;
			foreach (JDisplayTag tag in visibleTags) {
				tag.UpdateRotation(pos, fwd);
			}
		}
		
		public void RemoveGO(JResearchPoint rp) {
			if (visibleTags != null) {
				foreach (JDisplayTag tag in visibleTags) {
					if (tag.point == rp) {
						visibleTags.Remove(tag);
						GameObject.Destroy(tag.gameObject);
						return;
					}
				}
			}
		}
		
		public void RemoveTemporary() {
			List<JResearchPoint> removeList = new List<JResearchPoint>();
			foreach (JResearchPoint rp in researchPoints) {
				if (rp.RemoveTemporary()) {
					removeList.Add(rp);
					RemoveGO(rp);
				}
			}
			foreach (JResearchPoint rp in removeList) {
				researchPoints.Remove(rp);
			}
		}
		
		public void MakePermanent() {
			foreach (JResearchPoint rp in researchPoints) {
				rp.MakePermanent();
			}
		}
		
		public bool IsVisible { get { return (visibleTags != null); } }
		
		readonly JCellData cell;
		public List<JResearchPoint> researchPoints;
		List<JDisplayTag> visibleTags;
	}
	
	RPCell[,] rpCells;
	int cWidth;
	int cHeight;
	
	public static JResearchPointsMgr self;
	
	void Awake() {
		self = this;
	}
	
	void Start() {
	}
	
	EResearchTypes researchType = EResearchTypes.UNDEFINED;
	
	/**
	 * Can't be multithreaded anymore as it uses EcoScript
	 */
	public static long DoSuccessionAtCell(int cx, int cy) {
		return self.rpCells[cy, cx].CalculateSuccession();
	}
	
	public static void SetAddPointMode(EResearchTypes researchType) {
		self.researchType = researchType;
		self.tmpCosts = 0;
		self.projector.SetActive(true);
	}
	
	/**
	 * if makePermanent is true, all added points are made permanent, otherwise those points are removed again
	 */
	public static void StopAddPointMode(bool makePermanent) {
		self.projector.SetActive(false);
		self.researchType = EResearchTypes.UNDEFINED;
		foreach (RPCell rpc in self.rpCells) {
			if (makePermanent) {
				rpc.MakePermanent();
			}
			else {
				rpc.RemoveTemporary();
			}
		}
	}
	
	public void VisibilityChanged(int minX, int minY, int maxX, int maxY) {
		for (int cy = 0; cy < cHeight; cy++) {
			for (int cx = 0; cx < cWidth; cx++) {
				RPCell rpc = rpCells[cy, cx];
				if ((cx < minX) || (cx > maxX) || (cy < minY) || (cy > maxY)) {
					if (rpc.IsVisible) rpc.DestroyAllGOs();
				}
				else {
					if (!rpc.IsVisible) rpc.CreateAllGOs();
				}
			}
		}
	}
	
	void GameIsLoaded() {
		scenario = JGameControl.scenario;
		JScenarioProgress progress = scenario.progress;
		JTerrainData data = scenario.data;
		
		cHeight = data.cHeight;
		cWidth = data.cWidth;
		
		rpCells = new RPCell[cHeight, cWidth];
		for (int cy = 0; cy < cHeight; cy++) {
			for (int cx = 0; cx < cWidth; cx++) {
				rpCells[cy, cx] = new RPCell(data.GetCell(cx, cy));
			}
		}
		
		foreach (JResearchPoint rp in progress.GetResearchPointsArray()) {
			int cx = rp.x >> JTerrainData.CELL_SIZE2EXP;
			int cy = rp.y >> JTerrainData.CELL_SIZE2EXP;
			rpCells[cy, cx].AddResearchPoint(rp);
		}
		
		JRenderTerrain.self.AddNotifyCallback(VisibilityChanged);
		cameraTransform = JCamera.GetTransform();
		
//		Transform t = transform;
//		foreach (JResearchPoint rp in progress.GetUnmeasuredPointsArray()) {
//			GameObject go = (GameObject) Instantiate(newPointPrefab, rp.pos, Quaternion.identity);
//			go.transform.parent = t;
//			JDisplayTag dt = go.GetComponent<JDisplayTag>();
//			dt.point = rp;
//		}
//		foreach (JResearchPoint rp in progress.GetMeasuredPointsArray()) {
//			GameObject go = (GameObject) Instantiate(pointPrefab, rp.pos, Quaternion.identity);
//			go.transform.parent = t;
//			JDisplayTag dt = go.GetComponent<JDisplayTag>();
//			dt.point = rp;
//		}
	}
	
	JResearchPoint FindPointAtPos(int x, int y) {
		int cx = x >> JTerrainData.CELL_SIZE2EXP;
		int cy = y >> JTerrainData.CELL_SIZE2EXP;
		RPCell rpCell = rpCells[cy, cx];
		foreach (JResearchPoint rp in rpCell.researchPoints) {
			if ((rp.x == x) && (rp.y == y)) return rp;
		}
		
		return null;
	}

	void DoneSuccession() {
	}
	
	private string guiMsg;
	private JResearchPoint guiMsgRP = null;
	private Vector2 pos;
	private int guiNlCount;
	
	void OnGUI() {
		if (guiMsgRP != null) {
			float w = 240;
			float x = (pos.x < Screen.width - 300)?(pos.x + 32):(pos.x - w - 32);
			float h = 10 + 15 * guiNlCount;
			float y = Mathf.Clamp(Screen.height - pos.y - h / 2, 4, Screen.height - h - 10);
			GUI.Box(new Rect(x, y, w, h), guiMsg);
		}
	}
	
	public static void SetMessage(JResearchPoint rp, string msg, int nlCount, Vector3 pos) {
		self.guiMsgRP = rp;
		Vector3 screenPos = Camera.main.WorldToScreenPoint(pos);
		self.guiMsg = msg;
		self.pos = new Vector2(screenPos.x, screenPos.y);
		self.guiNlCount = nlCount;
	}
	
	public static void ClearMessage(JResearchPoint rp) {
		if (self.guiMsgRP == rp) {
			self.guiMsgRP = null;
		}
	}
	
	public static long TmpCosts { get { return self.tmpCosts; } }
	public static int TmpCount { get { return self.tmpCount; } }
	
	void UpdateRotations(Vector3 pos, Vector3 fwd) {
		if (rpCells != null) {
			foreach (RPCell rpc in rpCells) {
				rpc.UpdateRotations(pos, fwd);
			}
		}
	}
	
	Vector3 oldCameraPos;
	Vector3 oldCameraFwd;
	
	int ox = -1;
	int oy = -1;
	
	void Update() {
		if ((cameraTransform) && (JGameControl.scenario != null)) {
			if ((cameraTransform.position != oldCameraPos) || (cameraTransform.forward != oldCameraFwd)) {
				oldCameraPos = cameraTransform.position;
				oldCameraFwd = cameraTransform.forward;
				UpdateRotations(oldCameraPos, oldCameraFwd);
			}
		}
		if (researchType == EResearchTypes.UNDEFINED) return;
		
		
		Vector3 mousePos = Input.mousePosition;

		
		if (JWinMgr.InUIRect(mousePos.x, Screen.height - mousePos.y) || MyGUI.mouseIsOverGUI ||
			Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) {
			projector.SetActive(false);
			return;
		}
		
		projector.SetActive(true);
		
		Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
		RaycastHit hit;

		if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER)) {
			Vector3 point = hit.point;
			int x = (int) (point.x / JTerrainData.HORIZONTAL_SCALE);
			int y = (int) (point.z / JTerrainData.HORIZONTAL_SCALE);
			if ((x != ox) || (y != oy)) {
				ox = x;
				oy = y;
				projector.transform.position = new Vector3(((float) x + 0.5f) * JTerrainData.HORIZONTAL_SCALE, 500f, ((float) y + 0.5f) * JTerrainData.HORIZONTAL_SCALE);
			}
			if ((x < 0) || (y < 0) || (x >= scenario.data.width) || (y >= scenario.data.height) || (!scenario.data.NeedCalculateSuccession(x, y))) {
				return;
			}
			
		}
		else {
			return;
		}
		
		bool shift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
		if (Input.GetMouseButtonDown(0)) {
			JResearchPoint rp = FindPointAtPos(ox, oy);
			bool isNew = false;
			bool isDuplicate = false;
			if (rp == null) {
				if (!shift) {
					rp = new JResearchPoint(ox, oy);
					isNew = true;
				}
			}
			else {
				if (shift) {
					bool removedResearch = false;
					foreach (JResearchPoint.Measurement mi in rp.measurements) {
						if ((mi.msg == null) && (mi.researchType == researchType)) {
							removedResearch = true;
							rp.measurements.Remove(mi);
							break;
						}
					}
					if (removedResearch) {
						tmpCosts -= JGameControl.scenario.research[researchType].costPerTile;
						tmpCount--;
						if (rp.measurements.Count == 0) {
							int cx = ox >> JTerrainData.CELL_SIZE2EXP;
							int cy = oy >> JTerrainData.CELL_SIZE2EXP;
							JGameControl.scenario.progress.RemoveResearchPoint(rp);
							rpCells[cy, cx].RemoveResearchPoint(rp);
						}
					}
					
				}
				else {
					foreach (JResearchPoint.Measurement mi in rp.measurements) {
						if ((mi.msg == null) && (mi.researchType == researchType)) {
							isDuplicate = true;
							break;
						}
					}
				}
			}
			if (!shift && !isDuplicate) {
				guiMsgRP = null;
				JResearchPoint.Measurement m = new JResearchPoint.Measurement(JGameControl.scenario.progress.year, researchType);
				rp.AddMeasurement(m);
				tmpCosts += JGameControl.scenario.research[researchType].costPerTile;
				tmpCount++;
				if (isNew) {
					int cx = ox >> JTerrainData.CELL_SIZE2EXP;
					int cy = oy >> JTerrainData.CELL_SIZE2EXP;
					JGameControl.scenario.progress.AddResearchPoint(rp);
					rpCells[cy, cx].AddResearchPoint(rp);
				}
			}
		}
	}
}
