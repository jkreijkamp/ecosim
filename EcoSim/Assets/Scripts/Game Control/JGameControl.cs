using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;


public class JGameControl : MonoBehaviour {
	public enum EToolType { Tool, Settings, Help }
	[System.Serializable]
	public class Tool {
		public Texture2D icon;
		public Texture2D activeIcon;
		public JGameTool tool;
		public EToolType type;
		public string help;
		public string tooltip;
	}
	
	public static bool showToolHelp = true;
	
	public Texture2D arrowLeft;
	public Texture2D arrowRight;
	
	public Tool[] tools;
	public Texture2D[] avatars;
	
	public Texture2D closeIcon;
	public Texture2D closeIconActive;
	
	public Texture2D budgetIcon;
	public Texture2D calendarIcon;
	
	public Texture2D successieIcon;
	public Texture2D successieIconActive;
		
	private Texture2D articleTexture;
	
	public Texture2D titleBanner;
	//public Texture2D credits;
	
	public Texture2D gameOver;
	
	public RenderFontToTexture rft;
	
	public GUISkin skin;
	
	public static int width;
	public static int height;
	
	public static GUIStyle styleOn;
	public static GUIStyle styleOff;

	public static JGameControl self;

	public bool isBusy = false;

	private Dictionary<string,Texture2D>avatarDict;
	
	private JSimpleWindow window;
	
	public static JScenario scenario;
	
	
	public long yearEstimate = 0;
	public bool isGameOver = false;
	
	void Awake() {
		self = this;
		styleOn = skin.FindStyle("BGWhite");
		styleOff = skin.FindStyle("BGBlack");

		avatarDict = new Dictionary<string, Texture2D>();
		foreach (Texture2D t in avatars) {
			avatarDict.Add(t.name, t);
		}
//		if (state == SettingsState.SUCCESSION) {
//			GameObject go = GameObject.Find("Intro Screen");
//			if (go != null) GameObject.DestroyImmediate(go);
//		}
	}
	
	IEnumerator COGetPlayerInfo(JScenario scenario) {
		state = SettingsState.PLAYER_INFO;
		while (state == SettingsState.PLAYER_INFO) {
			yield return 0;
		}
	}
	
	IEnumerator Start() {
		if (articleTexture) GameObject.Destroy(articleTexture);
		
		articleTexture = new Texture2D(512, 512, TextureFormat.RGB24, false);
		rft = RenderFontToTexture.self;
		
		JGameInventarisationResultWindow.CloseAllWindows();
		
		if ((state != SettingsState.NONE) && (state != SettingsState.SETTINGS) && (state != SettingsState.ERROR)) {
			if (scenario != null) {
				JRenderTerrain.self.RemoveAllObjects(scenario);
			}
			isBusy = true;
			yield return 0;
			try {
				if ((state == SettingsState.NEW) || (state == SettingsState.NEW_CONFIRM)) {	
					ClearQueue();
					JScenario info = scenarios[loadSaveIndex];
					scenario = JScenario.Load(info.name, null, true, false);
					showToolHelp = true;
				}
				else if ((state == SettingsState.LOAD) || (state == SettingsState.LOAD_CONFIRM)) {
					ClearQueue();
					scenario = JScenario.LoadGame(loadSaveIndex, false);
				}
				else if (state == SettingsState.SUCCESSION) {
				}
			}
			catch (System.Exception e) {
				MyDebug.LogError(e.Message);
				Debug.LogError(e.StackTrace);
				state = SettingsState.ERROR;
				errorMsg = "Fout tijdens inlezen. Controlleer instellingen.\n\nMelding '" + e.Message + "'";
				scenario = null;
				Application.LoadLevel("Game");
				yield break;
			}
			bool justLoadedGame = (state != SettingsState.SUCCESSION);
			if (justLoadedGame) {
				yield return StartCoroutine(scenario.COLoadExtraAssets());
			}
			isGameOver = false;
			JConfig.SendMessageDownwards(gameObject, "GameIsLoaded");
			JCamera.SetScenario(scenario, justLoadedGame);
			if (justLoadedGame) {
				scenario.ProcessGoals(JScenarioGoal.M_AFTER_LOAD);
			}
			else {
				scenario.data.roads.RecreateRoads();
			}
			CalculateYearCosts();
			
			yield return 0;
			if ((scenario != null) && ((state == SettingsState.NEW_CONFIRM) || (state == SettingsState.NEW))) {
				yield return StartCoroutine(COGetPlayerInfo(scenario));
			}
			GameObject go = GameObject.Find("Intro Screen");
			if (go != null) GameObject.Destroy(go);
			isBusy = false;
			JShowSuccessionArea.UpdateMap();
			JHandleActionObjects.UpdateObjects();
		}
		else {
			GameObject.Find("Far Camera").SetActive(false);
			GameObject.Find("Scenario").SetActive(false);
			GameObject.Find("compass").SetActive(false);
		}
		if (state != SettingsState.ERROR) {
			state = (scenario != null)?(SettingsState.NONE):(SettingsState.SETTINGS);
		}
	}
	
	bool MyButton(int mx, int my, Rect r, string text, bool dark) {
		string s = dark?"Arial16-75-Centre":"Arial16-50-Centre";
		bool hover = r.Contains(new Vector2(mx, my));
		return MyGUI.Button(r, text, skin.FindStyle(hover?"Arial16-W-Centre":s));
	}

	bool MyButtonLeft(int mx, int my, Rect r, string text, bool dark) {
		string s = dark?"ArialB16-75":"ArialB16-50";
		bool hover = r.Contains(new Vector2(mx, my));
		return MyGUI.Button(r, text, skin.FindStyle(hover?"ArialB16-W":s));
	}
	
	
	void ShowYesNo(int mx, int my, float dt) {
	}
	
	private static string errorMsg = null;
	
	private enum SettingsState { NONE, SETTINGS, NEW, NEW_CONFIRM, LOAD, LOAD_CONFIRM, SAVE, SAVE_OVERWRITE, SUCCESSION, PLAYER_INFO, QUIT, ABOUT, THANKS, CONFIG, ERROR };
	private static SettingsState state = SettingsState.NONE;
	private static JScenario[] scenarios = null;
	private static JScenario[] saveGames = null;
	private static int loadSaveIndex = -1;

	void ShowError(int mx, int my, float dt) {
		int hwidth = width / 2;
		int hheight = height / 2;
		int xo = hwidth - 320;
		int yo = hheight - 128;
		bool overClose = (mx >= xo) && (my >= yo) && (mx < xo + 32) && (my < yo + 32);
		if (MyGUI.Button(new Rect(xo, yo, 32, 32), overClose?(closeIconActive):(closeIcon), overClose?JGameControl.styleOn:JGameControl.styleOff)) {
			state = SettingsState.SETTINGS;
		}
		MyGUI.Label(new Rect(xo + 33, yo, 640 - 33, 32), "FOUTMELDING", skin.FindStyle("ArialB16-75"));
		MyGUI.Label(new Rect(xo, yo + 33, 640, 256 - 33), errorMsg, skin.FindStyle("Arial16-50"));
	}
	
	string[] qualityStrList = new string[] { "Laagste kwaliteit", "Lage kwaliteit", "Middelmatige kwaliteit", "Hoge kwaliteit", "Hogere kwaliteit", "Hoogste kwaliteit" };

	string credits;

	void ShowAbout(int mx, int my, float dt) {
		if (credits == null) {
			TextAsset cr = Resources.Load("credits") as TextAsset;
			if (cr) credits = cr.text;
			else credits = "credits niet gevonden...";
		}
		GUI.Label(new Rect(0, 0, 200, 20), JConfig.BUILD_VERSION_STRING, GUIStyle.none);
		int hwidth = width / 2;
		int hheight = height / 2;
		int xo = hwidth - 310;
		int yo = hheight - 154;
		bool overClose = (mx >= xo) && (my >= yo) && (mx < xo + 32) && (my < yo + 32);
		if (MyGUI.Button(new Rect(xo, yo, 32, 32), overClose?(closeIconActive):(closeIcon), overClose?JGameControl.styleOn:JGameControl.styleOff)) {
			state = SettingsState.SETTINGS;
		}
		MyGUI.Label(new Rect(xo, yo - 154, 620, 153), titleBanner, GUIStyle.none);
		if (MyButtonLeft(mx, my, new Rect(xo + 33, yo, 620 - 33, 32), "http://www.ecosim.nl/", true)) {
			Application.OpenURL("http://www.ecosim.nl/");
		}
		//MyGUI.Label(new Rect(xo + 33, yo + 33, 620 - 33, 32), "", skin.FindStyle("ArialB16-75"));
		//MyGUI.Label(new Rect(xo, yo + 33, 640, 479), credits, GUIStyle.none);
		MyGUI.Label(new Rect(xo, yo + 33, 620, 436), credits, skin.FindStyle("Arial16-50"));
	}
	
	private string dankbetuiging = null;
	
	void ShowThanks(int mx, int my, float dt) {
		if (dankbetuiging == null) {
			TextAsset da = Resources.Load("dankbetuiging") as TextAsset;
			if (da) dankbetuiging = da.text;
			else dankbetuiging = "dankbetuiging niet gevonden.";
		}
		GUI.Label(new Rect(0, 0, 200, 20), JConfig.BUILD_VERSION_STRING, GUIStyle.none);
		int hwidth = width / 2;
		int hheight = height / 2;
		int xo = hwidth - 310;
		int yo = hheight - 154;
		bool overClose = (mx >= xo) && (my >= yo) && (mx < xo + 32) && (my < yo + 32);
		if (MyGUI.Button(new Rect(xo, yo, 32, 32), overClose?(closeIconActive):(closeIcon), overClose?JGameControl.styleOn:JGameControl.styleOff)) {
			state = SettingsState.SETTINGS;
		}
		MyGUI.Label(new Rect(xo, yo - 154, 620, 153), titleBanner, GUIStyle.none);
		if (MyButtonLeft(mx, my, new Rect(xo + 33, yo, 620 - 33, 32), "http://www.ecosim.nl/", true)) {
			Application.OpenURL("http://www.ecosim.nl/");
		}
		// MyGUI.Label(new Rect(xo + 33, yo, 620 - 33, 32), "", skin.FindStyle("ArialB16-75"));
		MyGUI.Label(new Rect(xo, yo + 33, 620, 479), dankbetuiging, skin.FindStyle("Arial16-50-formatted"));
	}
	
	
	void ShowConfig(int mx, int my, float dt) {
		int hwidth = width / 2;
		int hheight = height / 2;
		int xo = hwidth - 320;
		int yo = hheight - 256;
		bool overClose = (mx >= xo) && (my >= yo) && (mx < xo + 32) && (my < yo + 32);
		if (MyGUI.Button(new Rect(xo, yo, 32, 32), overClose?(closeIconActive):(closeIcon), overClose?JGameControl.styleOn:JGameControl.styleOff)) {
			state = SettingsState.SETTINGS;
		}
		MyGUI.Label(new Rect(xo + 33, yo, 640 - 33, 32), "Instellingen", skin.FindStyle("ArialB16-75"));
		MyGUI.Label(new Rect(xo, yo + 33, 133, 32), "Scenarios pad", skin.FindStyle("Arial16-50"));
		MyGUI.Label(new Rect(xo, yo + 66, 133, 32), "Voortgang pad", skin.FindStyle("Arial16-50"));
		string newScPath = MyGUI.TextField(new Rect(xo + 134, yo + 33, 640 - 134, 32), JConfig.definedScenarioPath, skin.FindStyle("ArialB16-50"));
		string newSvPath = MyGUI.TextField(new Rect(xo + 134, yo + 66, 640 - 134, 32), JConfig.definedSaveGamesPath, skin.FindStyle("ArialB16-50"));
		if (newScPath != JConfig.definedScenarioPath) {
			JConfig.definedScenarioPath = newScPath;
			PlayerPrefs.SetString("ScenarioPath", newScPath);
			PlayerPrefs.Save();
		}
		if (newSvPath != JConfig.definedSaveGamesPath) {
			JConfig.definedSaveGamesPath = newSvPath;
			PlayerPrefs.SetString("SaveGamesPath", newSvPath);
			PlayerPrefs.Save();
		}
		MyGUI.Label(new Rect(xo, yo + 99, 133, 32), "Weergave", skin.FindStyle("Arial16-50"));
		int level = QualitySettings.GetQualityLevel();
		if (level > 0) {
			if (MyButton(mx, my, new Rect(xo + 134, yo + 99, 133, 32), "Lager", true)) {
				QualitySettings.DecreaseLevel();
			}
		}
		else {
			MyGUI.Label(new Rect(xo + 134, yo + 99, 133, 32), "", skin.FindStyle("Arial16-50"));
		}
		MyGUI.Label(new Rect(xo + 268, yo + 99, 238, 32), qualityStrList[level], skin.FindStyle("Arial16-50"));
		if (level < 5) {
			if (MyButton(mx, my, new Rect(xo + 640 - 133, yo + 99, 133, 32), "Hoger", true)) {
				QualitySettings.IncreaseLevel();
			}
		}
		else {
			MyGUI.Label(new Rect(xo + 640 - 133, yo + 99, 133, 32), "", skin.FindStyle("Arial16-50"));
		}
		
		MyGUI.Label(new Rect(xo, yo + 132, 640 - 134, 32), "", skin.FindStyle("Arial16-50"));
		if (MyButton(mx, my, new Rect(xo + 640 - 133, yo + 132, 133, 32), "Herstel paden", true)) {
			JConfig.SetDefaultScenarioPath();
			JConfig.SetDefaultSaveGamesPath();
		}
		MyGUI.Label(new Rect(xo, yo + 165, 640, 32), "", skin.FindStyle("Arial16-50"));
		MyGUI.Label(new Rect(xo, yo + 198, 640 - 134, 32), "", skin.FindStyle("Arial16-50"));
		if (MyButton(mx, my, new Rect(xo + 640 - 133, yo + 198, 133, 32), "Sluiten", true)) {
			state = SettingsState.SETTINGS;
		}
	}
	
	void ShowConfirm(int mx, int my, float dt) {
		string descr = null;
		switch (state) {
		case SettingsState.LOAD_CONFIRM : 
		case SettingsState.NEW_CONFIRM :
			descr = "Huidige voortgang gaat verloren\nDoorgaan?"; break;
		case SettingsState.SAVE_OVERWRITE :
			descr = "Overschrijf bestand?"; break;
		case SettingsState.QUIT :
			descr = "EcoSim stoppen?"; break;
		}
		
		int hwidth = width / 2;
		int hheight = height / 2;
		MyGUI.Label(new Rect(hwidth - 128, hheight - 66 * 2, 256, 65), descr, skin.FindStyle("Arial16-75-Centre"));
		if (MyButton(mx, my, new Rect(hwidth - 128, hheight - 66 * 1, 256, 65), "Ja", false)) {
			switch (state) {
			case SettingsState.QUIT :
				if (Application.isEditor) {
					Debug.Break();
					state = SettingsState.SETTINGS;
				}
				else {
					Application.Quit();
				}
				return;
			case SettingsState.LOAD_CONFIRM : 
			case SettingsState.NEW_CONFIRM :
				Application.LoadLevel("Game");
				return;
			case SettingsState.SAVE_OVERWRITE :
				DoSave();
				return;
			}
		}
		if (MyButton(mx, my, new Rect(hwidth - 128, hheight - 66 * 0, 256, 65), "Nee", false)) {
			switch (state) {
			case SettingsState.LOAD_CONFIRM : state = SettingsState.LOAD; break;
			case SettingsState.SAVE_OVERWRITE : state = SettingsState.SAVE; break;
			case SettingsState.NEW_CONFIRM : state = SettingsState.SETTINGS; break;
			case SettingsState.QUIT : state = SettingsState.SETTINGS; break;
			}
		}
	}
	
	void ShowSceneChooserOld(int mx, int my, float dt) {
		if (MyButton(mx, my, new Rect(0, 0, 256, 32), "Terug", true)) {
			scenarios = null;
			state = SettingsState.SETTINGS;
			return;
		}
		for (int i = 0; i < scenarios.Length; i++) {
			Rect r = new Rect(0, 33 + i * 66, 256, 65);
			bool hover = r.Contains(new Vector2(mx, my));
			if (hover) {
				helpText = null;
				ShowHelpText(null, scenarios[i].description, "ecoloog");
			}
			string name = scenarios[i].name;
			if (scenarios[i].shortDesc != "") {
				name += "\n<size=12>" + scenarios[i].shortDesc + "</size>";
			}
			if (MyButton(mx, my, r, name, false)) {
				loadSaveIndex = i;
				if (scenario == null) {
					Application.LoadLevel("Game");
					return;
				}
				state = SettingsState.NEW_CONFIRM;
			}
		}
	}
	
	void ShowSceneChooser(int mx, int my, float dt) {
		int hwidth = width / 2;
		int hheight = height / 2;
		int vp = -3;
		GUI.Label(new Rect(hwidth - 310, hheight + 33 * vp - 154, 620, 153), titleBanner, GUIStyle.none);
		if (MyButton(mx, my, new Rect(hwidth - 310, hheight + 33 * (vp++), 620, 32), "Terug", false)) {
			scenarios = null;
			state = SettingsState.SETTINGS;
			return;
		}
		int count = scenarios.Length;
		for (int i = 0; i < scenarios.Length; i++) {
			Rect r = new Rect(hwidth - 310 + 207 * (i % 3), hheight + 33 * vp, 206, 65);
			bool hover = r.Contains(new Vector2(mx, my));
			if (hover) {
				helpText = null;
				ShowHelpText(null, scenarios[i].description, "ecoloog");
			}
			string name = scenarios[i].name;
			if (scenarios[i].shortDesc != "") {
				name += "\n<size=12>" + scenarios[i].shortDesc + "</size>";
			}
			if (MyButton(mx, my, r, name, false)) {
				loadSaveIndex = i;
				if (scenario == null) {
					Application.LoadLevel("Game");
					return;
				}
				state = SettingsState.NEW_CONFIRM;
			}
			if ((i % 3) == 2) vp += 2;
		}
		int leftOver = count % 3;
		if (leftOver > 0) {
			Rect r = new Rect(hwidth - 310 + 207 * leftOver, hheight + 33 * vp, 207 * (3 - leftOver) - 1, 65);
			MyGUI.Label(r, "", skin.FindStyle("Arial16-50"));
		}
	}
	
	void ShowSaveGameChooser(int mx, int my, float dt) {
		int hwidth = width / 2;
		int hheight = height / 2;
		int vp = -3;
		GUI.Label(new Rect(hwidth - 310, hheight + 33 * vp - 154, 620, 153), titleBanner, GUIStyle.none);
		if (MyButton(mx, my, new Rect(hwidth - 310, hheight + 33 * (vp++), 620, 32), "Terug", false)) {
			saveGames = null;
			state = SettingsState.SETTINGS;
			return;
		}
		int count = saveGames.Length;
		for (int i = 0 ; i < count; i++) {
			Rect r = new Rect(hwidth - 310 + 207 * (i % 3), hheight + 33 * vp, 206, 65);
			bool hover = r.Contains(new Vector2(mx, my));
			string description = "Vrij slot " + (i + 1);
			string name = "Vrij slot " + (i + 1);
			
			if (saveGames[i] != null) {
				description = saveGames[i].description;
				name = saveGames[i].name;
				name += "\n<size=12>" + saveGames[i].progress.playerFirstName + " " + saveGames[i].progress.playerFamilyName;
				name += "   " + saveGames[i].progress.year + "</size>";
			}
			if (hover) {
				helpText = null;
				ShowHelpText(null, description, "ecoloog");
			}
			if (MyButton(mx, my, r, name, false)) {
				loadSaveIndex = i;
				if (state == SettingsState.LOAD) {
					if (saveGames[i] != null) {
						if (scenario == null) {
							Application.LoadLevel("Game");
							return;
						}
						state = SettingsState.LOAD_CONFIRM;
					}
				}
				else {
					if (saveGames[i] != null) {
						state = SettingsState.SAVE_OVERWRITE;
					}
					else {
						DoSave();
					}
				}
			}
			if ((i % 3) == 2) vp += 2;
		}
		int leftOver = count % 3;
		if (leftOver > 0) {
			Rect r = new Rect(hwidth - 310 + 207 * leftOver, hheight + 33 * vp, 207 * (3 - leftOver) - 1, 65);
			MyGUI.Label(r, "", skin.FindStyle("Arial16-50"));
		}
	}

	void ShowSaveGameChooserOld(int mx, int my, float dt) {
		if (MyButton(mx, my, new Rect(0, 0, 256, 32), "Terug", true)) {
			saveGames = null;
			state = SettingsState.SETTINGS;
			return;
		}
		for (int i = 0 ; i < saveGames.Length; i++) {
			Rect r = new Rect(0, 33 + i * 66, 256, 65);
			bool hover = r.Contains(new Vector2(mx, my));
			string description = "Vrij slot " + (i + 1);
			string name = "Vrij slot " + (i + 1);
			
			if (saveGames[i] != null) {
				description = saveGames[i].description;
				name = saveGames[i].name;
				name += "\n<size=12>" + saveGames[i].progress.playerFirstName + " " + saveGames[i].progress.playerFamilyName;
				name += "   " + saveGames[i].progress.year + "</size>";
			}
			if (hover) {
				helpText = null;
				ShowHelpText(null, description, "ecoloog");
			}
			if (MyButton(mx, my, r, name, false)) {
				loadSaveIndex = i;
				if (state == SettingsState.LOAD) {
					if (saveGames[i] != null) {
						if (scenario == null) {
							Application.LoadLevel("Game");
							return;
						}
						state = SettingsState.LOAD_CONFIRM;
					}
				}
				else {
					if (saveGames[i] != null) {
						state = SettingsState.SAVE_OVERWRITE;
					}
					else {
						DoSave();
					}
				}
			}
		}
	}
	
	void DoSave() {
		try {
			scenario.SaveGame(loadSaveIndex);
			state = SettingsState.NONE;
		}
		catch (System.Exception e) {
			errorMsg = "Fout tijdens wegschrijven. Controlleer instellingen.\n\nMelding '" + e.Message + "'";
			state = SettingsState.ERROR;
		}
	}

	void ShowSettings(int mx, int my, float dt) {
		switch (state) {
		case SettingsState.LOAD : ShowSaveGameChooser(mx ,my, dt); return;
		case SettingsState.LOAD_CONFIRM : ShowConfirm(mx, my, dt); return;
		case SettingsState.NEW : ShowSceneChooser(mx, my, dt); return;
		case SettingsState.NEW_CONFIRM : ShowConfirm(mx, my, dt); return;
		case SettingsState.SAVE : ShowSaveGameChooser(mx, my, dt); return;
		case SettingsState.SAVE_OVERWRITE : ShowConfirm(mx, my, dt); return;
		case SettingsState.QUIT : ShowConfirm(mx, my, dt); return;
		}
		currentArticleText = null;

		bool isLoaded = (scenario != null);
		int hwidth = width / 2;
		int hheight = height / 2;
		int vp = -3;
		GUI.Label(new Rect(hwidth - 310, hheight + 33 * vp - 154, 620, 153), titleBanner, GUIStyle.none);
		if (isLoaded) {
			if (MyButton(mx, my, new Rect(hwidth - 310, hheight + 33 * (vp++), 620, 32), "Terug", false)) {
				state = SettingsState.NONE;
			}
		}
		if (MyButton(mx, my, new Rect(hwidth - 310, hheight + 33 * (vp++), 620, 32), "Start nieuw spel", true)) {
			scenarios = JScenario.GetScenarioPreviews();
			state = SettingsState.NEW;
		}
		if (MyButton(mx, my, new Rect(hwidth - 310, hheight + 33 * (vp++), 620, 32), "Laad opgeslagen spel", true)) {
			saveGames = JScenario.GetSaveGamePreviews();
			state = SettingsState.LOAD;
		}
		if (isLoaded && !isGameOver) {
			if (MyButton(mx, my, new Rect(hwidth - 310, hheight + 33 * (vp++), 620, 32), "Bewaar spel", true)) {
				saveGames = JScenario.GetSaveGamePreviews();
				state = SettingsState.SAVE;
			}
		}
		if (MyButton(mx, my, new Rect(hwidth - 310, hheight + 33 * (vp++), 620, 32), "Instellingen", true)) {
			state = SettingsState.CONFIG;
		}
		if (scenario == null) {
			if (MyButton(mx, my, new Rect(hwidth - 310, hheight + 33 * (vp++), 620, 32), "Over EcoSim", true)) {
				state = SettingsState.ABOUT;
			}
			if (MyButton(mx, my, new Rect(hwidth - 310, hheight + 33 * (vp++), 620, 32), "Met dank aan", true)) {
				state = SettingsState.THANKS;
			}
		}
		if (MyButton(mx, my, new Rect(hwidth - 310, hheight + 33 * (vp++), 620, 32), "Stoppen met EcoSim", true)) {
			state = SettingsState.QUIT;
		}
		if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
			if (MyButton(mx, my, new Rect(hwidth - 310, hheight + 33 * (vp++), 620, 32), "Scenario Editor", true)) {
				scenario = null;
				ClearQueue();
				Application.LoadLevel("Edit");
			}
		}
	}
	
	void Update() {
		if (state != SettingsState.NONE) {
			if (Input.GetKeyDown(KeyCode.LeftBracket)) {
				QualitySettings.DecreaseLevel();
				JRenderTerrain.ForceQualityCheck(scenario);
				// hack for fog setting
				if (!JCamera.IsNear) JCamera.SetToFar(false);
				SetHelp(null, "Beeldqualiteit gezet op niveau " + QualitySettings.GetQualityLevel(), true);
			}
			if (Input.GetKeyDown(KeyCode.RightBracket)) {
				QualitySettings.IncreaseLevel();
				JRenderTerrain.ForceQualityCheck(scenario);
				// hack for fog setting
				if (!JCamera.IsNear) JCamera.SetToFar(false);
				SetHelp(null, "Beeldqualiteit gezet op niveau " + QualitySettings.GetQualityLevel(), true);
			}
		}
		else {
			ManageQueue();
		}
	}
	
	private bool hasGender = false;
	
	void PlayerInfoGUI() {
		MyGUI.mouseIsOverGUI = true;
		JScenarioProgress progress = scenario.progress;
		GUIStyle b16_w = skin.FindStyle("ArialB16-W");
		GUIStyle n16_50 = skin.FindStyle("Arial16-50");
		GUIStyle b16_50 = skin.FindStyle("ArialB16-50");
		GUIStyle b16_75 = skin.FindStyle("ArialB16-75");
		GUI.Label(new Rect(width / 2 - 140, height / 2 - 80, 279, 39), "Kan je je even voorstellen?", b16_75);
		GUI.Label(new Rect(width / 2 - 140, height / 2 - 40, 139, 39), "Voornaam", b16_75);
		GUI.Label(new Rect(width / 2 - 140, height / 2 + 0, 139, 39), "Achternaam", b16_75);
		GUI.Label(new Rect(width / 2 - 140, height / 2 + 40, 139, 39), "Geslacht", b16_75);
		progress.playerFirstName = GUI.TextField(new Rect(width / 2, height / 2 - 40, 139, 39), progress.playerFirstName, n16_50);
		progress.playerFamilyName = GUI.TextField(new Rect(width / 2, height / 2 + 0, 139, 39), progress.playerFamilyName, n16_50);
		bool isMale = scenario.progress.isMale;
		if (GUI.Button(new Rect(width / 2, height / 2 + 40, 69, 39), "Man", (hasGender&&isMale)?b16_w:b16_50)) {
			hasGender = true;
			progress.isMale = true;
		}
		if (GUI.Button(new Rect(width / 2 + 70, height / 2 + 40, 69, 39), "Vrouw", (hasGender&&!isMale)?b16_w:b16_50)) {
			hasGender = true;
			progress.isMale = false;
		}
		if ((hasGender) && (progress.playerFirstName.Trim() != "") && (progress.playerFamilyName.Trim() != "")) {
			if (GUI.Button(new Rect(width / 2 - 140, height / 2 + 80, 279, 40), "Start...", b16_50)) {
				progress.playerFirstName = progress.playerFirstName.Trim();
				progress.playerFamilyName = progress.playerFamilyName.Trim();
				state = SettingsState.NONE;
			}
		}
	}
	
	void OnGUI() {
		if (Event.current.type == EventType.Layout) {
			MyGUI.mouseIsOverGUI = false;
			return;
		}
		// TODO if (JXToolBox.showToolbox) return;
		width = Screen.width;
		height = Screen.height;

		if (isGameOver) {
			GUI.Label(new Rect((width - gameOver.width) / 2, gameOver.height, gameOver.width, gameOver.height), gameOver);
		}
		
		Vector2 mousePos = Input.mousePosition;
		int mx = (int) mousePos.x;
		int my = height - (int) mousePos.y;
		float dt = Time.deltaTime;
		
		GUI.skin = skin;
		
		if (state == SettingsState.ERROR) {
			ShowError(mx, my, dt);
			return;
		}
		else if (state == SettingsState.CONFIG) {
			ShowConfig(mx, my, dt);
			return;
		}
		else if (state == SettingsState.ABOUT) {
			ShowAbout(mx, my, dt);
			return;
		}
		else if (state == SettingsState.THANKS) {
			ShowThanks(mx, my, dt);
			return;
		}
		else if (state == SettingsState.PLAYER_INFO) {
			PlayerInfoGUI();
			return;
		}
		
		if (isBusy) {
			GUI.Label(new Rect(width / 2 - 128, height / 2 - 192, 256, 64), "Even geduld...", skin.FindStyle("Arial16-W-Centre"));
			return;
		}
		

		if (state != SettingsState.NONE) {
			ShowSettings(mx, my, dt);
			ShowHelpText(helpIcon, helpText, avatar);
			return;
		}
		
		JHelpWindow.RenderWindows(mx, my, dt);
		
		if ((currentQueueItem != null) && (currentQueueItem.IsMessage)) {
			MyGUI.mouseIsOverGUI = true;

			if (ShowArticle(currentQueueItem.message)) {
				currentQueueItem = null;
			}
			if (!isGameOver) return;
		}
		
		JScenarioProgress progress = scenario.progress;
		bool toolIsActive = false;
		bool isNear = JCamera.IsNear;
		
		if (window != null) {
			toolIsActive = true;
			if (isNear && !window.Render(mx, my, dt)) {
				window = null;
				foreach (Tool t in tools) {
					if (t.tool != null) t.tool.Hide();
				}
			}
		}
		else {
			int x = 1;
			int y = 1;
			
			
			Tool overTool = null;
			
			helpText = null;
			
			int toolCount = tools.Length;
			if (!isNear) toolCount = 3;
			if (isGameOver) toolCount = 4;

			bool showToolHelpForIcon = true;
		
			for (int i = 0; i < toolCount; i++) {
				Tool t = tools[i];
				
				bool hover = (x <= mx) && (mx <= x + 32) && (y <= my) && (my <= y + 32);
				bool skip = (!progress.allowResearch && (t.tool is JGameResearch)) || (!progress.allowMeasures && (t.tool is JGameMeasure));
				if (!skip) {
					if (hover) {
						overTool = t;
					}
					if (t.tool != null) {
						if (hover) t.tool.KeepAlive();
						if (t.tool.Render(skin, x + 33, y+33, mx, my, dt)) {
							hover = true;
							toolIsActive = true;
							showToolHelpForIcon = false;
						}
					}
					if (hover) {
						SetHelp(t.icon, t.help, false);
					}
						
					if (MyGUI.Button(new Rect(x, y, 32, 32), hover?(t.activeIcon):(t.icon), hover?styleOn:styleOff)) {
						if (t.type == EToolType.Settings) {
							state = SettingsState.SETTINGS;
						}
						if (t.type == EToolType.Help) {
							new JHelpWindow(Screen.width / 2 - 256, Screen.height / 2 - 256, t.icon);
						}
					}
					if (showToolHelp && showToolHelpForIcon) {
						GUI.Label(new Rect(x + 32, y, 32, 32), arrowLeft, GUIStyle.none);
						GUI.Label(new Rect(x + 64, y, 140, 32), t.tooltip, skin.FindStyle("ToolHelp"));
					}
					y += 33;
				}
			}
			if (overTool != null) {
				foreach (Tool t in tools) {
					if ((t != overTool) && (t.tool != null)) t.tool.Hide();
				}
			}
		}
		
		if ((currentQueueItem != null) && (!currentQueueItem.IsMessage)) {
			ShowHelpText(null, currentQueueItem.help, currentQueueItem.avatar);
		}
		else {
			if (helpText != null) {
				ShowHelpText(helpIcon, helpText, avatar);
			}
		}
		if (showToolHelp) {
			GUI.Label(new Rect(width-34-105-32, 1, 32, 32), arrowRight, GUIStyle.none);
			GUI.Label(new Rect(width-34-105-32-96, 1, 96, 32), "Totaal budget", skin.FindStyle("ToolHelp"));
			GUI.Label(new Rect(width-34-105-32, 34, 32, 32), arrowRight, GUIStyle.none);
			GUI.Label(new Rect(width-34-105-32-96, 34, 96, 32), "Begroting dit jaar", skin.FindStyle("ToolHelp"));
			GUI.Label(new Rect(width-34-105-32, 67, 32, 32), arrowRight, GUIStyle.none);
			GUI.Label(new Rect(width-34-105-32-96, 67, 96, 32), "Huidig jaar", skin.FindStyle("ToolHelp"));
		}
		GUI.Label(new Rect(width-34-105, 1, 105, 32),
		          progress.budget.ToString("#,##0", CultureInfo.GetCultureInfo("nl-NL")) + 
		          ",-", skin.FindStyle("ArialB16-50"));
		GUI.Label(new Rect(width-33, 1, 32, 32), budgetIcon, skin.FindStyle("50"));

		GUI.Label(new Rect(width-34-105, 34, 105, 32),
		          yearEstimate.ToString("#,##0", CultureInfo.GetCultureInfo("nl-NL")) + 
		          ",-", skin.FindStyle("ArialB16-50"));
		GUI.Label(new Rect(width-33, 34, 32, 32), budgetIcon, skin.FindStyle("50"));

		GUI.Label(new Rect(width-34-105, 67, 105, 32), progress.year.ToString(), skin.FindStyle("ArialB16-50"));
		GUI.Label(new Rect(width-33, 67, 32, 32), calendarIcon, skin.FindStyle("50"));
		
		Rect successionR = new Rect(width - 65, height - 65, 64, 64);
		bool mouseOverSuccession = MyGUI.CheckMouseOver(successionR);
		
		if (!toolIsActive && !isBusy && !isGameOver && !JRenderTerrain.isRendering) {
			if (showToolHelp) {
				GUI.Label(new Rect(width-96, height - 48, 32, 32), arrowRight, GUIStyle.none);
				GUI.Label(new Rect(width-96-128, height - 48, 128, 32), "Ga naar volgend jaar", skin.FindStyle("ToolHelp"));
				
				string[] labels = JCamera.IsNear?nearMapHelp:farMapHelp;
				int index = Mathf.RoundToInt(Time.timeSinceLevelLoad / 3.0f) % labels.Length;
				GUI.Label(new Rect(width / 2 - 192, 0, 384, 32), labels[index], skin.FindStyle("ToolHelp"));
			}
			if (MyGUI.Button(new Rect(width - 65, height - 65, 64, 64),mouseOverSuccession?successieIconActive:successieIcon, GUIStyle.none)) {
				showToolHelp = false;
				if (!JDoSuccession.IsDoingSuccession) {
					StartCoroutine(COSuccession());
				}
			}
		}
	}
	
	private string[] nearMapHelp = new string[] {
		"Druk op <b>Esc</b> om naar mapoverzicht te gaan...",
		"Beweeg de muis naar de rand van het scherm of de camera te verplaatsen...",
		"Of verplaats de camera met cursortoetsen of WASD...",
		"Houdt de rechtermuisknop ingedrukt om de camera te draaien...",
		"<b>Page Up</b> en <b>Page Down</b> of scrollwheel voor zoom...",
		"Gebruik <b>Shift</b> toets om verplaatsen te versnellen...",
	};

	private string[] farMapHelp = new string[] {
		"Druk op <b>Esc</b> om naar mapoverzicht te gaan...",
		"Beweeg de muis naar de rand van het scherm of de camera te verplaatsen...",
		"Of verplaats de camera met cursortoetsen of WASD...",
		"<b>Page Up</b> en <b>Page Down</b> of scrollwheel voor zoom...",
		"Gebruik <b>Shift</b> toets om verplaatsen te versnellen",
	};
	
	public void CalculateYearCosts() {
		yearEstimate = scenario.progress.CalculateYearlyCosts();
	}
	
	void ShowHelpText(Texture helpIcon, string helpText, string avatar) {
		if (helpText != null) {
			GUI.Label(new Rect(1, Screen.height - 159, 528, 160-34), helpText, skin.FindStyle("Help"));
			if (helpIcon != null) {
				GUI.Label(new Rect(1, Screen.height - 32, 32, 32), helpIcon, skin.FindStyle("75"));
			}
			else {
				GUI.Label(new Rect(1, Screen.height - 32, 32, 32), "", skin.FindStyle("75"));
			}
			GUI.Label(new Rect(34, Screen.height - 32, 528-33, 32), "", skin.FindStyle("75"));
			if (avatarDict.ContainsKey(avatar)) {
				GUI.Label(new Rect(400, Screen.height - 256, 256, 256), avatarDict[avatar]);
			}
		}
	}
	
	string currentArticleText = null;
	float yPos = 0;
	int yLen = 0;
	bool showBar = false;
	
	bool ShowArticle(string message) {
		if (message == null) {
			currentArticleText = null;
			return false;
		}
		if (message != currentArticleText) {
			yLen = rft.RenderNewsArticle(message, scenario, articleTexture);
			yPos = 0f;
			currentArticleText = message;
			showBar = (message.ToLower().TrimStart().StartsWith("[avatar]"));
		}
		yPos = Mathf.Min((float) yLen, yPos + Time.deltaTime * 1024);
		if (showBar) {
			if (MyGUI.Button(new Rect(0, Screen.height - (int) yPos, Screen.width, articleTexture.height), "", GUIStyle.none)
				&& !isGameOver) {
				currentArticleText = null;
				return true;
			}
			MyGUI.Label(new Rect(0, Screen.height - (int) yPos + 64, Screen.width, articleTexture.height), "", skin.FindStyle("50"));
			MyGUI.Label(new Rect((Screen.width - articleTexture.width) / 2, Screen.height - (int) yPos,
			articleTexture.width, articleTexture.height), articleTexture);
		}
		else
		if (MyGUI.Button(new Rect((Screen.width - articleTexture.width) / 2, Screen.height - (int) yPos,
			articleTexture.width, articleTexture.height), articleTexture) && !isGameOver) {
			currentArticleText = null;
			return true;
		}
		return false;
	}
	
	IEnumerator COSuccession() {
		isBusy = true;
		JWaitSpinner.StartSuccession();
		yield return 0;
		scenario.ProcessGoals(JScenarioGoal.M_BEFORE_SUCCESSION);
//		progress.budget -= JXScenario.self.progress.areas.DoSuccession();
//		progress.budget -= JXScenario.self.progress.infoAreas.DoSuccession();
		JDoSuccession.StartSuccession(scenario);
		while (JDoSuccession.IsDoingSuccession) yield return 0;
		yield return 0;
//		yield return StartCoroutine(JXParameters.COSuccession());
//		JXScenario.self.RebuildTerrain();
		scenario.progress.year++;
//		CalculateYearCosts();
//		JConfig.SendMessageDownwards(gameObject, "DoneSuccession");
		yield return 0;
		scenario.ProcessGoals(JScenarioGoal.M_AFTER_SUCCESSION);
		scenario.oldContext.SetValue("LASTMEASURE", (int) EActionTypes.UNDEFINED);		
		scenario.oldContext.SetValue("LASTRESEARCH", (int) EResearchTypes.UNDEFINED);
		scenario.oldContext.SetValue("VIEWEDENCYCLOPEDIA", 0);
		
		scenario.context.Set("laatstemaatregel", "");
		scenario.context.Set("laatsteonderzoek", "");
		scenario.context.Set("laatstemaatregelgroep", "");
		scenario.context.Set("laatsteonderzoeksgroep", "");
//		JWaitSpinner.StopSuccession();
//		isBusy = false;
		state = SettingsState.SUCCESSION;
		Application.LoadLevel("Game");
	}
	
	private Texture2D helpIcon = null;
	private string helpText = null;
	private string avatar = null;

	public static void SetHelp(Texture2D icon, string help, bool force) {
		if ((help == null) || (help == "")) return;
		if ((self.helpText != null) && !force) return;
		self.helpIcon = icon;
		self.helpText = help;
		self.avatar = "ecoloog";
	}
	
	public static void SetHelp(Texture2D icon, string help, bool force, string avatar) {
		if ((help == null) || (help == "")) return;
		if ((self.helpText != null) && !force) return;
		self.helpIcon = icon;
		self.helpText = help;
		self.avatar = avatar;
	}
	
	public static void SetWindow(JSimpleWindow window) {
		self.window = window;
	}
	
	public class Queue {
		public Queue(string help, string msg, bool gameOver) {
			if (help != null) {
				int sep = help.IndexOf('|');
				if (sep > 0) {
					this.avatar = help.Substring(0, sep);
					this.help = help.Substring(sep + 1);
				}
				else {
					this.avatar = "ecoloog";
					this.help = help;
				}
			}
			else {
				this.message = msg;
			}
			this.gameOver = gameOver;
		}
		
		public readonly bool gameOver;
		public readonly string avatar;
		public readonly string help;
		public readonly string message;
		
		public bool IsMessage { get { return (message != null); } }
	}
	
	static List<Queue> queue = new List<Queue>();
	static float queueTimer = 0f;
	static Queue currentQueueItem = null;
	
	static void ClearQueue() {
		queue = new List<Queue>();
		queueTimer = 0f;
		currentQueueItem = null;
	}
	
	void ManageQueue() {
		if (currentArticleText != null) return;
		if (queueTimer > 0f) {
			float time = Time.time;
			if (queueTimer < time) {
				currentQueueItem = null;
				queueTimer = 0;
			}
		}
		else {
			if (queue.Count > 0) {
				currentQueueItem = queue[0];
				queue.RemoveAt(0);
				if (!currentQueueItem.IsMessage) queueTimer = Time.time + 15;
				if (currentQueueItem.gameOver) isGameOver = true;
			}
		}
	}
	
	public static void AddToHelpQueue(string help, bool gameOver) {
		Debug.Log("Help addded '" + help + "'");
		Queue q = new Queue(help, null, gameOver);
		queue.Add(q);
	}
	
	public static void AddToMessageQueue(string msg, bool gameOver) {
		Debug.Log("Message addded '" + msg + "'");
		Queue q = new Queue(null, msg, gameOver);
		queue.Add(q);
	}
	
}
