using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JShowSuccessionAreaCell : MonoBehaviour {
	public int cellX;
	public int cellY;
	public Material material;
	public JCellData cell;
	
	MeshFilter filter;
	MeshRenderer render;
	Mesh mesh;
	
	void OnDestroy() {
		DestroyImmediate(mesh);
	}
	
	void Start() {
		render = gameObject.AddComponent<MeshRenderer>();
		render.sharedMaterial = material;
		mesh = new Mesh();
		CalculateMesh();
		
		filter = gameObject.AddComponent<MeshFilter>();
		filter.sharedMesh = mesh;		
	}
	
	private const int CELL_SIZE = JTerrainData.CELL_SIZE;
	
	private void CalculateMesh() {
		
		int len = CELL_SIZE * CELL_SIZE;
		byte[] map = new byte[len];
		byte[] sbm = cell.GetSuccessionBitmap();
		
		int p = 0;
		int v = 0;
		int m = 0x100;
		for (int i = 0; i < len; i++) {
			if (m == 0x100) {
				m = 0x01;
				v = sbm[p++];
			}
			map[i] = (byte) (((v & m) != 0)?0x00:0xff);
			m = m << 1;
		}	
		JScenarioProgress progress = JGameControl.scenario.progress;
		foreach (EActionGroups ag in EnumExtensions.actionGroupArray) {
			if (progress.HasMeasureMap(ag)) {
				JAreaMapCell mapcell = progress.GetMeasureMap(ag).GetCell(cellX, cellY);
				if (mapcell != null) {
					p = 0;
					for (int y = 0; y < CELL_SIZE; y++) {
						for (int x = 0; x < CELL_SIZE; x++) {
							if (mapcell.Get(x, y) > 0) map[p] |= 0x01;
							p++;
						}
					}
				}
			}
		}

		if (progress.HasInventarisationMap()) {
			JAreaMapCell mapcell = progress.GetInventarisationMap().GetCell(cellX, cellY);
				if (mapcell != null) {
					p = 0;
					for (int y = 0; y < CELL_SIZE; y++) {
						for (int x = 0; x < CELL_SIZE; x++) {
							if (mapcell.Get(x, y) > 0) map[p] |= 0x02;
							p++;
						}
					}
				}
		}
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> indices = new List<int>();
		float uvStep = 1f / 2f;

		p = 0;
		int count = 0;
		for (int y = 0; y < CELL_SIZE; y++) {
			for (int x = 0; x < CELL_SIZE; x++) {
				v = map[p++];
				if (v != 0xff) {
					int uvX = v % 2;
					int uvY = v / 2;
					uv.Add(new Vector2(uvStep * uvX, uvStep * uvY));
					uv.Add(new Vector2(uvStep * (uvX + 1), uvStep * uvY));
					uv.Add(new Vector2(uvStep * uvX, uvStep * (uvY + 1)));
					uv.Add(new Vector2(uvStep * (uvX + 1), uvStep * (uvY + 1)));
					vertices.Add(new Vector3(x, cell.GetCornerHeight(x, y), y));
					vertices.Add(new Vector3(x + 1, cell.GetCornerHeight(x + 1, y), y));
					vertices.Add(new Vector3(x, cell.GetCornerHeight(x, y + 1), y + 1));
					vertices.Add(new Vector3(x + 1, cell.GetCornerHeight(x + 1, y + 1), y + 1));
					indices.Add(count + 1);
					indices.Add(count);
					indices.Add(count + 2);
					indices.Add(count + 1);
					indices.Add(count + 2);
					indices.Add(count + 3);
					count += 4;
				}
			}
		}
		mesh.Clear();
		mesh.vertices = vertices.ToArray();
		mesh.uv = uv.ToArray();
		mesh.triangles = indices.ToArray();
		mesh.Optimize();
		mesh.RecalculateNormals();
	}

	
	public void ForceUpdate() {
		CalculateMesh();
	}
}
