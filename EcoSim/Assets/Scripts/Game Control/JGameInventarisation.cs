using UnityEngine;
using System.Globalization;
using System.Collections.Generic;


[System.Serializable]
public class JGameInventarisation : JGameTool {
	
	private const int maxIcons = 12;
	
	private Texture2D[] icons;
	private Texture2D[] activeIcons;

	GUIStyle textStyle;
	GUIStyle textStyleSelected;
//	GUIStyle priceStyle;
//	GUIStyle priceStyleSelected;
	
	void Start() {
		
		GUISkin skin = JGameControl.self.skin;
		textStyle = skin.FindStyle("Arial16-50");
		textStyleSelected = skin.FindStyle("Arial16-W");
//		priceStyle = skin.FindStyle("Arial16-50-Right");
//		priceStyleSelected = skin.FindStyle("Arial16-W-Right");
	}
	
	private int totalTiles;
	void GameIsLoaded() {
		totalTiles = JGameControl.scenario.data.width * JGameControl.scenario.data.height;
		icons = new Texture2D[maxIcons];
		activeIcons = new Texture2D[maxIcons];
		JScenario scenario = JGameControl.scenario;
		for (int i = 0; i < maxIcons; i++) {
			string iconName = (i < 8)?("Inventarisatie" + (i + 1)):("DierInventarisatie" + (i - 7));
			icons[i] = scenario.LoadTexure("Icons", iconName + "_w");
			activeIcons[i] = scenario.LoadTexure("Icons", iconName + "_zw");
		}
	}
	
	JInventarisationResultMap[] results;
		
	public override bool Render(GUISkin skin, int xo, int yo, int mx, int my, float dt) {
		JGameInventarisationResultWindow.RenderWindows(mx, my, dt);

		if (!isShown) {
			results = null;
			return false;
		}
		
		if (results == null) {
			results = JGameControl.scenario.progress.GetInventarisationResultMapArray();
		}
		
		bool childIsAlive = false;
		
		timeOut -= dt;
		
		int titleWidth = 230;
		MyGUI.Label(new Rect(xo, yo - 33, titleWidth, 32), "Inventarisaties", skin.FindStyle("ArialB16-50"));	
		if (results.Length == 0) {
			MyGUI.Label(new Rect(xo, yo, titleWidth, 32), "<Leeg>", skin.FindStyle("ArialB16-75"));	
		}
		else {
			int x = xo;
			int y = yo;
			foreach (JInventarisationResultMap result in results) {
				int researchType = ((int) result.researchType) - 0x21;
				bool hover = (x <= mx) && (mx <= x + titleWidth) && (y <= my) && (my <= y + 32);
				bool isClicked = false;
				isClicked |= MyGUI.Button(new Rect(x, y, 32, 32), hover?(activeIcons[researchType]):(icons[researchType]), hover?JGameControl.styleOn:JGameControl.styleOff);
				isClicked |= MyGUI.Button(new Rect(x + 33, y, 66, 32), result.year.ToString(), hover?textStyleSelected:textStyle);
				int measuredTiles = totalTiles - result.Count(0);
				int emptyTiles = result.Count(1);
				int nonemptyTiles = measuredTiles - emptyTiles;
				string score = nonemptyTiles.ToString() + "/" + measuredTiles.ToString();
				isClicked |= MyGUI.Button(new Rect(x + 100, y, titleWidth - 100, 32), score, hover?textStyleSelected:textStyle);
				y += 33;
				
				childIsAlive |= hover;
				
				if (isClicked) {
					// JGameControl.SetWindow(new JGameInventarisationResultWindow(33, 33, icons[researchType], result));
					new JGameInventarisationResultWindow(33 + titleWidth, 33, icons[researchType], result);
				}

			}
		}
		
		if (childIsAlive) KeepAlive();
		
		if (timeOut < 0) {
			isShown = false;
		}
		return true;
	}

}
