using UnityEngine;

public abstract class JSimpleWindow {
	
	protected int xo;
	protected int yo;
	protected GUISkin skin;
	
	protected JSimpleWindow(int x, int y) {
		xo = x;
		yo = y;
		skin = JGameControl.self.skin;
	}
	
	public abstract bool Render(int mx, int my, float dt);
	
	
	private bool isDragging = false;
	private int omx;
	private int omy;
	
	protected void HandleDrag(int mx, int my, int w) {
		if (isDragging) {
			if (Input.GetMouseButton(0)) {
				xo += mx - omx;
				yo += my - omy;
				omx = mx;
				omy = my;
				xo = Mathf.Clamp(xo, -w, JGameControl.width - 66);
				yo = Mathf.Clamp(yo, 0, JGameControl.height - 33);
			}
			else {
				isDragging = false;
			}
		}
		else {
			if (Input.GetMouseButtonDown(0)) {
				if ((mx >= xo + 33) && (mx < xo + 33 + w) && (my >= yo) && (my < yo + 33)) {
					isDragging = true;
					omx = mx;
					omy = my;
				}
			}
		}	
	}
}
