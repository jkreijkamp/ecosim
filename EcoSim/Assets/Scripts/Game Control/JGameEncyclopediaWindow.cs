using UnityEngine;
using System.Globalization;
using System.Collections.Generic;


[System.Serializable]
public class JGameEncyclopediaWindow : JSimpleWindow {
	static List<JGameEncyclopediaWindow> windows = new List<JGameEncyclopediaWindow>();
	
	readonly JEncyclopediaEntry entry;
	readonly Texture2D icon;
	readonly Texture2D img;
	readonly int height;
	GUIStyle textStyle;
	GUIStyle textStyleSelected;
	
	public static void CloseAllWindows() {
		windows.Clear();
	}
	
	public static void CloseWindow(string name) {
		name = name.ToLower();
		foreach (JGameEncyclopediaWindow win in windows) {
			if (win.entry.item.ToLower() == name) {
				removeList.Add(win);
			}
		}
	}

	private static List<JGameEncyclopediaWindow> removeList = new List<JGameEncyclopediaWindow>();
	public static void RenderWindows(int mx, int my, float dt) {
		foreach (JGameEncyclopediaWindow win in windows.ToArray()) {
			if (!win.Render(mx, my, dt)) {
				removeList.Add(win);
			}
		}
		if (removeList.Count > 0) {
			foreach (JGameEncyclopediaWindow win in removeList) {
				windows.Remove(win);
			}
			removeList.Clear();
		}
	}
	
	public JGameEncyclopediaWindow(int x, int y, Texture2D icon, JEncyclopediaEntry entry) 
		: base(x, y) {
		this.entry = entry;
		this.icon = icon;
		CloseWindow(entry.item);
		windows.Add(this);
		JGameControl.scenario.progress.MarkEncyclopediaEntryViewed(entry.item);
		Texture2D img = new Texture2D(2, 2);
		RenderFontToTexture rft = RenderFontToTexture.self;
		height = rft.RenderNewsArticle("[enc]\n" + entry.text, JGameControl.scenario, img);
		this.img = img;
		GUISkin skin = JGameControl.self.skin;
		
		textStyle = skin.FindStyle("ArialB16-75");
		textStyleSelected = skin.FindStyle("ArialB16-W");		
	}
	
	public override bool Render(int mx, int my, float dt) {
		HandleDrag(mx, my, 512);
		MyGUI.CheckMouseOver(new Rect(xo, yo, 361, 197));
		bool overClose = (mx >= xo) && (my >= yo) && (mx < xo + 32) && (my < yo + 32);
		if (MyGUI.Button(new Rect(xo, yo, 32, 32), overClose?(JGameControl.self.closeIconActive):(JGameControl.self.closeIcon), overClose?JGameControl.styleOn:JGameControl.styleOff)) {
			return false;
		}
		MyGUI.Label(new Rect(xo + 33, yo, 32, 32), icon, skin.FindStyle("75"));
		if (entry.externalURL != null) {
			if (MyGUI.Button(new Rect(xo + 65, yo, 223, 32), entry.item, textStyle)) {
				windows.Remove(this);
				windows.Add(this);
			}
			bool hover = (xo + 65 + 224 <= mx) && (mx <= xo + 512) && (yo <= my) && (my <= yo + 32);
//			Debug.Log("mx = " + mx + " hover = " + hover + " xo = " + xo);
			if (MyGUI.Button(new Rect(xo + 65 + 224, yo, 222, 32), "Meer info...", hover?textStyleSelected:textStyle)) {
				Debug.Log("Open url '" + entry.externalURL + "'");
				Application.OpenURL(entry.externalURL);
			}
		}
		else {
			if (MyGUI.Button(new Rect(xo + 65, yo, 446, 32), entry.item, textStyle)) {
				windows.Remove(this);
				windows.Add(this);
			}
		}
		MyGUI.Label(new Rect(xo, yo + 33, 512, height), img, GUIStyle.none);
		return true;
	}
}
