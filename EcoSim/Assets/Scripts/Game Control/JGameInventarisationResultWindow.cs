using UnityEngine;
using System.Globalization;
using System.Collections.Generic;


public class JGameInventarisationResultWindow : JSimpleWindow {

	static List<JGameInventarisationResultWindow> windows = new List<JGameInventarisationResultWindow>();
	
	
	public static void CloseAllWindows() {
		windows.Clear();
	}
	
	public static void CloseWindow(string speciesName, int year) {
		string title = year.ToString() + " - " + speciesName;

		foreach (JGameInventarisationResultWindow win in windows) {
			if (win.title == title) {
				removeList.Add(win);
			}
		}
	}

	private static List<JGameInventarisationResultWindow> removeList = new List<JGameInventarisationResultWindow>();
	
	public static void RenderWindows(int mx, int my, float dt) {
		foreach (JGameInventarisationResultWindow win in windows) {
			if (!win.Render(mx, my, dt)) {
				removeList.Add(win);
			}
		}
		if (removeList.Count > 0) {
			foreach (JGameInventarisationResultWindow win in removeList) {
				windows.Remove(win);
			}
			removeList.Clear();
		}
	}
	
	
	Texture2D icon;
//	JInventarisationResultMap resultMap;
	string title;
	string speciesName;
	int noneCount = 0;
	int lowCount = 0;
	int mediumCount = 0;
	int highCount = 0;
	int deathCount = 0;
	int foodCount = 0;
	int nestsCount = 0;
	int trailCount = 0;
	bool isAnimalSpecies;
	
	Texture2D overledenIcon;
	Texture2D nestLocatieIcon;
	Texture2D padIcon;
	Texture2D fourageIcon;
	Texture2D afwezigIcon;
	Texture2D lowIcon;
	Texture2D mediumIcon;
	Texture2D highIcon;
	
	public JGameInventarisationResultWindow(int x, int y, Texture2D icon, JInventarisationResultMap resultMap)
		: base(x, y) {
		this.icon = icon;
//		this.resultMap = resultMap
		isAnimalSpecies = (resultMap.researchType > EResearchTypes.Inventarisatie8);
		int index = ((int) resultMap.researchType) & 0x0f;
		if (isAnimalSpecies) index -= 8;
		JScenario scenario = JGameControl.scenario;
		Texture2D tex = scenario.LoadTexure("AreaTextures", (isAnimalSpecies?"Aanwezigheid_diersoorten ":"Aanwezigheid_soorten ") + index);
	
		JDisplayAreas.Cleanup();
		
		JDisplayAreas.SetupDisplayAreas(tex, resultMap);
		noneCount = resultMap.Count(1);
		if (isAnimalSpecies) {
			foodCount = resultMap.Count(12);
			deathCount = resultMap.Count(13);
			nestsCount = resultMap.Count(14);
			trailCount = resultMap.Count(2) + resultMap.Count(3) + resultMap.Count(4) + resultMap.Count(5) + resultMap.Count(6)
				+ resultMap.Count(7);
			
			overledenIcon = scenario.LoadTexure("Icons", "Dier_Overleden");
			fourageIcon = scenario.LoadTexure("Icons", "Dier_Voedsel");
			nestLocatieIcon = scenario.LoadTexure("Icons", "Dier_NestLocatie");
			padIcon = scenario.LoadTexure("Icons", "Dier_Pad");
			afwezigIcon = scenario.LoadTexure("Icons", "Dier_Afwezig");
		}
		else {
			lowCount = resultMap.Count(2);
			mediumCount = resultMap.Count(3);
			highCount = resultMap.Count(4);
			afwezigIcon = scenario.LoadTexure("Icons", "Plant_Afwezig");
			lowIcon = scenario.LoadTexure("Icons", "Plant_Matig");
			mediumIcon = scenario.LoadTexure("Icons", "Plant_Frequent");
			highIcon = scenario.LoadTexure("Icons", "Plant_Algemeen");
		}
		speciesName = JGameControl.scenario.research[resultMap.researchType].name;
		title = resultMap.year.ToString() + " - " + speciesName;
		
		// CloseWindow(speciesName, resultMap.year);
		CloseAllWindows();
		windows.Add(this);
	}
	
	public override bool Render(int mx, int my, float dt) {
//		int pricePerTile = researchInfo.costPerTile;
//		selectedTiles = JSelectAreas.self.adjustedTotalTiles;
		HandleDrag(mx, my, 329);
		MyGUI.CheckMouseOver(new Rect(xo, yo, 361, 197));
		bool overClose = (mx >= xo) && (my >= yo) && (mx < xo + 32) && (my < yo + 32);
		if (MyGUI.Button(new Rect(xo, yo, 32, 32), overClose?(JGameControl.self.closeIconActive):(JGameControl.self.closeIcon), overClose?JGameControl.styleOn:JGameControl.styleOff)) {
			JDisplayAreas.Cleanup();
			return false;
		}
		MyGUI.Label(new Rect(xo + 33, yo, 32, 32), icon, skin.FindStyle("75"));
		MyGUI.Label(new Rect(xo + 65, yo, 296, 32), title, skin.FindStyle("ArialB16-75"));
		MyGUI.Label(new Rect(xo, yo + 33, 361, 32), "", skin.FindStyle("50"));
		
		if (isAnimalSpecies) {
			MyGUI.Label(new Rect(xo, yo + 66, 32, 32), nestLocatieIcon, skin.FindStyle("50"));
			MyGUI.Label(new Rect(xo + 33, yo + 66, 261, 32), "nest lokaties", skin.FindStyle("Arial16-50"));
			MyGUI.Label(new Rect(xo + 262, yo + 66, 99, 32), nestsCount.ToString(), skin.FindStyle("Arial16-50-Right"));

			MyGUI.Label(new Rect(xo, yo + 99, 32, 32), overledenIcon, skin.FindStyle("50"));
			MyGUI.Label(new Rect(xo + 33, yo + 99, 261, 32), "overleden", skin.FindStyle("Arial16-50"));
			MyGUI.Label(new Rect(xo + 262, yo + 99, 99, 32), deathCount.ToString(), skin.FindStyle("Arial16-50-Right"));
			
			MyGUI.Label(new Rect(xo, yo + 132, 32, 32), fourageIcon, skin.FindStyle("50"));
			MyGUI.Label(new Rect(xo + 33, yo + 132, 261, 32), "fourage", skin.FindStyle("Arial16-50"));
			MyGUI.Label(new Rect(xo + 262, yo + 132, 99, 32), foodCount.ToString(), skin.FindStyle("Arial16-50-Right"));
	
			MyGUI.Label(new Rect(xo, yo + 165, 32, 32), padIcon, skin.FindStyle("50"));
			MyGUI.Label(new Rect(xo + 33, yo + 165, 261, 32), "sporen", skin.FindStyle("Arial16-50"));
			MyGUI.Label(new Rect(xo + 262, yo + 165, 99, 32), trailCount.ToString(), skin.FindStyle("Arial16-50-Right"));
	
			MyGUI.Label(new Rect(xo, yo + 198, 32, 32), afwezigIcon, skin.FindStyle("50"));
			MyGUI.Label(new Rect(xo + 33, yo + 198, 261, 32), "afwezig", skin.FindStyle("Arial16-50"));
			MyGUI.Label(new Rect(xo + 262, yo + 198, 99, 32), noneCount.ToString(), skin.FindStyle("Arial16-50-Right"));
		}
		else {
			MyGUI.Label(new Rect(xo, yo + 66, 32, 32), afwezigIcon, skin.FindStyle("50"));
			MyGUI.Label(new Rect(xo + 33, yo + 66, 261, 32), "afwezig", skin.FindStyle("Arial16-50"));
			MyGUI.Label(new Rect(xo + 262, yo + 66, 99, 32), noneCount.ToString(), skin.FindStyle("Arial16-50-Right"));
	
			MyGUI.Label(new Rect(xo, yo + 99, 32, 32), lowIcon, skin.FindStyle("50"));
			MyGUI.Label(new Rect(xo + 33, yo + 99, 261, 32), "zeldzaam", skin.FindStyle("Arial16-50"));
			MyGUI.Label(new Rect(xo + 262, yo + 99, 99, 32), lowCount.ToString(), skin.FindStyle("Arial16-50-Right"));
	
			MyGUI.Label(new Rect(xo, yo + 132, 32, 32), mediumIcon, skin.FindStyle("50"));
			MyGUI.Label(new Rect(xo + 33, yo + 132, 261, 32), "frequent", skin.FindStyle("Arial16-50"));
			MyGUI.Label(new Rect(xo + 262, yo + 132, 99, 32), mediumCount.ToString(), skin.FindStyle("Arial16-50-Right"));
	
			MyGUI.Label(new Rect(xo, yo + 165, 32, 32), highIcon, skin.FindStyle("50"));
			MyGUI.Label(new Rect(xo + 33, yo + 165, 261, 32), "algemeen", skin.FindStyle("Arial16-50"));
			MyGUI.Label(new Rect(xo + 262, yo + 165, 99, 32), highCount.ToString(), skin.FindStyle("Arial16-50-Right"));
		}
		return true;
	}
}
