using UnityEngine;
using System.Globalization;
using System.Collections.Generic;


[System.Serializable]
public class JGameEncyclopedia : JGameTool {
	
	public Texture2D icon;
	
	GUIStyle textStyle;
	GUIStyle textStyleSelected;
	
	public static bool OpenEncyclopedia(string entryName) {
		GameObject go = GameObject.Find("Bibliotheek");
		if (!go) return false;
		JGameEncyclopedia self = go.GetComponent<JGameEncyclopedia>();
		if (!self) return false;
		entryName.ToLower();
		foreach (JEncyclopediaEntry e in JGameControl.scenario.encyclopedia) {
			if (e.item.ToLower() == entryName) {
				self.OpenEncyclopedia(32, 64, e);
				return true;
			}
		}
		return false;
	}
	
	public void OpenEncyclopedia(int xo, int yo, JEncyclopediaEntry entry) {
		JScenario scenario = JGameControl.scenario;
		new JGameEncyclopediaWindow(xo, yo, icon, entry);
		scenario.context.SetGlobal("bibliotheekartikel", entry.item);
		scenario.ProcessGoals(JScenarioGoal.M_AFTER_ENCYCLOPEDIA);			
	}
	
	void Start() {
		GUISkin skin = JGameControl.self.skin;
		textStyle = skin.FindStyle("Arial16-50");
		textStyleSelected = skin.FindStyle("Arial16-W");
	}
	
	private Dictionary<char, List<JEncyclopediaEntry>> entries;
	
	void GameIsLoaded() {
		entries = new Dictionary<char, List<JEncyclopediaEntry>>();
		foreach (JEncyclopediaEntry e in JGameControl.scenario.encyclopedia) {
			if (e.item.Length > 0) {
				char c = e.item[0];
				List<JEncyclopediaEntry> entriesForChar = null;
				if (!entries.ContainsKey(c)) {
					entriesForChar = new List<JEncyclopediaEntry>();
					entries.Add(c, entriesForChar);
				}
				else {
					entriesForChar = entries[c];
				}
				entriesForChar.Add(e);
			}
		}
		JGameEncyclopediaWindow.CloseAllWindows();
		JHelpWindow.CloseAllWindows();
	}
	
	private char lastChar = '\0';
		
	public override bool Render(GUISkin skin, int xo, int yo, int mx, int my, float dt) {
		JGameEncyclopediaWindow.RenderWindows(mx, my, dt);
		if (!isShown) {
			lastChar = '\0';
			return false;
		}
		
		
		bool childIsAlive = false;
		
		timeOut -= dt;
		
		int charCount = entries.Count;
		int width = Mathf.Max(charCount, 7) * 33 - 1;
		MyGUI.Label(new Rect(xo, yo - 33, width, 32), "Bibliotheek", skin.FindStyle("ArialB16-50"));
		int x = xo;
		int y = yo;
		foreach (KeyValuePair<char, List<JEncyclopediaEntry>> kv in entries) {
			bool hover = (x <= mx) && (mx <= x + 32) && (y <= my) && (my <= y + 32);
			childIsAlive |= hover;
			MyGUI.Label(new Rect(x, y, 32, 32), kv.Key.ToString(), hover?textStyleSelected:textStyle);
			if (hover) {
				lastChar = kv.Key;
			}
			if (kv.Key == lastChar) {
				List<JEncyclopediaEntry> charEntries = kv.Value;
				int y2 = y;
				for (int yy = 0; yy < charEntries.Count; yy++) {
					y2 += 33;
					hover = (x <= mx) && (mx <= x + 231) && (y2 <= my) && (my <= y2 + 32);
					childIsAlive |= hover;
					if (MyGUI.Button(new Rect(x, y2, 231, 32), charEntries[yy].item, hover?textStyleSelected:textStyle)) {
						lastChar = '\0';
						isShown = false;
						OpenEncyclopedia(x + 231, yo, charEntries[yy]);
						return false;
					}
				}
			}
			x += 33;
		}
		if (x - xo < width) {
			MyGUI.Label(new Rect(x, y, width - x + xo, 32), "", textStyle);			
		}
		if (childIsAlive) KeepAlive();
		
		if (timeOut < 0) {
			isShown = false;
		}
		return true;
	}

}
