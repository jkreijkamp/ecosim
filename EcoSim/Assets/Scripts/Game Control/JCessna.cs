using UnityEngine;
using System.Collections;

public class JCessna : MonoBehaviour {

	public GameObject[] banners;
	int currentBanner = 0;
	
	Renderer bannerRenderer;
	
	Vector3 target;
	Vector3 position;
	Quaternion rot;
	Transform myTransform;
	float minHeight = 50f;
	
	void Start() {
		myTransform = transform;
		rot = myTransform.localRotation;
		position = myTransform.localPosition;
		SelectBanner();
		SelectTarget();
		StartCoroutine(COSelectNewBanner());
		StartCoroutine(CORandomNewTarget());
		StartCoroutine(COPreventCrash());
	}
	
	void SelectBanner() {
		foreach (GameObject banner in banners) {
			banner.SetActive(false);
		}
		currentBanner = Random.Range(0, banners.Length);
		banners[currentBanner].SetActive(true);
		bannerRenderer = banners[currentBanner].GetComponentInChildren<Renderer>();
	}
	
	IEnumerator CORandomNewTarget() {
		while (true) {
			yield return new WaitForSeconds(Random.Range(2, 7));
			SelectTarget();
		}
	}
	
	IEnumerator COSelectNewBanner() {
		while (true) {
			yield return new WaitForSeconds(2);
			if (!bannerRenderer.isVisible) {
				SelectBanner();
			}
		}
	}
	

	IEnumerator COPreventCrash() {
		while (true) {
			yield return new WaitForSeconds(1.0f);
			if (JCamera.Scenario != null) {
				JTerrainData data = JCamera.Scenario.data;
				Vector3 pos = myTransform.localPosition;
				int x = Mathf.RoundToInt(pos.x / JTerrainData.HORIZONTAL_SCALE);
				int y = Mathf.RoundToInt(pos.z / JTerrainData.HORIZONTAL_SCALE);
				x = Mathf.Clamp(x, 0, data.width - 1);
				y = Mathf.Clamp(y, 0, data.height - 1);
				int cx = x >> JTerrainData.CELL_SIZE2EXP;
				int cy = y >> JTerrainData.CELL_SIZE2EXP;
			 	JCellData cell = data.GetCell(cx, cy);
				if (cell.isLoaded) {
					minHeight = data.GetHeight(x, y) + 30.0f;
				}
			}
		}
	}

	void SwitchedToNearCamera() {
		Transform camT = JCamera.GetTransform();
		Vector3 pos = camT.position - camT.forward * 100f - camT.up * 600f;
		pos.y = Mathf.Min(100, camT.position.y);
		myTransform.position = pos;
		position = pos;
		SelectTarget();
//		Debug.Log(name + " jumped to " + myTransform.position + " myt " + camT.name);
	}

	void SelectTarget() {
		if (JCamera.Scenario == null) {
			target = new Vector3(Random.Range(0, 5120), Random.Range(500, 1000), Random.Range(0, 5120));
			return;
		}
		JTerrainData data = JCamera.Scenario.data;
		
		Transform camT = JCamera.GetTransform();
		
		Vector3 pos = camT.position + camT.forward * Random.Range(-5f, 200f) + camT.right * Random.Range(-75f, 75f);
		pos.y += 25;
		
//		pos.y = myTransform.position.y;
		
		int x = Mathf.RoundToInt(pos.x / JTerrainData.HORIZONTAL_SCALE);
		int y = Mathf.RoundToInt(pos.z / JTerrainData.HORIZONTAL_SCALE);
		x = Mathf.Clamp(x, 0, data.width - 1);
		y = Mathf.Clamp(y, 0, data.height - 1);
		int cx = x >> JTerrainData.CELL_SIZE2EXP;
		int cy = y >> JTerrainData.CELL_SIZE2EXP;
	 	JCellData cell = data.GetCell(cx, cy);
		int ch = 200;
		if (cell.isLoaded) {
			ch = ((int) data.GetHeight(x, y)) + Random.Range(10, 30);
		}
		ch = Mathf.Max(ch, (int) pos.y);
		target = new Vector3(x * JTerrainData.HORIZONTAL_SCALE, ch, y * JTerrainData.HORIZONTAL_SCALE);
	}
	
	
	void Update() {
		float deltaTime = Time.deltaTime;
		rot = Quaternion.RotateTowards(rot, Quaternion.LookRotation(target - position, Vector3.up), 6 * deltaTime);
		myTransform.localRotation = rot;
		position += deltaTime * 35 * myTransform.forward;
		if (position.y < minHeight) {
			SelectTarget();
			position.y = minHeight;
		}
		myTransform.localPosition = position;
		if ((position - target).sqrMagnitude < 1000000) {
			SelectTarget();
		}
	}
}
