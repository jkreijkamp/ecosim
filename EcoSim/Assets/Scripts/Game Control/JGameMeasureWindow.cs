using UnityEngine;
using System.Globalization;


public class JGameMeasureWindow : JSimpleWindow {
	
	Texture2D icon;
	JScenario.ActionInfo actionInfo;
	int selectedTiles = 0;
	int startSelectedTiles = 0;
	
	public bool IsValidTile(int x, int y) {
		JTerrainData data = JGameControl.scenario.data;
		if ((actionInfo.action == EActionTypes.GravenSloot) && ((data.GetSpecial(x, y) & 0x0f) > 0)) return false;
		if (((actionInfo.action == EActionTypes.DempenSloot) || (actionInfo.action == EActionTypes.VerdiepenSloot) ||
			(actionInfo.action == EActionTypes.VerontdiepenSloot))
			&& ((data.GetSpecial(x, y) & 0x0f) == 0)) return false;
		return data.NeedCalculateSuccession(x, y) && data.GetVegetation(x, y).IsValidMeasure(actionInfo.action);
	}
	
	public JGameMeasureWindow(int x, int y, Texture2D icon, JScenario.ActionInfo ai)
		: base(x, y) {
		this.icon = icon;
		this.actionInfo = ai;
		EActionGroups actionGroup = (EActionGroups) (((int) ai.action) & 0xf0);
		int mapIndex = ((int) ai.action) & 0x0f;
		JAreaMap map = JGameControl.scenario.progress.GetMeasureMap(actionGroup);
		JSelectAreas.SetupSelectAreas(map, actionGroup.ToString(), mapIndex, IsValidTile);
		startSelectedTiles = JSelectAreas.self.adjustedTotalTiles;
		JShowSuccessionArea.ClearMap();
	}
	
	public override bool Render(int mx, int my, float dt) {
		int pricePerTile = actionInfo.costPerTile;
		selectedTiles = JSelectAreas.self.adjustedTotalTiles;
		HandleDrag(mx, my, 329+32);
		MyGUI.CheckMouseOver(new Rect(xo, yo, 329+32, 165 + 32));
		bool overClose = (mx >= xo) && (my >= yo) && (mx < xo + 32) && (my < yo + 32);
		if (MyGUI.Button(new Rect(xo, yo, 32, 32), overClose?(JGameControl.self.closeIconActive):(JGameControl.self.closeIcon), overClose?JGameControl.styleOn:JGameControl.styleOff)) {
			JSelectAreas.CleanupSelectArea();
			JShowSuccessionArea.UpdateMap();
			return false;
		}
		MyGUI.Label(new Rect(xo + 33, yo, 32, 32), icon, skin.FindStyle("75"));
		MyGUI.Label(new Rect(xo + 33 + 32, yo, 330, 32), actionInfo.name, skin.FindStyle("ArialB16-75"));

		MyGUI.Label(new Rect(xo, yo + 33, 395, 32), "", skin.FindStyle("50"));

		MyGUI.Label(new Rect(xo, yo + 66, 230, 32), "Geselecteerde tegels", skin.FindStyle("Arial16-50"));
		MyGUI.Label(new Rect(xo + 231, yo + 66, 131, 32), selectedTiles.ToString(), skin.FindStyle("Arial16-50-Right"));
		MyGUI.Label(new Rect(xo + 231 + 131 + 1, yo + 66, 32, 32), "", skin.FindStyle("50"));

		MyGUI.Label(new Rect(xo, yo + 99, 230, 32), "kosten per tegel", skin.FindStyle("Arial16-50"));
		MyGUI.Label(new Rect(xo + 231, yo + 99, 131, 32), pricePerTile.ToString("#,##0", CultureInfo.GetCultureInfo("nl-NL"))+",-",
		          skin.FindStyle("Arial16-50-Right"));
		MyGUI.Label(new Rect(xo + 231 + 131 + 1, yo + 99, 32, 32), "x", skin.FindStyle("Arial16-50"));

		MyGUI.Label(new Rect(xo, yo + 132, 230, 32), "Kosten totaal", skin.FindStyle("Arial16-50"));
		MyGUI.Label(new Rect(xo + 231, yo + 132, 131, 32), (pricePerTile * selectedTiles).ToString("#,##0", CultureInfo.GetCultureInfo("nl-NL"))+",-",
		          skin.FindStyle("Arial16-50-Right"));
		MyGUI.Label(new Rect(xo + 231 + 131 + 1, yo + 132, 32, 32), "=", skin.FindStyle("Arial16-50"));

		MyGUI.Label(new Rect(xo, yo + 165, 230, 32), "", skin.FindStyle("50"));
		
		Rect bRect = new Rect(xo + 231, yo + 165, 164, 32);
		bool hover = bRect.Contains(new Vector2(mx, my));
		if (MyGUI.Button(bRect, "Voer uit", skin.FindStyle(hover?"Arial16-W-Centre":"Arial16-75-Centre"))) {
			JSelectAreas.SaveMap();
			JSelectAreas.CleanupSelectArea();
			JGameControl.self.CalculateYearCosts();
			JGameControl.scenario.oldContext.SetValue("LASTMEASURE", (long) actionInfo.action);
			JGameControl.scenario.context.Set("laatstemaatregel", (actionInfo.action.ToString().ToLower()));
			JGameControl.scenario.context.Set("laatstemaatregelaantal", selectedTiles - startSelectedTiles);
			EActionGroups actionGroup = (EActionGroups) (((int) actionInfo.action) & 0xf0);
			JGameControl.scenario.context.Set("laatstemaatregelgroep", (actionGroup.ToString().ToLower()));
			

			JGameControl.scenario.ProcessGoals(JScenarioGoal.M_AFTER_MEASURE);
			JShowSuccessionArea.UpdateMap();

			return false;
		}
		
		
		return true;
	}
}
