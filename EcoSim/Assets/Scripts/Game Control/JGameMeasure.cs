using UnityEngine;
using System.Globalization;
using System.Collections.Generic;


[System.Serializable]
public class JGameMeasure : JGameTool {
		
	class MeasureInfo {
		public EActionTypes action;
		public Texture2D icon;
		public Texture2D activeIcon;
		public int price;
		public string name;
		public string description;
	}
	
	class ShowGroups {
		public Texture2D icon;
		public Texture2D activeIcon;
		public MeasureInfo[] measures;
	}
	
	ShowGroups[] showGroups;

	GUIStyle textStyle;
	GUIStyle textStyleSelected;
	GUIStyle priceStyle;
	GUIStyle priceStyleSelected;
	
	void Start() {
		GUISkin skin = JGameControl.self.skin;
		textStyle = skin.FindStyle("Arial16-50");
		textStyleSelected = skin.FindStyle("Arial16-W");
		priceStyle = skin.FindStyle("Arial16-50-Right");
		priceStyleSelected = skin.FindStyle("Arial16-W-Right");
	}
	
	void GameIsLoaded() {
//		Debug.Log("Game is Loaded!");
		JScenario scenario = JGameControl.scenario;
		if (scenario != null) {
			List<ShowGroups> sg = new List<ShowGroups>();
			foreach (EActionGroups eg in EnumExtensions.actionGroupArray) {
				string groupIconName = eg.ToString();
				Texture2D groupIcon = scenario.LoadTexure("Icons", groupIconName + "_w");
				Texture2D groupActiveIcon = scenario.LoadTexure("Icons", groupIconName + "_zw");
				List<MeasureInfo> infoList = new List<MeasureInfo>();
				foreach (EActionTypes ea in EnumExtensions.GetActionTypesForGroup(eg)) {
					JScenario.ActionInfo ai = scenario.GetActionInfo(ea);
					if (ai != null) {
						MeasureInfo mi = new MeasureInfo();
						mi.action = ea;
						mi.price = ai.costPerTile;
						mi.name = ai.name;
						mi.description = ai.descr;
						string iconName = ea.ToString();
						Texture2D icon = scenario.LoadTexure("Icons", iconName + "_w");
						Texture2D activeIcon = scenario.LoadTexure("Icons", iconName + "_zw");
						if (icon == null) {
							Debug.LogWarning("No icon for '" + iconName + "'");
							icon = groupIcon;
							activeIcon = groupActiveIcon;
						}
						mi.icon = icon;
						mi.activeIcon = activeIcon;
						
						infoList.Add(mi);
					}
				}
				if (infoList.Count > 0) {
					ShowGroups newSG = new ShowGroups();
						if (groupIcon == null) {
							Debug.LogWarning("No icon for '" + groupIconName + "'");
						}
					newSG.icon = groupIcon;
					newSG.activeIcon = groupActiveIcon;
					newSG.measures = infoList.ToArray();
					sg.Add(newSG);
				}
			}
			showGroups = sg.ToArray();
		}
	}
	
	int activeGroupIndex = -1;
	
	public override bool Render(GUISkin skin, int xo, int yo, int mx, int my, float dt) {
		if (!isShown) {
			activeGroupIndex = -1;
			return false;
		}
		timeOut -= dt;
		
		int titleWidth = Mathf.Max(33 * showGroups.Length, 230);
		MyGUI.Label(new Rect(xo, yo - 33, titleWidth, 32), "Maatregelen", skin.FindStyle("ArialB16-50"));		
		
		int x = xo;
		int y = yo;

		bool childIsAlive = false;
		
		int overGroupIndex = -1;
		for (int i = 0; i < showGroups.Length; i++) {
			ShowGroups g = showGroups[i];
			bool hover = (x <= mx) && (mx <= x + 32) && (y <= my) && (my <= y + 32);
			
			if (hover) overGroupIndex = i;
//			if (g.measure != null) {
//				if (hover) {
//					g.measure.KeepAlive();
//				}
//				hover = g.measure.Render(skin, x, y + 33, mx, my, dt);
//				if (hover) {
//					JXGameGUI.SetHelp(g.icon, g.help, false);
//				}
//			}
			MyGUI.Label(new Rect(x, y, 32, 32), hover?(g.activeIcon):(g.icon), hover?JGameControl.styleOn:JGameControl.styleOff);			
			childIsAlive |= hover;
			x += 33;
			
		}
		if (x - xo < titleWidth) {
			MyGUI.Label(new Rect(x, y, titleWidth - x + xo, 32), "", JGameControl.styleOff);			
		}
		if (overGroupIndex >= 0) activeGroupIndex = overGroupIndex;
		if (activeGroupIndex >= 0) {
			ShowGroups g = showGroups[activeGroupIndex];
			for (int i = 0; i < g.measures.Length; i++) {
				MeasureInfo m = g.measures[i];
				x = xo + 33 * activeGroupIndex;
				y = yo + 33 * (i + 1);
				bool hover = (x <= mx) && (mx <= x + 32 + 290 + 100 + 2) && (y <= my) && (my <= y + 32);
				bool isClicked = false;
				isClicked |= MyGUI.Button(new Rect(x, y, 32, 32), hover?(m.activeIcon):(m.icon), hover?JGameControl.styleOn:JGameControl.styleOff);
				isClicked |= MyGUI.Button(new Rect(x + 33, y, 290, 32), m.name, hover?textStyleSelected:textStyle);
				isClicked |= MyGUI.Button(new Rect(x + 33 + 290 + 1, y, 100, 32),
					m.price.ToString("#,##0", CultureInfo.GetCultureInfo("nl-NL"))+",-",
			        hover?priceStyleSelected:priceStyle);
				childIsAlive |= hover;
				if (hover && (m.description != "")) {
					JGameControl.SetHelp(m.icon, m.description, false);
				}
				if (isClicked) {
					JScenario scenario = JGameControl.scenario;
					JScenario.ActionInfo info = scenario.actions[m.action];
					if (info.actionGroup != null) {
						JGameControl.SetWindow(new JGameObjectMeasureWindow(33, 33, m.icon, JGameControl.scenario.GetActionInfo(m.action)));
					}
					else {
						JGameControl.SetWindow(new JGameMeasureWindow(33, 33, m.icon, JGameControl.scenario.GetActionInfo(m.action)));
					}
				}
			}
		}
//		if (overGroup != null) {
//			foreach (Group g in groups) {
//				if ((g != overGroup) && (g.measure != null)) g.measure.Hide();
//			}
//		}
		
		if (childIsAlive) KeepAlive();
		
		if (timeOut < 0) {
			isShown = false;
		}
		return true;
	}

}
