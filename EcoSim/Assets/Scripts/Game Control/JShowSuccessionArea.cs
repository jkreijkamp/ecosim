using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JShowSuccessionArea : MonoBehaviour {
	
	public Material material;
	
	private static JShowSuccessionArea self;
	
	private int updateId = 0;
	
	private JShowSuccessionAreaCell[,] cells = null;
	
	void Awake() {
		self = this;
	}
	
	public static void ClearMap() {
		if (self.cells != null) {
			JScenario scenario = JGameControl.scenario;
			int height = scenario.data.cHeight;
			int width = scenario.data.cWidth;
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					if (self.cells[y, x]) {
						Destroy(self.cells[y, x].gameObject);
						self.cells[y, x] = null;
					}
				}
			}
		}
	}
	
	public static void UpdateMap() {
		if (self) {
			self.StartCoroutine(self.COUpdateMap(++self.updateId));
		}
	}
	
	IEnumerator COUpdateMap(int id) {
		JScenario scenario = JGameControl.scenario;
		int height = scenario.data.cHeight;
		int width = scenario.data.cWidth;
		if (cells == null) {
			cells = new JShowSuccessionAreaCell[height, width];
		}
		float scale = JTerrainData.CELL_SIZE;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (id != updateId) yield break;
				JCellData cell = scenario.data.GetCell(x, y);
				if (cell.NeedCalculateSuccession()) {
					if (cells[y, x] == null) {
						GameObject go = new GameObject("ShowSA" + x + "." + y);
						go.transform.parent = transform;
						go.transform.localPosition = new Vector3(x * scale, 1f, y * scale);
						go.transform.localScale = Vector3.one;
						JShowSuccessionAreaCell newCell = go.AddComponent<JShowSuccessionAreaCell>();
						cells[y, x] = newCell;
						newCell.cellX = x;
						newCell.cellY = y;
						newCell.cell = cell;
						newCell.material = material;
					}
					else {
						cells[y, x].ForceUpdate();
					}
				}
				yield return 0;
			}
		}
	}
}
