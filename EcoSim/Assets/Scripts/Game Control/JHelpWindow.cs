using UnityEngine;
using System.Collections;

public class JHelpWindow : JSimpleWindow {

	readonly Texture2D icon;
	readonly Texture2D img;
	readonly int height;
	GUIStyle textStyle;
//	GUIStyle textStyleSelected;
	
	public static JHelpWindow window;
	
	public static void CloseAllWindows() {
		window = null;
	}
	
	public static void RenderWindows(int mx, int my, float dt) {
		if (window != null) {
			if (!window.Render(mx, my, dt)) {
				window = null;
				JGameControl.showToolHelp = false;
			}
		}
	}
	public JHelpWindow(int x, int y, Texture2D icon) 
		: base(x, y) {
		this.icon = icon;
		Texture2D img = new Texture2D(2, 2);
		RenderFontToTexture rft = RenderFontToTexture.self;
		string helpText;
		TextAsset da = Resources.Load("help") as TextAsset;
		if (da) helpText = da.text;
		else helpText = "Helptekst niet gevonden.";
		height = rft.RenderNewsArticle("[enc]\n" + helpText, JGameControl.scenario, img);
		this.img = img;
		GUISkin skin = JGameControl.self.skin;
		
		textStyle = skin.FindStyle("ArialB16-75");
//		textStyleSelected = skin.FindStyle("ArialB16-W");
		window = this;
		JGameControl.showToolHelp = true;
	}
	
	public override bool Render(int mx, int my, float dt) {
		HandleDrag(mx, my, 512);
		MyGUI.CheckMouseOver(new Rect(xo, yo, 361, 197));
		bool overClose = (mx >= xo) && (my >= yo) && (mx < xo + 32) && (my < yo + 32);
		if (MyGUI.Button(new Rect(xo, yo, 32, 32), overClose?(JGameControl.self.closeIconActive):(JGameControl.self.closeIcon), overClose?JGameControl.styleOn:JGameControl.styleOff)) {
			return false;
		}
		MyGUI.Label(new Rect(xo + 33, yo, 32, 32), icon, skin.FindStyle("75"));
		MyGUI.Label(new Rect(xo + 65, yo, 446, 32), "Interface uitleg", textStyle);
		MyGUI.Label(new Rect(xo, yo + 33, 512, height), img, GUIStyle.none);
		return true;
	}
	
}
