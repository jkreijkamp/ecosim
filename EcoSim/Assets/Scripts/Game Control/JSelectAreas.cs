using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class JSelectAreas : MonoBehaviour {
	
	public delegate bool delegateIsValid(int x, int y);

	public GameObject areaPrefab;
	public Material areaMaterial;
	
	public JTerrainData data;
	
	public static JSelectAreas self;

	private int xp1;
	private int xp2;
	private int yp1;
	private int yp2;
	
	private Mesh mesh;
	
	private JAreaMap originalMap;
	private JAreaMap map;
	private int mapIndex;
	private JSelectArea[,] areas;
//	private Dictionary<string, Texture2D> textures;
	
	private int totalTiles;
	public int adjustedTotalTiles;
	
	private delegateIsValid checkPointDelegate;
	
	void CalculateTotalTiles() {
		totalTiles = 0;
		foreach (JSelectArea area in areas) {
			if (area != null) {
				totalTiles += area.mapCell.GetCount(mapIndex);
			}
		}
		adjustedTotalTiles = totalTiles;
	}
	
	void GenerateMesh() {
		adjustedTotalTiles = totalTiles;
		if (data == null) {
			Debug.LogError("no terrain data");
			return;
		}
		int xMin = Mathf.Max(Mathf.Min(xp1, xp2), 0);
		int xMax = Mathf.Min(Mathf.Max(xp1, xp2), data.width - 1);
		int yMin = Mathf.Max(Mathf.Min(yp1, yp2), 0);
		int yMax = Mathf.Min(Mathf.Max(yp1, yp2), data.height - 1);
//		Debug.Log("[" + xMin +", " + yMin + "] - [" + xMax + ", " + yMax + "]");
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> indices = new List<int>();
		float uvStep = 1f / 4f;
		
		int count = 0;
		
		for (int y = yMin; y <= yMax; y++) {
			for (int x = xMin; x <= xMax; x++) {
				bool addPoint = isAdd;
				if ((isAdd) && (checkPointDelegate != null)) {
					addPoint = checkPointDelegate(x, y);
				}
				int tile = addPoint?mapIndex:15;
				int uvX = tile % 4;
				int uvY = tile / 4;
				uv.Add(new Vector2(uvStep * uvX, uvStep * uvY));
				uv.Add(new Vector2(uvStep * (uvX + 1), uvStep * uvY));
				uv.Add(new Vector2(uvStep * uvX, uvStep * (uvY + 1)));
				uv.Add(new Vector2(uvStep * (uvX + 1), uvStep * (uvY + 1)));
				vertices.Add(new Vector3(x, data.GetCornerHeight(x, y), y));
				vertices.Add(new Vector3(x + 1, data.GetCornerHeight(x + 1, y), y));
				vertices.Add(new Vector3(x, data.GetCornerHeight(x, y + 1), y + 1));
				vertices.Add(new Vector3(x + 1, data.GetCornerHeight(x + 1, y + 1), y + 1));
				indices.Add(count + 1);
				indices.Add(count);
				indices.Add(count + 2);
				indices.Add(count + 1);
				indices.Add(count + 2);
				indices.Add(count + 3);
				count += 4;
				if (addPoint) {
					if (!IsSelected(x, y)) adjustedTotalTiles++;
				}
				else if (!isAdd) {
					if (IsSelected(x, y)) adjustedTotalTiles--;
				}
			}
		}
		mesh.Clear();
		mesh.vertices = vertices.ToArray();
		mesh.uv = uv.ToArray();
		mesh.triangles = indices.ToArray();
		mesh.Optimize();
		mesh.RecalculateNormals();
//		Debug.Log("mesh vc = " + mesh.vertexCount);
		GetComponent<MeshFilter>().sharedMesh = mesh;
	}

	void AddOrSubstract(bool isAdding) {
		if (data == null) return;
		int xMin = Mathf.Max(Mathf.Min(xp1, xp2), 0);
		int xMax = Mathf.Min(Mathf.Max(xp1, xp2), data.width - 1);
		int yMin = Mathf.Max(Mathf.Min(yp1, yp2), 0);
		int yMax = Mathf.Min(Mathf.Max(yp1, yp2), data.height - 1);
		for (int y = yMin; y <= yMax; y++) {
			for (int x = xMin; x <= xMax; x++) {
				if (isAdding) {
					if (checkPointDelegate == null) {
						Add(x, y);
					}
					else {
						if (checkPointDelegate(x, y)) {
							Add(x, y);
						}
						else {
							Subtract(x, y);
						}
					}
				}
				else {
					Subtract(x, y);
				}
			}
		}
		CalculateTotalTiles();
	}
	
	JSelectArea MakeAreaGO(int cx, int cy) {
		GameObject go = (GameObject) Instantiate(self.areaPrefab);
		go.transform.parent = self.transform;
		go.transform.localPosition = new Vector3(cx * JTerrainData.CELL_SIZE, 0f, cy * JTerrainData.CELL_SIZE);
		go.transform.localScale = Vector3.one;
		JSelectArea area = go.GetComponent<JSelectArea>();
		area.SetupData(data, map.GetCell(cx, cy), mapIndex, cx, cy);
		self.areas[cy, cx] = area;
		return area;
	}
	
	public static int SetupSelectAreas(JAreaMap map, string actionName, int mapIndex, delegateIsValid d) {
		Texture2D tex = JGameControl.scenario.LoadTexure("AreaTextures", actionName);
		if (tex != null) {
			self.areaMaterial.mainTexture = tex;
		}
		self.checkPointDelegate = d;
		self.originalMap = map;
		self.map = map.Clone();
		self.map.ClearIsChangedFlags();
		self.mapIndex = mapIndex;
		self.data = JGameControl.scenario.data;
		
		JTerrainData data = JGameControl.scenario.data;
		self.areas = new JSelectArea[data.cWidth, data.cHeight];
		int totalCount = 0;
		for (int cy = 0; cy < data.cHeight; cy++) {
			for (int cx = 0; cx < data.cWidth; cx++) {
				if (data.CellNeedsCalculateSuccession(cx, cy)) {
					int count = map.GetCell(cx, cy).GetCount(mapIndex);
					self.MakeAreaGO(cx,cy);
					totalCount += count;
				}
			}
		}
		self.gameObject.SetActive(true);
		self.CalculateTotalTiles();
		if (self.totalTiles != totalCount) {
			Debug.LogError("tile count mismatched: " + totalCount + " != " + self.totalTiles);
		}
		return totalCount;
	}
	
	public static void SaveMap() {
		self.map.CopyInto(self.originalMap, true);
	}
	
	public static void CleanupSelectArea() {
		self.Clear();
		self.gameObject.SetActive(false);
	}
	
	void Awake() {
		self = this;
		mesh = new Mesh();
		GetComponent<MeshFilter>().sharedMesh = mesh;
		gameObject.SetActive(false);	
//		textures = new Dictionary<string, Texture2D>();
	}
	
	public void Clear() {
		foreach (Transform t in transform) {
			Destroy(t.gameObject);
		}
		areas = null;
	}
	
	public bool IsSelected(int x, int y) {
		int cx = x >> JTerrainData.CELL_SIZE2EXP;
		int cy = y >> JTerrainData.CELL_SIZE2EXP;
		
		int xx = x & JTerrainData.CELL_MASK;
		int yy = y & JTerrainData.CELL_MASK;

		return (map.GetCell(cx, cy).Get(xx, yy) == mapIndex);
	}
	
	public void Add(int x, int y) {
		int cx = x >> JTerrainData.CELL_SIZE2EXP;
		int cy = y >> JTerrainData.CELL_SIZE2EXP;
		int xx = x & JTerrainData.CELL_MASK;
		int yy = y & JTerrainData.CELL_MASK;
		JSelectArea area = areas[cy, cx];
		if (area == null) {
			area = MakeAreaGO(cx, cy);
		}
		area.Set(xx, yy);
	}
	
	public void Subtract(int x, int y) {
		int cx = x >> JTerrainData.CELL_SIZE2EXP;
		int cy = y >> JTerrainData.CELL_SIZE2EXP;
		int xx = x & JTerrainData.CELL_MASK;
		int yy = y & JTerrainData.CELL_MASK;
		JSelectArea area = areas[cy, cx];
		if (area != null) {
			if (!area.Clear(xx, yy)) {
				areas[cy, cx] = null;
			}
		}
	}
	
	public string debugStr = "";
	
	bool isDragging = false;
	bool isAdd = true;
	
	void Update() {
		Vector3 mousePos = Input.mousePosition;
		if (JWinMgr.InUIRect(mousePos.x, Screen.height - mousePos.y) || MyGUI.mouseIsOverGUI ||
			Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) {
//			mesh.Clear();
//			isDragging = false;
			return;
		}
		
		bool startDrag = false;
		bool shiftPressed = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
		if (Input.GetMouseButtonDown(0)) {
			startDrag = true;
			isAdd = !shiftPressed;
		}
		
		if (startDrag || isDragging) {		
			Ray screenRay = Camera.main.ScreenPointToRay(mousePos);
			RaycastHit hit;
			if (Physics.Raycast(screenRay, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER)) {
				Vector3 point = hit.point;
				int newX = (int) (point.x / JTerrainData.HORIZONTAL_SCALE);
				int newY = (int) (point.z / JTerrainData.HORIZONTAL_SCALE);
				bool needUpdate = false;
				if (startDrag) {
					isDragging = true;
					xp1 = newX;
					yp1 = newY;
					needUpdate = true;
				}
				if ((newX != xp2) || (newY != yp2)) {
					xp2 = newX;
					yp2 = newY;
					needUpdate = true;
				// debugStr = editor.scenario.data.GetDebugHeight(xc, yc);
				}
				if (isAdd == shiftPressed) {
					isAdd = !shiftPressed;
					needUpdate = true;
				}
				if (needUpdate) GenerateMesh();
			}
			if (Input.GetMouseButtonUp(0) && isDragging) {

				AddOrSubstract(isAdd);
				mesh.Clear();
				isDragging = false;
			}
		}
	}
}
