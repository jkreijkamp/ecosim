using UnityEngine;
using System.Collections;

public enum EResearchTypes {
	GeenResearch = 0x0,
	
	BodemSamenstelling,
	VoedselGraad,
	WaterHuishouding,
	PeilbuisExtra1,
	PeilbuisExtra2,
	PeilbuisExtra3,
	PeilbuisExtra4,
	
	Groep_Inventarisatie = 0x20,
	Inventarisatie1 = 0x21,
	Inventarisatie2 = 0x22,
	Inventarisatie3 = 0x23,
	Inventarisatie4 = 0x24,
	Inventarisatie5 = 0x25,
	Inventarisatie6 = 0x26,
	Inventarisatie7 = 0x27,
	Inventarisatie8 = 0x28,
	DierInventarisatie1 = 0x29,
	DierInventarisatie2 = 0x2a,
	DierInventarisatie3 = 0x2b,
	DierInventarisatie4 = 0x2c,
	
	
	UNDEFINED = 0xff,
}
