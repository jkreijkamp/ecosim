using System.IO;
using System.Xml;
using UnityEngine;

public static class JIOUtils {

	public static string MyURIFileEncode(string path) {
		path = WWW.EscapeURL(path).Replace("%2f", "/");
		if (!path.StartsWith("/")) path = "/" + path;
       return "file://" + path;
	}
	
	
	public static float[] ReadFA(BinaryReader reader, int len) {
		float[] result = new float[len];
		for (int i = 0; i < len; i++) result[i] = reader.ReadSingle();
		return result;
	}
	
	public static void WriteFA(BinaryWriter writer, float[] data) {
		foreach (float f in data) writer.Write(f);
	}
	
	public static Vector3 ReadVector3(BinaryReader reader) {
		return new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
	}

	public static Quaternion ReadQuaternion(BinaryReader reader) {
		return new Quaternion(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
	}
	
	public static void WriteVector3(BinaryWriter writer, Vector3 v) {
		writer.Write(v.x);
		writer.Write(v.y);
		writer.Write(v.z);
	}

	public static void WriteQuaternion(BinaryWriter writer, Quaternion q) {
		writer.Write(q.x);
		writer.Write(q.y);
		writer.Write(q.z);
		writer.Write(q.w);
	}
	
	public static void ReadUntilEndElement(XmlTextReader reader, string endElement) {
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == endElement)) {
					break;
				}
			}
		}
	}
	
}
