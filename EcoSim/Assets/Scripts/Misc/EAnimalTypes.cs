using UnityEngine;
using System.Collections;

public enum EAnimalTypes {
	FrieseKoe,
	Lakenvelder,
	Galloway,
	Hooglander,
	KoniksPaard,
	Schaap,
	Hert,
	Ree,
	UNDEFINED = -1
}
