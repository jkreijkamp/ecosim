using UnityEngine;
using System.Collections.Generic;

public static class JTextField {
	public class State {
		public long valLong;
		public float valFloat;
		public string str;
	}

	
	static Dictionary<System.Object, State> states = new Dictionary<object, State>();
	
	public static void ClearStates() {
		states.Clear();
	}
	
	public static void IntField(System.Object obj, ref int v, params GUILayoutOption[] options) {
		State s;
		if (states.ContainsKey(obj)) {
			s = states[obj];
			if (s.valLong != (long) v) {
				s.valLong = (long) v;
				s.str = v.ToString();
			}
		}
		else {
			s = new State();
			s.valLong = (long) v;
			s.str = v.ToString();
			states.Add(obj, s);
		}
		string newStr = GUILayout.TextField(s.str, options);
		if (newStr != s.str) {
			s.str = newStr;
			long.TryParse(newStr, out s.valLong);
		}
		v = (int) s.valLong;
	}


	public static void LongField(System.Object obj, ref long v, params GUILayoutOption[] options) {
		State s;
		if (states.ContainsKey(obj)) {
			s = states[obj];
			if (s.valLong != v) {
				s.valLong = v;
				s.str = v.ToString();
			}
		}
		else {
			s = new State();
			s.valLong = v;
			s.str = v.ToString();
			states.Add(obj, s);
		}
		string newStr = GUILayout.TextField(s.str, options);
		if (newStr != s.str) {
			s.str = newStr;
			long.TryParse(newStr, out s.valLong);
		}
		v = s.valLong;
	}
}
