using UnityEngine;

public static class JStringUtils {

	public static Vector3 StringToVector3(string s) {
		s = s.Trim();
		s = s.TrimStart('(').TrimEnd(')');
		string[] nrs = s.Split(',');
		return new Vector3(float.Parse(nrs[0]), float.Parse(nrs[1]), float.Parse(nrs[2]));
	}
	
	public static string Vector3ToString(Vector3 v) {
		return "(" + v.x + ',' + v.y + ',' + v.z + ')';
	}
	
	public static Color StringToColor(string s) {
		s = s.Trim();
		s = s.TrimStart('(').TrimEnd(')');
		string[] nrs = s.Split(',');
		return new Color(float.Parse(nrs[0]) / 255f, float.Parse(nrs[1]) / 255f, float.Parse(nrs[2]) / 255f);
	}
	
	public static string ColorToString(Color c) {
		return "(" + ((int) (c.r * 255)) + ',' + ((int) (c.g * 255)) + ',' + ((int) (c.b * 255)) + ')';
	}
	
	public static string BAToString(byte[] data) {
		if (data == null) return "<null>";
		string s = null;
		foreach (byte b in data) {
			if (s != null) {
				s += ", " + (int) b;
			}
			else {
				s = "" + (int) b;
			}
		}
		return s;
	}
	
	public static string IAToString(int[] ia) {
		string result = "";
		foreach (int i in ia) {
			if (result == "") result = i.ToString();
			else result += "," + i.ToString();
		}
		return result;
	}
	
	public static int[] StringToIA(string str) {
		string[] sa = str.Split(',');
		int[] result = new int[sa.Length];
		for (int i = 0; i < sa.Length; i++) result[i] = int.Parse(sa[i]);
		return result;
	}

	public static string CoordAToString(Coordinate[] coorda) {
		string result = "";
		foreach (Coordinate coord in coorda) {
			if (result != "") result += '|';
			result += coord.x.ToString() + ',' + coord.y.ToString();
		}
		return result;
	}

	public static string InflCoordAToString(InfluenceCoordinate[] coorda) {
		string result = "";
		foreach (InfluenceCoordinate coord in coorda) {
			if (result != "") result += '|';
			result += coord.x.ToString() + ',' + coord.y.ToString() + ',' + coord.v.ToString();
		}
		return result;
	}
	
	
	public static Coordinate[] StringToCoordA(string str) {
		str = str.Trim();
		if (str == "") return new Coordinate[0];
		string[] sa = str.Split('|');
		Coordinate[] result = new Coordinate[sa.Length];
		for (int i = 0; i < sa.Length; i++) {
			string[] ca = sa[i].Split(',');
			result[i] = new Coordinate(int.Parse(ca[0]), int.Parse(ca[1]));
		}
		return result;
	}

	public static InfluenceCoordinate[] StringToInflCoordA(string str) {
		str = str.Trim();
		if (str == "") return new InfluenceCoordinate[0];
		string[] sa = str.Split('|');
		InfluenceCoordinate[] result = new InfluenceCoordinate[sa.Length];
		for (int i = 0; i < sa.Length; i++) {
			string[] ca = sa[i].Split(',');
			if (ca.Length == 2) {
				result[i] = new InfluenceCoordinate(int.Parse(ca[0]), int.Parse(ca[1]), 100);
			}
			else {
				result[i] = new InfluenceCoordinate(int.Parse(ca[0]), int.Parse(ca[1]), int.Parse(ca[2]));
			}
		}
		return result;
	}
}
