using UnityEngine;
using System.Collections.Generic;

public static class MyDebug {
	public static List<string> log = new List<string>();
	
	public static void Clear() {
		log.Clear();
	}
	
	public static void Log(string s) {
		Debug.Log(s);
		log.Add(s);
	}

	public static void LogWarning(string s) {
		Debug.LogWarning(s);
		log.Add("WARNING: " + s);
	}

	public static void LogError(string s) {
		Debug.LogWarning(s);
		log.Add("ERROR: " + s);
	}
}
