using UnityEngine;
using System.Collections;

public static class JLayers {
	public const int L_DEFAULT = 0;
	public const int L_WATER = 4;
	public const int L_TERRAIN = 8;
	public const int L_ROADS = 9;
	public const int L_ROADHANDLE = 10;
	public const int L_ANIMALS = 11;
	public const int L_BUILDINGS = 12;
	public const int L_ACTIONOBJECTS = 13;
	public const int L_MAPSELECTION = 29;
	public const int L_MAP = 30;
	public const int L_GUI = 31;

	public const int M_DEFAULT = 1 << L_DEFAULT;
	public const int M_WATER = 1 << L_WATER;
	public const int M_TERRAIN = 1 << L_TERRAIN;
	public const int M_ROADS = 1 << L_ROADS;
	public const int M_ROADHANDLE = 1 << L_ROADHANDLE;
	public const int M_ANIMALS = 1 << L_ANIMALS;
	public const int M_BUILDINGS = 1 << L_BUILDINGS;
	public const int M_ACTIONOBJECTS = 1 << L_ACTIONOBJECTS;
	public const int M_MAPSELECTION = 1 << L_MAPSELECTION;
	public const int M_MAP = 1 << L_MAP;
	public const int M_GUI = 1 << L_GUI;
}
