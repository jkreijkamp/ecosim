using System.Collections.Generic;

public static class EnumExtensions {
	
	private static Dictionary<EActionGroups, EActionTypes[]> groupedActions;
	public static EAnimalTypes[] animalTypeArray;
	public static EParamTypes[] paramTypeArray;
	public static EActionGroups[] actionGroupArray;
	public static EResearchTypes[] researchTypeArray;
	
	static EnumExtensions() {
		List<EAnimalTypes> aList = new List<EAnimalTypes>();
		foreach (EAnimalTypes at in System.Enum.GetValues(typeof(EAnimalTypes))) {
			if (at != EAnimalTypes.UNDEFINED) {
				aList.Add(at);
			}
		}
		animalTypeArray = aList.ToArray();
		List<EParamTypes> pList = new List<EParamTypes>();
		foreach (EParamTypes at in System.Enum.GetValues(typeof(EParamTypes))) {
			if (at != EParamTypes.UNDEFINED) {
				pList.Add(at);
			}
		}
		paramTypeArray = pList.ToArray();
		List<EActionGroups> groups = new List<EActionGroups>();
		groupedActions = new Dictionary<EActionGroups, EActionTypes[]>();
		foreach (EActionGroups ag in System.Enum.GetValues(typeof(EActionGroups))) {
			List<EActionTypes> l = new List<EActionTypes>();
			int agInt = (int) ag;
			foreach (EActionTypes at in System.Enum.GetValues(typeof(EActionTypes))) {
				int atInt = (int) at;
				if ((atInt != agInt) && ((atInt & 0xf0) == agInt)) {
					l.Add(at);
				}
			}
			if (ag != EActionGroups.UNDEFINED) {
				groups.Add(ag);
				groupedActions.Add(ag, l.ToArray());
			}
		}
		actionGroupArray = groups.ToArray();
		List<EResearchTypes> restypes = new List<EResearchTypes>();
		foreach (EResearchTypes rt in System.Enum.GetValues(typeof(EResearchTypes))) {
			restypes.Add(rt);
		}
		researchTypeArray = restypes.ToArray();
	}
	
	public static int IndexOfParamType(EParamTypes t) {
		for (int i = 0; i < paramTypeArray.Length; i++) {
			if (paramTypeArray[i] == t) return i;
		}
		return -1;
	}
	
	public static EActionTypes[] GetActionTypesForGroup(EActionGroups grp) {
		return groupedActions[grp];
	}
	
	public static EActionTypes GetActionType(string name) {
		name = name.ToLower();
		foreach (EActionTypes t in System.Enum.GetValues(typeof(EActionTypes))) {
			if (t.ToString().ToLower() == name) return t;
		}
		return EActionTypes.UNDEFINED;
	}

	public static EActionGroups GetActionGroup(string name) {
		name = name.ToLower();
		foreach (EActionGroups t in System.Enum.GetValues(typeof(EActionGroups))) {
			if (t.ToString().ToLower() == name) return t;
		}
		return EActionGroups.UNDEFINED;
	}
	
	public static EResearchTypes GetResearchType(string name) {
		name = name.ToLower();
		foreach (EResearchTypes t in System.Enum.GetValues(typeof(EResearchTypes))) {
			if (t.ToString().ToLower() == name) return t;
		}
		return EResearchTypes.UNDEFINED;
	}

	public static EParamTypes GetParamType(string name) {
		name = name.ToLower();
		foreach (EParamTypes t in System.Enum.GetValues(typeof(EParamTypes))) {
			if (t.ToString().ToLower() == name) return t;
		}
		return EParamTypes.UNDEFINED;
	}
	
	public static float ParamByteToFloat(EParamTypes t, byte v) {
		float f = ((float) v) / 255;
		switch (t) {
		case EParamTypes.PH : f = f * 14; break;
		}
		return f;
	}
	
	public static string Range(float f) {
		if (f < 0.2f) return "-";
		if (f < 0.4f) return "+";
		if (f < 0.6f) return "++";
		if (f < 0.8f) return "+++";
		return "++++";
		
	}
	
	public static string ParamFloatToString(EParamTypes t, float f) {
		switch (t) {
		case EParamTypes.PH : return "pH = " + f.ToString("0.0");
		case EParamTypes.VoedselRijkdom : return "Voedsel = " + Range(f);
		}
		return t.ToString() + " " + Range(f);
	}
}
