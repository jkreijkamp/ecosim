using UnityEngine;
using System.Collections;

public enum EActionGroups {
	Groep_GrondBewerken = 0x10,
	Groep_Maaien = 0x20,
	Groep_AanplantenKappen = 0x30,
	Groep_Paden = 0x40,
	Groep_Koeien = 0x80,
	Groep_Schapen = 0x90,
	Groep_Galloways = 0xa0,
	Groep_Koniks = 0xb0,
	Groep_Sloten = 0xe0,
	Groep_Objecten = 0xf0,
	UNDEFINED = 0xf000,
}

public enum EActionTypes {
	GeenActie = 0x00,
	
	Groep_GrondBewerken = EActionGroups.Groep_GrondBewerken,
	PlaggenDreggen,
	AfgravenPN,
	Bemesten,
	Uitmijnen,
	
	Groep_Maaien = EActionGroups.Groep_Maaien,
	MaaienZomerNajaarAfvoeren,
	MaaienVoorjaarZomerAfvoeren,
	MaaienZomerAfvoeren,
	MaaienZomer,
	
	Groep_AanplantenKappen = EActionGroups.Groep_AanplantenKappen,
	Kappen,
	Aanplanten,
	Branden,
	OpenenKroonlaag,
	
	Groep_Paden = EActionGroups.Groep_Paden,
	PadenAfsluiten,
	PadenBetreden,
	
	Groep_Koeien = EActionGroups.Groep_Koeien,
	BegrazenKoeienIntJaarRond,
	BegrazenKoeienIntNaBloei,
	BegrazenKoeienExtJaarRond,
	BegrazenKoeienExtNaBloei,
	
	Groep_Schapen = EActionGroups.Groep_Schapen,
	BegrazenSchapenIntJaarRond,
	BegrazenSchapenIntNaBloei,
	BegrazenSchapenExtJaarRond,
	BegrazenSchapenExtNaBloei,
	
	Groep_Galloways = EActionGroups.Groep_Galloways,
	BegrazenGallowaysIntJaarRond,
	BegrazenGallowaysIntNaBloei,
	BegrazenGallowaysExtJaarRond,
	BegrazenGallowaysExtNaBloei,
	
	Groep_Koniks = EActionGroups.Groep_Koniks,
	BegrazenKoniksIntJaarRond,
	BegrazenKoniksIntNaBloei,
	BegrazenKoniksExtJaarRond,
	BegrazenKoniksExtNaBloei,
	
	Groep_Sloten = EActionGroups.Groep_Sloten,
	GravenSloot,
	VerdiepenSloot,
	DempenSloot,
	VerontdiepenSloot,
	StuwPlaatsen,
	StuwVerwijderen,
	StuwVerhogen,
	StuwVerlagen,
	
	Groep_Objecten = EActionGroups.Groep_Objecten,
	Object1,
	Object2,
	Object3,
	Object4,
	Object5,
	Object6,
	Object7,
	Object8,
	
	Groep_Undefined = EActionGroups.UNDEFINED,
	
	UNDEFINED = 0xf001,
}
