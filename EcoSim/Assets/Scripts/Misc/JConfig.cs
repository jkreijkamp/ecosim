using UnityEngine;
using System.Collections;
using System.IO;

public class JConfig : MonoBehaviour {
	
	public static string BUILD_VERSION_STRING = "1.1";
	public static long FILE_VERSION = 100;
	public static bool LOAD_THUMBNAILS = false;
	public static bool TEST_ACTIVE = false;
	public static bool SHOW_ECOSIM2_EXPORT = true;
	
	private static bool initialized = false;
	
	public static string appDataPath;
	public static string homePath;
	public static string desktopPath;
	public static string workPath;
	
	public static string definedScenarioPath;
	public static string definedSaveGamesPath;
	
	public static string scenarioPath {
		get {
			string path = null;
			if (definedScenarioPath.StartsWith("./") || definedScenarioPath.StartsWith(".\\")) {
				path = workPath + definedScenarioPath.Substring(2);
			}
			else if (definedScenarioPath.StartsWith("~/") || definedScenarioPath.StartsWith("~\\")) {
				path = homePath + definedScenarioPath.Substring(2);
			}
			else {
				path = definedScenarioPath;
			}
			if (!path.EndsWith(Path.DirectorySeparatorChar.ToString())) path += Path.DirectorySeparatorChar;
			return path;
		}
	}
	
	public static string ecoDataPath {
		get {
			return Path.GetFullPath(scenarioPath + "/../");
		}
	}
	
	public static string saveGamesPath {
		get {
			string path = null;
			if (definedSaveGamesPath.StartsWith("./") || definedSaveGamesPath.StartsWith(".\\")) {
				path = workPath + definedSaveGamesPath.Substring(2);
			}
			else if (definedSaveGamesPath.StartsWith("~/") || definedSaveGamesPath.StartsWith("~\\")) {
				path = homePath + definedSaveGamesPath.Substring(2);
			}
			else {
				path = definedSaveGamesPath;
			}
			if (!path.EndsWith(Path.DirectorySeparatorChar.ToString())) path += Path.DirectorySeparatorChar;
			return path;
		}
	}
	
	public static bool editHouses = true;
	
	
	public static EActionTypes GetActionType(string name) {
		foreach (EActionTypes t in System.Enum.GetValues(typeof(EActionTypes))) {
			if (t.ToString().ToLower() == name) return t;
		}
		return EActionTypes.UNDEFINED;
	}

	public static EResearchTypes GetResearchType(string name) {
		foreach (EResearchTypes t in System.Enum.GetValues(typeof(EResearchTypes))) {
			if (t.ToString().ToLower() == name) return t;
		}
		return EResearchTypes.UNDEFINED;
	}

	public static EParamTypes GetParamType(string name) {
		foreach (EParamTypes t in System.Enum.GetValues(typeof(EParamTypes))) {
			if (t.ToString().ToLower() == name) return t;
		}
		return EParamTypes.UNDEFINED;
	}
	
	public static void SendMessageDownwards(GameObject go, string methodName) {
		go.SendMessage(methodName, SendMessageOptions.DontRequireReceiver);
		foreach (Transform t in go.transform) {
			SendMessageDownwards(t.gameObject, methodName);
		}
	}

	
	private void SetupPaths() {
		Debug.LogWarning("original application Path = " + Application.dataPath);
		
		Debug.LogWarning("Get Full Path = " + Path.GetFullPath(Application.dataPath + "/../../"));
		if ((Application.platform == RuntimePlatform.OSXPlayer) || (Application.platform == RuntimePlatform.OSXDashboardPlayer) ||
			(Application.platform == RuntimePlatform.OSXEditor)) {
			appDataPath = System.Environment.GetEnvironmentVariable("HOME") + "/Library/Application Support/EcoSim/";
			desktopPath = System.Environment.GetEnvironmentVariable("HOME") + "/Desktop/";
			homePath = System.Environment.GetEnvironmentVariable("HOME") + "/";
		}
		else if (Application.platform == RuntimePlatform.LinuxPlayer) {
			appDataPath = System.Environment.GetEnvironmentVariable("HOME") + ".ecosim/";
			desktopPath = System.Environment.GetEnvironmentVariable("HOME") + "/Desktop/";
			homePath = System.Environment.GetEnvironmentVariable("HOME") + "/";
		}
		else if ((Application.platform == RuntimePlatform.WindowsPlayer) || (Application.platform == RuntimePlatform.WindowsEditor)) {
			appDataPath = System.Environment.GetEnvironmentVariable("APPDATA") + "\\EcoSim\\";
			desktopPath = System.Environment.GetEnvironmentVariable("HOMEDRIVE") +
				System.Environment.GetEnvironmentVariable("HOMEPATH") + "\\Desktop\\";
			homePath = System.Environment.GetEnvironmentVariable("HOMEDRIVE") +
				System.Environment.GetEnvironmentVariable("HOMEPATH") + "\\";
		}
		if (Application.platform == RuntimePlatform.WindowsPlayer) {
			workPath = Path.GetFullPath(Application.dataPath + "/../");
		}
		else if (Application.platform == RuntimePlatform.OSXPlayer) {
			workPath = Path.GetFullPath(Application.dataPath + "/../../");
		}
		else if (Application.platform == RuntimePlatform.LinuxPlayer) {
			workPath = Path.GetFullPath(Application.dataPath + "/../");
		}
		else {
			workPath = Path.GetFullPath(Application.dataPath + "/../");
		}
		if (!workPath.EndsWith(Path.DirectorySeparatorChar.ToString())) workPath += Path.DirectorySeparatorChar;
		
		if (PlayerPrefs.HasKey("SaveGamesPath")) {
			definedSaveGamesPath = PlayerPrefs.GetString("SaveGamesPath");
		}
		else {
			definedSaveGamesPath = appDataPath;
		}		
		if (PlayerPrefs.HasKey("ScenarioPath")) {
			definedScenarioPath = PlayerPrefs.GetString("ScenarioPath");
		}
		else {
			definedScenarioPath = "." + Path.DirectorySeparatorChar + "Scenarios" + Path.DirectorySeparatorChar;
		}
		Debug.Log("workPath = " + workPath);		
		Debug.Log("scenarioPath = " + scenarioPath);
		Debug.Log("saveGamesPath = " + saveGamesPath);
	}
	
	public static void SetDefaultSaveGamesPath() {
		PlayerPrefs.DeleteKey("SaveGamesPath");
		definedSaveGamesPath = appDataPath;
	}
	
	public static void SetDefaultScenarioPath() {
		PlayerPrefs.DeleteKey("ScenarioPath");
		definedScenarioPath = "." + Path.DirectorySeparatorChar + "Scenarios" + Path.DirectorySeparatorChar;
	}
	
	void Awake() {
		if (!initialized) {
			SetupPaths();
			initialized = true;
		}
	}
	
	private static string sMessage = "";
	
	public static void SetMessage(string msg) {
		Debug.LogWarning(msg);
		message = msg;
	}
	
	public static string message {
		get { return sMessage; }
		set { sMessage = value; }
	}
	
	public static void ClearMessage() {
		sMessage = "";
	}
	
	public static void SetMessage(System.Exception e) {
		sMessage = e.Source + ":" + e.Message;
	}
	
	public static string GetValue(string key, string defaultVal) {
		if (PlayerPrefs.HasKey(key)) return PlayerPrefs.GetString(key);
		return defaultVal;
	}
	
	public static void SetValue(string key, string val) {
		PlayerPrefs.SetString(key, val);
	}
}
