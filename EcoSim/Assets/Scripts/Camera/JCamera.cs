using UnityEngine;
using System.Collections;

public class JCamera : MonoBehaviour {

	public Transform nearCameraT;
	public Transform farCameraT;
	public Transform compassPlateT;
	public Transform compassNeedleT;
	public GameObject compass;

	[System.Serializable]
	public class FarCameraData
	{
		public float moveSpeed = 500f;
		public float fastMoveSpeed = 5000f;
		public float zoomSpeed = 4f;
		public float fastZoomSpeed = 8f;
	}
	public FarCameraData farCameraData;
	
	[System.Serializable]
	public class NearCameraData
	{
		public float rotationSpeed = 2f;
		public float moveSpeed = 20f;
		public float fastMoveSpeed = 100f;
		public float zoomSpeed = 4f;
		public float fastZoomSpeed = 8f;
		public float fastPanSpeed = 10f;
		public float panSpeed = 2f;
	}
	public NearCameraData nearCameraData;

	public float minAboveGround = 50f;
	public float border = 1000f;
	
	static Vector3 pos = new Vector3(-2560f, 500f, -2560f);
	Vector3 farPos;
	static float angle = 90f;
	static float direction = 0f;
	float maxOrtSize;
	
	const float MAX_ABOVE_GROUND = 1000f;
	
	private Vector3 lastMousePos;
	private bool doMoveCamera = true;
	//private Vector3 lastRotationChangedPos;
	private Vector3 lastCheckedPos = new Vector3(-100, -100, -100); // bogus value
	private float lastChangeTime = 0f;
	//private bool rotateToNorth = false;
	
	private JScenario scenario;
	
	private static JCamera self;
	
	public static JScenario Scenario { get { return self.scenario; } }
	public static bool IsNear { get { return isNear; } }
	public static float Angle { get { return angle; } }
	public static void SetToFar(bool lockCam) { self.SetFarCamera(); self.lockCamera = lockCam; }

	static bool isNear = false;
	private bool lockCamera = false;
	
	void Awake() {
		self = this;
		MyGUI.mouseIsOverGUI = false;
		JWinMgr.ForceClearRects();
		nearCameraT.gameObject.GetComponent<AudioListener>().velocityUpdateMode = AudioVelocityUpdateMode.Dynamic;
	}
	
	void Start() {
		StartCoroutine(COCleanup());
	}
	public static Vector3 GetPosition() {
		return self.nearCameraT.position;
	}
	
	public static Transform GetTransform() {
		return self.nearCameraT;
	}
	
	IEnumerator COCleanup() {
		JCellData.localTime = Time.time;
		while (true) {
			yield return new WaitForSeconds(2f);
			JCellData.localTime = Time.time;
			if (scenario != null) {
				JRenderTerrain.TryCleanup(scenario);
			}
		}
	}
	
	public static void SetScenario(JScenario scenario, bool reset) {
//		if (scenario == null) {
//			Debug.LogWarning("Scenario cleared");
//		}
//		else {
//			Debug.LogWarning("Scenario set to '" + scenario.name + "'");
//		}
		
		self.scenario = scenario;
//		Vector3 checkedPos = self.pos + self.nearCameraT.forward * 1000f;
		self.farPos = new Vector3(scenario.data.width * JTerrainData.HORIZONTAL_SCALE / 2, 1000f, scenario.data.height * JTerrainData.HORIZONTAL_SCALE / 2);
		self.maxOrtSize = Mathf.Max(scenario.data.width, scenario.data.height) * JTerrainData.HORIZONTAL_SCALE * 0.55f;
		self.farCameraT.camera.orthographicSize = self.maxOrtSize;
		JRenderTerrain.SetupForScenario(scenario);
		JRenderMap.SetupForScenario(scenario);
		// JRenderTerrain.Render(scenario, checkedPos.x, checkedPos.z, true);
		self.lastCheckedPos = new Vector3(-1000, -1000, -1000);
		self.lastChangeTime = Time.time;
		self.compassPlateT.localRotation = Quaternion.Inverse(self.nearCameraT.localRotation);
		self.compassNeedleT.localRotation = Quaternion.Euler(-angle, 0f, 0f);
		if (!scenario.IsGameMode) {
			self.minAboveGround = 10f;
			self.border = 0f;
		}
		if (reset || !isNear) {
			self.SetFarCamera();
		}
		else {
			self.SetNearCamera();
		}
	}
	
	public static void ForceRender() {
		JRenderTerrain.Render(self.scenario, self.lastCheckedPos.x, self.lastCheckedPos.z, true);
		self.lastChangeTime = Time.time;
	}
	
	void Update() {
		if (lockCamera) {
			lastMousePos = Input.mousePosition;
			return;
		}
		if (isNear) {
			UpdateNear();
		}
		else {
			UpdateFar();
		}
		lastMousePos = Input.mousePosition;
	}
	
	void OnGUIOld() {
		Vector3 screenPos = Input.mousePosition;
		Ray ray = Camera.main.ScreenPointToRay(screenPos);
		RaycastHit hit;
		if ((scenario != null) && Physics.Raycast(ray, out hit, 1000f, JLayers.M_TERRAIN | JLayers.M_WATER)) {
			Vector3 pos = hit.point;
			float height = pos.y;
			int x = (int) (pos.x / 20f);
			int y = (int) (pos.z / 20f);
			GUI.Label(new Rect(Screen.width - 300, 100, 290, 90), "pos = " + x + "," + y + "\nh = " + height + "\nLh = " + scenario.data.GetHeight(x, y) + "\nWh = " + scenario.data.GetAdjustedWaterHeight(x, y));
			
		}
	}
		
	
	void SetFarCamera() {
		Debug.Log("Set to Far");
//		RenderSettings.fog = false;
		nearCameraT.gameObject.SetActive(false);
		farCameraT.gameObject.SetActive(true);
		compass.SetActive(false);
		farCameraT.position = farPos;
		isNear = false;
	}
	
	void SetNearCamera() {
		Debug.Log("Set to Near");
//		RenderSettings.fog = true;
		nearCameraT.gameObject.SetActive(true);
		farCameraT.gameObject.SetActive(false);
		compass.SetActive(false);
		isNear = true;
		lastChangeTime = -1000;
		//lastRotationChangedPos = pos;
		
		Quaternion rot =  Quaternion.Euler(0f, direction, 0f) * Quaternion.Euler(angle, 0f, 0f);
				
		nearCameraT.rotation =  rot;
		//compassPlateT.localRotation = Quaternion.Euler(0f, direction, 0f);
		//compassPlateT.localRotation = Quaternion.Inverse(nearCameraT.localRotation);
		//compassNeedleT.localRotation = Quaternion.Euler(-angle, 0f, 0f);
		CalculateRealPos();
		
		GameObject[] flyingObjects = GameObject.FindGameObjectsWithTag("Flying");
		foreach (GameObject go in flyingObjects) {
			go.SendMessage("SwitchedToNearCamera", SendMessageOptions.DontRequireReceiver);
		}
	}
	
	void UpdateFar() {
		if (scenario == null) return;
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (pos.x < 0) return;
			SetNearCamera();
			return;
		}
		Vector3 mousePos = Input.mousePosition;
		if (MyGUI.mouseIsOverGUI || JWinMgr.InUIRect(mousePos.x, Screen.height - mousePos.y)) return;
		
		bool shift = (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift));

		// Check if the mouse is outside the boundaries
		float horizontal = Input.GetAxis("Horizontal");
		if (Input.mousePosition.x >= Screen.width * 0.975f) {
			horizontal = 1f;
		} else if (Input.mousePosition.x <= Screen.width * 0.025f) {
			horizontal = -1f;
		}

		float vertical = Input.GetAxis("Vertical");
		if (Input.mousePosition.y >= Screen.height * 0.975f) {
			vertical = 1f;
		} else if (Input.mousePosition.y <= Screen.height * 0.025f) {
			vertical = -1f;
		}

		float deltaTime = Mathf.Clamp(Time.deltaTime, 0.005f, 0.5f);
		float speed = deltaTime * (shift?farCameraData.fastMoveSpeed:farCameraData.moveSpeed);
		float zoom = -(shift?farCameraData.fastZoomSpeed:farCameraData.zoomSpeed) * Input.GetAxis("Mouse ScrollWheel");
		if (Input.GetKey(KeyCode.PageDown)) {
			zoom -= 1;
		}
		if (Input.GetKey(KeyCode.PageUp)) {
			zoom += 1;
		}
		if (zoom != 0.0f) {
			float os = farCameraT.camera.orthographicSize;
			os = Mathf.Clamp((1f + deltaTime * zoom) * os, 128f, maxOrtSize);
			farCameraT.camera.orthographicSize = os;
		}
		if ((horizontal != 0.0f) || (vertical != 0.0f)) {
			Vector3 deltaPos = new Vector3(horizontal * speed, 0f, vertical * speed);
			farPos += deltaPos;
			farCameraT.position = farPos;
		}
		if (Input.GetMouseButtonDown(0)) {
			Vector3 pos = farCameraT.camera.ScreenToWorldPoint(Input.mousePosition);
			pos.y = JCamera.pos.y;
			pos.x = Mathf.Clamp(pos.x, border, scenario.data.width * JTerrainData.HORIZONTAL_SCALE - border);
			pos.z = Mathf.Clamp(pos.z, border, scenario.data.height * JTerrainData.HORIZONTAL_SCALE - border);
			JCamera.pos = pos;
			angle = 90f;
			direction = 0f;
			SetNearCamera();
		}
	}

	void UpdateNear() {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			SetFarCamera();
			return;
		}

		Vector3 mousePos = Input.mousePosition;
		if (MyGUI.mouseIsOverGUI || JWinMgr.InUIRect(mousePos.x, Screen.height - mousePos.y)) return;
		
		bool shift = (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift));
		float deltaTime = Mathf.Min(Time.deltaTime, 0.1f);
		float h = Mathf.Clamp(pos.y * deltaTime / 5f, 10f, 1000f);
		bool alt = Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
		/*if (alt != compass.activeSelf) {
			compass.SetActive(alt);
		}*/
		/*if (Input.GetMouseButton(0) && alt) {
			if (/* !MyGUI.mouseIsOverGUI && * !JWinMgr.InUIRect(mousePos.x, Screen.height - mousePos.y)) {
				Vector3 deltaPos = (mousePos - lastMousePos) * deltaTime;
				
				angle = Mathf.Clamp(angle + 10 * deltaPos.y, 0.1f, 90f);
				direction = Mathf.Repeat(direction + 40 * deltaPos.x, 360f);
				Quaternion rot =  Quaternion.Euler(0f, direction, 0f) * Quaternion.Euler(angle, 0f, 0f);
				
				nearCameraT.rotation =  rot;
				// compassPlateT.localRotation = Quaternion.Euler(0f, direction, 0f);
				compassPlateT.localRotation = Quaternion.Inverse(nearCameraT.localRotation);
				compassNeedleT.localRotation = Quaternion.Euler(-angle, 0f, 0f);
				lastRotationChangedPos = pos;
				lastChangeTime = Time.time;

			}
			rotateToNorth = false;
		}
		if (Input.GetKeyDown(KeyCode.N)) {
			rotateToNorth = true;
		}
		if (rotateToNorth) {
			direction = direction + ((direction > 180)?180:-180) * deltaTime;
			if ((direction <= 0.0f) || (direction >= 360f)) {
				direction = 0.0f;
				rotateToNorth = false;
			}
			Quaternion rot =  Quaternion.Euler(0f, direction, 0f) * Quaternion.Euler(angle, 0f, 0f);
				
			nearCameraT.rotation =  rot;
			// compassPlateT.localRotation = Quaternion.Euler(0f, direction, 0f);
			compassPlateT.localRotation = Quaternion.Inverse(nearCameraT.localRotation);
		}
		lastRotationChangedPos.y = pos.y;
		if ((angle < 90) && ((lastRotationChangedPos - pos).sqrMagnitude > 6000*6000)) {
			angle = Mathf.Min(90.0f, angle + 90 * deltaTime);
			Quaternion rot =  Quaternion.Euler(0f, direction, 0f) * Quaternion.Euler(angle, 0f, 0f);
			nearCameraT.rotation =  rot;
			compassPlateT.localRotation = Quaternion.Inverse(nearCameraT.localRotation);
			compassNeedleT.localRotation = Quaternion.Euler(-angle, 0f, 0f);
		}*/

		float horizontal = Input.GetAxis("Horizontal");
		float vertical = Input.GetAxis("Vertical");

		if (Input.GetMouseButtonUp(1)) {
			// Don't move the camera via mouse if we release the RMB outside the screen
			if (Input.mousePosition.x >= Screen.width * 0.975f) {
				doMoveCamera = false;
			} else if (Input.mousePosition.x <= Screen.width * 0.025f) {
				doMoveCamera = false;
			}
			if (Input.mousePosition.y >= Screen.height * 0.975f) {
				doMoveCamera = false;
			} else if (Input.mousePosition.y <= Screen.height * 0.025f) {
				doMoveCamera = false;
			}
		}

		// Rotate camera
		if (Input.GetMouseButton(1) || alt) {
			// Rotate
			float rotH = (mousePos.x - lastMousePos.x) * nearCameraData.rotationSpeed * deltaTime;
			float rotV = -(mousePos.y - lastMousePos.y) * nearCameraData.rotationSpeed * deltaTime;
			nearCameraT.Rotate ( rotV, 0f, 0f, Space.Self );
			nearCameraT.Rotate ( 0f, rotH, 0f, Space.World );
		} 
		// Pan up/down
		else if (Input.GetMouseButton(2)) {
			float rotV = -(mousePos.y - lastMousePos.y) * deltaTime;
			float rotH = (mousePos.x - lastMousePos.x) * deltaTime;
			pos += Vector3.up * h * rotV * (shift?nearCameraData.fastPanSpeed:nearCameraData.panSpeed);
			pos -= nearCameraT.right * h * rotH * (shift?nearCameraData.fastPanSpeed:nearCameraData.panSpeed);
		}
		// Move camera
		else {
			bool moveHorizontal = false;
			bool moveVertical = false;

			float mouseHorizontal = 0f;
			float mouseVertical = 0f;

			if (Input.mousePosition.x >= Screen.width * 0.975f) {
				mouseHorizontal = 1f;
				moveHorizontal = true;
			} else if (Input.mousePosition.x <= Screen.width * 0.025f) {
				mouseHorizontal = -1f;
				moveHorizontal = true;
			}

			if (Input.mousePosition.y >= Screen.height * 0.975f) {
				mouseVertical = 1f;
				moveVertical = true;
			} else if (Input.mousePosition.y <= Screen.height * 0.025f) {
				mouseVertical = -1f;
				moveVertical = true;
			}

			// Make sure the camera does not move after rotating the camera
			if (!doMoveCamera) {
				if (!moveVertical && !moveHorizontal) doMoveCamera = true;
			} else {
				if (moveHorizontal) horizontal = mouseHorizontal;
				if (moveVertical) vertical = mouseVertical;
			}
		}

		// Zoom
		float zoom = -4f * Input.GetAxis("Mouse ScrollWheel");
		if (Input.GetKey(KeyCode.PageDown)) {
			zoom -= 1;
		}
		if (Input.GetKey(KeyCode.PageUp)) {
			zoom += 1;
		}
		if (zoom != 0.0f) {
			//pos += nearCameraT.forward * zoom * (shift?nearCameraData.fastZoomSpeed:nearCameraData.zoomSpeed); // Forward
			pos += Vector3.up * h * zoom * (shift?nearCameraData.fastZoomSpeed:nearCameraData.zoomSpeed); // Up/down
		}
		Vector3 right = nearCameraT.right;
		Vector3 forward = nearCameraT.forward;
		forward.y = 0f;
		right.y = 0f;
		forward.Normalize();
		right.Normalize();
		Vector3 deltaMovement = (forward * vertical + right * horizontal) * h * deltaTime * (shift?nearCameraData.fastMoveSpeed:nearCameraData.moveSpeed);
		pos += deltaMovement;
		pos.y = Mathf.Clamp(pos.y, minAboveGround, MAX_ABOVE_GROUND);
		if (scenario != null) {
			pos.x = Mathf.Clamp(pos.x, border, scenario.data.width * JTerrainData.HORIZONTAL_SCALE - border);
			pos.z = Mathf.Clamp(pos.z, border, scenario.data.height * JTerrainData.HORIZONTAL_SCALE - border);
		}
		CalculateRealPos();
	}
	
	void CalculateRealPos() {
		Vector3 realPos = pos;
		Ray ray = new Ray(pos + 400f * Vector3.up, -Vector3.up);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, 10000f, JLayers.M_TERRAIN | JLayers.M_WATER)) {
			realPos.y += hit.point.y;
		}
		Vector3 checkedPos = realPos + nearCameraT.forward * 500f;
		lastCheckedPos.y = checkedPos.y; // we don't want to take height in account
		if (Vector3.SqrMagnitude(checkedPos - lastCheckedPos) > 10000) {
			lastChangeTime = Time.time;
			lastCheckedPos = checkedPos;
			JRenderTerrain.SetSize(256);
			JRenderTerrain.RenderIfNeeded(scenario, checkedPos.x, checkedPos.z, 0.25f);
		}
		else {
			if (Time.time - lastChangeTime > 1.5f) {
				if (angle < 70) {
					JRenderTerrain.SetSize(512);
				}
				JRenderTerrain.RenderIfNeeded(scenario, checkedPos.x, checkedPos.z, 0.35f);
				lastCheckedPos = checkedPos;
			}
			else if (JRenderTerrain.isRendering) {
				lastChangeTime = Time.time;				
			}
		}
		nearCameraT.position = realPos;
	}	
}
