using UnityEngine;
using System.Collections;

public class JWaitSpinner : MonoBehaviour {
	
	public Material spinMaterial;
	public Material successionMaterial;
	
	private float timeOut = 0f;
	private int rotate = 0;
	
	static bool isSpinning;
	static bool isSuccessionSpinning;
	static JWaitSpinner self;
	
	public static void StartSpin() {
		isSpinning = true;
		self.gameObject.SetActive(true);
	}
	
	public static void StopSpin() {
		isSpinning = false;
		if (!isSuccessionSpinning) {
			self.gameObject.SetActive(false);
		}
	}
	
	public static void StartSuccession() {
		isSuccessionSpinning = true;
		self.renderer.sharedMaterial = self.successionMaterial;
		self.transform.localScale = new Vector3(2, 2, 2);
		self.gameObject.SetActive(true);
	}
	
	public static void StopSuccession() {
		isSuccessionSpinning = false;
		self.renderer.sharedMaterial = self.spinMaterial;
		self.transform.localScale = new Vector3(1, 1, 1);
		if (!isSpinning) {
			self.gameObject.SetActive(false);
		}
	}
	
	void Awake() {
		isSpinning = false;
		isSuccessionSpinning = false;
		self = this;
		gameObject.SetActive(false);
		renderer.sharedMaterial = self.spinMaterial;
	}
	
	void Update() {
		timeOut -= Time.deltaTime;
		if (timeOut <= 0) {
			rotate = (rotate + 20) % 360;
			transform.localRotation = Quaternion.Euler(0f, 0f, -rotate);
			timeOut = 0.05f;
		}
	}
}
