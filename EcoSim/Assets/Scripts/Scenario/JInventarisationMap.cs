using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

/**
 * indicates locations of where inventarisations of species are done
 * other types of research aren't done through a researchmap but
 * by markers
 */
public class JInventarisationMap : JAreaMap {
	
	public const string XML_ELEMENT = "inventarisation";
	
	private JInventarisationMap() {
	}
	
	/**
	 * makes an empty research map
	 */
	public JInventarisationMap(JScenario scenario) {
		int yLen = scenario.data.cHeight;
		int xLen = scenario.data.cWidth;
		maps = new JAreaMapCell16[yLen, xLen];
		for (int cy = 0; cy < yLen; cy++) {
			for (int cx = 0; cx < xLen; cx++) {
				maps[cy, cx] = new JAreaMapCell16(cx, cy);
			}
		}		
	}
	
	/**
	 * Copy constructor
	 */
	public JInventarisationMap(JInventarisationMap copyFrom) {
		maps = copyFrom.CloneMap();
	}
	
	/**
	 * clone method
	 */
	public override JAreaMap Clone() {
		JInventarisationMap mm = new JInventarisationMap();
		mm.maps = CloneMap();
		return mm;
	}
	
	
	/**
	 * Read in a research map from xml stream, stream should be at the element defining the map
	 */
	public static JInventarisationMap LoadFromXml(JScenario scenario, XmlTextReader reader) {
		string elementName = reader.Name.ToLower();
		JInventarisationMap map = new JInventarisationMap();
		
		int yLen = scenario.data.cHeight;
		int xLen = scenario.data.cWidth;
		
		map.maps = new JAreaMapCell[yLen, xLen];
		
		// read in stored cells
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "cell")) {
					JAreaMapCell cell = JAreaMapCell.LoadFromXML(reader);
					map.maps[cell.cy, cell.cx] = cell;
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == elementName)) {
					break;
				}
			}
		}
		
		// fill in empty cells
		for (int cy = 0; cy < yLen; cy++) {
			for (int cx = 0; cx < xLen; cx++) {
				if (map.maps[cy, cx] == null) {
					// for now it's always a 4-bit per tile map
					map.maps[cy, cx] = new JAreaMapCell16(cx, cy);
				}
			}
		}
		return map;
	}
	
	public override void SaveToXml(XmlTextWriter writer) {
		writer.WriteStartElement(XML_ELEMENT);
		foreach (JAreaMapCell cell in maps) {
			cell.SaveToXml(writer);
		}
		writer.WriteEndElement();
	}
	
	/**
	 * See baseclass for explaination
	 */
	public override void CopyInto(JAreaMap targetMap, bool onlyCopyChangedCells) {
		if (!(targetMap is JInventarisationMap)) {
			throw new System.Exception("targetMap is not a JResearchMap but a " + targetMap.GetType().ToString());
		}
		JInventarisationMap castedMap = (JInventarisationMap) targetMap;
		base.CopyInto(castedMap, onlyCopyChangedCells);
	}
}
