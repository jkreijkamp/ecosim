using UnityEngine;
using System.Collections;


[System.Serializable]
public class JXTerrainTileTreeData {
	
	public JXTerrainTileTreeData() {
	}
	
	public JXTerrainTileTreeData(JXTerrainTileTreeData source) {
		x = source.x;
		y = source.y;
		r = source.r;
		prototypeIndex = source.prototypeIndex;
		minHeight = source.minHeight;
		maxHeight = source.maxHeight;
		minWidthVariance = source.minWidthVariance;
		minWidthVariance = source.maxWidthVariance;
		colorFrom = source.colorFrom;
		colorTo = source.colorTo;
	}
	
	static Color start = new Color(0.5625f, 0.5625f, 0.5625f, 1f);
	static Color end = new Color(0.75f, 0.75f, 0.75f, 1f);
	
	public string GetName() {
		return JElements.GetTreePrototype(prototypeIndex).prefab.name;
	}
	
	public int prototypeIndex;
	public float x = 0.5f;
	public float y = 0.5f;
	public float r = 0.2f;
	public float minHeight = 0.9f;
	public float maxHeight = 1.1f;
	public float minWidthVariance = 0.9f;
	public float maxWidthVariance = 1.1f;
	
	public Color colorFrom = start;
	public Color colorTo = end;
}

[System.Serializable]
public class JXTerrainTileObjectData {
	
	public JXTerrainTileObjectData() {
	}
	
	public JXTerrainTileObjectData(JXTerrainTileObjectData source) {
		x = source.x;
		y = source.y;
		r = source.r;
		angle = source.angle;
		index = source.index;
		minHeight = source.minHeight;
		maxHeight = source.maxHeight;
		minWidthVariance = source.minWidthVariance;
		minWidthVariance = source.maxWidthVariance;
	}
	
	public string GetName() {
		return JElements.GetTileObjectPrefab(index).name;
	}
	
	public int index;
	public float x = 0.5f;
	public float y = 0.5f;
	public float r = 0.0f;
	public float angle = 0f;
	public float minHeight = 0.9f;
	public float maxHeight = 1.1f;
	public float minWidthVariance = 0.9f;
	public float maxWidthVariance = 1.1f;
}



[System.Serializable]
public class JXTerrainTile {
	public JXTerrainTile() {
		trees = new JXTerrainTileTreeData[0];
		objects = new JXTerrainTileObjectData[0];
		detailCounts = new int[0];
		extraLayers = new int[0];
	}
	
	public JXTerrainTile(JXTerrainTile source) {
		Assign(source);
	}
	
	public void Assign(JXTerrainTile source) {
		am0 = source.am0;
		am1 = source.am1;
		am2 = source.am2;
		detailCounts = new int[source.detailCounts.Length];
		source.detailCounts.CopyTo(detailCounts, 0);
		trees = new JXTerrainTileTreeData[source.trees.Length];
		for (int i = 0; i < trees.Length; i++) {
			trees[i] = new JXTerrainTileTreeData(source.trees[i]);
		}
		objects = new JXTerrainTileObjectData[source.objects.Length];
		for (int i = 0; i < objects.Length; i++) {
			objects[i] = new JXTerrainTileObjectData(source.objects[i]);
		}
		extraLayers = new int[source.extraLayers.Length];
		source.extraLayers.CopyTo(extraLayers, 0);
	}
	
	public int index;
	public JXVegetation vegetation;
	
	public float am0;
	public float am1;
	public float am2;
	
	public JXTerrainTileTreeData[] trees;
	public JXTerrainTileObjectData[] objects;
	public int[] detailCounts;
	public int[] extraLayers;
	
	public Texture2D icon;
	public Texture2D GetIcon() {
		if (icon == null) {
			JTileIconGenerator.NeedsIcon(this);
		}
		return icon;
	}
}
