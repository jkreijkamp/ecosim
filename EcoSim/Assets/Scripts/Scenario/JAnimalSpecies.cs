using UnityEngine;
using System.Collections;

public class JAnimalSpecies {
	public class Nest {
		public Coordinate position;
		public int males;
		public int females;
		public int food;
	}
	
	public string name;
	public int index;
	
	public Nest[] nests;
	public EParamTypes food;
	public EParamTypes foodOverrule;
	public EParamTypes danger;
	public float femaleWandering = 0.2f;
	public float maleWandering = 0.2f;
	public int femaleWalkDistance = 5;
	public int maleWalkDistance = 5;
	public float offspringPerPair = 3.0f;
	public float naturalDeath = 0.2f;
	public int growthCapPerNest = 5;
	
	public float populationDivider = 1f;	
}
