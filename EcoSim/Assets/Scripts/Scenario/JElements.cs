using UnityEngine;
using System.Collections;

[System.Serializable]
public class JXTEExtraTileLayerPrototype {
	public bool useWaterHeights;
	public float verticalOffset = 0.25f;
	public Material material;
}

[System.Serializable]
public class NamePairs {
	public string oldName;
	public string newName;
}


public class JElements : MonoBehaviour {

	[System.Serializable]
	public class AnimalPrefabs {
		public EAnimalTypes t;
		public GameObject prefab;
	}

	// needed as Prefabs can't be compared in not-main-threads, it sucks, i know...
	public class PrefabWrapper {
		public string name;
		public GameObject prefab;
	}
	
	public class BuildingWrapper {
		public string name;
		public string matName;
		public Material material;
		public Mesh mesh;
		public GameObject prefab;
	}
	
	public AnimalPrefabs[] animalPrefabs;
	public GameObject[] roadPrefabs;
	public GameObject[] buildingPrefabs;
	public JXTEExtraTileLayerPrototype[] layerPrototypes;
	public GameObject[] tileObjectPrefabs;
	public PrefabWrapper[] wrappedObjectPrefabs;
	public BuildingWrapper[] wrappedBuildings;
	
	public NamePairs[] namePairs;
	public Terrain terrain;
	
	
	
	public static JElements self;
	
	void Awake() {
		self = this;
		wrappedObjectPrefabs = new PrefabWrapper[tileObjectPrefabs.Length];
		for (int i = 0; i < tileObjectPrefabs.Length; i++) {
			wrappedObjectPrefabs[i] = new PrefabWrapper();
			wrappedObjectPrefabs[i].name = tileObjectPrefabs[i].name;
			wrappedObjectPrefabs[i].prefab = tileObjectPrefabs[i];
		}
		wrappedBuildings = new BuildingWrapper[buildingPrefabs.Length];
		for (int i = 0; i < buildingPrefabs.Length; i++) {
			GameObject building = buildingPrefabs[i];
			BuildingWrapper b = new BuildingWrapper();
			b.name = building.name;
			b.material = building.GetComponent<MeshRenderer>().sharedMaterial;
			b.matName = b.material.name;
			b.mesh = building.GetComponent<MeshFilter>().sharedMesh;
			b.prefab = building;
			wrappedBuildings[i] = b;
		}
	}
	
	public static JXTEExtraTileLayerPrototype GetTileLayerPrototype(int i) {
		return self.layerPrototypes[i];
	}
	
	public static int FindIndexOfExtraTileLayer(string name) {
		JXTEExtraTileLayerPrototype[] layerPrototypes = self.layerPrototypes;
		// name = UpdateName(name);
		for (int i = 0; i < layerPrototypes.Length; i++) {
			if (layerPrototypes[i].material.name == name) return i;
		}
		// LogWarningOnce("Couldn't find extra tile layer '" + name + "'");
		return -1;
	}
	
	public static GameObject GetAnimalPrefab(EAnimalTypes at) {
		foreach (AnimalPrefabs ap in self.animalPrefabs) {
			if (ap.t == at) return ap.prefab;
		}
		Debug.LogError("No prefab for " + at.ToString());
		return null;
	}
	
	
	public static int FindObject(string objName) {
		GameObject[] objects = self.tileObjectPrefabs;
		objName = objName.ToLower();
		for (int i = 0; i < objects.Length; i++) {
			if (objects[i].name.ToLower() == objName) return i;
		}
		throw new System.Exception("Tegelobject met naam '" + objName + "' niet gevonden.");
	}
	
	public static int FindTreePrototype(string prototypeName) {
		TerrainData data = self.terrain.terrainData;
		TreePrototype[] prototypes = data.treePrototypes;
		prototypeName = prototypeName.ToLower();
		for (int i = 0; i < prototypes.Length; i++) {
			if (prototypes[i].prefab.name.ToLower() == prototypeName) return i;
		}
		throw new System.Exception("Boom met naam '" + prototypeName + "' niet gevonden.");
	}

	public static int FindDetailPrototype(string prototypeName) {
		TerrainData data = self.terrain.terrainData;
		DetailPrototype[] prototypes = data.detailPrototypes;
		prototypeName = prototypeName.ToLower();
		for (int i = 0; i < prototypes.Length; i++) {
			DetailPrototype dp = prototypes[i];
			if ((dp.prototype != null)) {
				string n = dp.prototype.name;
				if ((n != null) && n.ToLower() == prototypeName) return i;
			}
			else if (dp.prototypeTexture != null) {
				string n = dp.prototypeTexture.name;
				if ((n != null) && n.ToLower() == prototypeName) return i;
			}
		}
		throw new System.Exception("Detailobject met naam '" + prototypeName + "' niet gevonden.");
	}
	
	public static DetailPrototype GetDetailPrototype(int i) {
		TerrainData data = self.terrain.terrainData;
		DetailPrototype[] prototypes = data.detailPrototypes;
		return prototypes[i];
	}

	public static TreePrototype GetTreePrototype(int i) {
		TerrainData data = self.terrain.terrainData;
		TreePrototype[] prototypes = data.treePrototypes;
		return prototypes[i];
	}
	
	public static GameObject GetTileObjectPrefab(int i) {
		return self.tileObjectPrefabs[i];
	}
	
	public static BuildingWrapper GetBuildingWrapper(string objName) {
		BuildingWrapper[] objects = self.wrappedBuildings;
		objName = objName.ToLower();
		for (int i = 0; i < objects.Length; i++) {
			if (objects[i].name.ToLower() == objName) return objects[i];
		}
		Debug.LogError("Gebouw met naam '" + objName + "' niet gevonden.");
		return objects[0];
	}
	
	public static int FindRoadIndex(string name) {
		name = name.ToLower();
		for (int i = 0; i < self.roadPrefabs.Length; i++) {
			if (name == (self.roadPrefabs[i].name.ToLower())) return i;
		}
		Debug.LogError("Road prefab '" + name + "' not found, returning 0");
		return 0; // safeguard
	}
}
