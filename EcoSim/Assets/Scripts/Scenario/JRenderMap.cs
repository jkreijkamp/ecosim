using UnityEngine;
using System.Collections;
using System.IO;

public class JRenderMap : MonoBehaviour {
	
	public Shader tileShader;
	public Color materialColor;
	private const int TSIZE = 256;
	private const int TEXTURE_SIZE = 1024;
	static JRenderMap self;
	JScenario scenario;
	static Mesh mesh;
	
	class Tiles {
		public Tiles(Material mat, Texture2D tex, GameObject go) {
			this.mat = mat;
			this.tex = tex;
			this.go = go;
		}
		
		public readonly Material mat;
		public readonly Texture2D tex;
		public readonly GameObject go;
	}
	
	Tiles[,] tiles;
	
	void LoadTile(int tx, int ty) {
		GameObject go = new GameObject("Tile" + tx + "x" + ty);
		go.layer = JLayers.L_MAP;
		Transform t = go.transform;
		t.parent = transform;
		t.localPosition = new Vector3(tx * TSIZE * JTerrainData.HORIZONTAL_SCALE, 0, ty * TSIZE * JTerrainData.HORIZONTAL_SCALE);
		Texture2D tex = new Texture2D(TEXTURE_SIZE, TEXTURE_SIZE, TextureFormat.RGB24, false);
		string fileName = scenario.scenarioPath + "tile" + tx + "x" + ty + ".png";
		if (File.Exists(fileName)) {
			byte[] bytes = File.ReadAllBytes(fileName);
			tex.LoadImage(bytes);
		}
		Material material = new Material(tileShader);
		material.mainTexture = tex;
		material.color = materialColor;
		MeshFilter filter = go.AddComponent<MeshFilter>();
		filter.sharedMesh = mesh;
		MeshRenderer render = go.AddComponent<MeshRenderer>();
		render.sharedMaterial = material;
		Tiles tile = new Tiles(material, tex, go);
		tiles[ty, tx] = tile;
	}
	
	public static void SaveTiles(string path) {
		if (self.tiles != null) {
			for (int ty = 0; ty < self.tiles.GetLength(0); ty++) {
				for (int tx = 0; tx < self.tiles.GetLength(1); tx++) {
					Tiles tile = self.tiles[ty, tx];
					byte[] bytes = tile.tex.EncodeToPNG();
					File.WriteAllBytes(path + "tile" + tx + "x" + ty + ".png", bytes);
				}
			}
		}	
	}
	
	bool isGeneratingTiles = false;
	public static void GenerateTiles() {
		if (!self.isGeneratingTiles) {
			self.StartCoroutine(self.COGenerateTiles());
		}
	}
	
	public static bool IsGeneratingTiles { get { return self.isGeneratingTiles; } }
	
	IEnumerator COGenerateTiles() {
		isGeneratingTiles = true;
		yield return 0;
		while (JRenderTerrain.isRendering) {
			yield return 0;
		}
		JCamera.SetToFar(true);
		JRenderTerrain.SetSize(256);
		JRenderTerrain.SetGenerateMapQuality();
		GameObject go = new GameObject("mapRenderCamera");
		Transform t = go.transform;
		
		Camera cam = go.AddComponent<Camera>();
		go.AddComponent<JRTAlpha>();
		t.localRotation = Quaternion.Euler(90f, 0f, 0f);
		cam.orthographic = true;
		float tileWidth = TSIZE * JTerrainData.HORIZONTAL_SCALE;
		float offset = tileWidth / 2;
		cam.orthographicSize = offset;
		cam.aspect = 1.0f;
		cam.near = 10f;
		cam.nearClipPlane = 10f;
		cam.far = 1100f;
		cam.farClipPlane = 1100f;
		cam.enabled = false;
	
		
		
		for (int ty = 0; ty < tiles.GetLength(0); ty++) {
			for (int tx = 0; tx < tiles.GetLength(1); tx++) {
				Tiles tile = tiles[ty, tx];
				cam.transform.localPosition = new Vector3(tx * tileWidth + offset, 1000f, ty * tileWidth + offset);
				JRenderTerrain.TryCleanup(scenario);
				yield return 0;
				JRenderTerrain.LowLevelStartRender(scenario, tx * TSIZE, ty * TSIZE);
				while (JRenderTerrain.isRendering) {
					yield return 0;
				}
				yield return 0;
				yield return 0;
				RenderTexture rt =  RenderTexture.GetTemporary(TEXTURE_SIZE, TEXTURE_SIZE, 24, RenderTextureFormat.ARGB32);
				rt.useMipMap = false;
				rt.wrapMode = TextureWrapMode.Clamp;
				cam.targetTexture = rt;
				cam.Render();

				RenderTexture.active = rt;
				tile.tex.ReadPixels(new Rect(0, 0, TEXTURE_SIZE, TEXTURE_SIZE), 0, 0, false);
				tile.tex.Apply();
				RenderTexture.active = null;
				RenderTexture.ReleaseTemporary(rt);
				
				yield return 0;
				yield return 0;
			}
		}
		yield return 0;
		JRenderTerrain.ForceQualityCheck(scenario);
		Destroy(go);
		JCamera.SetToFar(false);		
		isGeneratingTiles = false;
	}
	
	void Awake() {
		self = this;
		if (mesh == null) {
			mesh = new Mesh();
			GenerateMesh();
		}
	}
		
	public static void SetupForScenario(JScenario scenario) {
		self.DeleteTiles();
		self.scenario = scenario;
		JTerrainData data = scenario.data;
		int tWidth = data.width / TSIZE;
		int tHeight = data.height / TSIZE;
		self.tiles = new Tiles[tHeight, tWidth];
		for (int ty = 0; ty < tHeight; ty++) {
			for (int tx = 0; tx < tWidth; tx++) {
				self.LoadTile(tx, ty);
			}
		}	
	}
	
	void DeleteTiles() {
		if (tiles != null) {
			foreach (Tiles tile in tiles) {
				Destroy(tile.tex);
				Destroy(tile.mat);
				Destroy(tile.go);
			}
		}
		tiles = null;
	}

	public void OnDestroy() {
		DeleteTiles();
	}
	
	void GenerateMesh() {
		float ws = TSIZE * JTerrainData.HORIZONTAL_SCALE;
		float hs = TSIZE * JTerrainData.HORIZONTAL_SCALE;
		
		Vector3[] points = new Vector3[] {
			new Vector3(0f, 0f, 0f),
			new Vector3(ws, 0f, 0f),
			new Vector3(0f, 0f, hs),
			new Vector3(ws, 0f, hs)
		};
		
		Vector2[] uv = new Vector2[] {
			new Vector2(0f, 0f),
			new Vector2(1f, 0f),
			new Vector2(0f, 1f),
			new Vector2(1f, 1f)
		};
		
		int[] indices = new int[] {
			1, 0, 2,  1, 2, 3
		};
		
		mesh.vertices = points;
		mesh.uv = uv;
		mesh.triangles = indices;
	}
	
}
