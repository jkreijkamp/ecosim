using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;


public class JScenarioGoal {
	public const string XML_ELEMENT = "scenariogoal";
			
	public class Action {
		public int articleId = -1;
		public string hintStr;
		public string counterName = null;
		public string expression;
		public bool endGame;
		public bool removeGoal;
	}
	
	public const int M_AFTER_RESEARCH = 0x01;
	public const int M_AFTER_MEASURE = 0x02;
	public const int M_AFTER_LOAD = 0x04;
	public const int M_BEFORE_SUCCESSION = 0x08;
	public const int M_AFTER_SUCCESSION = 0x10;
	public const int M_AFTER_ENCYCLOPEDIA = 0x20;
	
	public string name;
	public string[] expressions = new string[] { "" };
	public int checkMask = M_AFTER_SUCCESSION;
	public Action success;
	public Action failure;
	
	public static Action LoadAction(XmlReader reader, string elementName) {
		Action a = new Action();
		if (reader.GetAttribute("articleid") != null) {
			a.articleId = int.Parse(reader.GetAttribute("articleid"));
		}
		if (reader.GetAttribute("hint") != null) {
			a.hintStr = reader.GetAttribute("hint");
		}
		if (reader.GetAttribute("endgame") != null) {
			a.endGame = (reader.GetAttribute("endgame").ToLower() == "true");
		}
		if (reader.GetAttribute("removegoal") != null) {
			a.removeGoal = (reader.GetAttribute("removegoal").ToLower() == "true");
		}
		if (reader.GetAttribute("assign") != null) {
			a.counterName = reader.GetAttribute("assign");
			a.expression = reader.GetAttribute("expression");
		}
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == elementName)) {
					break;
				}
			}
		}
		return a;
	}
	
	public void SaveAction(Action a, XmlWriter writer, string elementName) {
		writer.WriteStartElement(elementName);
		if (a.articleId >= 0) {
			writer.WriteAttributeString("articleid", a.articleId.ToString());
		}
		if (a.hintStr != null) {
			writer.WriteAttributeString("hint", a.hintStr);
		}
		if (a.counterName != null) {
			writer.WriteAttributeString("assign", a.counterName);
			writer.WriteAttributeString("expression", a.expression);
		}
		if (a.endGame) {
			writer.WriteAttributeString("endgame", "true");
		}
		if (a.removeGoal) {
			writer.WriteAttributeString("removegoal", "true");
		}
		writer.WriteEndElement();
	}
	
	public void DoAction(int goalId, JVarContext context, Action a) {
		if ((a.counterName != null) && (a.expression != null)) {
			long val = JExpression.ParseExpression(a.expression, context, false);
			context.SetValue(a.counterName, val);
		}
		if (a.removeGoal) {
			context.SetValue("_Disabled"+goalId, 1);
		}
		if (a.articleId >= 0) {
			JGameControl.AddToMessageQueue(context.scenario.articles[a.articleId], a.endGame);
		}
		if (a.hintStr != null) {
			JGameControl.AddToHelpQueue(a.hintStr, a.endGame);
		}
	}
	
	public void TryGoal(int goalId, JVarContext context, int mode) {
		try {
			if ((mode & checkMask) == 0) return;
			if (context.HasVar("_Disabled" + goalId)) return;
			foreach (string e in expressions) {
				long val = JExpression.ParseExpression(e, context, false);
				if (val == 0) {
					if (failure != null) {
						DoAction(goalId, context, failure);
					}
					return;
				}
			}
			if (success != null) {
				DoAction(goalId, context, success);
			}
		} catch (System.Exception e) {
			MyDebug.LogError("Regel #" + (goalId + 1) + " '" + name + "' " + e.Message);
			Debug.LogError(e.StackTrace);
		}
	}
	
	public static JScenarioGoal Load(XmlReader reader) {
		JScenarioGoal g = new JScenarioGoal();
		g.name = reader.GetAttribute("name");
		string expr = reader.GetAttribute("expression");
		g.expressions = expr.Split('|');
		g.checkMask = int.Parse(reader.GetAttribute("mask"));
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "successaction")) {
					g.success = LoadAction(reader, "successaction");
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "failaction")) {
					g.failure = LoadAction(reader, "failaction");
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == XML_ELEMENT)) {
					break;
				}
			}
		}
		return g;
	}
	
	public void Save(XmlWriter writer) {
		writer.WriteStartElement(XML_ELEMENT);
		if (name != null) {
			writer.WriteAttributeString("name", name);
		}
		writer.WriteAttributeString("mask", checkMask.ToString());
		string expr = "";
		foreach (string e in expressions) {
			if (expr != "") expr += '|';
			expr += e;
		}
		writer.WriteAttributeString("expression", expr);
		if (success != null) {
			SaveAction(success, writer, "successaction");
		}
		if (failure != null) {
			SaveAction(failure, writer, "failaction");
		}
		writer.WriteEndElement();
	}
}
