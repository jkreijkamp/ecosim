using UnityEngine;
using System.Collections;

public class JGooseFlock : MonoBehaviour {
	
	public Vector3 target;
	Vector3 position;
	Quaternion rot;
	Transform myTransform;
	float minHeight = 25f;
	
	public float distanceMultiplier = 1f;
	
	void Start() {
		myTransform = transform;
		rot = myTransform.localRotation;
		position = myTransform.localPosition;
		SelectTarget();
		StartCoroutine(CORandomNewTarget());
		StartCoroutine(COPreventCrash());
	}
	
	IEnumerator CORandomNewTarget() {
		while (true) {
			yield return new WaitForSeconds(Random.Range(1, 5));
			Vector3 cameraPos = JCamera.GetPosition();
			cameraPos.y = myTransform.position.y;
			float distance = Vector3.Distance(cameraPos, myTransform.position);
			if (distance > 500) {
				SelectTarget();
			}
		}
	}
	
	IEnumerator COPreventCrash() {
		while (true) {
			yield return new WaitForSeconds(1.0f);
			if (JCamera.Scenario != null) {
				JTerrainData data = JCamera.Scenario.data;
				Vector3 pos = myTransform.localPosition;
				int x = Mathf.RoundToInt(pos.x / JTerrainData.HORIZONTAL_SCALE);
				int y = Mathf.RoundToInt(pos.z / JTerrainData.HORIZONTAL_SCALE);
				x = Mathf.Clamp(x, 0, data.width - 1);
				y = Mathf.Clamp(y, 0, data.height - 1);
				int cx = x >> JTerrainData.CELL_SIZE2EXP;
				int cy = y >> JTerrainData.CELL_SIZE2EXP;
			 	JCellData cell = data.GetCell(cx, cy);
				if (cell.isLoaded) {
					minHeight = data.GetHeight(x, y) + 50.0f;
				}
			}
		}
	}
	
	void SwitchedToNearCamera() {
		Transform camT = JCamera.GetTransform();
		Vector3 pos = camT.position + (camT.forward * -100f + camT.up * -600f + camT.right * Random.Range(-200, 200)) * distanceMultiplier;
		pos.y = Mathf.Clamp(camT.position.y, 50f, 150f);
		myTransform.position = pos;
		position = pos;
		SelectTarget();
	}
	
	void SelectTarget() {
		if (JCamera.Scenario == null) {
			target = new Vector3(Random.Range(0, 5120), Random.Range(500, 1000), Random.Range(0, 5120));
			return;
		}		
		Transform camT = JCamera.GetTransform();
		
		Vector3 pos = camT.position + (camT.forward * Random.Range(-10f, 100f) + camT.right * Random.Range(-50f, 50f)) * distanceMultiplier;
		pos.y = camT.position.y + Random.Range(-5f, 25f);
		
//		pos.y = myTransform.position.y;
		pos.y = Mathf.Clamp(pos.y, 20f, 200f);
		target = pos;
	}
	
	
	
	void Update() {
		float deltaTime = Time.deltaTime;
		rot = Quaternion.RotateTowards(rot, Quaternion.LookRotation(target - position, Vector3.up), 15 * deltaTime);
		myTransform.localRotation = rot;
		position += deltaTime * 30 * myTransform.forward;
		if (position.y < minHeight) {
			SelectTarget();
			position.y = minHeight;
		}
		myTransform.localPosition = position;
		if ((position - target).sqrMagnitude < 10000) {
			SelectTarget();
		}
	}
}
