using UnityEngine;
using System.Collections;

public class JHorse: JAnimal {
	
	public AudioClip[] clips;
	
	public Animation anim;
	public float walkSpeed = 1.0f;
	
	const string A_EAT = "grazen2";
	const string A_CHEW = "uitkijken2";
	const string A_WALK = "loopje2";
	
	const float MAX_DIST = 80;
	const float MAX_DIST_SQR = MAX_DIST * MAX_DIST;
	
	Vector3 startPos;
	
	// Use this for initialization
	void Start () {
//		anim = gameObject.GetComponent<Animation>();
	
		anim[A_EAT].wrapMode = WrapMode.Once;
		anim[A_CHEW].wrapMode = WrapMode.Once;
		anim[A_WALK].wrapMode = WrapMode.Loop;
		
		startPos = transform.position;
		
		StartCoroutine(COControl());
	}
	
	IEnumerator COControl() {
		CharacterController ctrl = gameObject.GetComponent<CharacterController>();
		bool hitGround = false;
		while (!hitGround) {
			hitGround = ctrl.SimpleMove(-0.5f * Vector3.up);
			if (transform.position.y < -10f) {
				Destroy(gameObject);
				yield break;
			}
			yield return 0;
		}
		while (true) {			
			float rnd = Random.value;
			if (rnd < 0.2f) {
				anim.Play(A_CHEW);
				yield return new WaitForSeconds(1f);
				AudioSource.PlayClipAtPoint(clips[Random.Range(0, clips.Length)], transform.position);
				yield return new WaitForSeconds(5f);
			}
			else if (rnd < 0.6f) {
				anim.Play(A_EAT);
				yield return new WaitForSeconds(anim[A_EAT].length);
			}
			else {
				Vector3 pos = transform.position;
				Vector3 rndPos = Random.insideUnitSphere * MAX_DIST + startPos;
				rndPos.y = transform.position.y;
				transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(rndPos - pos), 45f);
				anim.Play(A_WALK);
				float dur = Random.Range(4f, 8f);
				Ray ray = new Ray(pos + Vector3.up * 1000, -Vector3.up);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit, Mathf.Infinity, JLayers.M_TERRAIN)) {
					transform.localPosition = hit.point;
				}
				while ((dur > 0) && (Vector3.SqrMagnitude(transform.position + transform.forward - startPos) < MAX_DIST_SQR)) {
					if (JRenderTerrain.isRendering) {
						while (JRenderTerrain.isRendering) {
							yield return 0;
						}
						yield return 0;
						break;
					}
					dur -= Time.deltaTime;
					ctrl.SimpleMove(walkSpeed * transform.forward - 0.005f * Vector3.up);
					yield return 0;
				}
				if (transform.position.y < -10f) {
					Destroy(gameObject);
					yield break;
				}
			}
		}
	}	
}
