using UnityEngine;
using System.Globalization;
using System.Collections.Generic;
using System.Xml;

/** 
 * a terrain sized map that holds small int values
 */
public abstract class JAreaMap {

	protected JAreaMapCell[,] maps;

	/**
	 * makes a copy of this JAreaMap into target (target type must match source type)
	 */
	public void CopyInto(JAreaMap target) {
		CopyInto(target, false);
	}
	
	/**
	 * clone method
	 */
	public abstract JAreaMap Clone();
	
	protected JAreaMapCell[,] CloneMap() {
		int yLen = maps.GetLength(0);
		int xLen = maps.GetLength(1);
		JAreaMapCell[,] m = new JAreaMapCell[yLen, xLen];
		for (int cy = 0; cy < yLen; cy++) {
			for (int cx = 0; cx < xLen; cx++) {
				m[cy,cx] = maps[cy,cx].Clone();
			}
		}
		return m;
	}
	
	/**
	 * writes areamap to writer
	 * the root element of the areamap is determined in the superclass, the
	 * string used is publicly defined as XML_ELEMENT
	 */
	public abstract void SaveToXml(XmlTextWriter writer);
	
	/**
	 * marks all area map cells as not changed, thus marks areamap as not changed
	 */
	public virtual void ClearIsChangedFlags() {
		foreach (JAreaMapCell cell in maps) cell.IsChanged = false;
	}
	
	/** 
	 * return number of times value 'index' is stored in map
	 */
	public int Count(int index) {
		int count = 0;
		foreach (JAreaMapCell mc in maps) {
			count += mc.GetCount(index);
		}
		return count;
	}
	
	/** 
	 * return true if map only contains 0
	 */
	public bool IsEmpty() {
		foreach (JAreaMapCell mc in maps) {
			if (!mc.IsEmpty()) return false;
		}
		return true;
	}
	
	/**
	 * return true if map of cell at cx, cy only contains 0
	 */
	public bool IsEmpty(int cx, int cy) {
		return maps[cy, cx].IsEmpty();
	}
	
	/**
	 * return map cell at cx, cy
	 */
	public JAreaMapCell GetCell(int cx, int cy) {
		return maps[cy, cx];
	}
	
	public int Get(int x, int y) {
		int cx = x >> JTerrainData.CELL_SIZE2EXP;
		int cy = y >> JTerrainData.CELL_SIZE2EXP;
		x = x & JTerrainData.CELL_MASK;
		y = y & JTerrainData.CELL_MASK;
		JAreaMapCell cell = GetCell(cx, cy);
		if (cell != null) return cell.Get(x, y);
		return 0;
	}

	public void Set(int x, int y, int v) {
		int cx = x >> JTerrainData.CELL_SIZE2EXP;
		int cy = y >> JTerrainData.CELL_SIZE2EXP;
		x = x & JTerrainData.CELL_MASK;
		y = y & JTerrainData.CELL_MASK;
		JAreaMapCell cell = GetCell(cx, cy);
		if (cell == null) throw new System.Exception("Cell[" + cx + ", " + cy + "] doesn't exist!");
		cell.Set(x, y, v);
	}
	
	/**
	 * generates mesh for cell at cx, cy.
	 */
	public void GenerateMesh(int cx, int cy, JTerrainData data, Mesh mesh) {
		maps[cy, cx].GenerateMesh(data.GetCell(cx, cy), mesh);
	}
	
	/**
	 * copy this area map into targetMap
	 * if onlyCopyChangedCells is true only cells in this map that are marked
	 * changed are copied to targetMap. The targetMap cell is then marked changed
	 * and the original cell (in this instance) marked unchanged.
	 * if onlyCopyChangedCells is false all cells are copied, including the
	 * isChanged flag, the isChanged flag is not modified in the original cell
	 * (thus an exact copy is made without changing the original)
	 * Note: targetMap must be of the same class as the instance class.
	 */
	public virtual void CopyInto(JAreaMap targetMap, bool onlyCopyChangedCells) {
		int cHeight = maps.GetLength(0);
		int cWidth = maps.GetLength(1);
		for (int cy = 0; cy < cHeight; cy++) {
			for (int cx = 0; cx < cWidth; cx++) {
				JAreaMapCell cell = maps[cy, cx];
				if (onlyCopyChangedCells) {
					if (cell.IsChanged) {
						// only copy when original cell has changed
						targetMap.maps[cy, cx] = cell.Clone();
						// mark original cell as not changed (target cell, being copy is marked as changed!)
						cell.IsChanged = false;
					}
				}
				else {
					// we use clone instead of CopyInto to be able to handle different
					// versions of the JAreaMapCell class than originally used.
					targetMap.maps[cy, cx] = cell.Clone();
				}
			}
		}
	}	
}
