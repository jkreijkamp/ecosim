using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class JResearchPoint {
	
	public const string XML_ELEMENT = "researchpoint";
	
	public class Value {
		public Value(EParamTypes p, float v) {
			this.p = p;
			this.v = v;
		}
		public readonly EParamTypes p;
		public readonly float v;
	}

	
	public class Measurement {
		public Measurement(int year, EResearchTypes researchType) {
			this.year = year;
			this.researchType = researchType;
		}
		
		public readonly int year;
		public readonly EResearchTypes researchType;
		public string msg;
//		public Value[] values;
		public bool isTemporary = true;
	}

	public readonly int x;
	public readonly int y;
	
	public JResearchPoint(int x, int y) {
		this.x = x;
		this.y = y;
		measurements = new List<Measurement>();
	}
	
	public List<Measurement> measurements;
	
	public void AddMeasurement(Measurement m) {
		measurements.Add(m);
	}
	
	public void MakePermanent() {
		foreach (Measurement m in measurements) {
			m.isTemporary = true;
		}
	}
	
	public bool RemoveTemporary() {
		List<Measurement> toRemove = new List<Measurement>();
		foreach (Measurement m in measurements) {
			if (m.isTemporary) {
				toRemove.Add(m);
			}
		}
		foreach (Measurement m in toRemove) {
			measurements.Remove(m);
		}
		return (measurements.Count == 0);
	}
	
	public bool HasTmpMeasurement {
		get {
			foreach (Measurement m in measurements) {
				if (m.isTemporary) return true;
			}
			return false;
		}
	}
	
	public bool HasNewMeasurement {
		get {
			foreach (Measurement m in measurements) {
				if (m.msg == null) return true;
//				if (m.values == null) return true;
			}
			return false;
		}
	}
	
	public long DoSuccession(JCellData cell, int x, int y) {
		long total = 0;
		foreach (Measurement m in measurements) {
			if (m.msg == null) {
				
				JScenario scenario = JGameControl.scenario;
				m.msg = scenario.context.Call("Peilbuis" + m.researchType.ToString() + "String", new EcoTileVar(scenario.context, cell.xOffset + x, cell.yOffset + y, "")) as string;
				total += JGameControl.scenario.research[m.researchType].costPerTile;
			}
		}
		return total;
	}
	
	static Measurement LoadMeasurement(XmlTextReader reader) {
		int year = int.Parse(reader.GetAttribute("year"));
		EResearchTypes researchType = EnumExtensions.GetResearchType(reader.GetAttribute("research"));
		Measurement m = new Measurement(year, researchType);
		m.msg = reader.GetAttribute("message");
		m.isTemporary = false;
				
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "data")) {
					JIOUtils.ReadUntilEndElement(reader, "data");
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "measurement")) {
					break;
				}
			}
		}
		return m;
	}
	
	public static JResearchPoint LoadFromXml(XmlTextReader reader) {
		string elementName = reader.Name.ToLower();
		
		int x = int.Parse(reader.GetAttribute("x"));
		int y = int.Parse(reader.GetAttribute("y"));
		JResearchPoint rp = new JResearchPoint(x, y);
		List<Measurement> mList = rp.measurements;
		
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "measurement")) {
					mList.Add(LoadMeasurement(reader));
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == elementName)) {
					break;
				}
			}
		}
		return rp;
	}
	
	public void SaveToXml(XmlTextWriter writer) {
		writer.WriteStartElement(XML_ELEMENT);
		writer.WriteAttributeString("x", x.ToString());
		writer.WriteAttributeString("y", y.ToString());
		
		foreach (Measurement m in measurements) {
			writer.WriteStartElement("measurement");
			writer.WriteAttributeString("year", m.year.ToString());
			writer.WriteAttributeString("research", m.researchType.ToString());
			if (m.msg != null) {
				writer.WriteAttributeString("message", m.msg);
			}
			writer.WriteEndElement();
		}
		
		writer.WriteEndElement();
	}
}
