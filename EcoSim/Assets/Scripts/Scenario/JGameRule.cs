using System.Collections.Generic;
using System.Xml;

public class JGameRule {
	public const string XML_ELEMENT = "gamerule";
	
	public const int M_AFTER_RESEARCH = 0x01;
	public const int M_AFTER_MEASURE = 0x02;
	public const int M_AFTER_LOAD = 0x04;
	public const int M_BEFORE_SUCCESSION = 0x08;
	public const int M_AFTER_SUCCESSION = 0x10;
	public const int M_AFTER_ENCYCLOPEDIA = 0x20;
	
	
	public string ruleName;
	public int ruleMask;
	public bool disabled = false;
	private string script;
	private EcoBlock block;
	
	public void Disable() {
		disabled = true;
	}
	
	public JGameRule(string ruleName, int ruleMask, string script, bool isDisabled) {
		this.ruleName = ruleName;
		this.ruleMask = ruleMask;
		this.script = script;
	}

	public string CompileScript(EcoContext context) {
		string errStr = null;
		try {
			block = null;
			EcoScript es = new EcoScript();
			block = es.ParseBlock(context, script);
		}
		catch (EcoException e) {
			errStr = "Script fout: " + e.Message;
			UnityEngine.Debug.LogException(e);
		}
		catch (System.Exception e2) {
			errStr = "Systeem fout: " + e2.Message;
			UnityEngine.Debug.LogException(e2);
		}
		return errStr;
	}
	
	
	public void SetScript(string script) {
		this.script = script;
	}
	
	public string GetScript() {
		return script;
	}
	
	public string Execute(EcoContext context) {
		if (block == null) {
			MyDebug.LogError("No compiled script for '" + ruleName + "'");
			return "No compiled script for '" + ruleName + "'";
		}
		string errStr = null;
		List<Dictionary<string, EcoVar>> stack = context.PopAllReturnStack();
		try {
			context.Push(); // add new level for local function vars
			context.Set("_disablerule", false);
			block.Execute(context);
			disabled = (context.Get("_disablerule") as EcoBoolValue).v;
		}
		catch (EcoException e) {
			errStr = ruleName + ": Script fout: " + e.Message;
			MyDebug.LogError(errStr);
			UnityEngine.Debug.LogException(e);
		}
		catch (System.Exception e2) {
			errStr = ruleName + ": Systeem fout: " + e2.Message;
			MyDebug.LogError(errStr);
			UnityEngine.Debug.LogException(e2);
		}
		context.PopAll();
		context.PushStackBack(stack);
		return errStr;
	}
	
	public static JGameRule Load(XmlTextReader reader) {
		string name = reader.GetAttribute("name");
		int mask = int.Parse(reader.GetAttribute("mask"));
		string script = reader.GetAttribute("script");
		
		JIOUtils.ReadUntilEndElement(reader, XML_ELEMENT);
		JGameRule g = new JGameRule(name, mask, script, false);
		return g;
	}
	
	public void Save(XmlTextWriter writer) {
		writer.WriteStartElement(XML_ELEMENT);
		writer.WriteAttributeString("name", ruleName);
		writer.WriteAttributeString("mask", ruleMask.ToString());
		writer.WriteAttributeString("script", script);
		writer.WriteEndElement();
	}
	
}
