using UnityEngine;
using System.Collections;


public class JSpecies {
	
	public class Parameter {
		public EParamTypes type;
		public float min = 0.0f;
		public float max = 1.0f;
	}
	
	public class Vegetation {
		public Vegetation(int si, int vi) { successionIndex = si; vegetationIndex = vi; }
		public int successionIndex;
		public int vegetationIndex;
		public bool isCompatible(JXVegetation veg) {
			if (successionIndex < 0) return true;
			if (successionIndex == veg.succession.index) {
				if ((vegetationIndex < 0) || (vegetationIndex == veg.index)) return true;
			}
			return false;
		}
	}
	
	public class Rule {
		public Rule() {
			conditions = new Parameter[0];
			vegetations = new Vegetation[0];
		}
		public Parameter[] conditions;
		public Vegetation[] vegetations;
		public float chance = 1.0f;
		public int delta = 1;
		public bool canSpawn = true;
	}
	
	public string name;
	public int index;
	public Rule[] rules;
	public int spawnRadius = 5;
	public int spawnCount = 10;
	public int spawnMultiplier = 0;
	
}
