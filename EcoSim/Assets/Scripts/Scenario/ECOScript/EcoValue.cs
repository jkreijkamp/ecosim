using System;
using System.Collections.Generic;


public abstract class EcoValue
{	
	public abstract object GetLLValue();

	public virtual string StringRepresentation() { return null; }		
}

public abstract class EcoFieldValue : EcoValue
{
	public abstract object GetLLValue(string field);
	public abstract EcoValue GetValue(string key);
	public abstract bool HasField(string field);
}

public class EcoNumberValue : EcoValue {
	public EcoNumberValue(long v) {
		this.v = v;
	}
	public readonly long v;

	public override object GetLLValue() { return v; }
	
	public override string StringRepresentation() { return v.ToString(); }
	
}

public class EcoFloatValue : EcoValue {
	public EcoFloatValue(double v) {
		this.v = v;
	}
	public readonly double v;

	public override object GetLLValue() { return v; }

	public override string StringRepresentation() { return v.ToString("0.0000"); }
}


public class EcoBoolValue : EcoValue {
	public static EcoBoolValue FALSE = new EcoBoolValue(false);
	public static EcoBoolValue TRUE = new EcoBoolValue(true);
	
	public static EcoBoolValue Cast(bool b) { return b?TRUE:FALSE; }
	
	// use cast to create an EcoBoolValue instead
	private EcoBoolValue(bool v) {
		this.v = v;
	}
	
	public readonly bool v;

	public override object GetLLValue() { return v; }

	public override string StringRepresentation() { return v?"true":"false"; }
}

public class EcoNullValue : EcoValue {
	public static EcoNullValue NULL = new EcoNullValue();
		
	private EcoNullValue() {
	}
	
	public override object GetLLValue() { return null; }

	public override string StringRepresentation() { return "null"; }
}

public class EcoStringValue : EcoValue {
	public static EcoStringValue EMPTY = new EcoStringValue("");
	
	public EcoStringValue(string v) {
		this.v = v;
	}
	
	public readonly string v;

	public override object GetLLValue() { return v; }
	
	public override string StringRepresentation() {
		string s = "";
		foreach (char c in v) {
			if (c == '\n') s += "\\n";
			else if (c == '\t') s += "\\t";
			else if (c == '"') s += "\\\"";
			else if (c == '\'') s += "\\'";
			else if (c == '\\') s += "\\\\";
			else s += c;
		}
		return "\"" + s + "\"";
	}
	
}

public class EcoFieldReference : EcoFieldValue {
	public EcoFieldReference(EcoVar v) {
		this.v = v;
	}
	
	public readonly EcoVar v;
	
	public override object GetLLValue() { return v.GetValue(); }	
	
	public override object GetLLValue(string key) { return GetValue(key); }
	
	public override EcoValue GetValue(string key) {
		return v.GetValue(null, key);
	}
	
	public override bool HasField(string field) {
		return v.HasField(field);
	}
	
	public IEnumerator<EcoVar> GetEnumerator() {
		return v.GetEnumerator();
	}
}

public class EcoStructValue : EcoFieldValue {
	public EcoStructValue(params object[] args) {
		Dictionary<string, EcoValue> d = new Dictionary<string, EcoValue>();
		for (int i = 0; i < args.Length; i+= 2) {
			string key = (string) args[i];
			object val = args[i + 1];
			if (val is EcoValue) {
				d.Add(key, val as EcoValue);
			}
			else if (val is int) {
				int intVal = (int) val;
				d.Add(key, new EcoNumberValue((long) intVal));
			}
			else if (val is long) {
				d.Add(key, new EcoNumberValue((long) val));
			}
			else if (val is string) {
				d.Add(key, new EcoStringValue((string) val));
			}
			else if (val is bool) {
				d.Add(key, EcoBoolValue.Cast((bool) val));
			}
			else if (val is EcoVar) {
				d.Add(key, new EcoFieldReference((EcoVar) val));
			}
			else {
				MyDebug.LogError("cannot add type " + val.GetType().ToString() + " to string (value = \"" + val.ToString() + "\"");
			}
		}
		dict = d;
	}
	
	readonly Dictionary<string, EcoValue> dict;

	public override object GetLLValue() { throw new EcoException("struct needs field for accessing"); }	
	
	public override object GetLLValue(string key) { return GetValue(key); }
	
	public override EcoValue GetValue(string key) {
		EcoValue result = null;
		if (dict.TryGetValue(key, out result)) {
			return result;
		}
		throw new EcoException("struct doesn't contain field '" + key + "'");
	}
	
	public override bool HasField(string field) {
		return dict.ContainsKey(field);
	}

}
