using System.Collections.Generic;

public class EcoForLoop : EcoStatement
{
	readonly string varName;
	readonly EcoExpression startValue;
	readonly EcoExpression endValue;
	
	readonly EcoStatement stmt;
	
	public EcoForLoop(int lineNr, string varName, EcoExpression startValue, EcoExpression endValue, EcoStatement stmt) : base(lineNr) {
		this.varName = varName;
		this.startValue = startValue;
		this.endValue = endValue;
		this.stmt = stmt;
	}
	
	public override void Execute(EcoContext context) {
		EcoValue v1 = startValue.Execute(context);
		if (!(v1 is EcoNumberValue)) throw new EcoException("for loop start value needs to be a number", lineNr);
		EcoValue v2 = endValue.Execute(context);
		if (!(v2 is EcoNumberValue)) throw new EcoException("for loop end value needs to be a number", lineNr);
		long l1 = (long) (v1.GetLLValue());
		long l2 = (long) (v2.GetLLValue());
		if (l1 == l2) return; // nothing to loop over
		int dir = (l1 < l2)?1:-1;
		UnityEngine.Debug.Log("l1 = " + l1 + ", " + l2 + ", " + dir);
		context.Push();
		context.CreateLocalVar(varName, new EcoNumberValue(l1));
		stmt.Execute(context);
		while (l1 != l2) {
			l1 += dir;
			context.Set(varName, new EcoNumberValue(l1));
			stmt.Execute(context);
		}
		context.Pop();
	}
}