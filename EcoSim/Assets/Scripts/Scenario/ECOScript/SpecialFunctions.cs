using System.Collections.Generic;
using UnityEngine;

public class EcoFnDebug : EcoBaseFunction {
	public EcoFnDebug() : base(-1, "debug") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 1; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		string debugStr = values[0].GetLLValue().ToString();
		// Debug.LogWarning(debugStr);
		MyDebug.Log(debugStr);
		return EcoBoolValue.FALSE;
	}
}

public class EcoFnDoelGebied : EcoBaseFunction {
	public EcoFnDoelGebied() : base(-1, "doelgebied") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 1; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (!(values[0] is EcoNumberValue)) throw new EcoException("argument 1 of '" + name + "' should be a number");
		return new EcoFieldReference(new EcoTargetVar(context, (int) ((values[0] as EcoNumberValue).v), "_intern_"));
	}
}

public class EcoFnSuccessieGebied : EcoBaseFunction {
	public EcoFnSuccessieGebied() : base(-1, "successiegebied") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 0; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		return new EcoFieldReference(new EcoTargetVar(context, -1, "_intern_"));
	}
}

public class EcoFnTegel : EcoBaseFunction {
	public EcoFnTegel() : base(-1, "tegel") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (!(values[0] is EcoNumberValue)) throw new EcoException("argument 1 of '" + name + "' should be a number");
		if (!(values[1] is EcoNumberValue)) throw new EcoException("argument 2 of '" + name + "' should be a number");
		int x = (int) ((EcoNumberValue) values[0]).v;
		int y = (int) ((EcoNumberValue) values[1]).v;
		return new EcoFieldReference(new EcoTileVar(context, x, y, "_intern_"));
	}	
}

public class EcoFnPeilbuizen : EcoBaseFunction {
	public EcoFnPeilbuizen() : base(-1, "peilbuizen") {
	}

	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 1; }

	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (!(values[0] is EcoNumberValue)) throw new EcoException("argument 1 of '" + name + "' should be a number");
		int year = (int) ((EcoNumberValue) values[0]).v;
		return new EcoFieldReference(new EcoResearchPointVar(context, "_intern_", year));
	}
}

public class EcoFnInventarisatieProduct : EcoBaseFunction {
	public EcoFnInventarisatieProduct() : base(-1, "inventarisatieproduct") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if ((values[0] == null) ||  !(values[0] is EcoInventarisationVar)) throw new EcoException("argument 1 of '" + name + "' should be an inventarisatie");
		if (!(values[1] is EcoNumberValue)) throw new EcoException("argument 2 of '" + name + "' should be a number");
		EcoInventarisationVar eiv = (EcoInventarisationVar) (values[0]);
		int nr = (int) ((EcoNumberValue) values[1]).v;
		JInventarisationResultMap map = eiv.map.ProductOfTargetMap(context.scenario, nr);
		return new EcoInventarisationVar(context, map, "_intern_");
	}
}

public class EcoFnZetMaatregel : EcoBaseFunction {
	public EcoFnZetMaatregel() : base(-1, "zetmaatregel") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if ((values[0] == null) || (!(values[0] is EcoTileVar))) throw new EcoException("argument 1 of '" + name + "' should be of type 'Tile'");
		EcoTileVar tile = values[0] as EcoTileVar;
		if (!(values[1] is EcoStringValue)) throw new EcoException("argument 2 of function '" + name + "' must be a string");
		string atstr = (values[1] as EcoStringValue).v;
		EActionTypes at = EnumExtensions.GetActionType(atstr);
		EActionGroups ag = (EActionGroups) ((int) at & 0xf0);
		if (at == EActionTypes.UNDEFINED) throw new EcoException("'" + atstr + "' is an unknown maatregel");
		JMeasureMap m = context.scenario.progress.GetMeasureMap(ag);
		m.Set((int) tile.x, (int) tile.y, ((int) at) & 0x0f);
		return EcoBoolValue.FALSE;
	}
}

public class EcoFnLeesMaatregel : EcoBaseFunction {
	public EcoFnLeesMaatregel() : base(-1, "leesmaatregel") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if ((values[0] == null) || (!(values[0] is EcoTileVar))) throw new EcoException("argument 1 of '" + name + "' should be of type 'Tile'");
		EcoTileVar tile = values[0] as EcoTileVar;
		if (!(values[1] is EcoStringValue)) throw new EcoException("argument 2 of function '" + name + "' must be a string");
		string atstr = (values[1] as EcoStringValue).v;
		EActionTypes at = EnumExtensions.GetActionType(atstr);
		EActionGroups ag = (EActionGroups) ((int) at & 0xf0);
		if (at == EActionTypes.UNDEFINED) throw new EcoException("'" + atstr + "' is an unknown maatregel");
		JMeasureMap m = context.scenario.progress.GetMeasureMap(ag);
		int offset = m.Get((int) tile.x, (int) tile.y);
		if (offset == 0) return EcoStringValue.EMPTY;
		at = (EActionTypes) (((int) ag) + offset);
		return new EcoStringValue(at.ToString().ToLower());
	}
}

public class EcoFnZetSoort : EcoBaseFunction {
	public EcoFnZetSoort() : base(-1, "zetsoort") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 3; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if ((values[0] == null) || (!(values[0] is EcoTileVar))) throw new EcoException("argument 1 of '" + name + "' should be of type 'Tile'");
		EcoTileVar tile = values[0] as EcoTileVar;
		if (!(values[1] is EcoNumberValue)) throw new EcoException("argument 2 of function '" + name + "' must be a number");
		int speciesId = (int) (values[1] as EcoNumberValue).v;
		if (!(values[2] is EcoNumberValue)) throw new EcoException("argument 3 of function '" + name + "' must be a number");
		int v = (int) (values[2] as EcoNumberValue).v;
		if ((v < 0) || (v > 3)) throw new EcoException("argument 2 should be between 0 and 3");
		context.scenario.data.SetSpecies((int) tile.x, (int) tile.y, speciesId, v);
		return EcoBoolValue.FALSE;
	}
}

public class EcoFnLeesSoort : EcoBaseFunction {
	public EcoFnLeesSoort() : base(-1, "leessoort") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if ((values[0] == null) || (!(values[0] is EcoTileVar))) throw new EcoException("argument 1 of '" + name + "' should be of type 'Tile'");
		if (!(values[1] is EcoNumberValue)) throw new EcoException("argument 2 of function '" + name + "' must be a number");
		int speciesId = (int) (values[1] as EcoNumberValue).v;
		EcoTileVar tile = values[0] as EcoTileVar;
		return new EcoNumberValue(context.scenario.data.GetSpecies((int) tile.x, (int) tile.y, speciesId));
	}
}

public class EcoFnLeesParameter : EcoBaseFunction {
	public EcoFnLeesParameter() : base(-1, "leesparameter") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if ((values[0] == null) || (!(values[0] is EcoTileVar))) throw new EcoException("argument 1 of '" + name + "' should be of type 'Tile'");
		if (!(values[1] is EcoStringValue)) throw new EcoException("argument 2 of function '" + name + "' must be a string");
		EParamTypes param = EnumExtensions.GetParamType(((EcoStringValue)values[1]).v);
		EcoTileVar tile = values[0] as EcoTileVar;
		return new EcoNumberValue(context.scenario.data.GetData(param, (int) tile.x, (int) tile.y));
	}
}

public class EcoFnZetParameter : EcoBaseFunction {
	public EcoFnZetParameter() : base(-1, "zetparameter") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 3; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if ((values[0] == null) || (!(values[0] is EcoTileVar))) throw new EcoException("argument 1 of '" + name + "' should be of type 'Tile'");
		if (!(values[1] is EcoStringValue)) throw new EcoException("argument 2 of function '" + name + "' must be a string");
		if (!(values[2] is EcoNumberValue)) throw new EcoException("argument 3 of function '" + name + "' must be a number");
		EParamTypes param = EnumExtensions.GetParamType(((EcoStringValue)values[1]).v);
		EcoTileVar tile = values[0] as EcoTileVar;
		byte val = (byte) (values[2] as EcoNumberValue).v;
		context.scenario.data.SetData(param, (int) tile.x, (int) tile.y, val);
		return EcoBoolValue.FALSE;
	}
}


public class EcoFnZetVegetatie : EcoBaseFunction {
	public EcoFnZetVegetatie() : base(-1, "zetvegetatie") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 4; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if ((values[0] == null) || (!(values[0] is EcoTileVar))) throw new EcoException("argument 1 of '" + name + "' should be of type 'Tile'");
		if (!(values[1] is EcoNumberValue)) throw new EcoException("argument 2 of function '" + name + "' must be a number");
		if (!(values[2] is EcoNumberValue)) throw new EcoException("argument 3 of function '" + name + "' must be a number");
		if (!(values[3] is EcoNumberValue)) throw new EcoException("argument 4 of function '" + name + "' must be a number");
		int successionId = (int) (values[1] as EcoNumberValue).v;
		int vegetationId = (int) (values[2] as EcoNumberValue).v;
		int tileId = (int) (values[3] as EcoNumberValue).v;
		JScenario scenario = context.scenario;
		if ((successionId < 0) || (successionId >= scenario.successions.Length)) throw new EcoException("'" + name + "' succession index out of range");
		JXSuccession succession = scenario.successions[successionId];
		if ((vegetationId < 0) || (vegetationId >= succession.vegetation.Length)) throw new EcoException("'" + name + "' vegetation index out of range");
		JXVegetation vegetation = succession.vegetation[vegetationId];
		if (tileId < 0) {
			tileId = Random.Range(0, vegetation.tiles.Length);
		}
		else {
			if (tileId >= vegetation.tiles.Length) throw new EcoException("'" + name + "' tile index out of range");
		}
		EcoTileVar tile = values[0] as EcoTileVar;
		scenario.data.SetVegetation(tile.x, tile.y, vegetation, tileId);
		return EcoBoolValue.FALSE;
	}
}

public class EcoFnMaakNest : EcoBaseFunction {
	public EcoFnMaakNest() : base(-1, "maaknest") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 4; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if ((values[0] == null) || (!(values[0] is EcoTileVar))) throw new EcoException("argument 1 of '" + name + "' should be of type 'Tile'");
		if (!(values[1] is EcoNumberValue)) throw new EcoException("argument 2 of function '" + name + "' must be a number");
		if (!(values[2] is EcoNumberValue)) throw new EcoException("argument 3 of function '" + name + "' must be a number");
		if (!(values[3] is EcoNumberValue)) throw new EcoException("argument 4 of function '" + name + "' must be a number");
		EcoTileVar tile = values[0] as EcoTileVar;
		int id = (int) (values[1] as EcoNumberValue).v;
		int female = (int) (values[2] as EcoNumberValue).v;
		int male = (int) (values[3] as EcoNumberValue).v;
		JAnimalSpecies.Nest n = new JAnimalSpecies.Nest();
		n.position = new Coordinate(tile.x, tile.y);
		n.females = female;
		n.males = male;
		List<JAnimalSpecies.Nest> list = new List<JAnimalSpecies.Nest>(context.scenario.animalSpecies[id].nests);
		list.Add(n);
		context.scenario.animalSpecies[id].nests = list.ToArray();
		return EcoBoolValue.FALSE;
	}
}

public class EcoFnNesten : EcoBaseFunction {
	public EcoFnNesten() : base(-1, "nesten") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 1; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (!(values[0] is EcoNumberValue)) throw new EcoException("argument 1 of function '" + name + "' must be a number");
		int id = (int) (values[0] as EcoNumberValue).v;
		return new EcoFieldReference(new EcoNestsVar(context, context.scenario.animalSpecies[id], "__internal__"));
	}
}


public class EcoFnVeldVeranderd : EcoBaseFunction {
	public EcoFnVeldVeranderd() : base(-1, "veldveranderd") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 0; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		JCamera.ForceRender();
		return EcoBoolValue.FALSE;
	}
}



public class EcoFnVerwijderRegel : EcoBaseFunction {
	public EcoFnVerwijderRegel() : base(-1, "verwijderregel") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 0; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		EcoVar disV = context.Find("_disablerule");
		if (disV == null) throw new EcoException("'" + name + "' can only be used within game rules");
		disV.SetValue(null, null, EcoBoolValue.TRUE);
		return EcoBoolValue.FALSE;
	}
}


public class EcoFnToonEncyclopedie : EcoBaseFunction {
	public EcoFnToonEncyclopedie() : base(-1, "toonencyclopedie") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 1; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (!(values[0] is EcoStringValue)) throw new EcoException("argument 1 of function '" + name + "' must be a string");
		
		string entry = values[0].GetLLValue().ToString();
		return EcoBoolValue.Cast(JGameEncyclopedia.OpenEncyclopedia(entry));
	}
}

public class EcoFnEncyclopedieGeraadpleegd : EcoBaseFunction {
	public EcoFnEncyclopedieGeraadpleegd() : base(-1, "encyclopediegeraadpleegd") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 1; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (!(values[0] is EcoStringValue)) throw new EcoException("argument 1 of function '" + name + "' must be a string");
		
		string entry = values[0].GetLLValue().ToString();
		if (context.scenario.progress == null) return EcoBoolValue.FALSE;
		return EcoBoolValue.Cast(context.scenario.progress.IsEncyclopediaEntryViewed(entry));
	}
}


public class EcoFnHint : EcoBaseFunction {
	public EcoFnHint() : base(-1, "hint") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 3; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (!(values[0] is EcoStringValue)) throw new EcoException("argument 1 'avatar' of function 'hint' must be a string");
		if (!(values[2] is EcoBoolValue)) throw new EcoException("argument 3 'gameover' of function 'hint' must be a boolean");
		string av = values[0].GetLLValue().ToString();
		string msg = values[1].GetLLValue().ToString();
		bool gameOver = (values[2] as EcoBoolValue).v;
		JGameControl.AddToHelpQueue(av + "|" + msg, gameOver);
		return EcoBoolValue.FALSE;
	}
}


public class EcoFnArticle : EcoBaseFunction {
	public EcoFnArticle() : base(-1, "artikel") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (!(values[1] is EcoBoolValue)) throw new EcoException("argument 2 'gameover' of function 'artikel' must be a boolean");
		bool gameOver = (values[1] as EcoBoolValue).v;
		if (values[0] is EcoNumberValue) {
			int id = (int) ((values[0] as EcoNumberValue).v);
			if ((id < 0) || (id >= context.scenario.articles.Length))
				throw new EcoException("invalid article id '" + id + "'");
			Debug.Log("toon artikel " + id);
			JGameControl.AddToMessageQueue(context.scenario.articles[id], gameOver);
			return EcoBoolValue.FALSE;
		}
		else if (values[0] is EcoStringValue) {
			string msg = ((values[0] as EcoStringValue).v);
			JGameControl.AddToMessageQueue(msg, gameOver);
			return EcoBoolValue.FALSE;
		}
		throw new EcoException("argument 1 'artikel' of function 'artikel' must be a number (article id) or a string (formatted article)");
	}
}



public class EcoToFloat : EcoBaseFunction {
	public EcoToFloat() : base(-1, "tofloat") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 1; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (values[0] is EcoNumberValue) {
			long v1 = (long) values[0].GetLLValue();
			return new EcoFloatValue((double) v1);
		}
		if (values[0] is EcoStringValue) {
			string v1 = (string) values[0].GetLLValue();
			double result;
			if (double.TryParse(v1, out result)) {
				return new EcoFloatValue(result);
			}
			throw new EcoException("argument 1 ('" + v1 + "') is not a valid float string");
		}
		throw new EcoException("argument 1 of 'tofloat' should either be a string or a number value");
	}
}

public class EcoToNumber : EcoBaseFunction {
	public EcoToNumber() : base(-1, "tonumber") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 1; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (values[0] is EcoFloatValue) {
			double v1 = (double) values[0].GetLLValue();
			return new EcoNumberValue((long) v1);
		}
		if (values[0] is EcoStringValue) {
			string v1 = (string) values[0].GetLLValue();
			long result;
			if (long.TryParse(v1, out result)) {
				return new EcoNumberValue(result);
			}
			throw new EcoException("argument 1 ('" + v1 + "') is not a valid number string");
		}
		throw new EcoException("argument 1 of 'tofloat' should either be a string or a float value");
	}
}

public class EcoToString : EcoBaseFunction {
	public EcoToString() : base(-1, "tostring") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (!(values[1] is EcoStringValue)) throw new EcoException("argument 2 of 'tostring' needs to be a format string");
		string format = (values[1] as EcoStringValue).v;
		if (values[0] is EcoNumberValue) {
			long v1 = (long) values[0].GetLLValue();
			try {
				return new EcoStringValue(v1.ToString(format));
			}
			catch (System.Exception e) {
				throw new EcoException(e.Message);
			}
		}
		else if (values[0] is EcoFloatValue) {
			double v1 = (double) values[0].GetLLValue();
			try {
				return new EcoStringValue(v1.ToString(format));
			}
			catch (System.Exception e) {
				throw new EcoException(e.Message);
			}
		}
		else if (values[0] is EcoBoolValue) {
			bool v1 = (bool) values[0].GetLLValue();
			try {
				return new EcoStringValue(v1?"true":"false");
			}
			catch (System.Exception e) {
				throw new EcoException(e.Message);
			}
		}
		throw new EcoException("argument 1 of 'tostring' should either be a number or a float value");
	}
}
