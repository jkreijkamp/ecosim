using System.Collections.Generic;

public abstract class EcoExpression
{
	public abstract EcoValue Execute(EcoContext context);
}

public class EcoBinaryExpression : EcoExpression {
	public enum BinOperator { ADD, SUB, MUL, DIV, AND, OR, LT, LE, GT, GE, EQ, NE, };
	
	public EcoBinaryExpression(BinOperator op, EcoExpression lhs, EcoExpression rhs) {
		this.op = op;
		this.leftHandSide = lhs;
		this.rightHandSide = rhs;
	}
	
	readonly EcoExpression leftHandSide;
	readonly EcoExpression rightHandSide;
	readonly BinOperator op;
	
	
	public override EcoValue Execute(EcoContext context) {
		switch (op) {
			case BinOperator.ADD : {
				EcoValue v1 = leftHandSide.Execute(context);
				EcoValue v2 = rightHandSide.Execute(context);
				if ((v1 is EcoNumberValue) && (v2 is EcoNumberValue)) {
					long l1 = (v1 as EcoNumberValue).v;
					long l2 = (v2 as EcoNumberValue).v;
					return new EcoNumberValue(l1 + l2);
				}
				else if ((v1 is EcoFloatValue) && (v2 is EcoFloatValue)) {
					double l1 = (v1 as EcoFloatValue).v;
					double l2 = (v2 as EcoFloatValue).v;
					return new EcoFloatValue(l1 + l2);
				}
				else if (v1 is EcoStringValue) {
					if ((v2 is EcoStringValue) || (v2 is EcoNumberValue) || (v2 is EcoFloatValue)) {
						return new EcoStringValue(((string) v1.GetLLValue()) + v2.GetLLValue());
					}
				}
				break;
			}
			case BinOperator.SUB : {
				EcoValue v1 = leftHandSide.Execute(context);
				EcoValue v2 = rightHandSide.Execute(context);
				if ((v1 is EcoNumberValue) && (v2 is EcoNumberValue)) {
					long l1 = (v1 as EcoNumberValue).v;
					long l2 = (v2 as EcoNumberValue).v;
					return new EcoNumberValue(l1 - l2);
				}
				else if ((v1 is EcoFloatValue) && (v2 is EcoFloatValue)) {
					double l1 = (v1 as EcoFloatValue).v;
					double l2 = (v2 as EcoFloatValue).v;
					return new EcoFloatValue(l1 - l2);
				}
				break;
			}
			case BinOperator.MUL : {
				EcoValue v1 = leftHandSide.Execute(context);
				EcoValue v2 = rightHandSide.Execute(context);
				if ((v1 is EcoNumberValue) && (v2 is EcoNumberValue)) {
					long l1 = (v1 as EcoNumberValue).v;
					long l2 = (v2 as EcoNumberValue).v;
					return new EcoNumberValue(l1 * l2);
				}
				else if ((v1 is EcoFloatValue) && (v2 is EcoFloatValue)) {
					double l1 = (v1 as EcoFloatValue).v;
					double l2 = (v2 as EcoFloatValue).v;
					return new EcoFloatValue(l1 * l2);
				}
				break;
			}
			case BinOperator.DIV : {
				EcoValue v1 = leftHandSide.Execute(context);
				EcoValue v2 = rightHandSide.Execute(context);
				if ((v1 is EcoNumberValue) && (v2 is EcoNumberValue)) {
					long l1 = (v1 as EcoNumberValue).v;
					long l2 = (v2 as EcoNumberValue).v;
					if (l2 == 0) throw new EcoException("divide by zero");
					return new EcoNumberValue(l1 / l2);
				}
				else if ((v1 is EcoFloatValue) && (v2 is EcoFloatValue)) {
					double l1 = (v1 as EcoFloatValue).v;
					double l2 = (v2 as EcoFloatValue).v;
					if (l2 == 0.0) throw new EcoException("divide by zero");
					return new EcoFloatValue(l1 / l2);
				}
				break;
			}
			case BinOperator.AND : {
				EcoValue v1 = leftHandSide.Execute(context);
				if (v1 is EcoBoolValue) {
					if ((v1 as EcoBoolValue).v == false) return EcoBoolValue.Cast(false);
					else {
						EcoValue v2 = rightHandSide.Execute(context);
						if (v2 is EcoBoolValue) {
							return EcoBoolValue.Cast((bool) v2.GetLLValue());
						}
					}
				}
				break;
			}
			case BinOperator.OR : {
				EcoValue v1 = leftHandSide.Execute(context);
				if (v1 is EcoBoolValue) {
					if ((v1 as EcoBoolValue).v == true) return EcoBoolValue.Cast(true);
					else {
						EcoValue v2 = rightHandSide.Execute(context);
						if (v2 is EcoBoolValue) {
							return EcoBoolValue.Cast((bool) v2.GetLLValue());
						}
					}
				}
				break;
			}
			case BinOperator.LT : {
				EcoValue v1 = leftHandSide.Execute(context); 		
				EcoValue v2 = rightHandSide.Execute(context);
				if ((v1 is EcoNumberValue) && (v2 is EcoNumberValue)) {
					return EcoBoolValue.Cast(((long) v1.GetLLValue()) < ((long) v2.GetLLValue()));
				}
				else if ((v1 is EcoFloatValue) && (v2 is EcoFloatValue)) {
					return EcoBoolValue.Cast(((double) v1.GetLLValue()) < ((double) v2.GetLLValue()));
				}
				break;
			}
			case BinOperator.GT : {
				EcoValue v1 = leftHandSide.Execute(context); 		
				EcoValue v2 = rightHandSide.Execute(context);
				if ((v1 is EcoNumberValue) && (v2 is EcoNumberValue)) {
					return EcoBoolValue.Cast(((long) v1.GetLLValue()) > ((long) v2.GetLLValue()));
				}
				else if ((v1 is EcoFloatValue) && (v2 is EcoFloatValue)) {
					return EcoBoolValue.Cast(((double) v1.GetLLValue()) > ((double) v2.GetLLValue()));
				}
				break;
			}
			case BinOperator.LE : {
				EcoValue v1 = leftHandSide.Execute(context); 		
				EcoValue v2 = rightHandSide.Execute(context);
				if ((v1 is EcoNumberValue) && (v2 is EcoNumberValue)) {
					return EcoBoolValue.Cast(((long) v1.GetLLValue()) <= ((long) v2.GetLLValue()));
				}
				else if ((v1 is EcoFloatValue) && (v2 is EcoFloatValue)) {
					return EcoBoolValue.Cast(((double) v1.GetLLValue()) <= ((double) v2.GetLLValue()));
				}
				break;
			}
			case BinOperator.GE : {
				EcoValue v1 = leftHandSide.Execute(context); 		
				EcoValue v2 = rightHandSide.Execute(context);
				if ((v1 is EcoNumberValue) && (v2 is EcoNumberValue)) {
					return EcoBoolValue.Cast(((long) v1.GetLLValue()) >= ((long) v2.GetLLValue()));
				}
				else if ((v1 is EcoFloatValue) && (v2 is EcoFloatValue)) {
					return EcoBoolValue.Cast(((double) v1.GetLLValue()) >= ((double) v2.GetLLValue()));
				}
				break;
			}
			case BinOperator.EQ : {
				EcoValue v1 = leftHandSide.Execute(context); 		
				EcoValue v2 = rightHandSide.Execute(context);
				if (v1.GetType() == v2.GetType()) {
					if (v1 is EcoNumberValue) {
						return EcoBoolValue.Cast(((long) v1.GetLLValue()) == ((long) v2.GetLLValue()));
					}
					if (v1 is EcoFloatValue) {
						return EcoBoolValue.Cast(((double) v1.GetLLValue()) == ((double) v2.GetLLValue()));
					}
					if (v1 is EcoStringValue) {
						return EcoBoolValue.Cast(((string) v1.GetLLValue()) == ((string) v2.GetLLValue()));
					}
					if (v1 is EcoBoolValue) {
						return EcoBoolValue.Cast(((bool) v1.GetLLValue()) == ((bool) v2.GetLLValue()));
					}
				}
				break;
			}
			case BinOperator.NE : {
				EcoValue v1 = leftHandSide.Execute(context); 		
				EcoValue v2 = rightHandSide.Execute(context);
				if (v1.GetType() == v2.GetType()) {
					if (v1 is EcoNumberValue) {
						return EcoBoolValue.Cast(((long) v1.GetLLValue()) != ((long) v2.GetLLValue()));
					}
					if (v1 is EcoFloatValue) {
						return EcoBoolValue.Cast(((double) v1.GetLLValue()) != ((double) v2.GetLLValue()));
					}
					if (v1 is EcoStringValue) {
						return EcoBoolValue.Cast(((string) v1.GetLLValue()) != ((string) v2.GetLLValue()));
					}
					if (v1 is EcoBoolValue) {
						return EcoBoolValue.Cast(((bool) v1.GetLLValue()) != ((bool) v2.GetLLValue()));
					}
				}
				break;
			}			
		}
		EcoValue v1err = leftHandSide.Execute(context); 		
		EcoValue v2err = rightHandSide.Execute(context);

		throw new EcoException("Type mismatch " + op.ToString() + " on " + v1err + ", " + v2err);
	}
}

public class EcoUnaryExpression : EcoExpression {
	public enum UnaryOperator { NEGATE, NOT, };

	
	public EcoUnaryExpression(UnaryOperator op, EcoExpression rhs) {
		this.op = op;
		this.rightHandSide = rhs;
	}
	
	readonly EcoExpression rightHandSide;
	readonly UnaryOperator op;
	
	
	public override EcoValue Execute(EcoContext context) {
		EcoValue v1 = rightHandSide.Execute(context);
		switch (op) {
			case UnaryOperator.NEGATE : {	
				if (v1 is EcoNumberValue) {
					return new EcoNumberValue(-(long) (v1.GetLLValue()));
				}
				if (v1 is EcoFloatValue) {
					return new EcoFloatValue(-(double) (v1.GetLLValue()));
				}
				break;
			}
			case UnaryOperator.NOT : {
				if (v1 is EcoBoolValue) {
					return EcoBoolValue.Cast(!(bool) (v1.GetLLValue()));
				}
				break;
			}
		}
		throw new EcoException("Type mismatch " + op.ToString() + " on " + v1.GetType().ToString());
	}
}

public class EcoVarAccess : EcoExpression {

	public EcoVarAccess(EcoContext context, string id) {
		this.id = id;
		arguments = null;
	}

	public EcoVarAccess(EcoContext context, string id, EcoExpression[] args) {
		this.id = id;
		arguments = args;
	}
	
	readonly EcoExpression[] arguments;
	readonly string id;
	
	public override EcoValue Execute(EcoContext context) {
		if (arguments == null) {
			EcoValue v = context.Get(id);
			if (v == null) throw new EcoException("variable '" + id + "' doesn't exist");
			return v;
		}
		else {
			int[] values = new int[arguments.Length];
			for (int i = 0; i < arguments.Length; i++) {
				EcoValue val = arguments[i].Execute(context);
				if (!(val is EcoNumberValue)) throw new EcoException("index " + (i + 1) + " for '" + id + "' is not a number");
				values[i] = (int) ((val as EcoNumberValue).v);
			}
			EcoValue v = context.Get(id, values, null);
			if (v == null) throw new EcoException("variable '" + id + "' doesn't exist");
			return v;
		}
	}
}

public class EcoFunctionCall : EcoExpression {
	public EcoFunctionCall(EcoBaseFunction fn, EcoExpression[] args) {
		function = fn;
		arguments = args;
	}
	
	readonly EcoExpression[] arguments;
	readonly EcoBaseFunction function;
	
	public override EcoValue Execute(EcoContext context) {
		EcoValue[] values = new EcoValue[arguments.Length];
		for (int i = 0; i < arguments.Length; i++) {
			values[i] = arguments[i].Execute(context);
			while (values[i] is EcoReferenceVar) {
				values[i] = ((EcoReferenceVar) values[i]).val;
			}
			while (values[i] is EcoFieldReference) {
				values[i] = ((EcoFieldReference) values[i]).v;
			}
		}
		return function.Execute(context, values);
	}
}

public class EcoNullConstant : EcoExpression {
	public EcoNullConstant() {
	}
	
	public override EcoValue Execute(EcoContext context) {
		return EcoNullValue.NULL;
	}
}

public class EcoConstant : EcoExpression {
	public EcoConstant(string str) {
		val = new EcoStringValue(str);
	}

	public EcoConstant(long nr) {
		val = new EcoNumberValue(nr);
	}
	
	public EcoConstant(double nr) {
		val = new EcoFloatValue(nr);
	}
	
	public EcoConstant(bool b) {
		val = EcoBoolValue.Cast(b);
	}
		
	public readonly EcoValue val;
	
	public override EcoValue Execute(EcoContext context) {
		return val;
	}
}

public class EcoIsDefined : EcoExpression {
	public EcoIsDefined(string varName, string fieldName) {
		this.varName = varName;
		this.fieldName = fieldName;
	}
	
	readonly string varName;
	readonly string fieldName;

	
	public override EcoValue Execute(EcoContext context) {
		EcoVar v = context.Find(varName);
		if (fieldName == null) {
			return EcoBoolValue.Cast(v != null);
		}
		return EcoBoolValue.Cast(v.HasField(fieldName));
	}
}