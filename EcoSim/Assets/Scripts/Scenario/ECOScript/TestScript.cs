using UnityEngine;
using System.Collections;

public class TestScript : MonoBehaviour {
	EcoContext context;
	string script = "";
	string block = "";
	string error = "";
	EcoScript es = new EcoScript();
	EcoBlock b = null;
	
	Rect winDebugR = new Rect(600, 100, 500, 500);
	
	// Use this for initialization
	void Start () {
	}
	
	Vector2 scroll;
	
	void ConsoleWindow(int winId) {
//		MyGUI.CheckMouseOver(winConsoleR);
		scroll = GUILayout.BeginScrollView(scroll);
		GUILayout.BeginVertical();
		foreach (string s in MyDebug.log) {
			GUILayout.Label(s, GUILayout.Width(380));
		}
		GUILayout.EndVertical();
		GUILayout.EndScrollView();
		if (GUILayout.Button("Wis log")) {
			MyDebug.Clear();
		}
		GUI.DragWindow();
	}
	
	// Update is called once per frame
	void OnGUI() {
		winDebugR = GUI.Window(1000, winDebugR, ConsoleWindow, "Console");
		script = GUI.TextArea(new Rect(10, 10, 500, 400), script);
		block = GUI.TextArea(new Rect(10, 520, 500, 100), block);
		if (GUI.Button(new Rect(10, 630, 100, 30), "Test")) {
			context = new EcoContext(null);
			try {
				es.Parse(context, script);
			}
			catch (EcoException e) {
				error = "Parsing script: " + e.Message;
				throw e;
			}
			try {
				b = es.ParseBlock(context, block);
			}
			catch (EcoException e) {
				error = "Parsing block: " + e.Message;
				throw e;
			}
			try {
				context.Set("voortgang", new EcoStructValue("jaar", new EcoNumberValue(2012), "budget", new EcoNumberValue(10000000)));
				b.Execute(context);
				error = "Block executed";
			}
			catch (EcoException e) {
				error = "Execute block: " + e.Message;
				throw e;
			}
		}
		GUI.Label(new Rect(10, 670, 500, 100), error);
	}
}
