using System.Collections.Generic;

public class EcoSetValue : EcoStatement
{
	readonly EcoExpression expression;
	readonly string variableName;
	readonly public EcoExpression[] indices;
	readonly string field;
	
	public EcoSetValue(int lineNr, string varName, EcoExpression[] indices, string field, EcoExpression expr) : base(lineNr) {
		variableName = varName;
		expression = expr;
		this.indices = indices;
		this.field = field;
	}
	
	public override void Execute(EcoContext context) {
		EcoValue val = expression.Execute(context);
		if (indices == null) {
			context.Set(variableName, null, field, val);
		}
		else {
			int[] indicesVal = new int[indices.Length];
			for (int i = 0; i < indices.Length; i++) {
				indicesVal[i] = (int) ((indices[i].Execute(context) as EcoNumberValue).v);
			}
			context.Set(variableName, indicesVal, field, val);
		}
	}
}