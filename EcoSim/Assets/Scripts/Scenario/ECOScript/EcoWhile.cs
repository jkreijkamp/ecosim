using System.Collections.Generic;

public class EcoWhile : EcoStatement
{
	readonly EcoExpression expression;
	readonly EcoStatement trueStmt;
	
	public EcoWhile(int lineNr, EcoExpression expression, EcoStatement trueStmt) : base(lineNr) {
		this.expression = expression;
		this.trueStmt = trueStmt;
	}
	
	public override void Execute(EcoContext context) {
		while (true) {
			EcoValue val = expression.Execute(context);
			if (val is EcoBoolValue) {
				if ((val as EcoBoolValue).v == true) {
					trueStmt.Execute(context);
				}
				else {
					return;
				}
			}
			else {
				throw new EcoException("expression needs to result in boolean value", lineNr);
			}
		}
	}
}

