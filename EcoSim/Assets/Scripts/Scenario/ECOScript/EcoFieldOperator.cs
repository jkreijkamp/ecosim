using System.Collections.Generic;

public class EcoFieldOperator : EcoExpression
{
	readonly EcoExpression expression;
	readonly string field;
	
	public EcoFieldOperator(EcoContext context, EcoExpression expression, string field) {
		this.expression = expression;
		this.field = field;
	}
	
	public override EcoValue Execute(EcoContext context) {
		EcoValue v = expression.Execute(context);
//		if (!(v is EcoFieldValue)) throw new EcoException("field operator '" + field + "' used on a non-struct value");
		return ((EcoFieldValue) v).GetValue(field);
	}
}