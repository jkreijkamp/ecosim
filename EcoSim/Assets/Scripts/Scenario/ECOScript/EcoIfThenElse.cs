using System.Collections.Generic;

public class EcoIfThenElse : EcoStatement
{
	readonly EcoExpression expression;
	readonly EcoStatement trueStmt;
	readonly EcoStatement falseStmt;
	
	public EcoIfThenElse(int lineNr, EcoExpression expression, EcoStatement trueStmt, EcoStatement falseStmt) : base(lineNr) {
		this.expression = expression;
		this.trueStmt = trueStmt;
		this.falseStmt = falseStmt;
	}
	
	public override void Execute(EcoContext context) {
		EcoValue val = expression.Execute(context);
		if (val is EcoBoolValue) {
			if ((val as EcoBoolValue).v == true) {
				trueStmt.Execute(context);
			}
			else {
				if (falseStmt != null) {
					falseStmt.Execute(context);
				}
			}
		}
		else {
			throw new EcoException("expression needs to result in boolean value", lineNr);
		}
	}
}


