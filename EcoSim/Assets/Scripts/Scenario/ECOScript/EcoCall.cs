using System.Collections.Generic;

public class EcoCall : EcoStatement
{
	readonly EcoExpression expression;
	
	public EcoCall(int lineNr, EcoExpression expression) : base(lineNr) {
		this.expression = expression;
	}
	
	public override void Execute(EcoContext context) {
		expression.Execute(context);
	}
}


