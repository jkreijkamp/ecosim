using UnityEngine;
using System.Collections.Generic;

public class EcoScript {
	
	private string parseStr;
	private int index;
	private int len;
	private int lineNr;
	private bool redefineFunctions = false;
	
	int FindEndString() {
		int startLineNr = lineNr;
		for (int i = index + 1; i < len; i++) {
			if (parseStr[i] == '\n') lineNr++;
			else if (parseStr[i] == '\\') i++;
			else if (parseStr[i] == '"') return i;
		}
		throw new EcoException("End  '\"' missing for string", Where(), startLineNr);
	}
	
	EcoConstant ReadNumberOrFloat() {
		string nrStr = "";
		if (Peek() == '-') {
			nrStr = "-";
			index++;
		}
		if (!char.IsDigit(Peek())) throw new EcoException("invalid number constant", Where(), lineNr);
		while (char.IsDigit(Peek())) {
			nrStr += parseStr[index++];
		}
		if (Peek() == '.') {
			index++;
			nrStr += ".";
			if (!char.IsDigit(Peek())) throw new EcoException("invalid float constant", Where(), lineNr);
			while (char.IsDigit(Peek())) {
				nrStr += parseStr[index++];
			}
			return new EcoConstant(double.Parse(nrStr));
		}
		else {
			return new EcoConstant(long.Parse(nrStr));
		}
	}
	
	string ReadString() {
		if (Peek() != '"') return null;
		string result = "";
		for (int i = index + 1; i < len; i++) {
			if (parseStr[i] == '\n') {
				throw new EcoException("End  '\"' missing for string", Where(), lineNr);
			}
			if (parseStr[i] == '\\') {
				i++;
				if (i >= len) break;
				char c = parseStr[i];
				switch (c) {
				case 'n' : result += '\n'; break;
				case 't' : result += '\t'; break;
				case '"' : result += '"'; break;
				case '\'' : result += '\''; break;
				case '\\' : result += '\\'; break;
				default : throw new EcoException("unsupported escape char '\\" + c + "' in string", Where(), lineNr);
				}
			}
			else if (parseStr[i] == '"') {
				index = i + 1;
				return result;
			}
			else {
				result += parseStr[i];
			}
		}
		throw new EcoException("End  '\"' missing for string '" + result + "'", Where(), lineNr);
	}
	
	string Where() {
		int start = Mathf.Max(0, index - 20);
		int end = Mathf.Min(index + 20, len);
		return (parseStr.Substring(start, index - start) + "<^>" + parseStr.Substring(index, end - index)).Replace("\n", " ").Replace("\t", " ").Replace("  ", " ");
	}
	
	int FindClosing(char openElt, char closeElt) {
		int startLineNr = lineNr;
		int openCount = 0;
		for (int i = index; i < len; i++) {
			if (parseStr[i] == openElt) openCount++;
			else if (parseStr[i] == closeElt) {
				openCount--;
				if (openCount == 0) return i;
			}
			else if (parseStr[i] == '"') {
				i = FindEndString();
			}
			else if (parseStr[i] == ';') {
				break;
			}
		}
		throw new EcoException("Missing '" + closeElt + "' for '" + openElt + "'", Where(), startLineNr);
	}
	
	bool IsEOF() {
		return (index >= len);
	}
	
	void SkipWhiteSpace() {
		bool isComment = false;
		while (index < len) {
			if ((parseStr[index] == '/') && (Peek(1) == '/')) {
				index++;
				isComment = true;
			}
			else if (parseStr[index] == '\n') {
				lineNr++;
				isComment = false;
			}
			else if (!isComment && !char.IsWhiteSpace(parseStr[index])) return;
			index++;
		}
	}
	
	char Peek() {
		if (index >= len) return '\0';
		return parseStr[index];
	}

	char Peek(int offset) {
		if (index + offset >= len) return '\0';
		return parseStr[index + offset];
	}
	
	string GetKeyword() {
		int startIndex = index;
		while (index < len) {
			if (!char.IsLetter(parseStr[index])) break;
			index++;
		}
		return parseStr.Substring(startIndex, index - startIndex);
	}

	string PeekKeyword() {
		int saveIndex = index;
		int saveLineNr = lineNr;
		string kw = GetKeyword();
		index = saveIndex;
		lineNr = saveLineNr;
		return kw;
	}
	
	string GetIdentifier() {
		if ((index >= len) || (!char.IsLetter(parseStr[index]))) return "";
		int startIndex = index;
		while (index < len) {
			char c = parseStr[index];
			if ((c != '_') && !char.IsLetterOrDigit(c)) break;
			index++;
		}
		return parseStr.Substring(startIndex, index - startIndex);
	}
	
	EcoExpression ParseDefined(EcoContext context) {
		SkipWhiteSpace();
		if (Peek() != '(') throw new EcoException("special function call to 'defined' missing '('", Where(), lineNr);
		index++;
		string varname = GetIdentifier();
		string fieldName = null;
		if (varname == "") throw new EcoException("special function call to 'defined' missing variable name as argument", Where(), lineNr);
		if (Peek() == '.') {
			index++;
			fieldName = GetIdentifier();
			if (fieldName == "") throw new EcoException("field name expected", Where(), lineNr);
		}
		SkipWhiteSpace();
		if (Peek() != ')') throw new EcoException("special function call to 'defined' missing ')'", Where(), lineNr);
		index++;
		return new EcoIsDefined(varname, fieldName);
	}
	
	EcoExpression ParseExpression(EcoContext context) {
		return ParseExpression(context, 0, 0);
	}
	
	EcoExpression ParseUnaryExpression(EcoContext context, int level) {
		EcoExpression lhs = null;
		int startLineNr = lineNr;
		SkipWhiteSpace();
		char c = Peek();
		if (c == '(') {
			return ParseBrackets(context, level);
		}
		else if (c == '!') {
			index++;
			lhs = new EcoUnaryExpression(EcoUnaryExpression.UnaryOperator.NOT, ParseUnaryExpression(context, level));
		}
		else if (char.IsDigit(c) || ((c == '-') && (char.IsDigit(Peek(1))))) {
			return ReadNumberOrFloat();
		}
		else if (c == '-') {
			index++;
			lhs = new EcoUnaryExpression(EcoUnaryExpression.UnaryOperator.NEGATE, ParseUnaryExpression(context, level));
		}
		else if (c == '"') {
			return new EcoConstant(ReadString());
		}
		else if (char.IsLetter(c)) {
			// found an id, could be function or a variable
			string id = GetIdentifier();
			if (id == "true") {
				return new EcoConstant(true);
			}
			else if (id == "false") {
				return new EcoConstant(false);
			}
			else if (id == "null") {
				return new EcoNullConstant();
			}
			if (id == "defined") {
				return ParseDefined(context);
			}
			SkipWhiteSpace();
			if (Peek() == '(') {
				// function
				index++;
				List<EcoExpression> args = new List<EcoExpression>();
				SkipWhiteSpace();
				if (!(Peek() == ')')) {
					args.Add(ParseExpression(context, 0, 0));
					SkipWhiteSpace();
					while (Peek() == ',') {
						index++;
						args.Add(ParseExpression(context, 0, 0));
						SkipWhiteSpace();
					}
				}
				if (Peek() != ')') throw new EcoException("Missing ')' for function call '" + id + "'", Where(), lineNr);
				index++;
				if (!context.functions.ContainsKey(id)) throw new EcoException("function '" + id + "' does not exist", Where(), lineNr);
				if (context.functions[id].GetParamCount() != args.Count) {
					throw new EcoException("parameter count mismatch for call to function '" + id + "'", Where(), lineNr);
				}
				lhs = new EcoFunctionCall(context.functions[id], args.ToArray());
			}
			else {
				// variable
				if (Peek() == '[') {
					// var has array indexing
					index++;
					List<EcoExpression> args = new List<EcoExpression>();
					args.Add(ParseExpression(context, 0, 0));
					SkipWhiteSpace();
					while (Peek() == ',') {
						args.Add(ParseExpression(context, 0, 0));
						SkipWhiteSpace();
					}
					if (Peek() != ']') throw new EcoException("Missing ']' for '" + id + "'", Where(), lineNr);
					index++;
					lhs = new EcoVarAccess(context, id, args.ToArray());
				}
				else {
					lhs = new EcoVarAccess(context, id);
				}
			}
			while (Peek() == '.') {
				index++;
				string field = GetIdentifier();
				lhs = new EcoFieldOperator(context, lhs, field);
			}
		}
		else {
			throw new EcoException("syntax error in expression", Where(), startLineNr);
		}
		return lhs;
	}
	
	int GetPrio(char c, out EcoBinaryExpression.BinOperator op) {
		op = EcoBinaryExpression.BinOperator.ADD;
		
		switch (c) {
		case '|' :
			if (Peek(1) == '|') {
				index += 2;
				op = EcoBinaryExpression.BinOperator.OR;
				return 0;
			}
			return -1;
		case '&' :
			if (Peek(1) == '&') {
				index += 2;
				op = EcoBinaryExpression.BinOperator.AND;
				return 0;
			}
			return -1;
		case '<' :
			if (Peek(1) == '=') {
				index += 2;
				op = EcoBinaryExpression.BinOperator.LE;
				return 1;
			}
			else {
				index += 1;
				op = EcoBinaryExpression.BinOperator.LT;
				return 1;
			}
		case '>' :
			if (Peek(1) == '=') {
				index += 2;
				op = EcoBinaryExpression.BinOperator.GE;
				return 1;
			}
			else {
				index += 1;
				op = EcoBinaryExpression.BinOperator.GT;
				return 1;
			}
		case '=' :
			if (Peek(1) == '=') {
				index += 2;
				op = EcoBinaryExpression.BinOperator.EQ;
				return 1;
			}
			return -1;
		case '!' :
			if (Peek(1) == '=') {
				index += 2;
				op = EcoBinaryExpression.BinOperator.NE;
				return 1;
			}
			return -1;
		case '+' :
			index += 1;
			op = EcoBinaryExpression.BinOperator.ADD;
			return 2;
		case '-' :
			index += 1;
			op = EcoBinaryExpression.BinOperator.SUB;
			return 2;
		case '/' :
			index += 1;
			op = EcoBinaryExpression.BinOperator.DIV;
			return 3;
		case '*' :
			index += 1;
			op = EcoBinaryExpression.BinOperator.MUL;
			return 4;
		}
		return -1;
	}
	
	EcoExpression ParseBrackets(EcoContext context, int level) {
		index++;
		EcoExpression expr = ParseExpression(context, level + 1, 0);
		if (Peek() != ')') {
			throw new EcoException("missing ')'", Where(), lineNr);
		}
		index++;
		SkipWhiteSpace();
		return expr;
	}
	
	EcoExpression ParseExpression(EcoContext context, int level, int prio) {
		SkipWhiteSpace();
		EcoExpression lhs = ParseUnaryExpression(context, level);

		while (true) {
			SkipWhiteSpace();
			int saveIndex = index;
			EcoBinaryExpression.BinOperator op;
			char c = Peek();
			int newPrio = GetPrio(c, out op);
			if (newPrio < prio) {
				index = saveIndex;
				return lhs;
			}
			
			EcoExpression rhs = ParseExpression(context, level, newPrio);
			lhs = new EcoBinaryExpression(op, lhs, rhs);
		}
	}
	
	EcoSetValue ParseSetValue(EcoContext context) {
		int startLineNr = lineNr;
		SkipWhiteSpace();
		string varName = GetIdentifier();
		if (varName == "") throw new EcoException("identifier expected", Where(), lineNr);
		EcoExpression[] indices = null;
		string field = null;
		if (Peek() != '.') {
			SkipWhiteSpace();
			if (Peek() == '[') {
				List<EcoExpression> eeList = new List<EcoExpression>();
				index++;
				SkipWhiteSpace();
				eeList.Add(ParseExpression(context));
				SkipWhiteSpace();
				while (Peek() == ',') {
					index++;
					SkipWhiteSpace();
					eeList.Add(ParseExpression(context));
				}
				indices = eeList.ToArray();
				if (Peek() != ']') throw new EcoException("']' missing", Where(), lineNr);
				index++;
			}
		}
		if (Peek() == '.') {
			index++;
			field = GetIdentifier();
			if (field == "") throw new EcoException("field expected", Where(), lineNr);
		}
		SkipWhiteSpace();
		if (Peek() != '=') throw new EcoException("'=' missing", Where(), lineNr);
		index++;
		SkipWhiteSpace();
		EcoExpression expr = ParseExpression(context);
		SkipWhiteSpace();
		if (Peek() != ';') throw new EcoException("';' missing", Where(), lineNr);
		index++;
		EcoSetValue esv = new EcoSetValue(startLineNr, varName, indices, field, expr);
		return esv;
	}
	
	EcoIfThenElse ParseIfThenElse(EcoContext context) {
		int startLineNr = lineNr;
		SkipWhiteSpace();
		EcoExpression expr = ParseExpression(context);
		EcoStatement falseStmt = null;
		SkipWhiteSpace();
		string keyword = GetKeyword();
		if (keyword != "then") {
			throw new EcoException("'then' expected", Where(), lineNr);
		}
		EcoStatement trueStmt = ParseStatement(context);
		SkipWhiteSpace();
		if (PeekKeyword() == "else") {
			GetKeyword();
			falseStmt = ParseStatement(context);
		}
		return new EcoIfThenElse(startLineNr, expr, trueStmt, falseStmt);
	}

	EcoWhile ParseWhile(EcoContext context) {
		int startLineNr = lineNr;
		SkipWhiteSpace();
		EcoExpression expr = ParseExpression(context);
		SkipWhiteSpace();
		string keyword = GetKeyword();
		if (keyword != "do") {
			throw new EcoException("'do' expected", Where(), lineNr);
		}
		EcoStatement stmt = ParseStatement(context);
		return new EcoWhile(startLineNr, expr, stmt);
	}
	
	EcoForLoop ParseForLoop(EcoContext context) {
		int startLineNr = lineNr;
		SkipWhiteSpace();
		bool needClose = false;
		if (Peek() == '(') {
			needClose = true;
			index++;
			SkipWhiteSpace();
		}
		string varName = GetIdentifier();
		if (varName == "") throw new EcoException("expected variable name for loop", Where(), lineNr);
		SkipWhiteSpace();
		if (Peek() != '=') throw new EcoException("expected '='", Where(), lineNr);
		index++;
		SkipWhiteSpace();		
		EcoExpression startExpr = ParseExpression(context);
		SkipWhiteSpace();
		string keyword = GetKeyword();
		if (keyword != "to") {
			throw new EcoException("'to' expected", Where(), lineNr);
		}
		SkipWhiteSpace();
		EcoExpression endExpr = ParseExpression(context);
		SkipWhiteSpace();
		if (needClose && Peek() != ')') throw new EcoException("')' expected", Where(), lineNr);
		if (needClose) index++;
		SkipWhiteSpace();
		keyword = GetKeyword();
		if (keyword != "do") {
			throw new EcoException("'do' expected", Where(), lineNr);
		}
		EcoStatement stmt = ParseStatement(context);
		return new EcoForLoop(startLineNr, varName, startExpr, endExpr, stmt);
	}
	

	EcoForEachLoop ParseForEachLoop(EcoContext context) {
		int startLineNr = lineNr;
		SkipWhiteSpace();
		bool needClose = false;
		if (Peek() == '(') {
			needClose = true;
			index++;
			SkipWhiteSpace();
		}
		string varName = GetIdentifier();
		if (varName == "") throw new EcoException("expected variable name for loop", Where(), lineNr);
		SkipWhiteSpace();
		if (PeekKeyword() != "in") throw new EcoException("expected 'in'", Where(), lineNr);
		GetKeyword();
		SkipWhiteSpace();		
		EcoExpression iterator = ParseExpression(context);
		SkipWhiteSpace();
		if (needClose && Peek() != ')') throw new EcoException("')' expected", Where(), lineNr);
		if (needClose) index++;
		SkipWhiteSpace();
		string keyword = GetKeyword();
		if (keyword != "do") {
			throw new EcoException("'do' expected", Where(), lineNr);
		}
		EcoStatement stmt = ParseStatement(context);
		return new EcoForEachLoop(startLineNr, varName, iterator, stmt);
	}
	
	
	

	EcoCall ParseCall(EcoContext context) {
		int startLineNr = lineNr;
		SkipWhiteSpace();
		EcoExpression expr = ParseExpression(context);
		SkipWhiteSpace();
		if (Peek() != ';') throw new EcoException("missing ';'", Where(), lineNr);
		index++;
		return new EcoCall(startLineNr, expr);
	}
	
	
	void ParseFunctionDef(EcoContext context) {
		SkipWhiteSpace();
		int startLineNr = lineNr;
		string funName = GetIdentifier();
		if (funName == "") throw new EcoException("missing function name", startLineNr);
		SkipWhiteSpace();
		if (Peek() != '(') {
			throw new EcoException("function definition for '" + funName + "' missing '('", Where(), startLineNr);
		}
		index++;
		SkipWhiteSpace();
		List<string> paramNames = new List<string>();
		if (Peek() != ')') {
			string p = GetIdentifier();
			if (p == "") throw new EcoException("missing parameter name", Where(), lineNr);
			paramNames.Add(p);
			SkipWhiteSpace();
			while (Peek() != ')') {
				if (Peek() != ',') throw new EcoException("missing ','", Where(), lineNr);
				index++;
				SkipWhiteSpace();
				p = GetIdentifier();
				if (p == "") throw new EcoException("missing parameter name", Where(), lineNr);
				paramNames.Add(p);
				SkipWhiteSpace();
			}			
		}
		index++;
		SkipWhiteSpace();
		if (Peek() == ';') {
			index++;
			if (!context.functions.ContainsKey(funName)) {
				EcoFunction fn = new EcoFunction(startLineNr, funName, null, paramNames.ToArray());
				context.functions.Add(funName, fn);
			}
			else {
				throw new EcoException("redefinition of function '" + funName + "'", startLineNr);
			}
			return;
		}
		else if (Peek() == '{') {
			index++;
			EcoFunction fn = new EcoFunction(startLineNr, funName, ParseStatements(context).ToArray(), paramNames.ToArray());
			if (!(Peek() == '}')) {
				throw new EcoException("missing '}' for function '" + funName + "'", lineNr);
			}
			index++;
			if (context.functions.ContainsKey(funName)) {
				EcoBaseFunction oldFn = context.functions[funName];
				if (!redefineFunctions && (oldFn.HasDefinition())) {
					throw new EcoException("redefinition of '" + funName + "'", lineNr);
				}
				if (!redefineFunctions && (!(oldFn as EcoFunction).CompareParameters(fn))) {
					throw new EcoException("parameter count of '" + funName + "' differs from earlier declaration", lineNr);
				}
				context.functions.Remove(funName);
			}
			context.functions.Add(funName, fn);
			return;
		}
		throw new EcoException("bad function definition: expecting '{' or ';'", Where(), lineNr);		
	}
	
	EcoBlock ParseBlock(EcoContext context) {
		List<EcoStatement> statements = ParseStatements(context);
		if (Peek() != '}') {
			throw new EcoException("missing '}'", Where(), lineNr);
		}
		index++;
		return new EcoBlock(lineNr, statements.ToArray());
	}
	
	EcoStatement ParseStatement(EcoContext context) {
		SkipWhiteSpace();
		if (IsEOF()) {
			throw new EcoException("unexpected EOF", lineNr);
		}
		if (Peek() == '{') {
			index++;
			return ParseBlock(context);
		}
		else {
			string keyword = PeekKeyword();
			if (keyword == "") throw new EcoException("expected keyword", Where(), lineNr);
			else if (keyword == "set") {
				GetKeyword();
				return ParseSetValue(context);
			}
			else if (keyword == "if") {
				GetKeyword();
				return ParseIfThenElse(context);
			}
			else if (keyword == "call") {
				GetKeyword();
				return ParseCall(context);
			}
			else if (keyword == "while") {
				GetKeyword();
				return ParseWhile(context);
			}
			else if (keyword == "for") {
				GetKeyword();
				return ParseForLoop(context);
			}
			else if (keyword == "foreach") {
				GetKeyword();
				return ParseForEachLoop(context);
			}
			else if (context.functions.ContainsKey(keyword)) {
				return ParseCall(context);
			}
			else {
				throw new EcoException("unexpected keyword '" + keyword + "' (unkown function or variable name?)", Where(), lineNr);
			}
		}
	}
	
	List<EcoStatement> ParseStatements(EcoContext context) {
		List<EcoStatement> statements = new List<EcoStatement>();
		while (true) {
			SkipWhiteSpace();
			if (IsEOF() || (Peek() == '}')) {
				return statements;
			}
			else {
				statements.Add(ParseStatement(context));
			}
		}
	}

	public EcoValue ParseValue(string valStr) {
		parseStr = valStr;
		index = 0;
		lineNr = 1;
		len = valStr.Length;
		SkipWhiteSpace();
		string sv = ReadString();
		if (sv != null) {
			return new EcoStringValue(sv);
		}
		string keyword = GetKeyword();
		if (keyword == "true") return EcoBoolValue.TRUE;
		else if (keyword == "false") return EcoBoolValue.FALSE;
		
		EcoConstant c = ReadNumberOrFloat();
		return c.val;
	}
	
	public EcoExpression ParseExpression(EcoContext context, string script) {
		parseStr = script;
		index = 0;
		lineNr = 1;
		len = script.Length;
		return ParseExpression(context);
	}
	
	public EcoBlock ParseBlock(EcoContext context, string script) {
		parseStr = script;
		index = 0;
		lineNr = 1;
		len = script.Length;
		List<EcoStatement> statements = ParseStatements(context);
		SkipWhiteSpace();
		if (!IsEOF()) {
			throw new EcoException("garbage after rule", Where(), lineNr);
		}
		return new EcoBlock(1, statements.ToArray());
	}

	public void Parse(EcoContext context, string script) {
		Parse(context, script, false);
	}	
	
	public void Parse(EcoContext context, string script, bool redefineFunctions) {
		this.redefineFunctions = redefineFunctions;
		parseStr = script;
		index = 0;
		lineNr = 1;
		len = script.Length;
		
		List<EcoSetValue> setValueStatements = new List<EcoSetValue>();
		while (true) {
			SkipWhiteSpace();
			if (!IsEOF()) {
				string keyword = GetKeyword();
				if (keyword == "") throw new EcoException("expected keyword", Where(), lineNr);
				if (keyword == "set") {
					setValueStatements.Add(ParseSetValue(context));
				}
				else if (keyword == "function") {
					ParseFunctionDef(context);
				}
				else {
					throw new EcoException("unexpected keyword '" + keyword + "'", Where(), lineNr);
				}
			}
			else {
				break;
			}
		}
		
		foreach (EcoSetValue esv in setValueStatements) {
			esv.Execute(context);
		}
	}
}
