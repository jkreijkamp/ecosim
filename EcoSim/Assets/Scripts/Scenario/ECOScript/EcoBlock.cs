using System.Collections.Generic;

public class EcoBlock : EcoStatement
{
	readonly EcoStatement[] statements;
	
	public EcoBlock(int lineNr, EcoStatement[] statements) : base(lineNr) {
		this.statements = statements;
	}
	
	public override void Execute(EcoContext context) {
		context.Push();
		foreach (EcoStatement s in statements) {
			s.Execute(context);
		}
		context.Pop();
	}
}
