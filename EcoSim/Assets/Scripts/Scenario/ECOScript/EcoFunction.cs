using System.Collections.Generic;

public abstract class EcoBaseFunction {
	protected readonly int lineNr;
	protected readonly string name;
	
	public EcoBaseFunction(int lineNr, string name) {
		this.lineNr = lineNr;
		this.name = name;
	}
	
	public abstract bool HasDefinition();
	public abstract int GetParamCount();
	
	public abstract EcoValue Execute(EcoContext context, params EcoValue[] values);
	
}

public class EcoFunction : EcoBaseFunction
{
	readonly EcoStatement[] statements;
	readonly string[] paramNames;	
	
	public EcoFunction(int lineNr, string name, EcoStatement[] statements, string[] paramNames) : base(lineNr, name) {
		this.statements = statements;
		this.paramNames = paramNames;
	}
	
	public override bool HasDefinition() { return (statements != null); }
	public override int GetParamCount() { return paramNames.Length; }
	
	public bool CompareParameters(EcoFunction other) {
		if (paramNames.Length != other.paramNames.Length) return false;
		for (int i = 0; i < paramNames.Length; i++) {
			if (paramNames[i] != other.paramNames[i]) return false;
		}
		return true;
	}
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (statements == null) {
			throw new EcoException("function '" + name + "' is called but has only been declared, not defined");
		}
		context.Push();
		context.CreateLocalVar("return", EcoBoolValue.FALSE);
		for (int i = 0; i < values.Length; i++) {
			EcoValue val = values[i];
			context.Set(paramNames[i], val);
		}
		foreach (EcoStatement s in statements) {
			s.Execute(context);
		}
		EcoValue returnVal = context.Get("return");
		context.Pop();
		
		// we default to false as return value
		if (returnVal == null) return EcoBoolValue.FALSE;
		return returnVal;
	}
}

