using System.Collections.Generic;

public class EcoFnOnderzoek : EcoBaseFunction {
	public EcoFnOnderzoek() : base(-1, "onderzoek") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (!(values[0] is EcoNumberValue)) throw new EcoException("special function '" + name + "' argument 1 needs to be a number");
		if (!(values[1] is EcoStringValue)) throw new EcoException("special function '" + name + "' argument 2 needs to be a string");
		
		int targetId = (int) ((values[0] as EcoNumberValue).v);
		string researchStr = (values[1] as EcoStringValue).v;
		EResearchTypes r = EnumExtensions.GetResearchType(researchStr);
		if (r == EResearchTypes.UNDEFINED) throw new EcoException("onderzoek type '" + researchStr + "' unknown");
		
		JScenario scenario = context.scenario;
		Extra extra = new Extra();
		extra.researchType = r;
		
		
		if ((int) r < 0x20) {
			JScenarioProgress progress = scenario.progress;
			JResearchPoint[] points = progress.GetResearchPointsArray();
			JTerrainData data = scenario.data;
			foreach (JResearchPoint point in points) {
				bool isRightResearch = false;
				foreach (JResearchPoint.Measurement measure in point.measurements) {
					if (measure.researchType == r) {
						isRightResearch = true;
						break;
					}
				}
				if (isRightResearch) {
					if (data.CheckTargetMapAt(targetId, point.x, point.y)) {
						extra.total++;
					}
				}
			}
		}
		else {
			scenario.data.ProcessTargetAreaResearch(targetId, r, DJustCount, extra);
		}
		
		
//		EcoStructValue s = new EcoStructValue(
//			"totaal", new EcoNumberValue(extra.total)
//			);
		return new EcoNumberValue(extra.total);
	}
	
	static void DJustCount(JCellData cell, int x, int y, System.Object extra) {
		Extra e = extra as Extra;
		e.total++;
	}

	
	private class Extra {
		public EResearchTypes researchType;
		public int total;
	}
}