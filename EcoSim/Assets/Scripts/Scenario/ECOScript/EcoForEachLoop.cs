using System.Collections.Generic;

public class EcoForEachLoop : EcoStatement
{
	readonly string varName;
	readonly EcoExpression iterator;
	readonly EcoStatement stmt;
	
	public EcoForEachLoop(int lineNr, string varName, EcoExpression iterator, EcoStatement stmt) : base(lineNr) {
		this.varName = varName;
		this.iterator = iterator;
		this.stmt = stmt;
	}
	
	public override void Execute(EcoContext context) {
		EcoValue v1 = iterator.Execute(context);
		if (!(v1 is EcoFieldReference)) throw new EcoException("can not iterate over value in foreach");
		EcoFieldReference vr = v1 as EcoFieldReference;
		context.Push();
		context.CreateLocalVar(varName, EcoBoolValue.FALSE);
		foreach (EcoVar v in vr) {
			context.Set(varName, v);
			stmt.Execute(context);
		}
		context.Pop();
	}
}