using System.Collections.Generic;

public abstract class EcoStatement
{
	public readonly int lineNr;
	
	public EcoStatement(int lineNr) {
		this.lineNr = lineNr;
	}
	
	public abstract void Execute(EcoContext context);
}