using UnityEngine;
using System.Collections.Generic;
using System.Xml;

public class EcoContext {
	public const string XML_ELEMENT = "variable";
	
	public EcoContext(JScenario scenario) {
		this.scenario = scenario;
		Reset();
	}
	
	public void Reset() {
		variables.Clear();
		Dictionary<string, EcoVar> globalVars = new Dictionary<string, EcoVar>();
		variables.Add(globalVars);
		functions.Clear();
		functions.Add("debug", new EcoFnDebug());
		functions.Add("artikel", new EcoFnArticle());
		functions.Add("hint", new EcoFnHint());
		functions.Add("tonumber", new EcoToNumber());
		functions.Add("tofloat", new EcoToFloat());
		functions.Add("tostring", new EcoToString());

		functions.Add("min", new EcoFnMin());
		functions.Add("max", new EcoFnMax());
		functions.Add("clamp", new EcoFnClamp());
		functions.Add("log", new EcoFnLog());
		functions.Add("pow", new EcoFnPow());
		functions.Add("sin", new EcoFnSin());
		functions.Add("cos", new EcoFnCos());
		functions.Add("tan", new EcoFnTan());
		functions.Add("sqrt", new EcoFnSqrt());
		functions.Add("round", new EcoFnRound());
		functions.Add("rndrange", new EcoFnRndRange());
		functions.Add("rndodds", new EcoFnRndOdds());
		
		functions.Add("verwijderregel", new EcoFnVerwijderRegel());
		
		functions.Add("toonencyclopedie", new EcoFnToonEncyclopedie());
		functions.Add("encyclopediegeraadpleegd", new EcoFnEncyclopedieGeraadpleegd());
		
		functions.Add("soorten", new EcoFnSoorten());
		functions.Add("parameters", new EcoFnParameters());
		functions.Add("onderzoek", new EcoFnOnderzoek());
		functions.Add("maatregelen", new EcoFnMaatregelen());

		functions.Add("doelgebied", new EcoFnDoelGebied());
		functions.Add("successiegebied", new EcoFnSuccessieGebied());
		
		functions.Add("tegel", new EcoFnTegel());
		
		functions.Add("zetmaatregel", new EcoFnZetMaatregel());
		functions.Add("leesmaatregel", new EcoFnLeesMaatregel());
		functions.Add("zetsoort", new EcoFnZetSoort());
		functions.Add("leessoort", new EcoFnLeesSoort());
		functions.Add("zetvegetatie", new EcoFnZetVegetatie());
		
		functions.Add("leesparameter", new EcoFnLeesParameter());
		functions.Add("zetparameter", new EcoFnZetParameter());
		
		functions.Add("maaknest", new EcoFnMaakNest());
		functions.Add("nesten", new EcoFnNesten());

		functions.Add("veldveranderd", new EcoFnVeldVeranderd());
		
		functions.Add("inventarisatieproduct", new EcoFnInventarisatieProduct());
		
		functions.Add("peilbuizen", new EcoFnPeilbuizen());
		
		globalVars.Add("voortgang", new EcoVoortgangVar(this, "voortgang"));
		globalVars.Add("inventarisaties", new EcoInventarisationsVar(this, "inventarisaties"));
		SetGlobal("laatstemaatregel", "");
		SetGlobal("laatstemaatregelgroep", "");
		SetGlobal("laatsteonderzoek", "");
		SetGlobal("laatsteonderzoeksgroep", "");
		SetGlobal("laatstemaatregelaantal", 0);
		SetGlobal("laatsteonderzoekaantal", 0);
	}
	
	public JScenario scenario;
	
	public Dictionary<string, EcoBaseFunction> functions = new Dictionary<string, EcoBaseFunction>();
	public List<Dictionary<string, EcoVar>> variables = new List<Dictionary<string, EcoVar>>();
	
	public void Push() {
		variables.Add(new Dictionary<string, EcoVar>());
	}
	
	public void Pop() {
		variables.RemoveAt(variables.Count - 1);
	}
	
	public void PopAll() {
		while (variables.Count > 1) Pop();
	}
	
	public List<Dictionary<string, EcoVar>> PopAllReturnStack() {
		List<Dictionary<string, EcoVar>> result = variables;
		variables = new List<Dictionary<string, EcoVar>>();
		variables.Add(result[0]);
		result.RemoveAt(0);
		return result;
	}
	
	public void PushStackBack(List<Dictionary<string, EcoVar>> stack) {
		foreach (Dictionary<string, EcoVar> dict in stack) {
			variables.Add(dict);
		}
	}
	
	public EcoVar Find(string name) {
		EcoVar result = null;
		for (int i = variables.Count - 1; i >= 0; i--) {
			if (variables[i].TryGetValue(name, out result)) return result;
		}
		return null;
	}
	
	
	public void CreateLocalVar(string name, EcoValue val) {
		EcoVar v = new EcoNormalVar(name);
		variables[variables.Count - 1].Add(name, v);
		v.SetValue(null, null, val);
	}

	public void SetGlobal(string name, string val) {
		SetGlobal(name, new EcoStringValue(val));
	}

	public void SetGlobal(string name, long val) {
		SetGlobal(name, new EcoNumberValue(val));
	}

	public void SetGlobal(string name, double val) {
		SetGlobal(name, new EcoFloatValue(val));
	}

	public void SetGlobal(string name, bool val) {
		SetGlobal(name, EcoBoolValue.Cast(val));
	}
	
	
	public void Set(string name, string val) {
		Set(name, new EcoStringValue(val));
	}

	public void Set(string name, long val) {
		Set(name, new EcoNumberValue(val));
	}

	public void Set(string name, double val) {
		Set(name, new EcoFloatValue(val));
	}

	public void Set(string name, bool val) {
		Set(name, EcoBoolValue.Cast(val));
	}

	public void SetGlobal(string name, EcoValue val) {
		EcoVar v = Find(name);
		if (v == null) {
			v = new EcoNormalVar(name);
			variables[0].Add(name, v);
		}
		v.SetValue(null, null, val);
	}
	
	public void SetSimpleType(string name, EcoValue val) {
		EcoVar v = Find(name);
		if (v == null) {
			v = new EcoNormalVar(name);
			variables[variables.Count - 1].Add(name, v);
		}
		v.SetValue(null, null, val);
	}

	public void SetReferenceType(string name, EcoVar val) {
		EcoVar rv = new EcoReferenceVar(name, val);
		EcoVar v = Find(name);
		if (v == null) {
			variables[variables.Count - 1].Add(name, rv);
		}
		else {
			for (int i = variables.Count - 1; i >= 0; i--) {
				if (variables[i].ContainsKey(name)) {
					variables[i][name] = rv;
					return;
				}
			}
		}
	}

	public void Set(string name, EcoValue val) {
		if (!(val is EcoVar)) {
			SetSimpleType(name, val);
			return;
		}
		if (!(val is EcoNormalVar)) {
			// need a reference...
			SetReferenceType(name, (EcoVar) val);
			return;
		}
		EcoVar s = (EcoVar) val;
		EcoVar v = Find(name);
		if (v == null) {
			variables[variables.Count - 1].Add(name, s);
		}
		else {
			for (int i = variables.Count - 1; i >= 0; i--) {
				if (variables[i].ContainsKey(name)) {
					variables[i][name] = s;
				}
			}
		}
	}
	
	public void Set(string name, int[] indices, string field, EcoValue val) {
		if ((indices == null) && (field == null)) {
			Set(name, val);
			return;
		}
		EcoVar v = Find(name);
		if (v == null) {
			v = new EcoNormalVar(name);
			variables[variables.Count - 1].Add(name, v);
		}
		v.SetValue(indices, field, val);
	}
	
	public EcoValue Get(string name) {
		EcoVar v = Find(name);
		if (v == null) {
			return null;
		}
		return v.GetValue();
	}

	public EcoValue Get(string name, int[] indices, string field) {
		EcoVar v = Find(name);
		if (v == null) {
			return null;
		}
		return v.GetValue(indices, field);
	}
	
	public void SaveVars(XmlTextWriter writer) {
		PopAll();
		foreach (KeyValuePair<string, EcoVar> kvp in variables[0]) {
			string repr = kvp.Value.GetValue().StringRepresentation();
			if (repr != null) {
				writer.WriteStartElement(XML_ELEMENT);
				writer.WriteAttributeString("name", kvp.Key);
				writer.WriteAttributeString("value", repr);
				writer.WriteEndElement();
			}
		}
	}
	
	public void LoadVar(XmlTextReader reader) {
		string name = reader.GetAttribute("name");
		string val = reader.GetAttribute("value");
		EcoScript es = new EcoScript();
		Set(name, es.ParseValue(val));
		JIOUtils.ReadUntilEndElement(reader, XML_ELEMENT);
	}
	
	public System.Object Call(string fnName, params System.Object[] p) {
		int len = (p != null)?(p.Length):0;
		if (!functions.ContainsKey(fnName)) {
			throw new EcoException("Function '" + fnName + "' does not exist.");
		}
		EcoBaseFunction fn = functions[fnName];
		if (fn.GetParamCount() != len) {
			throw new EcoException("Function '" + fnName + "' called with " + len + " parameters, expected " + fn.GetParamCount());
		}
		EcoValue[] values = new EcoValue[len];
		for (int i = 0; i < len; i++) {
			System.Object arg = p[i];
			if (arg is EcoVar) values[i] = new EcoFieldReference((EcoVar) arg);
			else if (arg is EcoValue) values[i] = (EcoValue) arg;
			else if (arg is string) values[i] = new EcoStringValue((string) arg);
			else if (arg is int) values[i] = new EcoNumberValue((int) arg);
			else if (arg is long) values[i] = new EcoNumberValue((long) arg);
			else if (arg is float) values[i] = new EcoFloatValue((float) arg);
			else if (arg is double) values[i] = new EcoFloatValue((double) arg);
			else if (arg is bool) values[i] = EcoBoolValue.Cast((bool) arg);
			else throw new EcoException(fnName + " argument " + (i + 1) + " of unsupported type '" + (arg.GetType().ToString()) + "'");
		}
		
		EcoValue result = fn.Execute(this, values);
		if (result is EcoStringValue) return ((EcoStringValue) result).v;
		if (result is EcoNumberValue) return ((EcoNumberValue) result).v;
		if (result is EcoFloatValue) return ((EcoFloatValue) result).v;
		if (result is EcoBoolValue) return ((EcoBoolValue) result).v;
		throw new EcoException("Return value is of unsupported type '" + result.GetType().ToString() + "'");
	}
}
