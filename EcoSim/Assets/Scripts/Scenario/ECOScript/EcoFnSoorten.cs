using System.Collections.Generic;

public class EcoFnSoorten : EcoBaseFunction {
	public EcoFnSoorten() : base(-1, "soorten") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (!(values[0] is EcoNumberValue)) throw new EcoException("special function '" + name + "' argument 1 needs to be a number");
		if (!(values[1] is EcoNumberValue)) throw new EcoException("special function '" + name + "' argument 2 needs to be a number");
		
		int targetId = (int) ((values[0] as EcoNumberValue).v);
		int soortId = (int) ((values[1] as EcoNumberValue).v);
		
		JScenario scenario = context.scenario;
		Extra extra = new Extra();
		extra.species = soortId;
		scenario.data.ProcessTargetArea(targetId, DSpecies, extra);
		double avg = (extra.total == 0)?0.0:(((double) extra.sum) / extra.total);		
		EcoStructValue s = new EcoStructValue(
			"min", new EcoNumberValue(extra.min),
			"max", new EcoNumberValue(extra.max),
			"aantalmin", new EcoNumberValue(extra.minCount),
			"aantalmax", new EcoNumberValue(extra.maxCount),
			"totaal", new EcoNumberValue(extra.total),
			"som", new EcoNumberValue(extra.sum),
			"nietnul", new EcoNumberValue(extra.notZero),
			"gemiddelde", new EcoFloatValue(avg)
			);
		return s;
	}
	
	static void DSpecies(JCellData cell, int x, int y, System.Object extra) {
		Extra e = extra as Extra;
		e.total++;
		int val = cell.GetSpeciesValue(x, y, e.species);
		// if (val > 0) Debug.Log("val = " + val + " x = " + x + " y = " + y);
		if (val > 0) e.notZero++;
		e.sum += val;
		if (val < e.min) {
			e.min = val;
			e.minCount = 1;
		}
		else if (val == e.min) {
			e.minCount++;
		}
		if (val > e.max) {
			e.max = val;
			e.maxCount = 1;
		}
		else if (val == e.max) {
			e.maxCount++;
		}
	}
	
	private class Extra {
		public int species;
		public int min;
		public int max;
		public int minCount;
		public int maxCount;
		public int sum;
		public int total;
		public int notZero;
	}
}

