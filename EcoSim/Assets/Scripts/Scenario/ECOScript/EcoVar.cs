using System;
using System.Collections.Generic;


public abstract class EcoVar : EcoFieldValue {
	public readonly string name;
	
	public EcoVar(string name) {
		this.name = name;
	}
	
	public abstract void SetValue(int[] indices, string field, EcoValue v);
	
	public override object GetLLValue() { return GetValue(); }	

	public override object GetLLValue(string field) { return GetValue(null, field); }	

	public abstract EcoValue GetValue();
	
	public override EcoValue GetValue(string field) { return GetValue(null, field); }
	
	public abstract EcoValue GetValue(int[] indices, string field);
	
//	public abstract bool HasField(string field);
	
	public virtual IEnumerator<EcoVar> GetEnumerator() {
		throw new EcoException("Var '" + name +"' is not enumerable");
	}
}

public class EcoReferenceVar : EcoVar {
	public EcoReferenceVar(string name, EcoVar val) : base(name) {
		this.val = val;
	}
	
	public EcoVar val;

	public override EcoValue GetValue() {
		return val;
	}
	
	public override EcoValue GetValue(int[] indices, string field) {
		return val.GetValue(indices, field);
	}
	
	public override void SetValue(int[] indices, string field, EcoValue v) {
		if ((indices == null)  && (field == null)) {
			if (v is EcoVar) {
				val = (EcoVar) v;
			}
			else {
				val.SetValue(indices, field, v);
			}
		}
		else {
			val.SetValue(indices, field, v);
		}
	}	
	
	public override bool HasField(string field) {
		if (val is EcoFieldValue) {
			return ((EcoFieldValue) val).HasField(field);
		}
		throw new EcoException("field access not supported for reference var " + name);
	}
}

public class EcoNormalVar : EcoVar {
	public EcoNormalVar(string name) : base(name) {
	}
	
	EcoValue v;
	
	public override void SetValue(int[] indices, string field, EcoValue v) {
		if ((v is EcoFieldReference) && ((indices != null) || (field != null))) {
			throw new EcoException("Variable '" + name + "' is a reference, which currently don't support modifying");
		}
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field != null) {
			throw new EcoException("Variable '" + name + "' is not a struct, can't access field '" + field + "' " + this.GetType().Name);
		}
		this.v = v;
	}
	
	public override EcoValue GetValue() {
		return v;
	}
	
	public override EcoValue GetValue(int[] indices, string field) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (v is EcoFieldReference) {
			return ((EcoFieldReference) v).GetValue(field);
		}
		if (field != null) throw new EcoException("Variable '" + name + "' is not a struct, can't access field '" + field + "'");
		return v;
	}
	
	public override bool HasField(string field) {
		if (v is EcoFieldReference) {
			return ((EcoFieldReference) v).HasField(field);
		}
		if (!(v is EcoFieldValue)) throw new EcoException("trying to access field '" + field + "' on non-struct variable '"+ name + "'");
		return (v as EcoFieldValue).HasField(field);
	}
}

public class EcoTileVar : EcoVar {
	public EcoTileVar(EcoContext context, int x, int y, string name) : base(name) {
		this.context = context;
		this.x = x;
		this.y = y;
	}
	
	readonly EcoContext context;
	
	public int x;
	public int y;
	public int successionId = -1;
	public int vegetationId = -1;
	public int tileId = -1;

	public override void SetValue(int[] indices, string field, EcoValue v) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field == null) throw new EcoException("Variable '" + name + "' is a struct, and needs an access field");
		switch (field) {
		case "x" : 
			if (!(v is EcoNumberValue)) throw new EcoException(name + "." + field + " must be a number value.");
			x = (int) (v as EcoNumberValue).v;
			successionId = -1;
			break;
		case "y" : 
			if (!(v is EcoNumberValue)) throw new EcoException(name + "." + field + " must be a number value.");
			y = (int) (v as EcoNumberValue).v;
			successionId = -1;
			break;
		case "successie" :
		case "vegetatie" :
		case "tegel" :
			throw new EcoException(name + "." + field + " is readonly.");
		default :
			throw new EcoException(name + " has no field '" + field + "'");
		}
	}


	public override EcoValue GetValue() {
		return new EcoFieldReference(this);
	}
	
	public void GetSuccessionInfo() {
		JXVegetation veg = context.scenario.data.GetVegetation((int) x, (int) y, out tileId);
		successionId = veg.succession.index;
		vegetationId = veg.index;
	}
	
	public override EcoValue GetValue(int[] indices, string field) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field == null) throw new EcoException("Variable '" + name + "' is a struct, and needs an access field");
		switch (field) {
		case "x" :
			return new EcoNumberValue(x);
		case "y" :
			return new EcoNumberValue(y);
		case "successie" :
			if (successionId < 0) GetSuccessionInfo();
			return new EcoNumberValue(successionId);
		case "vegetatie" :
			if (successionId < 0) GetSuccessionInfo();
			return new EcoNumberValue(vegetationId);
		case "tegel" :
			if (successionId < 0) GetSuccessionInfo();
			return new EcoNumberValue(tileId);
		default :
			throw new EcoException(name + " has no field '" + field + "'");
		}
	}
	
	public override bool HasField(string field) {
		switch (field) {
		case "x" :
		case "y" :
		case "successie" :
		case "vegetatie" :
		case "tegel" :
			return true;
		default :
			return false;
		}
	}
	
}

public class EcoResearchPointVar : EcoVar {
	public EcoResearchPointVar(EcoContext context, string name, int year) : base(name) {
		this.context = context;
		this.year = year;
	}
	
	readonly EcoContext context;
	readonly int year;

	public override void SetValue(int[] indices, string field, EcoValue v) {
		throw new EcoException("Variable '" + name + "' can not be assigned to");
	}

	public override bool HasField(string field) {
		return false;
	}
	
	public override EcoValue GetValue() {
		return new EcoFieldReference(this);
	}

	public override EcoValue GetValue(int[] indices, string field) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field != null) throw new EcoException("Variable '" + name + "' is not a struct");
		return new EcoFieldReference(this);
	}
	
	public override IEnumerator<EcoVar> GetEnumerator() {
		foreach (JResearchPoint pnt in context.scenario.progress.GetResearchPointsArray()) {
			foreach (JResearchPoint.Measurement m in pnt.measurements) {
				if ((year == 0) || (m.year == year)) {
					EcoStructValue s = new EcoStructValue("jaar", m.year, "onderzoek", m.researchType.ToString().ToLower(), "tegel", new EcoTileVar(context, pnt.x, pnt.y, "_intern"));
					EcoNormalVar vr = new EcoNormalVar("_internal");
					vr.SetValue(null, null, s);
					yield return vr;
				}
			}
		}
	}	
}

public class EcoNestVar : EcoNormalVar {
	public EcoNestVar(EcoContext context, JAnimalSpecies species, JAnimalSpecies.Nest nest, string name) : base(name) {
		this.context = context;
		this.species = species;
		this.nest = nest;
	}
	
	readonly EcoContext context;
	public readonly JAnimalSpecies species;
	public readonly JAnimalSpecies.Nest nest;

	public override void SetValue(int[] indices, string field, EcoValue v) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field == null) throw new EcoException("Variable '" + name + "' is a struct, and needs an access field");		
		switch (field) {
		case "mannetjes" : 
			if (v is EcoNumberValue) {
				nest.males = (int) ((EcoNumberValue) v).v;
			}
			else {
				throw new EcoException(name + "." + field + " is a number, tried writing " + v.GetType().ToString());
			}
			break;
		case "vrouwtjes" : 
			if (v is EcoNumberValue) {
				nest.females = (int) ((EcoNumberValue) v).v;
			}
			else {
				throw new EcoException(name + "." + field + " is a number, tried writing " + v.GetType().ToString());
			}
			break;
		default :
			throw new EcoException(name + "." + field + " is readonly.");
		}
	}
	
	public override EcoValue GetValue() {
		return new EcoFieldReference(this);
	}
	
	public override EcoValue GetValue(int[] indices, string field) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field == null) throw new EcoException("Variable '" + name + "' is a struct, and needs an access field");		
		switch (field) {
		case "tegel" : return new EcoTileVar(context, nest.position.x, nest.position.y, "_internal");
		case "soortnr	" : return new EcoNumberValue(species.index);
		case "mannetjes" : return new EcoNumberValue(nest.males);
		case "vrouwtjes" : return new EcoNumberValue(nest.females);
		default :
			throw new EcoException(name + " has no field '" + field + "'");
		}
	}

	public override bool HasField(string field) {
		switch (field) {
		case "tegel" :
		case "soortnr" :
		case "mannetjes" :
		case "vrouwtjes" :
			return true;
		default :
			return false;
		}
	}
}

public class EcoInventarisationVar : EcoVar {
	public EcoInventarisationVar(EcoContext context, JInventarisationResultMap map, string name) : base(name) {
		this.context = context;
		this.map = map;
	}
	
	readonly EcoContext context;
	public readonly JInventarisationResultMap map;

	public override void SetValue(int[] indices, string field, EcoValue v) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field == null) throw new EcoException("Variable '" + name + "' is a struct, and needs an access field");
		throw new EcoException(name + " is readonly.");
	}
	
	public override EcoValue GetValue() {
		return this;
	}
	
	public override EcoValue GetValue(int[] indices, string field) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field == null) throw new EcoException("Variable '" + name + "' is a struct, and needs an access field");
		if (map.researchType >= EResearchTypes.DierInventarisatie1) {
			switch (field) {
			case "inventarisatie" : return new EcoStringValue(map.researchType.ToString().ToLower());
			case "soortnr" : return new EcoNumberValue(((int) map.researchType & 0x0f) - 9);
			case "jaar" : return new EcoNumberValue(map.year);
			case "geen" : return new EcoNumberValue(map.Count(1));
			case "spoor" : return new EcoNumberValue(map.Count(2) + map.Count(3) + map.Count(4) + map.Count(5) + map.Count(7));
			case "cadaver" : return new EcoNumberValue(map.Count(13));
			case "nest" : return new EcoNumberValue(map.Count(14));
			case "voeding" : return new EcoNumberValue(map.Count(12));
			case "tegels" : return new EcoNumberValue(context.scenario.data.width * context.scenario.data.height - map.Count(0));
			default :
				throw new EcoException(name + " has no field '" + field + "'");
			}
			
		}
		else {
			switch (field) {
			case "inventarisatie" : return new EcoStringValue(map.researchType.ToString().ToLower());
			case "soortnr" : return new EcoNumberValue(((int) map.researchType & 0x0f) - 1);
			case "jaar" : return new EcoNumberValue(map.year);
			case "geen" : return new EcoNumberValue(map.Count(1));
			case "enkele" : return new EcoNumberValue(map.Count(2));
			case "matig" : return new EcoNumberValue(map.Count(3));
			case "veel" : return new EcoNumberValue(map.Count(4));
			case "tegels" : return new EcoNumberValue(context.scenario.data.width * context.scenario.data.height - map.Count(0));
			default :
				throw new EcoException(name + " has no field '" + field + "'");
			}
		}
	}

	
	public override bool HasField(string field) {
		if (map.researchType >= EResearchTypes.DierInventarisatie1) {
			switch (field) {
			case "inventarisatie" :
			case "soortnr" :
			case "jaar" :
			case "geen" :
			case "spoor" :
			case "cadaver" :
			case "nest" :
			case "voeding" :
			case "tegels" :
				return true;
			default :
				return false;
			}
		}
		else {
			switch (field) {
			case "inventarisatie" :
			case "soortnr" :
			case "jaar" :
			case "geen" :
			case "enkele" :
			case "matig" :
			case "veel" :
			case "tegels" :
				return true;
			default :
				return false;
			}
		}
	}
	
}

public class EcoInventarisationsVar : EcoVar {
	
	public EcoInventarisationsVar(EcoContext context, string name) : base(name) {
		this.context = context;
	}
	
	readonly EcoContext context;

	public override void SetValue(int[] indices, string field, EcoValue v) {
		throw new EcoException("Variable '" + name + "' can not be assigned to");
	}

	public override bool HasField(string field) {
		return false;
	}
	
	public override EcoValue GetValue() {
		return new EcoFieldReference(this);
	}

	public override EcoValue GetValue(int[] indices, string field) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field != null) throw new EcoException("Variable '" + name + "' is not a struct");
		return new EcoFieldReference(this);
	}
	
	public override IEnumerator<EcoVar> GetEnumerator() {
		foreach (JInventarisationResultMap map in context.scenario.progress.GetInventarisationResultMapArray()) {
			yield return new EcoInventarisationVar(context, map, "_internal");
		}
	}	
}

public class EcoNestsVar : EcoVar {
	
	public EcoNestsVar(EcoContext context, JAnimalSpecies species, string name) : base(name) {
		this.context = context;
		this.species = species;
	}
	
	readonly EcoContext context;
	readonly JAnimalSpecies species;

	public override void SetValue(int[] indices, string field, EcoValue v) {
		throw new EcoException("Variable '" + name + "' can not be assigned to");
	}

	public override bool HasField(string field) {
		return false;
	}
	
	public override EcoValue GetValue() {
		return new EcoFieldReference(this);
	}

	public override EcoValue GetValue(int[] indices, string field) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field != null) throw new EcoException("Variable '" + name + "' is not a struct");
		return new EcoFieldReference(this);
	}
	
	public override IEnumerator<EcoVar> GetEnumerator() {
		foreach (JAnimalSpecies.Nest nest in species.nests) {
			yield return new EcoNestVar(context, species, nest, "_internal");
		}
	}	
}


public class EcoTargetVar : EcoVar {
	
	public EcoTargetVar(EcoContext context, int targetId, string name) : base(name) {
		this.context = context;
		this.targetId = targetId;
	}
	
	readonly EcoContext context;
	readonly int targetId;

	public override void SetValue(int[] indices, string field, EcoValue v) {
		throw new EcoException("Variable '" + name + "' can not be assigned to");
	}

	public override bool HasField(string field) {
		return false;
	}
	
	public override EcoValue GetValue() {
		return new EcoFieldReference(this);
	}

	public override EcoValue GetValue(int[] indices, string field) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field != null) throw new EcoException("Variable '" + name + "' is not a struct");
		return new EcoFieldReference(this);
	}
	
	public override IEnumerator<EcoVar> GetEnumerator() {
		foreach (JCellData cell in context.scenario.data.cells) {
			byte[] targetMap = cell.GetTargetMap(targetId);
			if (targetMap != null) {
				int count = 0;
				int p = 0;
				int pp = 0x80; // bit mask (start with 0x80 instead of 0x01 to force reading in new byte)
				int val = 0; // current byte of target mask
				for (int y = 0; y < JTerrainData.CELL_SIZE; y++) {
					for (int x = 0; x < JTerrainData.CELL_SIZE; x++) {
						if (pp == 0x80) {
							// we need to read in new byte from target bitmap
							val = targetMap[p++];
							// and set mask to first bit
							pp = 0x01;
						}
						else {
							// shift bit mask to look at next bit
							pp = pp << 1;
						}
						if ((val & pp) != 0) {
							yield return new EcoTileVar(context, x + cell.xOffset, y + cell.yOffset, "_internal");
							count++;
						}
					}
				}
			}
		}		
	}	
}

public class EcoVoortgangVar : EcoVar {
	public EcoVoortgangVar(EcoContext context, string name) : base(name) {
		scenario = context.scenario;
	}
		
	readonly JScenario scenario;

	public override void SetValue(int[] indices, string field, EcoValue v) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field == null) throw new EcoException("Variable '" + name + "' is a struct, and needs an access field");
		switch (field) {
		case "jaar" :
			if (!(v is EcoNumberValue)) throw new EcoException(name + "." + field + " must be a number value.");
			scenario.progress.year = (int) ((v as EcoNumberValue).v);
			break;
		case "budget" :
			if (!(v is EcoNumberValue)) throw new EcoException(name + "." + field + " must be a number value.");
			scenario.progress.budget = ((v as EcoNumberValue).v);
			break;
		case "voornaam" :
			if (!(v is EcoStringValue)) throw new EcoException(name + "." + field + " must be a string value.");
			scenario.progress.playerFirstName = ((v as EcoStringValue).v);
			break;
		case "achternaam" :
			if (!(v is EcoStringValue)) throw new EcoException(name + "." + field + " must be a string value.");
			scenario.progress.playerFamilyName = ((v as EcoStringValue).v);
			break;
		case "maatregelen" :
			if (!(v is EcoBoolValue)) throw new EcoException(name + "." + field + " must be a bool value.");
			scenario.progress.allowMeasures = (v as EcoBoolValue).v;
			break;
		case "onderzoek" :
			if (!(v is EcoBoolValue)) throw new EcoException(name + "." + field + " must be a bool value.");			
			scenario.progress.allowResearch = (v as EcoBoolValue).v;
			break;
		default :
			throw new EcoException(name + " has no field '" + field + "'");
		}
	}

	public override EcoValue GetValue() {
		return new EcoFieldReference(this);
	}
	
	public override EcoValue GetValue(int[] indices, string field) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field == null) throw new EcoException("Variable '" + name + "' is a struct, and needs an access field");
		switch (field) {
		case "jaar" :
			return new EcoNumberValue(scenario.progress.year);
		case "budget" :
			return new EcoNumberValue(scenario.progress.budget);
		case "voornaam" :
			return new EcoStringValue(scenario.progress.playerFirstName);
		case "achternaam" :
			return new EcoStringValue(scenario.progress.playerFamilyName);
		case "onderzoek" :
			return EcoBoolValue.Cast(scenario.progress.allowResearch);
		case "maatregelen" :
			return EcoBoolValue.Cast(scenario.progress.allowMeasures);
		default :
			throw new EcoException(name + " has no field '" + field + "'");
		}
	}
	
	public override bool HasField(string field) {
		switch (field) {
		case "jaar" :
		case "budget" :
		case "voornaam" :
		case "achternaam" :
		case "onderzoek" :
		case "maatregelen" :
			return true;
		default :
			return false;
		}
	}
}

public class EcoStructVar : EcoVar {
	public EcoStructVar(EcoContext context, string name, params EcoVar[] parList) : base(name) {
		vars = new Dictionary<string, EcoVar>();
		foreach (EcoVar v in parList) {
			vars.Add(v.name, v);
		}
	}
	
	private Dictionary<string, EcoVar> vars;
	
	public override void SetValue(int[] indices, string field, EcoValue v) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field == null) throw new EcoException("Variable '" + name + "' is a struct, and needs an access field");
		if (vars.ContainsKey(field)) {
			vars[field].SetValue(null, null, v);
		}
		else {
			throw new EcoException(name + " has no field '" + field + "'");
		}
	}

	public override EcoValue GetValue() {
		return new EcoFieldReference(this);
	}
	
	public override EcoValue GetValue(int[] indices, string field) {
		if (indices != null) throw new EcoException("Variable '" + name + "' not an array");
		if (field == null) throw new EcoException("Variable '" + name + "' is a struct, and needs an access field");
		if (vars.ContainsKey(field)) {
			return vars[field].GetValue();
		}
		else {
			throw new EcoException(name + " has no field '" + field + "'");
		}
	}
	
	public override bool HasField(string field) {
		return vars.ContainsKey(field);
	}
}