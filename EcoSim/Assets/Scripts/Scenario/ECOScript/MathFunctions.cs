using UnityEngine;
using System.Collections;
using System;

public abstract class EcoMathFunction : EcoBaseFunction {
	public EcoMathFunction(int lineNr, string name) : base(lineNr, name) {
	}
	
	public override bool HasDefinition() { return true; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (values.Length == 1) {
			if (values[0] is EcoNumberValue) return Calc((EcoNumberValue) values[0]);
			else if (values[0] is EcoFloatValue) return Calc((EcoFloatValue) values[0]);
			throw new EcoException(name + ": " + values[0].GetType().ToString() + " not supported");
		}
		else if (values.Length == 2) {
			if (values[0] is EcoNumberValue) {
				if (values[1] is EcoNumberValue) return Calc((EcoNumberValue) values[0], (EcoNumberValue) values[1]);
				else if (values[1] is EcoFloatValue) return Calc((EcoNumberValue) values[0], (EcoFloatValue) values[1]);
			}
			else if (values[0] is EcoFloatValue) {
				if (values[1] is EcoNumberValue) return Calc((EcoFloatValue) values[0], (EcoNumberValue) values[1]);
				else if (values[1] is EcoFloatValue) return Calc((EcoFloatValue) values[0], (EcoFloatValue) values[1]);
			}
			throw new EcoException(name + ": " + values[0].GetType().ToString() + ", "
				+ values[1].GetType().ToString() + " not supported");
		}
		else if (values.Length == 3) {
			if (values[0] is EcoNumberValue && values[1] is EcoNumberValue && values[2] is EcoNumberValue) {
				return Calc((EcoNumberValue) values[0], (EcoNumberValue) values[1], (EcoNumberValue) values[2]);
			}
			else if (values[0] is EcoFloatValue && values[1] is EcoFloatValue && values[2] is EcoFloatValue) {
				return Calc((EcoFloatValue) values[0], (EcoFloatValue) values[1], (EcoFloatValue) values[2]);
			}
			throw new EcoException(name + ": " + values[0].GetType().ToString() + ", "
				+ values[1].GetType().ToString() + ", " + values[2].GetType().ToString() + " not supported");
		}
		throw new EcoException(name + ": " + values[0].GetType().ToString() + " not supported");
	}
	
	public virtual EcoValue Calc(EcoFloatValue v1) {
		throw new EcoException(name + "(float) is not supported");
	}
	
	public virtual EcoValue Calc(EcoNumberValue v1) {
		throw new EcoException(name + "(number) is not supported");
	}

	public virtual EcoValue Calc(EcoFloatValue v1, EcoFloatValue v2) {
		throw new EcoException(name + "(float, float) is not supported");
	}
	
	public virtual EcoValue Calc(EcoNumberValue v1, EcoNumberValue v2) {
		throw new EcoException(name + "(number, number) is not supported");
	}

	public virtual EcoValue Calc(EcoFloatValue v1, EcoNumberValue v2) {
		throw new EcoException(name + "(float, number) is not supported");
	}
	
	public virtual EcoValue Calc(EcoNumberValue v1, EcoFloatValue v2) {
		throw new EcoException(name + "(number, float) is not supported");
	}

	public virtual EcoValue Calc(EcoNumberValue v1, EcoNumberValue v2, EcoNumberValue v3) {
		throw new EcoException(name + "(number, number, number) is not supported");
	}

	public virtual EcoValue Calc(EcoFloatValue v1, EcoFloatValue v2, EcoFloatValue v3) {
		throw new EcoException(name + "(float, float, float) is not supported");
	}

}

public class EcoFnMin : EcoMathFunction {
	public EcoFnMin() : base(-1, "min") {
	}
	
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Calc(EcoFloatValue v1, EcoFloatValue v2) {
		return new EcoFloatValue(Math.Min(v1.v, v2.v));
	}
	
	public override EcoValue Calc(EcoNumberValue v1, EcoNumberValue v2) {
		return new EcoNumberValue(Math.Min(v1.v, v2.v));
	}	
}

public class EcoFnMax : EcoMathFunction {
	public EcoFnMax() : base(-1, "max") {
	}
	
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Calc(EcoFloatValue v1, EcoFloatValue v2) {
		return new EcoFloatValue(Math.Max(v1.v, v2.v));
	}
	
	public override EcoValue Calc(EcoNumberValue v1, EcoNumberValue v2) {
		return new EcoNumberValue(Math.Max(v1.v, v2.v));
	}	
}

public class EcoFnClamp : EcoMathFunction {
	public EcoFnClamp() : base(-1, "clamp") {
	}
	
	public override int GetParamCount() { return 3; }
	
	public override EcoValue Calc(EcoFloatValue v1, EcoFloatValue v2, EcoFloatValue v3) {
		if (v1.v < v2.v) return v2;
		else if (v1.v > v3.v) return v3;
		else return v1;
	}
	
	public override EcoValue Calc(EcoNumberValue v1, EcoNumberValue v2, EcoNumberValue v3) {
		if (v1.v < v2.v) return v2;
		else if (v1.v > v3.v) return v3;
		else return v1;
	}
}



public class EcoFnLog : EcoMathFunction {
	public EcoFnLog() : base(-1, "log") {
	}
	
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Calc(EcoFloatValue v1, EcoFloatValue v2) {
		return new EcoFloatValue(System.Math.Log(v1.v, v2.v));
	}
	
	public override EcoValue Calc(EcoFloatValue v1, EcoNumberValue v2) {
		return new EcoFloatValue(System.Math.Log(v1.v, (double) v2.v));
	}
}

public class EcoFnPow : EcoMathFunction {
	public EcoFnPow() : base(-1, "pow") {
	}
	
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Calc(EcoFloatValue v1, EcoFloatValue v2) {
		return new EcoFloatValue(System.Math.Pow(v1.v, v2.v));
	}
	
	public override EcoValue Calc(EcoFloatValue v1, EcoNumberValue v2) {
		return new EcoFloatValue(System.Math.Pow(v1.v, (double) v2.v));
	}

	public override EcoValue Calc(EcoNumberValue v1, EcoNumberValue v2) {
		return new EcoFloatValue(System.Math.Pow((double) v1.v, (double) v2.v));
	}

	public override EcoValue Calc(EcoNumberValue v1, EcoFloatValue v2) {
		return new EcoFloatValue(System.Math.Pow((double) v1.v, v2.v));
	}
}

public class EcoFnRound : EcoMathFunction {
	public EcoFnRound() : base(-1, "round") {
	}
	
	public override int GetParamCount() { return 1; }
	
	public override EcoValue Calc(EcoFloatValue v1) {
		return new EcoFloatValue(System.Math.Round(v1.v));
	}
}

public class EcoFnSqrt : EcoMathFunction {
	public EcoFnSqrt() : base(-1, "sqrt") {
	}
	
	public override int GetParamCount() { return 1; }
	
	public override EcoValue Calc(EcoFloatValue v1) {
		return new EcoFloatValue(System.Math.Sqrt(v1.v));
	}
}

public class EcoFnCos : EcoMathFunction {
	public EcoFnCos() : base(-1, "cos") {
	}
	
	public override int GetParamCount() { return 1; }
	
	public override EcoValue Calc(EcoFloatValue v1) {
		return new EcoFloatValue(System.Math.Cos(v1.v));
	}
}

public class EcoFnSin : EcoMathFunction {
	public EcoFnSin() : base(-1, "sin") {
	}
	
	public override int GetParamCount() { return 1; }
	
	public override EcoValue Calc(EcoFloatValue v1) {
		return new EcoFloatValue(System.Math.Sin(v1.v));
	}
}

public class EcoFnTan : EcoMathFunction {
	public EcoFnTan() : base(-1, "tan") {
	}
	
	public override int GetParamCount() { return 1; }
	
	public override EcoValue Calc(EcoFloatValue v1) {
		return new EcoFloatValue(System.Math.Tan(v1.v));
	}
}

public class EcoFnRndRange : EcoMathFunction {
	public EcoFnRndRange() : base(-1, "rndrange") {
	}
	
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Calc(EcoFloatValue v1, EcoFloatValue v2) {
		return new EcoFloatValue(UnityEngine.Random.Range((float) v1.v, (float) v2.v));
	}
	
	public override EcoValue Calc(EcoNumberValue v1, EcoNumberValue v2) {
		return new EcoNumberValue(UnityEngine.Random.Range((int) v1.v, (int) v2.v));
	}
}

public class EcoFnRndOdds : EcoMathFunction {
	public EcoFnRndOdds() : base(-1, "rndodds") {
	}
	
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Calc(EcoNumberValue v1, EcoFloatValue v2) {
		int success = 0;
		float odds = (float) (v2.v);
		for (int i = (int) (v1.v); i > 0; i--) {
			if (UnityEngine.Random.value <= odds) success++;
		}
		return new EcoNumberValue(success);
	}
	
}
