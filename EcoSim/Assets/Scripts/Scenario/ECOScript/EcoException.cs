public class EcoException : System.Exception {

	public EcoException(string msg) :
	base(msg) {
	}
	
	public EcoException(string msg, int lineNr) :
	base(msg + " at linenr " + lineNr) {
	}

	public EcoException(string msg, string location, int lineNr) :
	base(msg + " at linenr " + lineNr + " parser was at " + location) {
	}

}
