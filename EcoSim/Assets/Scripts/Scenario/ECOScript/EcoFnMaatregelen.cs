using System.Collections.Generic;

public class EcoFnMaatregelen : EcoBaseFunction {
	public EcoFnMaatregelen() : base(-1, "maatregelen") {
	}
	
	public override bool HasDefinition() { return true; }
	public override int GetParamCount() { return 2; }
	
	public override EcoValue Execute(EcoContext context, EcoValue[] values) {
		if (!(values[0] is EcoNumberValue)) throw new EcoException("special function '" + name + "' argument 1 needs to be a number");
		if (!(values[1] is EcoStringValue)) throw new EcoException("special function '" + name + "' argument 2 needs to be a string");
		
		int targetId = (int) ((values[0] as EcoNumberValue).v);
		string actionStr = (values[1] as EcoStringValue).v;
		EActionTypes a = EnumExtensions.GetActionType(actionStr);
		if (a == EActionTypes.UNDEFINED) throw new EcoException("maatregel type '" + actionStr + "' unknown");
		
		JScenario scenario = context.scenario;
		Extra extra = new Extra();
		extra.actionType = a;	
		
		scenario.data.ProcessTargetAreaMeasure(targetId, a, DJustCount, extra);
				
		return new EcoNumberValue(extra.total);
	}
	
	static void DJustCount(JCellData cell, int x, int y, System.Object extra) {
		Extra e = extra as Extra;
		e.total++;
	}

	
	private class Extra {
		public EActionTypes actionType;
		public int total;
	}
}
