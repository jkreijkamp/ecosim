using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class JRoadMgr {
	
	public class RoadData {
		public int prefabIndex;
		public Vector3[] path;
		public List<Vector3> points;
		public Vector3 startCtrl;
		public Vector3 endCtrl;
		public Bounds bounds;
		public JRoad instance;
	}
	
	List<RoadData> roads = new List<RoadData>();
	
	public int GetRoadCount() {
		return roads.Count;
	}
	
	public void MoveRoads(int x, int y, int w, int h) {
		// TODO: remove roads outside boundaries
		List<RoadData> toBeDeleted = new List<RoadData>();
		Vector3 offset = new Vector3(-x * JTerrainData.HORIZONTAL_SCALE, 0f, -y * JTerrainData.HORIZONTAL_SCALE);
		foreach (RoadData r in roads) {
			if (!r.instance) InstantiateRoad(r);
			// r.instance.UpdatePath(false);
			Vector3 minBounds = r.bounds.min;
			Vector3 maxBounds = r.bounds.max;
			if (((x + w) * JTerrainData.HORIZONTAL_SCALE < minBounds.x) ||
				((y + h) * JTerrainData.HORIZONTAL_SCALE < minBounds.z) ||
				(x * JTerrainData.HORIZONTAL_SCALE > maxBounds.x) ||
				(y * JTerrainData.HORIZONTAL_SCALE > maxBounds.z)) {
				toBeDeleted.Add(r);
			} else {
				// r.startCtrl += offset;
				// r.endCtrl += offset;
				for (int i = 0; i < r.path.Length; i++) {
					r.path[i] = r.path[i] + offset;
				}
				for (int i = 0; i < r.points.Count; i++) {
					r.points[i] = r.points[i] + offset;
				}
			}
		}
		foreach (RoadData r in toBeDeleted) {
			if (r.instance) GameObject.Destroy(r.instance.gameObject);
			roads.Remove(r);
		}
	}
	
	public void RemoveAllRoadInstances() {
		foreach (RoadData r in roads) {
			GameObject.Destroy(r.instance.gameObject);
		}
	}
	
	public JRoad GetRoadFromIndex(int index) {
		RoadData data = roads[index];
		if (!data.instance) InstantiateRoad(data);
		return data.instance;
	}
	
	public void DeleteRoad(JRoad road) {
		road.DeleteRoad();
		roads.Remove(road.roadData);
	}
	
	public GameObject CreateRoad(int prefabIndex) {
		RoadData data = new RoadData();
		data.prefabIndex = prefabIndex;
		data.points = new List<Vector3>();
		roads.Add(data);
		return InstantiateRoad(data);
	}
	
	public GameObject InstantiateRoad(RoadData road) {
		GameObject newRoad = (GameObject) GameObject.Instantiate(JElements.self.roadPrefabs[road.prefabIndex], Vector3.zero, Quaternion.identity);
		newRoad.transform.parent = JRenderTerrain.self.roadsHolder.transform;
		JRoad r = newRoad.GetComponent<JRoad>();
		r.Setup(road);
		return newRoad;
	}
	
	public JRoadMgr(JScenario scenario) {
//		this.scenario = scenario;
	}
	
	public void Load(JScenario scenario) {
		roads.Clear();
//		this.scenario = scenario;
		string fileName = scenario.saveGamePath;
		if ((fileName == null) || !(scenario.IsGameMode)) {
			fileName = scenario.scenarioPath;
		}
		fileName += "roads.dat";
		if (!File.Exists(fileName)) return;
		FileStream stream = new FileStream(fileName, FileMode.Open);
		BinaryReader reader = new BinaryReader(stream);
		long header = reader.ReadInt64();
		if (header != JConfig.FILE_VERSION) {
			throw new System.Exception("incompatible road version");
		}
		int count = reader.ReadInt32();
		for (int i = 0; i < count; i++) {
			string prefabName = reader.ReadString();
			RoadData data = new RoadData();
			data.prefabIndex = JElements.FindRoadIndex(prefabName);
			data.points = new List<Vector3>();
			int count2 = reader.ReadInt32();
			for (int j = 0; j < count2; j++) {
				data.points.Add(JIOUtils.ReadVector3(reader));
			}
			data.startCtrl = JIOUtils.ReadVector3(reader);
			data.endCtrl = JIOUtils.ReadVector3(reader);
			count2 = reader.ReadInt32();
			data.path = new Vector3[count2];
			Vector3 min = Vector3.zero;
			Vector3 max = Vector3.zero;
			for (int j = 0; j < count2; j++) {
				Vector3 v = JIOUtils.ReadVector3(reader);
				data.path[j] = v;
				if (j == 0) {
					min = v;
					max = v;
				}
				else {
					min.x = Mathf.Min(min.x, v.x);
					min.y = Mathf.Min(min.y, v.y);
					min.z = Mathf.Min(min.z, v.z);
					max.x = Mathf.Max(min.x, v.x);
					max.y = Mathf.Max(min.y, v.y);
					max.z = Mathf.Max(min.z, v.z);
				}
			}
			data.bounds = new Bounds((min + max) / 2, (max - min));
			roads.Add(data);
			InstantiateRoad(data);
		}
		reader.Close();
	}
	
	public void Save(string path) {
		FileStream stream = new FileStream(path + Path.DirectorySeparatorChar + "roads.dat", FileMode.Create);
		BinaryWriter writer = new BinaryWriter(stream);
		writer.Write(JConfig.FILE_VERSION);
		writer.Write(roads.Count);
		foreach (RoadData road in roads) {
			writer.Write(JElements.self.roadPrefabs[road.prefabIndex].name);
			writer.Write(road.points.Count);
			foreach (Vector3 v in road.points) {
				JIOUtils.WriteVector3(writer, v);
			}
			JIOUtils.WriteVector3(writer, road.startCtrl);
			JIOUtils.WriteVector3(writer, road.endCtrl);
			writer.Write(road.path.Length);
			foreach (Vector3 v in road.path) {
				JIOUtils.WriteVector3(writer, v);
			}
		}
		writer.Close();
		stream.Close();	
	}
	
	public void RecreateRoads() {
		foreach (RoadData road in roads) {
			InstantiateRoad(road);
		}	
	}
}
