using UnityEngine;
using System.Collections;

[System.Serializable]
public class JXSuccession {
	public int index;
	public string name;
	public JXVegetation[] vegetation;
	
	public JXSuccession() {
		vegetation = new JXVegetation[0];
	}
}
