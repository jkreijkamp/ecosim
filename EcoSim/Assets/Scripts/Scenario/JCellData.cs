using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;
using UnityEngine;

/**
 * Class stores a small area of the map, stores heights, water, vegetation, ...
 */
public class JCellData {
	public const int TARGET_MAP_COUNT = 8;
	
	private volatile Thread isLoading = null;
	public bool isLoaded = false; // true: cell data loaded in memory
	private bool hasData = false; // true: cell has data (data doesn't have to be in memory)
	public bool isChanged = false; // true: cell has changes in memory that aren't stored on disk yet
	private bool isHeightmapChanged = false; // true: if heighmap needs to be recalculated
	private bool areTilesChanged = false; // true: tiles have changed and cached object data is invalid
	private bool dataInSaveGame = false; // true: load cell from game data directory instead of scenario
	private bool calculateSuccession = false; // true: calculate succession on this cell
	public bool isVisible = false; // true: cell is in scenario terrain
	
	private Dictionary<EParamTypes, byte[]> data;
	
	// heights, waterHeights, adjustedWaterHeights is (CELL_SIZE + 2) * (CELL_SIZE + 2) as
	// it contains a border of neighbouring cells.
	
	private float[] heights; // land height map
	private float[] waterHeights; // the water height map, normally static in game
	private float[] adjustedWaterHeights; // water heights adjusted for canals and other measures
	
	
	private float[,] heightMap; // higher resolution generated map for terrain
	private float[,] hiResWaterHeightMap; // higher resolution generated map for water tiles
	private float[,] waterHeightMap; // for generating water mesh
	private bool[,] waterVisibleMap; // which points of water are above land map
	private bool hasVisibleWater; // if false, no water is visible in cell
	
	private byte[] successionMap; // succession type of cells
	private byte[] vegetationMap; // vegetation type within succession of cells
	private byte[] variantMap; // variation within vegetation
	private byte[] specialMap; // for succession stability and canals
	private byte[] successionBitmap; // bitmap to determine which cells handle succession
	
	
	public class AnimalData {
		public AnimalData(JCellData cell, EAnimalTypes t, int x, int y) {
			this.cell = cell;
			this.t = t;
			this.x = x;
			this.y = y;
		}
		
		public readonly JCellData cell;
		public readonly EAnimalTypes t;
		public readonly int x;
		public readonly int y;
		public GameObject go;
	}
	
	class SpeciesData {
		public int count;
		public byte[] bitmap;
	}
	
	private SpeciesData[] speciesData;
	private SpeciesData[] animalSpeciesData;
	private byte[][] targetMaps; // used for checking game rules
		
	public int xIndex; // cell index x-direction (0, 1, ...)
	public int yIndex; // cell index y-direction (0, 1, ...)
	public int xOffset { get { return xIndex * CELL_SIZE; } }
	public int yOffset { get { return yIndex * CELL_SIZE; } }
	readonly JScenario scenario;
	
	private const byte NOT_EXIST = 0x00;
	private const byte USE_SCENE_LOC = 0x01;
	private const byte USE_SAVEGAME_LOC = 0x02;
	private const byte SUCCESSION_MASK = 0x10;
	
	private const int CELL_SIZE = JTerrainData.CELL_SIZE;
	private const int CELL_SIZE2EXP = JTerrainData.CELL_SIZE2EXP;
	private const int CELL_SIZE2EXP_BITMAP = JTerrainData.CELL_SIZE2EXP - 3;
	private const int HM_SIZE = CELL_SIZE * 4 + 1;
	private const float VERTICAL_SCALE = JTerrainData.VERTICAL_SCALE;
	
	
	// internal enum used for HandleSpecial.
	private enum EHandleSpecial { heights, waterHeights, adjustedWaterHeights };
	
	// for thread safe random (a cell is never modified by more than one thread)
	public System.Random rnd;
	
	// internal class for object instances used by tiles
	private class GOData {
		public Vector3 pos;
		public Vector3 scale;
		public Quaternion rotation;
	}
	
	public class BuildingData {
		public JElements.BuildingWrapper building;
		public Vector3 pos;
		public Vector3 scale;
		public Quaternion rotation;
	}
	
	private List<AnimalData> animals;
	private List<AnimalData> successionAnimals;
	private List<AnimalData> newSuccessionAnimals = null;
	private Dictionary<string,List<BuildingData>> buildings;
	private Dictionary<JElements.PrefabWrapper,List<GOData>> renderObjects;
	private List<GameObject> renderedObjects;
	private List<TreeInstance> cachedTrees;
	private GameObject cachedStickerObject;
	private JRenderTerrainSticker sticker;
	private GameObject cachedStickerGO;
	private float lastRendered = -100f;
	public static float localTime = 0f;
	
	public bool HasParameterType(EParamTypes t) {
		if (!isLoaded) Load();
		return (data != null) && (data.ContainsKey(t));
	}
	
	public void AddAnimal(GameObject parent, EAnimalTypes type, int x, int y) {
		if (!isLoaded) Load();
		AnimalData ad = new AnimalData(this, type, x, y);
		animals.Add(ad);
		isChanged = true;
		if (isVisible) {
			float scale = JTerrainData.HORIZONTAL_SCALE;
			PlaceAnimal(parent, ad, xIndex * CELL_SIZE * scale, yIndex * CELL_SIZE * scale, scale);
		}
	}
	
	public void PlaceAnimal(GameObject parent, AnimalData ad, float offsetX, float offsetY, float scale) {
		float posX = offsetX + ((float) ad.x + 0.5f) * scale;
		float posY = offsetY + ((float) ad.y + 0.5f) * scale;
		float posH = heightMap[(ad.y << 2) + 2, (ad.x << 2) + 2] + 0.5f;
		GameObject go = (GameObject) GameObject.Instantiate(JElements.GetAnimalPrefab(ad.t), new Vector3(posX, posH, posY), Quaternion.Euler(0f, Random.Range(0f, 360f), 0f));
		go.transform.parent = parent.transform;
		go.GetComponent<JAnimal>().animalData = ad;
		ad.go = go;
		renderedObjects.Add(go);
	}
	
	public void DeleteAnimal(AnimalData ad) {
		if (!isLoaded) Load();
		animals.Remove(ad);
		isChanged = true;
	}
	
	public void SetNewSuccessionAnimals(List<AnimalData> animalData) {
		if (!isLoaded) Load();		
		if (isVisible) {
			// we can't update the animals on screen from calling thread so we place it in temp list and update the animals on screen later
			newSuccessionAnimals = animalData;
		}
		else {
			if (animalData == null) {
				successionAnimals.Clear();
			}
			else {
				successionAnimals = animalData;
			}
			newSuccessionAnimals = null;
		}
	}
	
	/**
	 * should be called on all visible cells at end of succession to update the animals shown to the new list
	 * of animals (the temp list is newSuccessionAnimals, the result list is successionAnimals).
	 */
	public void UpdateShownSuccessionAnimals(GameObject parent) {
		if (!isLoaded) Load();
		foreach (AnimalData ad in successionAnimals) {
			if (ad.go) {
				renderedObjects.Remove(ad.go);
				GameObject.Destroy(ad.go);
			}
		}
		if (newSuccessionAnimals != null) {
			successionAnimals = newSuccessionAnimals;
			float scale = JTerrainData.HORIZONTAL_SCALE;
			float offsetX = scale * xIndex * CELL_SIZE;
			float offsetY = scale * yIndex * CELL_SIZE;

			foreach (AnimalData ad in successionAnimals) {
				float posX = offsetX + ((float) ad.x + 0.5f) * scale;
				float posY = offsetY + ((float) ad.y + 0.5f) * scale;
				float posH = heightMap[(ad.y << 2) + 2, (ad.x << 2) + 2] + 0.5f;
				GameObject go = (GameObject) GameObject.Instantiate(JElements.GetAnimalPrefab(ad.t), new Vector3(posX, posH, posY), Quaternion.Euler(0f, Random.Range(0f, 360f), 0f));
				go.transform.parent = parent.transform;
				go.GetComponent<JAnimal>().animalData = ad;
				ad.go = go;
				renderedObjects.Add(go);		
			}
		}
		else {
			successionAnimals.Clear();
		}
	}
		
	public int CountAnimalSpecies(int index) {
		if (!isLoaded) Load();
		if ((index >= animalSpeciesData.Length) || (animalSpeciesData[index] == null)) return 0;
		return animalSpeciesData[index].count;
	}
	
	public void ClearAnimalSpecies(int index) {
		if (!isLoaded) Load();
		if ((index >= animalSpeciesData.Length) || (animalSpeciesData[index] == null)) return;
		animalSpeciesData[index] = null;
		isChanged = true;
	}

	public int GetAnimalSpeciesValue(int x, int y, int index) {
		if (!isLoaded) Load();
		if ((index >= animalSpeciesData.Length) || (animalSpeciesData[index] == null)) return 0;

	
		int i = (y << CELL_SIZE2EXP) | x;
		SpeciesData s = animalSpeciesData[index];
		int b = s.bitmap[i >> 1];
		
		int r = (b >> (4 * (i & 0x01))) & 0x0f;
		return r;
	}
	
	void ResizeAnimalSpeciesDataArray(int index) {
		// we don't want to recurse
//		if (!isLoaded) Load();
		SpeciesData[] s = new SpeciesData[index + 1];
		for (int i = 0; i < animalSpeciesData.Length; i++) s[i] = animalSpeciesData[i];
		animalSpeciesData = s;
	}
	
	public void SetAnimalSpeciesValue(int x, int y, int index, int v) {
		if (!isLoaded) Load();
		if (v == 0) {
			ClearAnimalSpeciesValue(x, y, index);
			return;
		}
		if (index >= animalSpeciesData.Length) ResizeAnimalSpeciesDataArray(index);
		SpeciesData s = animalSpeciesData[index];
		if (s == null) {
			s = new SpeciesData();
			s.bitmap = new byte[CELL_SIZE * CELL_SIZE / 2];
			s.count = 0;
			animalSpeciesData[index] = s;
		}
		int i = (y << CELL_SIZE2EXP) | x; 
		int j = 4 * (i & 0x01);
		i = i >> 1;
		int b = s.bitmap[i];
		int mask = 0x0f << j;
		if ((b & mask) == 0) s.count++;
		b = (b & ~mask) | (v << j);
		s.bitmap[i] = (byte) b;
		isChanged = true;
	}
	
	public void ClearAnimalSpeciesValue(int x, int y, int index) {
		if (!isLoaded) Load();
		if (index >= animalSpeciesData.Length) return;
		SpeciesData s = animalSpeciesData[index];
		if (s == null) return;
		int i = (y << CELL_SIZE2EXP) | x;
		int j = 4 * (i & 0x01);
		i = i >> 1;
		int b = s.bitmap[i];
		int mask = 0x0f << j;
		if ((b & mask) != 0) {
			isChanged = true;
			if (s.count <= 1) {
				// we now get an empty bitmap, just delete speciesdata entry
				animalSpeciesData[index] = null;
				return;
			}
			s.count--;
			b = b & ~mask;
			s.bitmap[i] = (byte) b;
		}		
	}
	
	
	public int CountSpecies(int index) {
		if (!isLoaded) Load();
		if ((index >= speciesData.Length) || (speciesData[index] == null)) return 0;
//		Debug.Log("count " + GetCellName() + " " + index + " = " + speciesData[index].count);
		return speciesData[index].count;
	}
	
	public void ClearSpecies(int index) {
		if (!isLoaded) Load();
		if ((index >= speciesData.Length) || (speciesData[index] == null)) return;
		speciesData[index] = null;
		isChanged = true;
	}
	
	public int GetSpeciesValue(int x, int y, int index) {
		if (!isLoaded) Load();
		if ((index >= speciesData.Length) || (speciesData[index] == null)) return 0;
//		Debug.Log("cell " + GetCellName() + " x = " + x + " y = " + y);
		int i = (y << CELL_SIZE2EXP) | x;
		SpeciesData s = speciesData[index];
		int b = s.bitmap[i >> 2];
		
		int r = (b >> (2 * (i & 0x03))) & 0x03;
//		Debug.Log(GetCellName() + " GetSpeciesValue(" + x + ", " + y + ", " + index + ") i = " + (i >> 2) + " b = " + (int) b + " result = " + r);
		return r;
	}
	
	void ResizeSpeciesDataArray(int index) {
		// we don't want to recurse...
//		if (!isLoaded) Load();
		SpeciesData[] s = new SpeciesData[index + 1];
		for (int i = 0; i < speciesData.Length; i++) s[i] = speciesData[i];
		speciesData = s;
	}
	
	public void SetSpeciesValue(int x, int y, int index, int v) {
		if (!isLoaded) Load();
		if (v == 0) {
			ClearSpeciesValue(x, y, index);
			return;
		}
		if (index >= speciesData.Length) ResizeSpeciesDataArray(index);
		SpeciesData s = speciesData[index];
		if (s == null) {
			s = new SpeciesData();
			s.bitmap = new byte[CELL_SIZE * CELL_SIZE / 4];
			s.count = 0;
			speciesData[index] = s;
		}
		int i = (y << CELL_SIZE2EXP) | x; 
		int j = 2 * (i & 0x03);
		i = i >> 2;
		int b = s.bitmap[i];
		int mask = 0x03 << j;
		if ((b & mask) == 0) s.count++;
		b = (b & ~mask) | (v << j);
		s.bitmap[i] = (byte) b;
//		Debug.Log(GetCellName() + " SetSpeciesValue(" + x + ", " + y + ", " + index + ", " + v + ") i = " + i + " b = " + (int) b);
		isChanged = true;
	}
	
	public void ClearSpeciesValue(int x, int y, int index) {
		if (!isLoaded) Load();
		if (index >= speciesData.Length) return;
		SpeciesData s = speciesData[index];
		if (s == null) return;
		int i = (y << CELL_SIZE2EXP) | x;
		int j = 2 * (i & 0x03);
		i = i >> 2;
		int b = s.bitmap[i];
		int mask = 0x03 << j;
		if ((b & mask) != 0) {
			isChanged = true;
			if (s.count <= 1) {
				// we now get an empty bitmap, just delete speciesdata entry
				speciesData[index] = null;
				return;
			}
			s.count--;
			b = b & ~mask;
			s.bitmap[i] = (byte) b;
		}		
	}
	
	public bool HasTargetMap(int index) {
		if (!calculateSuccession) return false;
		if (!isLoaded) Load();
		if (index == -1) return true;
		if ((targetMaps == null) || (targetMaps[index] == null)) return false;
		return true;
	}
	
	public byte[] GetTargetMap(int index) {
		if (!calculateSuccession) return null;
		if (!isLoaded) Load();
		if (index == -1) return successionBitmap;
		if (targetMaps == null) return null;
		return targetMaps[index];
	}
	
	public void SetTarget(int index, int x, int y) {
		if (NeedCalculateSuccession(x, y)) {
			if (targetMaps == null) {
				targetMaps = new byte[TARGET_MAP_COUNT][];
			}
			if (targetMaps[index] == null) {
				targetMaps[index] = new byte[CELL_SIZE * CELL_SIZE / 8];
			}
			int i = (y << CELL_SIZE2EXP) | x;
			int offset = 1 << (i & 0x7);
			targetMaps[index][i >> 3] |= (byte) offset;
			isChanged = true;
		}
	}
	
	public void ClearTargetMap(int index) {
		if ((targetMaps != null) && (targetMaps[index] != null)) {
			targetMaps[index] = null;
			isChanged = true;
		}
	}

	public void ClearTarget(int index, int x, int y) {
		if (NeedCalculateSuccession(x, y)) {
			if (targetMaps == null) {
				return;
			}
			if (targetMaps[index] == null) {
				return;
			}
			int i = (y << CELL_SIZE2EXP) | x;
			int offset = 1 << (i & 0x7);
			targetMaps[index][i >> 3] |= (byte) offset;
			bool hasValues = false;
			foreach (byte b in targetMaps[index]) {
				if (b != 0) {
					hasValues = true;
					break;
				}
			}
			if (!hasValues) {
				targetMaps[index] = null;
			}
			isChanged = true;
		}
	}
	
	public bool NeedCalculateSuccession() {
		return calculateSuccession;
	}
	
	public bool NeedCalculateSuccession(int x, int y) {
		if (!calculateSuccession) return false;
		if (!isLoaded) Load();
		if (successionBitmap == null) return false;
		int i = (y << CELL_SIZE2EXP) | x;
		int val = successionBitmap[i >> 3];
		int offset = 1 << (i & 0x7);
		return (val & offset) != 0;
	}
	
	public void SetNeedCalculateSuccession(int x, int y) {
		if (!isLoaded) Load();
		if (successionBitmap == null) {
			successionBitmap = new byte[CELL_SIZE * CELL_SIZE / 8];
		}
		int i = (y << CELL_SIZE2EXP) | x;
		int offset = 1 << (i & 0x7);
		successionBitmap[i >> 3] |= (byte) offset;
		
		calculateSuccession = true;
		isChanged = true;
	}

	public void ClearParamType(EParamTypes t) {
		if (!isLoaded) {
			Load();
		}
		if (data.ContainsKey(t)) {
			data.Remove(t);
			isChanged = true;
		}
	}
		
	public void RemoveUnusedParameters() {
		if (!isLoaded) {
			Load();
		}
		if (successionBitmap == null) {
			data.Clear();
			isChanged = true;
			return;
		}
		byte[] sb = successionBitmap;
		foreach (EParamTypes pt in System.Enum.GetValues(typeof(EParamTypes))) {
			if (data.ContainsKey(pt)) {
				byte[] d = data[pt];
				int len = d.Length;
				int count = 0;
				int p = 0;
				int pp = 0x80;
				int val = 0;
				for (int i = 0; i < len; i++) {
					if (pp == 0x80) {
						val = sb[p++];
						pp = 0x01;
					}
					else {
						pp = pp << 1;
					}
					if ((val & pp) != 0) {
						if (d[i] != 0) count++;
					}
					else {
						if (d[i] != 0) {
							d[i] = 0;
							isChanged = true;
						}
					}
				}
				if (count == 0) {
					data.Remove(pt);
					isChanged = true;
				}
			}
		}
	}
	
	public void ClearNeedCalculateSuccession() {
		if (!calculateSuccession) return;
		if (!isLoaded) Load();		
		if (successionBitmap == null) {
			return;
		}
		successionBitmap = null;
		calculateSuccession = false;
		isChanged = true;
	}
	
	public void ClearNeedCalculateSuccession(int x, int y) {
		if (!calculateSuccession) return;
		if (!isLoaded) Load();				
		if (successionBitmap == null) {
			return;
		}
		int i = (y << CELL_SIZE2EXP) | x;
		int offset = ~(1 << (i & 0x7));
		successionBitmap[i >> 3] &= (byte) offset;
		
		bool empty = true;
		foreach (byte b in successionBitmap) {
			if (b != 0) {
				empty = false;
				break;
			}
		}
		if (empty) {
			calculateSuccession = false;
			successionBitmap = null;
			isChanged = true;
		}
	}
	
	public byte[] GetSuccessionBitmap() {
		if (!isLoaded) Load();
		return successionBitmap;
	}
	
	public void SetupDefaultParameters(int x, int y) {
		if (!isLoaded) Load();
		JXVegetation veg = GetVegetation(x, y);
		foreach (JXVegetation.NewParam changes in veg.newParams) {
			SetData(changes.type, x, y, (byte) RndRange(changes.minRange, changes.maxRange));
		}
	}
	
	public JCellData(JScenario scenario, int x, int y) {
		this.scenario = scenario;
		xIndex = x;
		yIndex = y;
		isLoaded = false;
		hasData = false;
		rnd = new System.Random();
	}
	
	public static JCellData SetupCell(JScenario scenario, byte modus, int x, int y) {
		JCellData cell = new JCellData(scenario, x, y);
		if ((modus & 0x0f) == USE_SAVEGAME_LOC) {
			cell.dataInSaveGame = true;
		}
		if ((modus & SUCCESSION_MASK) != 0) {
			cell.calculateSuccession = true;
		}
		return cell;
	}
	
	public void DeleteBuilding(BuildingData building) {
		foreach (string key in buildings.Keys) {
			List<BuildingData> list = buildings[key];
			if (list.Contains(building)) {
				if (list.Count <= 1) {
					buildings.Remove(key);
				}
				else {
					list.Remove(building);
				}
				return;
			}
		}
	}
	
	// used by editor if building is added on the fly
	public void AddRenderedObject(GameObject go) {
		renderedObjects.Add(go);
	}
	
	public void SetModus(byte modus) {
		calculateSuccession = ((modus & SUCCESSION_MASK) != 0);
		modus &= 0xf;
		if (modus == NOT_EXIST) {
			hasData = false;
		}
		else {
			hasData = true;
			dataInSaveGame = (modus == USE_SAVEGAME_LOC);
		}
	}
	
	/**
	 * returns byte that indicates state of cell
	 * (lives in scene or savegame or doesn't exist and if succession needs to be calculated)
	 */
	public byte GetModus() {
		byte modus = NOT_EXIST;
		if (hasData) {
			modus = dataInSaveGame?USE_SAVEGAME_LOC:USE_SCENE_LOC;
		}
		if (calculateSuccession) modus |= SUCCESSION_MASK;
		return modus;
	}
	
	/**
	 * To make cells if there's no data defined for it yet (no heightmap, or anything)
	 */
	public void InitNoData() {
		int s = CELL_SIZE * CELL_SIZE;
		int s2 = (CELL_SIZE + 2) * (CELL_SIZE + 2);
		heights = new float[s2];
		waterHeights = new float[s2];
		adjustedWaterHeights = null;
		successionMap = new byte[s];
		vegetationMap = new byte[s];
		variantMap = new byte[s];
		specialMap = new byte[s];
		data = new Dictionary<EParamTypes, byte[]>();
		isLoaded = true;
		isChanged = false;
		buildings = new Dictionary<string, List<BuildingData>>();
		speciesData = new SpeciesData[scenario.species.Length];
		animalSpeciesData = new SpeciesData[scenario.animalSpecies.Length];
		animals = new List<AnimalData>();
		successionAnimals = new List<AnimalData>();
		targetMaps = null;
	}
	
	/**
	 * Loads the cell data from disk, can be from scenario dir or savegame dir
	 */
	public void Load() {
//		Debug.LogWarning("cell " + GetCellName() + " start load");
		while (isLoading != null) {
			if (isLoading == Thread.CurrentThread) {
				throw new System.Exception("Recursive call of Load for cell " + GetCellName());
			}
			Debug.LogError("Is Loading is true!");
			System.Threading.Thread.Sleep(50);
		}
		isLoading = Thread.CurrentThread;
//		Debug.LogWarning("cell " + GetCellName() + " isloading = true");
		
		if (!hasData) {
			InitNoData();
		}
		else {
			string path = (dataInSaveGame?scenario.saveGamePath:scenario.scenarioPath) + GetCellName();
			if (File.Exists(path)) {
				data = new Dictionary<EParamTypes, byte[]>();
				buildings = new Dictionary<string, List<BuildingData>>();
				speciesData = new SpeciesData[scenario.species.Length];
				animalSpeciesData = new SpeciesData[scenario.animalSpecies.Length];
				animals = new List<AnimalData>();
				successionAnimals = new List<AnimalData>();

				hasVisibleWater = false;
				calculateSuccession = false;

				FileStream stream = new FileStream(path, FileMode.Open, System.IO.FileAccess.Read);
				BinaryReader reader = new BinaryReader(stream);
				
				if (reader.ReadInt64() != JConfig.FILE_VERSION) throw new System.Exception("Wrong version for " + GetCellName());
				int s = CELL_SIZE * CELL_SIZE;
				int s2 = (CELL_SIZE + 2) * (CELL_SIZE + 2); // heights, water, ... 
				while (reader.PeekChar() >= 0) {
					string typeStr = reader.ReadString();
					int len = reader.ReadInt32();
					// Debug.Log("type '" + typeStr + "' len = " + len);
					if (typeStr == "animals") {
						for (int i = 0; i < len; i++) {
							AnimalData ad = new AnimalData(this, (EAnimalTypes) reader.ReadByte(), reader.ReadByte(), reader.ReadByte());
							animals.Add(ad);
						}
					}
					else if (typeStr == "successionanimals") {
						for (int i = 0; i < len; i++) {
							AnimalData ad = new AnimalData(this, (EAnimalTypes) reader.ReadByte(), reader.ReadByte(), reader.ReadByte());
							successionAnimals.Add(ad);
						}
					}
					else if (typeStr == "buildings") {
						for (int i = 0; i < len; i++) {
							BuildingData bd = new BuildingData();
							string name = reader.ReadString();
							bd.building = JElements.GetBuildingWrapper(name);
							bd.pos = JIOUtils.ReadVector3(reader);
							bd.rotation = JIOUtils.ReadQuaternion(reader);
							bd.scale = JIOUtils.ReadVector3(reader);
							if (buildings.ContainsKey(bd.building.matName)) {
								buildings[bd.building.matName].Add(bd);
							}
							else {
								List<BuildingData> bdList = new List<BuildingData>();
								bdList.Add(bd);
								buildings.Add(bd.building.matName, bdList);
							}
						}
					}
					else if ((typeStr == "heights") && (len == s2 * 4)) {
						heights = JIOUtils.ReadFA(reader, s2);
					}
					else if ((typeStr == "water") && (len == s2 * 4)) {
						waterHeights = JIOUtils.ReadFA(reader, s2);
					}
					else if ((typeStr == "adjustedwater") && (len == s2 * 4)) {
						adjustedWaterHeights = JIOUtils.ReadFA(reader, s2);
					}
					else if (typeStr == "heightmap" && (len == HM_SIZE * HM_SIZE * 4)) {
						heightMap = new float[HM_SIZE, HM_SIZE];
						for (int y = 0; y < HM_SIZE; y++) {
							for (int x = 0; x < HM_SIZE; x++) {
								heightMap[y, x] = reader.ReadSingle();
							}
						}
					}
					else if (typeStr == "waterheightmap" && (len == ((CELL_SIZE + 1) * (CELL_SIZE + 1) * 5))) {
						waterHeightMap = new float[CELL_SIZE + 1, CELL_SIZE + 1];
						waterVisibleMap = new bool[CELL_SIZE + 1, CELL_SIZE + 1];
						for (int y = 0; y <= CELL_SIZE; y++) {
							for (int x = 0; x <= CELL_SIZE; x++) {
								waterHeightMap[y, x] = reader.ReadSingle();
								waterVisibleMap[y, x] = reader.ReadBoolean();
							}
						}
						hasVisibleWater = true;
					}
					else if ((typeStr == "special") && (len == s)) {
						specialMap = reader.ReadBytes(s);
					}
					else if ((typeStr == "succession") && (len == s)) {
						successionMap = reader.ReadBytes(s);
					}
					else if ((typeStr == "vegetation") && (len == s)) {
						vegetationMap = reader.ReadBytes(s);
					}
					else if ((typeStr == "variant") && (len == s)) {
						variantMap = reader.ReadBytes(s);
					}
					else if ((typeStr == "successionbitmap") && (len == s / 8)) {
						successionBitmap = reader.ReadBytes(s / 8);
						calculateSuccession = true;
					}
					else if ((typeStr == "speciesbitmap") && (len == (s / 4) + 8)) {
						SpeciesData sd = new SpeciesData();
						int sdindex = reader.ReadInt32();
						sd.count = reader.ReadInt32();
						sd.bitmap = reader.ReadBytes(s / 4);
						if (sdindex >= speciesData.Length) ResizeSpeciesDataArray(sdindex);
						speciesData[sdindex] = sd;
					}
					else if ((typeStr == "animalspeciesbitmap") && (len == (s / 2) + 8)) {
						SpeciesData sd = new SpeciesData();
						int sdindex = reader.ReadInt32();
						sd.count = reader.ReadInt32();
						sd.bitmap = reader.ReadBytes(s / 2);
						if (sdindex >= animalSpeciesData.Length) ResizeAnimalSpeciesDataArray(sdindex);
						animalSpeciesData[sdindex] = sd;
					}
					else if ((typeStr.StartsWith("targetmap")) && (len == s / 8)) {
						int index = int.Parse(typeStr.Substring(9));
						if (targetMaps == null) {
							targetMaps = new byte[TARGET_MAP_COUNT][];
						}
						targetMaps[index] = reader.ReadBytes(s / 8);
					}
					else if ((EnumExtensions.GetParamType(typeStr) != EParamTypes.UNDEFINED) && (len == s)) {
						data.Add(EnumExtensions.GetParamType(typeStr), reader.ReadBytes(s));
					}
					else {
						// throw new System.Exception("unknown type " + typeStr + " or incorrect length " + len);
						Debug.LogError("Unknown type '" + typeStr + "'");
						reader.ReadBytes(len);
					}
				}
				
				reader.Close();
				stream.Close();
				isLoaded = true;
				lastRendered = localTime;
			}
			else {
				UnityEngine.Debug.LogError("No file for cell " + xIndex + ", " + yIndex);
				InitNoData();
			}
		}
		if (!calculateSuccession) {
			targetMaps = null;
		}
//		Debug.LogWarning("cell " + GetCellName() + " isloading = false");
		isLoading = null;
	}
	
	public string GetCellName() {
		return "cell" + xIndex.ToString("X2") + yIndex.ToString("X2") + ".dat";
	}

		
	/**
	 * Saves cell if changes need to be saved
	 * will save when in editing mode, is changed or cell is in savefile directory
	 * and the path differs from old savefile directory (so in this case it needs
	 * to be copied to the new savefile directory and will be loaded first if not
	 * in memory).
	 */
	public void SaveIfNeeded(string path, bool needCopying, bool saveAll) {
		bool willWrite = false;
		if (!scenario.IsGameMode) willWrite = saveAll;
		if (isChanged) willWrite = true;
		if (dataInSaveGame && needCopying) willWrite = true;
		
		if (saveAll && !isLoaded) {
			if ((path == scenario.scenarioPath) ||
				(System.String.Compare(
				Path.GetFullPath(path).TrimEnd('\\'),
				Path.GetFullPath(scenario.scenarioPath).TrimEnd('\\'),
				System.StringComparison.InvariantCultureIgnoreCase) == 0)) {
				// we are writing to source directory, so we don't have
				// to copy at all!
				Debug.LogWarning("source dir is same as dest dir skip copy/write for " + GetCellName());
				return;
			}
			// we can just copy instead of load first and save again
			// this saves memory usage.
			string toFile = path + Path.DirectorySeparatorChar + GetCellName();
			string fromFile = scenario.scenarioPath + GetCellName();
			if (File.Exists(fromFile)) {
				Debug.Log("Quick copy '" + fromFile + "' -> '" + toFile + "'");
				File.Delete(toFile);
				File.Copy(fromFile, toFile);
			}
			return;
		}
		
		// willWrite will be true if the cell has to be written, either because we're editing instead of playing,
		// the cell was saved in a different savegame or it has changed.
		if (willWrite) {
			// Debug.Log("Going to write " + xIndex + "," + yIndex + " " + isChanged + " " + isLoaded);
			if (!isLoaded) {
				Load(); // if not in memory load it first!
			}
			if (isHeightmapChanged || (heightMap == null)) {
				GenerateHeightmap();
			}
			if (scenario.IsGameMode) dataInSaveGame = true;
			isChanged = false;
			
			// do the actual writing...
			FileStream stream = new FileStream(path + Path.DirectorySeparatorChar + GetCellName(), FileMode.Create);
			BinaryWriter writer = new BinaryWriter(stream);
			writer.Write(JConfig.FILE_VERSION);
			writer.Write("heights");
			writer.Write(heights.Length * 4);
			JIOUtils.WriteFA(writer, heights);
			writer.Write("water");
			writer.Write(waterHeights.Length * 4);
			JIOUtils.WriteFA(writer, waterHeights);
			if (adjustedWaterHeights != null) {
				writer.Write("adjustedwater");
				writer.Write(adjustedWaterHeights.Length * 4);
				JIOUtils.WriteFA(writer, adjustedWaterHeights);
			}
			writer.Write("succession");
			writer.Write(successionMap.Length);
			writer.Write(successionMap);
			writer.Write("vegetation");
			writer.Write(vegetationMap.Length);
			writer.Write(vegetationMap);
			writer.Write("variant");
			writer.Write(variantMap.Length);
			writer.Write(variantMap);
			writer.Write("succession");
			writer.Write(successionMap.Length);
			writer.Write(successionMap);
			writer.Write("special");
			writer.Write(specialMap.Length);
			writer.Write(specialMap);
			if ((heightMap != null) && !isHeightmapChanged) {
				writer.Write("heightmap");
				writer.Write(HM_SIZE * HM_SIZE * 4);
				for (int y = 0; y < HM_SIZE; y++) {
					for (int x = 0; x < HM_SIZE; x++) {
						writer.Write(heightMap[y, x]);
					}
				}
			}
			if (hasVisibleWater && (waterHeightMap != null)) {
				writer.Write("waterheightmap");
				writer.Write((CELL_SIZE + 1) * (CELL_SIZE + 1) * 5);
				for (int y = 0; y <= CELL_SIZE; y++) {
					for (int x = 0; x <= CELL_SIZE; x++) {
						writer.Write(waterHeightMap[y, x]);
						writer.Write(waterVisibleMap[y, x]);
					}
				}
			}
			if (successionBitmap != null) {
				calculateSuccession = false;
				foreach (byte v in successionBitmap) {
					if (v != 0) {
						calculateSuccession = true;
						break;
					}
				}
				if (calculateSuccession) {
					writer.Write("successionbitmap");
					writer.Write(successionBitmap.Length);
					writer.Write(successionBitmap);
				}
				else {
					successionBitmap = null;
				}
				if (targetMaps != null) {
					for (int i = 0; i < TARGET_MAP_COUNT; i++) {
						if (targetMaps[i] != null) {
							writer.Write("targetmap" + i);
							writer.Write(targetMaps[i].Length);
							writer.Write(targetMaps[i]);
						}
					}
				}
			}
			for (int i = 0; i < speciesData.Length; i++) {
				SpeciesData sd = speciesData[i];
				if (sd != null) {
					writer.Write("speciesbitmap");
					writer.Write(sd.bitmap.Length + 8);
					writer.Write(i);
					writer.Write(sd.count);
					writer.Write(sd.bitmap);
				}
			}
			for (int i = 0; i < animalSpeciesData.Length; i++) {
				SpeciesData sd = animalSpeciesData[i];
				if (sd != null) {
					writer.Write("animalspeciesbitmap");
					writer.Write(sd.bitmap.Length + 8);
					writer.Write(i);
					writer.Write(sd.count);
					writer.Write(sd.bitmap);
				}
			}
			if (data != null) {
				foreach (KeyValuePair<EParamTypes, byte[]> keyVal in data) {
					writer.Write(keyVal.Key.ToString());
					writer.Write(keyVal.Value.Length);
					writer.Write(keyVal.Value);
				}
			}
			int buildingsCount = 0;
			foreach (KeyValuePair<string,List<BuildingData>> kv in buildings) {
				buildingsCount += kv.Value.Count;
			}
			if (buildingsCount > 0) {
				writer.Write("buildings");
				writer.Write(buildingsCount);
				foreach (KeyValuePair<string,List<BuildingData>> kv in buildings) {
					foreach (BuildingData bd in kv.Value) {
						writer.Write(bd.building.name);
						JIOUtils.WriteVector3(writer, bd.pos);
						JIOUtils.WriteQuaternion(writer, bd.rotation);
						JIOUtils.WriteVector3(writer, bd.scale);
					}
				}
			}
			if (animals.Count > 0) {
				writer.Write("animals");
				writer.Write(animals.Count);
				foreach (AnimalData ad in animals) {
					writer.Write((byte) ad.t);
					writer.Write((byte) ad.x);
					writer.Write((byte) ad.y);
				}
			}
			if (successionAnimals.Count > 0) {
				writer.Write("successionanimals");
				writer.Write(successionAnimals.Count);
				foreach (AnimalData ad in successionAnimals) {
					writer.Write((byte) ad.t);
					writer.Write((byte) ad.x);
					writer.Write((byte) ad.y);
				}
			}
			writer.Close();
			stream.Close();
			hasData = true;
			lastRendered = localTime;
		}
		// TryRelease();
	}
	
	/**
	 * releases cell data if it isn't marked changed.
	 */
	public void TryRelease() {
		if (!isChanged) {
			data = null;
			hiResWaterHeightMap = null;
			heightMap = null;
			heights = null;
			waterHeights = null;
			waterHeightMap = null;
			adjustedWaterHeights = null;
			waterVisibleMap = null;
			successionMap = null;
			vegetationMap = null;
			variantMap = null;
			specialMap = null;
			successionMap = null;
			waterVisibleMap = null;
			targetMaps = null;
			isLoaded = false;
		}
	}
	
	/**
	 * WARNING Get/Set functions don't do bounds checking!
	 */
	
	/**
	 * hx = 4 <==> x = 0, so resolution is 4 times the resolution in both directions
	 * (so 16 times total) 
	 */
	public float getHiResHeight(int hx, int hy) {
		if (!isLoaded) Load();
		if ((heightMap == null) || (isHeightmapChanged)) {
			GenerateHeightmap();
		}
		return heightMap[hy, hx];
	}
	
	public float GetHeight(int x, int y) {
		if (!isLoaded) Load();
		return heights[(x + 1) + (y + 1) * (CELL_SIZE + 2)];
	}
	
	public float GetWaterHeight(int x, int y) {
		if (!isLoaded) Load();
		return waterHeights[(x + 1) + (y + 1) * (CELL_SIZE + 2)];
	}
	
	public float GetAdjustedWaterHeight(int x, int y) {
		if (!isLoaded) Load();
		if (adjustedWaterHeights != null) {
			return adjustedWaterHeights[(x + 1) + (y + 1) * (CELL_SIZE + 2)];
		}
		return waterHeights[(x + 1) + (y + 1) * (CELL_SIZE + 2)];
	}
	
	public int GetSuccessionIndex(int x, int y) {
		if (!isLoaded) Load();
		int index = x + (y << CELL_SIZE2EXP);
		return successionMap[index];
	}

	public int GetVegetationIndex(int x, int y) {
		if (!isLoaded) Load();
		int index = x + (y << CELL_SIZE2EXP);
		return vegetationMap[index];
	}
	
	public int GetTileIndex(int x, int y) {
		if (!isLoaded) Load();
		int index = x + (y << CELL_SIZE2EXP);
		return variantMap[index];
	}
		
	public JXSuccession GetSuccession(int x, int y) {
		if (!isLoaded) Load();
		int index = x + (y << CELL_SIZE2EXP);
		int successionIndex = successionMap[index];
		return scenario.successions[successionIndex];
	}
	
	public JXVegetation GetVegetation(int x, int y) {
		if (!isLoaded) Load();
		int index = x + (y << CELL_SIZE2EXP);
		int successionIndex = successionMap[index];
		int vegetationIndex = vegetationMap[index];
		return scenario.successions[successionIndex].vegetation[vegetationIndex];
	}
	
	public JXVegetation GetVegetation(int x, int y, out int variantIndex) {
		if (!isLoaded) Load();
		int index = x + (y << CELL_SIZE2EXP);
		int successionIndex = successionMap[index];
		int vegetationIndex = vegetationMap[index];
		variantIndex = variantMap[index];
		return scenario.successions[successionIndex].vegetation[vegetationIndex];
	}

	public JXTerrainTile GetVegetationTile(int x, int y) {
		if (!isLoaded) Load();
		int index = x + (y << CELL_SIZE2EXP);
		int successionIndex = successionMap[index];
		int vegetationIndex = vegetationMap[index];
		int variantIndex = variantMap[index];
		return scenario.successions[successionIndex].vegetation[vegetationIndex].tiles[variantIndex];
	}
	
	
	
	public byte GetData(EParamTypes t, int x, int y) {
		if (!isLoaded) Load();
		if ((int) t >= 80) return GetCombinedData(t, x, y);
		if (!data.ContainsKey(t)) return 0;
		int index = x + (y << CELL_SIZE2EXP);
		return data[t][index];
	}
	
	public byte GetCombinedData(EParamTypes t, int x, int y) {
		if (!scenario.combinedData.ContainsKey(t)) return 0;
		JScenario.CombinedData cd = scenario.combinedData[t];
		
		float val = (float) cd.offset;
		for (int i = 0; i < cd.parameters.Length; i++) {
			int p = GetData(cd.parameters[i], x, y);
			val += cd.multipliers[i] * p;
		}
		int result = Mathf.Clamp((int) val, 0, 255);
		return (byte) result;
		
	}
	
	public void ClearSpecialWithMask(int mask) {
		if (!isLoaded) Load();
		for (int i = CELL_SIZE * CELL_SIZE - 1; i >= 0; i--) {
			specialMap[i] = (byte) (specialMap[i] & mask);
		}
		isChanged = true;
	}
	
	public byte GetSpecial(int x, int y) {
		if (!isLoaded) Load();
		int index = x + (y << CELL_SIZE2EXP);
		return specialMap[index];
	}
	
	public void SetHeight(int x, int y, float v) {
		if (!isLoaded) Load();
		heights[(x + 1) + (y + 1) * (CELL_SIZE + 2)] = v;
		isChanged = true;
		isHeightmapChanged = true;
		areTilesChanged = true;
		if ((x == 0) || (y == 0) || (x == CELL_SIZE - 1) || (y == CELL_SIZE - 1)) {
			HandleBorders(x, y, v, EHandleSpecial.heights);
		}
	}
	
	public void SetWaterHeight(int x, int y, float v) {
		if (!isLoaded) Load();
		waterHeights[(x + 1) + (y + 1) * (CELL_SIZE + 2)] = v;
		if ((x == 0) || (y == 0) || (x == CELL_SIZE - 1) || (y == CELL_SIZE - 1)) {
			HandleBorders(x, y, v, EHandleSpecial.waterHeights);
		}
		isChanged = true;
		isHeightmapChanged = true;
	}

	public void SetAdjustedWaterHeight(int x, int y, float v) {
		if (!isLoaded) Load();
		if (adjustedWaterHeights == null) {
			if (v == GetWaterHeight(x, y)) return;
			MakeAdjustedWaterHeights();
		}
		adjustedWaterHeights[(x + 1) + (y + 1) * (CELL_SIZE + 2)] = v;
		if ((x == 0) || (y == 0) || (x == CELL_SIZE - 1) || (y == CELL_SIZE - 1)) {
			HandleBorders(x, y, v, EHandleSpecial.adjustedWaterHeights);
		}
		isChanged = true;
		isHeightmapChanged = true;
	}
	
	public void MakeAdjustedWaterHeights() {
		if (adjustedWaterHeights != null) return;
		adjustedWaterHeights = new float[waterHeights.Length];
		waterHeights.CopyTo(adjustedWaterHeights, 0);
	}
	
	// can use variant == -1 to automatically choose a variation
	public void SetVegetation(int x, int y, JXVegetation vegetation, int variant) {
		if (!isLoaded) Load();
		int index = x + (y << CELL_SIZE2EXP);
		successionMap[index] = (byte) (vegetation.succession.index);
		vegetationMap[index] = (byte) (vegetation.index);
		if (variant < 0) {
			int variations = vegetation.tiles.Length;
			if (variations == 1) variant = 0;
			else if (variations == 2) variant = 1;
			else variant = RndRange(1, variations);
			
		}
		variantMap[index] = (byte) variant;
		isChanged = true;
		areTilesChanged = true;
	}
	
	public void SetVegetation(int x, int y, JXTerrainTile tile) {
		if (!isLoaded) Load();
		int index = x + (y << CELL_SIZE2EXP);
		successionMap[index] = (byte) (tile.vegetation.succession.index);
		vegetationMap[index] = (byte) (tile.vegetation.index);
		variantMap[index] = (byte) (tile.index);
		isChanged = true;
		areTilesChanged = true;
	}
	
	public void SetData(EParamTypes t, int x, int y, byte v) {
		if (!isLoaded) Load();
		if (!data.ContainsKey(t)) {
			if (v == 0) return;
			data.Add(t, new byte[CELL_SIZE * CELL_SIZE]);
		}
		int index = x + (y << CELL_SIZE2EXP);
		byte[] bytes = data[t];
		if (bytes[index] != v) {
			bytes[index] = v;
			isChanged = true;
		}
	}

	public void SetSpecial(int x, int y, byte v) {
		if (!isLoaded) Load();
		int index = x + (y << CELL_SIZE2EXP);
		specialMap[index] = v;
		isChanged = true;
	}

	public void SetSpecial(int x, int y, byte v, byte mask) {
		if (!isLoaded) Load();
		int index = x + (y << CELL_SIZE2EXP);
		int val = specialMap[index];
		int newVal = (val & ~mask) | (v & mask);
		if (newVal != val) {
			specialMap[index] = (byte) newVal;
			isChanged = true;
			if (mask == 0x07) isHeightmapChanged = true; // sloten hack
		}
	}
	
	/**
	 * As the height, water, adjustedwater maps have a border shared
	 * with neigbour cells, we need to update neighbours in some occasions.
	 */
	private void HandleBorders(int x, int y, float v, EHandleSpecial special) {
		JTerrainData data = scenario.data;

		
		int minX = ((x == 0) && (xIndex > 0))?(xIndex - 1):(xIndex);
		int maxX = ((x == CELL_SIZE - 1) && (xIndex < data.cWidth - 1))?(xIndex + 1):(xIndex);
		int minY = ((y == 0) && (yIndex > 0))?(yIndex - 1):(yIndex);
		int maxY = ((y == CELL_SIZE - 1) && (yIndex < data.cHeight - 1))?(yIndex + 1):(yIndex);
		
		for (int yc = minY; yc <= maxY; yc++) {
			for (int xc = minX; xc <= maxX; xc++) {
				if ((xc != xIndex) || (yc != yIndex)) {
					int xx = (xc < xIndex)?(CELL_SIZE + 1):((xc > xIndex)?0:(x+1));
					int yy = (yc < yIndex)?(CELL_SIZE + 1):((yc > yIndex)?0:(y+1));
					JCellData neighbour = data.cells[xc + yc * data.cWidth];
					if (!neighbour.isLoaded) neighbour.Load();
					int index = xx + yy * (CELL_SIZE + 2);
					switch (special) {
					case EHandleSpecial.heights :
						neighbour.heights[index] = v;
						neighbour.isHeightmapChanged = true; 
						break;
					case EHandleSpecial.waterHeights :
						neighbour.waterHeights[index] = v;
						neighbour.isHeightmapChanged = true;
						break;
					case EHandleSpecial.adjustedWaterHeights :
						if (neighbour.adjustedWaterHeights == null) {
							neighbour.MakeAdjustedWaterHeights();
						}
						neighbour.adjustedWaterHeights[index] = v;
						neighbour.isHeightmapChanged = true;
						break;
					}
					neighbour.isChanged = true;
				}
			}
		}
	}
	
	public string GetDebugHeight(int x, int y) {
		if (!isLoaded) {
			Load();
		}
		string result = "";
		for (int yy = 2; yy >= 0; yy--) {
			for (int xx = 0; xx <= 2; xx++) {
				result += " " + heights[(x + xx) + (CELL_SIZE + 2) * (y + yy)].ToString("00.00");
			}
			result += "\n";
		}
		for (int yy = 3; yy >= 0; yy--) {
			for (int xx = 0; xx <= 3; xx++) {
				result += " " + heightMap[(y << 2) + yy, (x << 2) + xx].ToString("00.00");
			}
			result += "\n";
		}
		return result;
	}
	
	public float GetCornerHeight(int x, int y) {
		if (!isLoaded) {
			Load();
		}
		if (isHeightmapChanged || (heightMap == null)) {
			GenerateHeightmap();
		}
		return heightMap[y << 2, x << 2];
	}
	
	void DoGravenSloot(int x, int y) {
		SetSpecial(x, y, 0x03, 0x07);
		JXVegetation veg = GetVegetation(x, y);
		SetVegetation(x, y, veg, 0);
	}

	void DoDempenSloot(int x, int y) {
		SetSpecial(x, y, 0x00, 0x07);
		JXVegetation veg = GetVegetation(x, y);
		int count = veg.tiles.Length;
		if (count > 1) {
			SetVegetation(x, y, veg, RndRange(1, count));
		}
	}

	void DoVerdiepenSloot(int x, int y) {
		int oldDepth = GetSpecial(x, y) & 0x07;
		if (oldDepth == 0) {
			JXVegetation veg = GetVegetation(x, y);
			SetVegetation(x, y, veg, 0);
		}
		if (oldDepth < 7) {
			SetSpecial(x, y, (byte) (oldDepth + 1), 0x07);
		}
	}

	void DoVerontdiepenSloot(int x, int y) {
		int oldDepth = GetSpecial(x, y) & 0x07;
		if (oldDepth > 0) {
			if (oldDepth == 1) {
				JXVegetation veg = GetVegetation(x, y);
				SetVegetation(x, y, veg, 0);
				SetSpecial(x, y, (byte) (oldDepth - 1), 0x07);
			}
		}
	}

	
	public bool CalculateSuccession(int x, int y, EActionTypes action) {
		if (!isLoaded) {
			Load();
		}
		bool clearData = false;
		switch (action) {
		case EActionTypes.Aanplanten :
		case EActionTypes.AfgravenPN :
		case EActionTypes.Bemesten :
		case EActionTypes.Branden :
		case EActionTypes.Kappen :
		case EActionTypes.OpenenKroonlaag :
		case EActionTypes.PadenAfsluiten :
		case EActionTypes.PadenBetreden :
		case EActionTypes.PlaggenDreggen :
		case EActionTypes.Uitmijnen :
			clearData = true;
			break;					
		case EActionTypes.GravenSloot :
			clearData = true;
			DoGravenSloot(x, y);
			break;					
		case EActionTypes.DempenSloot :
			clearData = true;
			DoDempenSloot(x, y);
			break;					
		case EActionTypes.VerdiepenSloot :
			clearData = true;
			DoVerdiepenSloot(x, y);
			break;					
		case EActionTypes.VerontdiepenSloot :
			clearData = true;
			DoVerontdiepenSloot(x, y);
			break;
		default :
			break;
		}
		int variant;
		JXVegetation vegetation = GetVegetation(x, y, out variant);
		foreach (JXVegetation.Step step in vegetation.steps) {
			if ((step.action == action) && (step.chance >= RndValue())) {
				bool match = true;
				foreach (JXVegetation.ParamCondition cnd in step.conditions) {
					byte val = GetData(cnd.type, x, y);
					if ((val < cnd.minRange) || (val > cnd.maxRange)) {
						match = false; // failed to match...
						break;
					}
				}
				if (match) {
					isChanged = true;
					areTilesChanged = true;
					// we need to change!
					JXVegetation newVeg = vegetation.succession.vegetation[step.newVegetation];
					if (vegetation.index != step.newVegetation) {
						// we only actually change when vegetation is different
						// this to prevent setting data values
						if (variant != 0) {
							variant = RndRange(1, newVeg.tiles.Length);
						}
						SetVegetation(x, y, newVeg, variant);
						foreach (JXVegetation.NewParam np in newVeg.newParams) {
							byte val = (byte) RndRange((int) np.minRange, (int) np.maxRange + 1);
							SetData(np.type, x, y, val);
						}
						foreach (JXVegetation.ParamChange pc in newVeg.changes) {
							if ((pc.action == action) && (pc.chance >= RndValue())) {
								foreach (JXVegetation.ParamDelta pd in pc.deltas) {
									int val = Mathf.Clamp((int) GetData(pd.type, x, y) + pd.delta, pd.minRange, pd.maxRange);
									SetData(pd.type, x, y, (byte) val);
								}
							}
						}
						NormalizeSoilParams(x, y);
					}
					SetSpecial(x, y, (byte) ((newVeg.isStable)?0xc0:0x40), (byte) (0xc0));
					return clearData;
				}
			}
		}					
		
		return clearData;
	}
	
	public void NormalizeSoilParams(int x, int y) {
		int zand = GetData(EParamTypes.Zand, x, y);
		int klei = GetData(EParamTypes.Klei, x, y);
		int silt = GetData(EParamTypes.Silt, x, y);
		int veen = GetData(EParamTypes.Veen, x, y);
		int total = zand + klei + silt + veen;
		if (total != 255) {
			if (total == 0) {
				if (successionBitmap == null) return; // no need to normalize if cell hasn't succession on it
				SetData(EParamTypes.Zand, x, y, (byte) 255);
				return;
			}
			float factor = 255f / total;
			klei = (int) (factor * klei);
			silt = (int) (factor * silt);
			veen = (int) (factor * veen);
			zand = 255 - klei - silt - veen;
			SetData(EParamTypes.Zand, x, y, (byte) zand);
			SetData(EParamTypes.Klei, x, y, (byte) klei);
			SetData(EParamTypes.Silt, x, y, (byte) silt);
			SetData(EParamTypes.Veen, x, y, (byte) veen);
		}
	}

	public BuildingData AddBuilding(Vector3 pos, Quaternion rot, Vector3 scale, JElements.BuildingWrapper building) {
		if (!isLoaded) {
			Load();
		}
		BuildingData bd = new BuildingData();
		bd.building = building;
		bd.pos = pos;
		bd.rotation = rot;
		bd.scale = scale;
		if (buildings.ContainsKey(building.matName)) {
			buildings[building.matName].Add(bd);
		}
		else {
			List<BuildingData> bdList = new List<BuildingData>();
			bdList.Add(bd);
			buildings.Add(building.matName, bdList);
		}
		return bd;
		// isChanged = true;
	}	

	static float[] weightsT = new float[] { 0.875f, 0.625f, 0.375f, 0.125f };
	static float[] weightsRT = new float[] { 0.125f, 0.375f, 0.625f, 0.875f };
	
	
	const float slootDelta = 0.2f;
	const float slootOffset = 1.5f;
	
	private void MakeSloot(JTerrainData data, int x, int y, int sloot) {
		int cx = x << 2;
		int cy = y << 2;
		// heights[cy + 1, cx + 1] -= slootDelta;
		// heights[cy + 1, cx + 2] -= slootDelta;
		// heights[cy + 2, cx + 1] -= slootDelta;
		heightMap[cy + 2, cx + 2] -= sloot * slootDelta + slootOffset;
		
		x += (xIndex << CELL_SIZE2EXP);
		y += (yIndex << CELL_SIZE2EXP);
		
		int special = data.GetSpecial(x - 1, y) & 0x07;
		if (special != 0) {
			// heights[cy + 1, cx] -= slootDelta;
			heightMap[cy + 2, cx] -= special * slootDelta + slootOffset;
			heightMap[cy + 2, cx + 1] -= special * slootDelta + slootOffset;
		}
		special = data.GetSpecial(x + 1, y) & 0x07;
		if (special != 0) {
			// heights[cy + 1, cx + 3] -= special * slootDelta + slootOffset;
			heightMap[cy + 2, cx + 3] -= special * slootDelta + slootOffset;
		}
		special = data.GetSpecial(x, y - 1) & 0x07;
		if (special != 0) {
			// heights[cy, cx + 1] -= special * slootDelta + slootOffset;
			heightMap[cy, cx + 2] -= special * slootDelta + slootOffset;
			heightMap[cy + 1, cx + 2] -= special * slootDelta + slootOffset;
		}
		special = data.GetSpecial(x, y + 1) & 0x07;
		if (special != 0) {
			// heights[cy + 3, cx + 1] -= special * slootDelta + slootOffset;
			heightMap[cy + 3, cx + 2] -= special * slootDelta + slootOffset;
		}
	}
	
	public void GenerateHRWaterHeightmap() {
		if ((waterHeights == null) || (!hasVisibleWater)) {
			hiResWaterHeightMap = null;
			return;
		}
		if (hiResWaterHeightMap == null) {
			hiResWaterHeightMap = new float[HM_SIZE, HM_SIZE];
		}
		const int row = CELL_SIZE + 2;
		for (int y = 0; y < HM_SIZE; y++) {
			int ys = y % 4;
			for (int x = 0; x < HM_SIZE; x++) {
				int xs = x % 4;
				int x0 = x >> 2;
				int y0 = y >> 2;
				
				
				float h00 = waterHeights[x0 + row * y0];
				float h01 = waterHeights[x0 + row * (y0 + 1)];
				float h10 = waterHeights[x0 + 1 + row * y0];
				float h11 = waterHeights[x0 + 1 + row * (y0 + 1)];
				float h = h00 * weightsT[xs] * weightsT[ys] + h01 * weightsT[xs] * weightsRT[ys] +
					h10 * weightsRT[xs] * weightsT[ys] + h11 * weightsRT[xs] * weightsRT[ys];
				float lh = heightMap[y, x];
				hiResWaterHeightMap[y, x] = (h > lh)?h:lh;
			}
		}
	}
	
	public void GenerateHeightmap() {
		waterHeightMap = null;
		if (!isLoaded) Load();
//		Debug.Log(GetCellName() + " is generating heightmap");
		if (heightMap == null) {
			heightMap = new float[HM_SIZE, HM_SIZE];
		}
		const int row = CELL_SIZE + 2;
		for (int y = 0; y < HM_SIZE; y++) {
			int ys = y % 4;
			for (int x = 0; x < HM_SIZE; x++) {
				int xs = x % 4;
				int x0 = x >> 2;
				int y0 = y >> 2;
				
				
				float h00 = heights[x0 + row * y0];
				float h01 = heights[x0 + row * (y0 + 1)];
				float h10 = heights[x0 + 1 + row * y0];
				float h11 = heights[x0 + 1 + row * (y0 + 1)];
				float h = h00 * weightsT[xs] * weightsT[ys] + h01 * weightsT[xs] * weightsRT[ys] +
					h10 * weightsRT[xs] * weightsT[ys] + h11 * weightsRT[xs] * weightsRT[ys];
				heightMap[y, x] = h;
			}
		}
		float[] hgrid = (adjustedWaterHeights != null)?adjustedWaterHeights:waterHeights; 
		float[,] wh = new float[CELL_SIZE + 1, CELL_SIZE + 1];
		bool[,] pointVisible = new bool[CELL_SIZE + 1, CELL_SIZE + 1];
		int count = 0;
		for (int y = 0; y <= CELL_SIZE; y++) {
			for (int x = 0; x <= CELL_SIZE; x++) {
				float h = (hgrid[x + y * row] + hgrid[x + y * row + 1] +
				           hgrid[x + (y + 1) * row] + hgrid[x + (y + 1) * row + 1]) * 0.25f;
				wh[y, x] = h;
				if (h > heightMap[y << 2, x << 2]) {
					pointVisible[y, x] = true;
					count++;
				}
			}
		}
		JTerrainData data = scenario.data;
		int p = 0;
		for (int y = 0; y < CELL_SIZE; y++) {
			for (int x = 0; x < CELL_SIZE; x++) {
				if ((specialMap[p] & 0x07) != 0) {
					MakeSloot(data, x, y, specialMap[p] & 0x07);
					pointVisible[y, x] = true;
					pointVisible[y, x + 1] = true;
					pointVisible[y + 1, x] = true;
					pointVisible[y + 1, x + 1] = true;
					count += 4;
				}
				p++;
			}
		}
		// Debug.Log("cell " + xIndex + ":" + yIndex + " count = " + count + " use adjusted water map: " + (adjustedWaterHeights != null));
		if (count == 0) {
			hasVisibleWater = false;
			waterVisibleMap = null;
			waterHeightMap = null;
		}
		else {
			hasVisibleWater = true;
			waterHeightMap = wh;
			waterVisibleMap = pointVisible;			
		}
		isHeightmapChanged = false;
	}
	

	public Mesh MakeWaterMesh() {
		if (!hasVisibleWater) {
			return null;
		}
		int indice = 0;
		bool alternate = true;
		// we store indice + 1 in array, so we can use the default 0 value as not defined
		int[,] vertexIndices = new int[CELL_SIZE + 1, CELL_SIZE + 1];
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uvs = new List<Vector2>();
		List<int> indicesList = new List<int>();
		for (int y = 0; y < CELL_SIZE; y++) {
			for (int x = 0; x < CELL_SIZE; x++) {
				int yp = alternate?(y+1):y;
				if (waterVisibleMap[yp, x + 1] || waterVisibleMap[y, x] || waterVisibleMap[y + 1, x]) {
					if (vertexIndices[yp, x + 1] == 0) {
						indicesList.Add(indice++);
						vertexIndices[yp, x + 1] = indice;
						vertices.Add(new Vector3(x + 1, waterHeightMap[yp, x + 1], yp));
						uvs.Add(new Vector2(yp, x + 1));
					}
					else {
						indicesList.Add(vertexIndices[yp, x + 1] - 1);
					}
					if (vertexIndices[y, x] == 0) {
						indicesList.Add(indice++);
						vertexIndices[y, x] = indice;
						vertices.Add(new Vector3(x + 1, waterHeightMap[y, x], y));
						uvs.Add(new Vector2(y, x));
					}
					else {
						indicesList.Add(vertexIndices[y, x] - 1);
					}
					if (vertexIndices[y + 1, x] == 0) {
						indicesList.Add(indice++);
						vertexIndices[y + 1, x] = indice;
						vertices.Add(new Vector3(x, waterHeightMap[yp, x + 1], y + 1));
						uvs.Add(new Vector2(y + 1, x));
					}
					else {
						indicesList.Add(vertexIndices[y + 1, x] - 1);
					}
				}
				yp = alternate?y:(y+1);
				if (waterVisibleMap[yp, x] || waterVisibleMap[y, x + 1] || waterVisibleMap[y + 1, x + 1]) {
					if (vertexIndices[yp, x] == 0) {
						indicesList.Add(indice++);
						vertexIndices[yp, x] = indice;
						vertices.Add(new Vector3(x, waterHeightMap[yp, x + 1], yp));
						uvs.Add(new Vector2(yp, x));
					}
					else {
						indicesList.Add(vertexIndices[yp, x] - 1);
					}
					if (vertexIndices[y + 1, x + 1] == 0) {
						indicesList.Add(indice++);
						vertexIndices[y + 1, x + 1] = indice;
						vertices.Add(new Vector3(x + 1, waterHeightMap[yp, x + 1], y + 1));
						uvs.Add(new Vector2(y + 1, x + 1));
					}
					else {
						indicesList.Add(vertexIndices[y + 1, x + 1] - 1);
					}
					if (vertexIndices[y, x + 1] == 0) {
						indicesList.Add(indice++);
						vertexIndices[y, x + 1] = indice;
						vertices.Add(new Vector3(x + 1, waterHeightMap[yp, x + 1], y));
						uvs.Add(new Vector2(y, x + 1));
					}
					else {
						indicesList.Add(vertexIndices[y, x + 1] - 1);
					}
					alternate = !alternate;
				}
			}
		}
		Mesh m = new Mesh();
		m.vertices = vertices.ToArray();
		m.uv = uvs.ToArray();
		m.triangles = indicesList.ToArray();
		m.Optimize();
		m.RecalculateBounds();
		m.RecalculateNormals();
		return m;
	}
	
	
	public void CopyHeightmap(float[,] toMap, int offsetX, int offsetY) {
		if (!isLoaded) Load();
		if ((heightMap == null) || isHeightmapChanged) {
			GenerateHeightmap();
		}
		for (int y = 0; y < HM_SIZE; y++) {
			for (int x = 0; x < HM_SIZE; x++) {
				toMap[y + offsetY, x + offsetX] = heightMap[y, x] / VERTICAL_SCALE;
			}
		}
	}

	private float TreeHeight(float x, float y) {
		int cx = (int) (x * 4);
		int cy = (int) (y * 4);
		float xs = (x * 4) - cx;
		float ys = (y * 4) - cy;
		float h00 = heightMap[cy, cx];
		float h01 = heightMap[cy, cx + 1];
		float h10 = heightMap[cy + 1, cx + 1];
		float h11 = heightMap[cy + 1, cx + 1];
		
		return h00 * (1 - xs) * (1 - ys) + h01 * xs * (1 - ys) + h10 * (1 - xs) * ys + h11 * xs * ys;
	}
	
	public float RndValue() {
		return (float) rnd.NextDouble();
	}
	
	public float RndRange(float min, float max) {
		if (min >= max) return min;
		return min + (max - min) * (float) rnd.NextDouble();
	}

	public Color RndRange(Color min, Color max) {
		return Color.Lerp(min, max, (float) rnd.NextDouble());
	}

	public int RndRange(int min, int max) {
		if (min >= max) return min;
		return (rnd.Next() % (max - min)) + min;
	}
	
	public void RemoveAllObjects() {
		if (cachedStickerGO) {
			GameObject.Destroy(cachedStickerGO);
			cachedStickerGO = null;
		}
		if (renderedObjects != null) {
			foreach (GameObject go in renderedObjects) {
				GameObject.Destroy(go);
			}
			renderedObjects = null;
		}
	}
	/**
	 * lost visibility, objects need to be removed
	 */
	public void BecameInvisible() {
		RemoveAllObjects();
		if (isVisible) {
			lastRendered = localTime;		
		}
		isVisible = false;
	}
	
	public bool TryCleanup() {
		// Debug.Log("Cell " + xIndex + "," + yIndex + " isVisible " + isVisible + " isLoaded " + isLoaded + " last Rendered " + lastRendered);
		if (!isVisible && isLoaded && (lastRendered + 60.0f < localTime)) {
			cachedTrees = null;
			renderObjects = null;
			if (renderedObjects != null) {
				Debug.LogWarning("Try cleanup " + GetCellName() + " rendered objects != null");
				RemoveAllObjects();
			}
			// Debug.LogWarning("Cleanup tile " + xIndex + "," + yIndex + " isChanged = " + isChanged);
			if (!isChanged) {
				TryRelease();
			}
			return true;
		}
		return false;
	}
	
	public void PrepareAsyncPart() {
		if (cachedTrees == null) {
			cachedTrees = new List<TreeInstance>();
			areTilesChanged = true;
		}
		
		if (areTilesChanged) {
			// Possibly get this out of here if we're going to multithread.
			cachedTrees.Clear();
			RemoveAllObjects();
		}
		
		if (hasVisibleWater && (hiResWaterHeightMap == null)) {
			GenerateHRWaterHeightmap();
		}
		
		if (areTilesChanged || (sticker == null)) {
			sticker = new JRenderTerrainSticker(JElements.self.layerPrototypes.Length, CELL_SIZE, 0.5f);
		}
	}
	
	public void AddElementsAsyncPart(float[,,] alpha, JRenderTerrain.DetailMap[] detailMaps, int offsetX, int offsetY) {
		if (!isLoaded) Load();
		int index = 0;
		JXSuccession[] succession = scenario.successions;
		
		JXTEExtraTileLayerPrototype[] stickerPrototypes = JElements.self.layerPrototypes;
		
		bool generateRenderObjects = false;
		if (renderObjects == null) {
			renderObjects = new Dictionary<JElements.PrefabWrapper, List<GOData>>();
			generateRenderObjects = true;
		}

		System.Random oldRnd = rnd;
		for (int y = 0; y < CELL_SIZE; y++) {
			for (int x = 0; x < CELL_SIZE; x++) {
				rnd = new System.Random((offsetX + x) | (y + offsetY) << 12);
				int successionIndex = successionMap[index];
				int vegetationIndex = vegetationMap[index];
				int variantIndex = variantMap[index];
				JXTerrainTile tile = succession[successionIndex].vegetation[vegetationIndex].tiles[variantIndex];
				alpha[y + offsetY, x + offsetX, 0] = tile.am0;
				alpha[y + offsetY, x + offsetX, 1] = tile.am1;
				alpha[y + offsetY, x + offsetX, 2] = tile.am2;
				alpha[y + offsetY, x + offsetX, 3] = 1f - tile.am2 - tile.am1 - tile.am0;
				
				if (areTilesChanged) {
					foreach (int tileLayerIndex in tile.extraLayers) {
						if (stickerPrototypes[tileLayerIndex].useWaterHeights) {
							if ((hasVisibleWater) && (hiResWaterHeightMap != null)) {
								sticker.AddTile(tileLayerIndex, x, y, hiResWaterHeightMap);
							}
						}
						else {
							sticker.AddTile(tileLayerIndex, x, y, heightMap);
						}
					}
					
					foreach (JXTerrainTileTreeData td in tile.trees) {
						TreeInstance tree = new TreeInstance();
						tree.prototypeIndex = td.prototypeIndex;
	
						float tx = td.x;
						float ty = td.y;
						float rad = td.r;
						if (rad > 0f) {
							float angle = (float) rnd.NextDouble() * Mathf.PI * 2;
							rad = rad * (float) rnd.NextDouble();
							tx += Mathf.Sin(angle) * rad;
							ty += Mathf.Cos(angle) * rad;
						}
						tx = x + Mathf.Clamp(tx, 0f, 0.999f);
						ty = y + Mathf.Clamp(ty, 0f, 0.999f);
						float treeHeight = TreeHeight(tx, ty);
					
						tree.position = new Vector3(tx / (float) CELL_SIZE, treeHeight / VERTICAL_SCALE, ty / (float) CELL_SIZE);
						tree.prototypeIndex = td.prototypeIndex;
						tree.heightScale = RndRange(td.minHeight, td.maxHeight);
						tree.widthScale = tree.heightScale * RndRange(td.minWidthVariance, td.maxWidthVariance);
						Color c = RndRange(td.colorFrom, td.colorTo);
						tree.color = c;
						tree.lightmapColor = Color.white;
					
						cachedTrees.Add(tree);
					}
				}
				
				if (generateRenderObjects) {
					foreach (JXTerrainTileObjectData od in tile.objects) {
						GOData god = new GOData();
						// god.prefab = JElements.GetTileObjectPrefab(od.index);
						
						JElements.PrefabWrapper prefab = JElements.self.wrappedObjectPrefabs[od.index];
						god.rotation = Quaternion.Euler(0f, od.angle, 0f);
						float tx = od.x;
						float ty = od.y;
						float rad = od.r;
						if (rad > 0f) {
							float angle = (float) rnd.NextDouble() * Mathf.PI * 2;
							rad = rad * (float) rnd.NextDouble();
							tx += Mathf.Sin(angle) * rad;
							ty += Mathf.Cos(angle) * rad;
						}
						tx = x + Mathf.Clamp(tx, 0f, 0.999f);
						ty = y + Mathf.Clamp(ty, 0f, 0.999f);
						god.pos = new Vector3(tx, TreeHeight(tx, ty), ty);
						float heightScale = JUtil.RndRange(ref rnd, od.minHeight, od.maxHeight);
						float widthScale = heightScale * JUtil.RndRange(ref rnd, od.minWidthVariance, od.maxWidthVariance);
						god.scale = new Vector3(widthScale, heightScale, widthScale);
						// cachedObjects.Add(god);
						if (renderObjects.ContainsKey(prefab)) {
							renderObjects[prefab].Add(god);
						}
						else {
							List<GOData> list = new List<GOData>();
							list.Add(god);
							renderObjects.Add(prefab, list);
						}
					}
				}
				for (int i = 0; i < tile.detailCounts.Length; i++) {
					int count = tile.detailCounts[i];
					if (count > 0) {
						JRenderTerrain.DetailMap d = detailMaps[i];
						if (d.isEmpty) {
							d.map = new int[CELL_SIZE, CELL_SIZE];
							d.isEmpty = false;
						}
						d.map[y,x] = count;
					}
				}
				index++;
			}
		}
		rnd = oldRnd;
		areTilesChanged = false;
		isVisible = true;
	}
		
	public void AddTrees(List<TreeInstance> trees, int offsetX, int offsetY, int resolution) {
		if (cachedTrees != null) {
			float ox = (float) offsetX / (float) resolution;
			float oy = (float) offsetY / (float) resolution;
			float scale =  (float) CELL_SIZE / (float) resolution;
			foreach (TreeInstance tree in cachedTrees) {
				TreeInstance newTree = tree;
				Vector3 pos = tree.position;
				newTree.position = new Vector3(pos.x * scale + ox, pos.y, pos.z * scale + oy);
				trees.Add(newTree);
			}
		}
	}
	
	private Material[] MakeStickerMesh(JRenderTerrainSticker sticker, ref Mesh mesh) {
		if (sticker == null) {
			Debug.LogError("No sticker!");
			return null;
		}
		if (sticker.points == null) {
			Debug.LogError("No sticker points!");
			return null;
		}
		if (mesh == null) mesh = new Mesh();
		mesh.vertices = sticker.points.ToArray();
		mesh.uv = sticker.uvs.ToArray();
		List<int[]> indiceLists = new List<int[]>();
		List<Material> materials = new List<Material>();
		for (int i = 0; i < JElements.self.layerPrototypes.Length; i++) {
			if (sticker.indices[i] != null) {
				indiceLists.Add(sticker.indices[i].ToArray());
				materials.Add(JElements.GetTileLayerPrototype(i).material);
			}
		}
		if (materials.Count == 0) return null;
		mesh.subMeshCount = indiceLists.Count;
		for (int i = 0; i < indiceLists.Count; i++) {
			mesh.SetTriangles(indiceLists[i], i);
		}
		// mesh.Optimize();
		mesh.RecalculateBounds();
		mesh.RecalculateNormals();
		return materials.ToArray();
	}
	
	public void AddObjects(GameObject parent, float offsetX, float offsetY, float scale) {
		if (renderedObjects != null) return;
		Transform parentT = parent.transform;
		
		if (renderedObjects == null) {
			renderedObjects = new List<GameObject>();
		}
		else if (renderedObjects.Count > 0) {
			foreach (GameObject go in renderedObjects) {
				GameObject.Destroy(go);
			}
			renderedObjects.Clear();
			Debug.LogError("Needed to clear rendered objects!");
		}
		if (cachedStickerGO) {
			GameObject.Destroy(cachedStickerGO);
			cachedStickerGO = null;
			Debug.LogError("Needed to clear sticker object!");
		}
		Vector3 scaleVector = new Vector3(scale, 1f, scale);
		
		foreach (KeyValuePair<JElements.PrefabWrapper, List<GOData>> kv in renderObjects) {
			GameObject prefab = kv.Key.prefab;
			int count = kv.Value.Count;
			if (count == 0) {
				Debug.LogError("Shouldn't happen!");
			}
			else if (count == 1) {
				GOData god = kv.Value[0];
				GameObject go = (GameObject) GameObject.Instantiate(prefab);
				Transform cot = go.transform;
				cot.parent = parent.transform;
				Vector3 pos = god.pos;
				pos.Scale(scaleVector);
				cot.localPosition = new Vector3(offsetX, 0f, offsetY) + pos;
				cot.localRotation = god.rotation;
				cot.localScale = god.scale;
				go.layer = JLayers.L_TERRAIN;
				renderedObjects.Add(go);
			}
			else {
				Mesh baseMesh = prefab.GetComponent<MeshFilter>().sharedMesh;
				int vertexCount = baseMesh.vertexCount;
				Material baseMaterial = prefab.renderer.sharedMaterial;
				
				List<CombineInstance> combineInstances = new List<CombineInstance>();
				int totalVertices = 0;
				foreach (GOData data in kv.Value) {
					Matrix4x4 t = new Matrix4x4();
					Vector3 pos = data.pos;
					pos.Scale(scaleVector);
					t.SetTRS(pos, data.rotation, data.scale);
					CombineInstance i = new CombineInstance();
					i.mesh = baseMesh;
					i.subMeshIndex = 0;
					i.transform = t;
					if (totalVertices + vertexCount >= 65000) {
						renderedObjects.Add(MakeGOOfMeshInstances(parentT, "objects " + prefab.name,
							combineInstances, baseMaterial, new Vector3(offsetX, 0f, offsetY), JLayers.L_TERRAIN));
						combineInstances.Clear();
						totalVertices = 0;
					}
					combineInstances.Add(i);
					totalVertices += vertexCount;
				}
				renderedObjects.Add(MakeGOOfMeshInstances(parentT, "objects " + prefab.name,
					combineInstances, baseMaterial, new Vector3(offsetX, 0f, offsetY), JLayers.L_TERRAIN));
			}
		}
		
		// add the sticker meshes
		Mesh mesh = null;
		Material[] materials = MakeStickerMesh(sticker, ref mesh);
		if (materials != null) {
			GameObject inst = new GameObject("sticker" + xIndex + ":" + yIndex);
			Transform t = inst.transform;
			t.parent = parentT;
			t.localPosition = new Vector3(offsetX, 0f, offsetY);
			t.localRotation = Quaternion.identity;
			t.localScale = Vector3.one;
			
			MeshFilter filter = inst.AddComponent<MeshFilter>();
			MeshRenderer render = inst.AddComponent<MeshRenderer>();
			filter.sharedMesh = mesh;
			render.sharedMaterials = materials;
			cachedStickerGO = inst;
		}
		
		// add buildings
		foreach (KeyValuePair<string, List<BuildingData>> kv in buildings) {
			List<BuildingData> bdList = kv.Value;
	
			int count = bdList.Count;
			if (count == 0) {
				Debug.LogError("Shouldn't happen!");
			}
			else if (JConfig.editHouses) {
				foreach (BuildingData bd in bdList) {
					GameObject go = (GameObject) GameObject.Instantiate(bd.building.prefab);
					Collider col = go.GetComponent<Collider>();
					if (col) {
						GameObject.DestroyImmediate(col);
					}
					if (JConfig.editHouses) {
						MeshFilter filter = go.GetComponent<MeshFilter>();
						BoxCollider col2 = go.AddComponent<BoxCollider>();
						filter.sharedMesh.RecalculateBounds();
						Bounds b = filter.sharedMesh.bounds;
						col2.center = b.center;
						col2.size = b.size;
						JBuildingHandle handle = go.AddComponent<JBuildingHandle>();
						handle.cell = this;
						handle.data = bd;
					}
					
					go.layer = JLayers.L_BUILDINGS;
					go.tag = "BuildingHandle";
					Transform t = go.transform;
					t.parent = parentT;
					Vector3 pos = bd.pos;
					pos.Scale(scaleVector);
					t.localPosition = pos + new Vector3(offsetX, 0f, offsetY);
					t.localRotation = bd.rotation;
					t.localScale = bd.scale;
					renderedObjects.Add(go);
				}
			}
			else {
				int verticesCount = 0;
				List<CombineInstance> combineInstances = new List<CombineInstance>();
				for (int i = 0; i < count; i++) {
					BuildingData bd = bdList[i];
					Mesh m = bd.building.mesh;
					CombineInstance ci = new CombineInstance();
					Matrix4x4 t = new Matrix4x4();
					Vector3 pos = bd.pos;
					pos.Scale(scaleVector);
					t.SetTRS(pos, bd.rotation, bd.scale);
					ci.mesh = m;
					ci.subMeshIndex = 0;
					ci.transform = t;
					if (m.vertexCount + verticesCount >= 65000) {
						renderedObjects.Add(MakeGOOfMeshInstances(parentT, "buildings " + bd.building.matName,
							combineInstances, bd.building.material, new Vector3(offsetX, 0f, offsetY), JLayers.L_BUILDINGS));
						combineInstances.Clear();
						verticesCount = 0;
					}
					combineInstances.Add(ci);
					verticesCount += m.vertexCount;
				}
				renderedObjects.Add(MakeGOOfMeshInstances(parentT, "buildings " + bdList[0].building.matName,
					combineInstances, bdList[0].building.material, new Vector3(offsetX, 0f, offsetY), JLayers.L_BUILDINGS));				
			}
		}
		foreach (AnimalData ad in animals) {
			float posX = offsetX + ((float) ad.x + 0.5f) * scale;
			float posY = offsetY + ((float) ad.y + 0.5f) * scale;
			float posH = heightMap[(ad.y << 2) + 2, (ad.x << 2) + 2] + 0.5f;
			GameObject go = (GameObject) GameObject.Instantiate(JElements.GetAnimalPrefab(ad.t), new Vector3(posX, posH, posY), Quaternion.Euler(0f, Random.Range(0f, 360f), 0f));
			go.transform.parent = parent.transform;
			go.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
			go.GetComponent<JAnimal>().animalData = ad;
			ad.go = go;
			renderedObjects.Add(go);		
		}
		foreach (AnimalData ad in successionAnimals) {
			float posX = offsetX + ((float) ad.x + 0.5f) * scale;
			float posY = offsetY + ((float) ad.y + 0.5f) * scale;
			float posH = heightMap[(ad.y << 2) + 2, (ad.x << 2) + 2] + 0.5f;
			GameObject go = (GameObject) GameObject.Instantiate(JElements.GetAnimalPrefab(ad.t), new Vector3(posX, posH, posY), Quaternion.Euler(0f, Random.Range(0f, 360f), 0f));
			go.transform.parent = parent.transform;
			go.GetComponent<JAnimal>().animalData = ad;
			ad.go = go;
			renderedObjects.Add(go);		
		}
	}
	
	private GameObject MakeGOOfMeshInstances(Transform parent, string name, List<CombineInstance> combineInstances, Material mat, Vector3 localPos, int layer) {
		GameObject go = new GameObject(name);
		Transform cot = go.transform;
		cot.parent = parent;
		cot.localPosition = localPos;
		cot.localRotation = Quaternion.identity;
		cot.localScale = Vector3.one;
		go.layer = layer;
		MeshFilter filter = go.AddComponent<MeshFilter>();
		Mesh comesh = new Mesh();
		comesh.CombineMeshes(combineInstances.ToArray(), true, true);
		filter.sharedMesh = comesh;
		MeshRenderer render = go.AddComponent<MeshRenderer>();
		render.sharedMaterial = mat;
		return go;
	}
	
	public void ExportBuildings(System.Xml.XmlTextWriter writer, ref int id) {
		Vector3 offset = new Vector3(xOffset * JTerrainData.HORIZONTAL_SCALE, 0f, yOffset * JTerrainData.HORIZONTAL_SCALE);
		foreach (List<BuildingData> blist in buildings.Values) {
			foreach (BuildingData building in blist) {
				id++;
				writer.WriteStartElement("building");
				writer.WriteAttributeString("id", id.ToString());
				writer.WriteAttributeString("name", building.building.name);
				Vector3 pos = building.pos;
				pos.x = pos.x * JTerrainData.HORIZONTAL_SCALE;
				pos.z = pos.z * JTerrainData.HORIZONTAL_SCALE;
				writer.WriteAttributeString("position", JStringUtils.Vector3ToString(pos + offset));
				writer.WriteAttributeString("rotation", JStringUtils.Vector3ToString(building.rotation.eulerAngles));
				writer.WriteAttributeString("scale", JStringUtils.Vector3ToString(building.scale));
				writer.WriteEndElement();
			}
		}
	}
}

