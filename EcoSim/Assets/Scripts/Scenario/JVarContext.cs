using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class JVarContext {
	public JVarContext(JScenario s) {
		scenario = s;
		if (consts == null) SetupConsts();
		SetValue("LASTMEASURE", (int) EActionTypes.UNDEFINED);		
		SetValue("LASTRESEARCH", (int) EResearchTypes.UNDEFINED);		
		SetValue("VIEWEDENCYCLOPEDIA", 0);		
	}
	public readonly JScenario scenario;
	
	public const string XML_ELEMENT = "context";
	public Dictionary<string, long> vars = new Dictionary<string, long>();
	private static Dictionary<string, long> consts;
	
	public bool HasVar(string name) {
		return vars.ContainsKey(name) || consts.ContainsKey(name);
	}
	
	public void SetValue(string name, long v) {
		if (vars.ContainsKey(name)) {
			vars[name] = v;
		}
		else {
			vars.Add(name, v);
		}
	}
	
	public long GetValue(string name) {
		if (consts.ContainsKey(name)) {
			return consts[name];
		}
		else if (vars.ContainsKey(name)) {
			return vars[name];
		}
		return 0;
	}
	
	void SetupConsts() {
		consts = new Dictionary<string, long>();
		foreach (EActionTypes at in System.Enum.GetValues(typeof(EActionTypes))) {
			int atInt = (int) at;
			if ((atInt > 0x00) && (atInt < 0xff) && ((atInt & 0x0f) != 0x00)) {
				consts.Add(at.ToString().ToUpper(), atInt);
			}
		}
		foreach (EResearchTypes rt in System.Enum.GetValues(typeof(EResearchTypes))) {
			int rtInt = (int) rt;
			if ((rtInt > 0x00) && (rtInt < 0xff) && ((rtInt & 0x0f) != 0x00)) {
				consts.Add(rt.ToString().ToUpper(), rtInt);
			}
		}
		foreach (EParamTypes pt in System.Enum.GetValues(typeof(EParamTypes))) {
			if (pt != EParamTypes.UNDEFINED) {
				consts.Add(pt.ToString().ToUpper(), (int) pt);
			}
		}
		consts.Add("true", 1);
		consts.Add("false", 0);
		consts.Add("NOMEASURE", 0xff);
		consts.Add("NORESEARCH", 0xff);
	}
	
	public void Load(XmlReader reader) {
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "variable")) {
					string name = reader.GetAttribute("name");
					long v = long.Parse(reader.GetAttribute("val"));
					if (vars.ContainsKey(name)) {
						vars[name] = v;
					}
					else {
						vars.Add(name, v);
					}
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == XML_ELEMENT)) {
					break;
				}
			}
		}
	}
	
	public void Save(XmlWriter writer) {
		if (vars.Count > 0) {
			writer.WriteStartElement(XML_ELEMENT);
			foreach (KeyValuePair<string, long> kvp in vars) {
				writer.WriteStartElement("variable");
				writer.WriteAttributeString("name", kvp.Key);
				writer.WriteAttributeString("val", kvp.Value.ToString());
				writer.WriteEndElement();
			}
			writer.WriteEndElement();
		}
	}
}
