using UnityEngine;
using System.IO;
using System.Xml;
using System.Collections.Generic;

/**
 * Loads/Saves vegetation data
 */
public static class JVegetationFile {
	
	private static Texture2D LoadIcon(XmlTextReader reader, FileStream imageStream) {
		Texture2D result = null;
		if (!reader.IsEmptyElement) {
			string base64 = reader.ReadElementString().Trim();
			byte[] data = System.Convert.FromBase64String(base64);
			result = new Texture2D(4, 4, TextureFormat.RGB24, false);
			result.LoadImage(data);
		}
		else {
			int offset = int.Parse(reader.GetAttribute("offset").ToString());
			int len = int.Parse(reader.GetAttribute("length").ToString());
			if ((len > 0) && (offset >= 0) && (len <= 1024 * 1024)) {
				imageStream.Seek(offset, SeekOrigin.Begin);
				byte[] data = new byte[len];
				imageStream.Read(data, 0, len);
				result = new Texture2D(4, 4, TextureFormat.RGB24, false);
				result.LoadImage(data);
			}
		}
		return result;
	}
	
	private static JXTerrainTile LoadTerrainTile(XmlTextReader reader, FileStream imageStream) {
		JXTerrainTile tt = new JXTerrainTile();
		tt.am0 = float.Parse(reader.GetAttribute("splat0"));
		tt.am1 = float.Parse(reader.GetAttribute("splat1"));
		tt.am2 = float.Parse(reader.GetAttribute("splat2"));
		int maxDetailIndex = -1;
		List<int> tileLayers = new List<int>();
		if (!reader.IsEmptyElement) {
			List<JXTerrainTileTreeData> treeList = new List<JXTerrainTileTreeData>();
			List<JXTerrainTileObjectData> objectList = new List<JXTerrainTileObjectData>();
			Dictionary<int, int> detailDict = new Dictionary<int, int>();
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "image")) {
					if (imageStream != null) {
						tt.icon = LoadIcon(reader, imageStream);
					}
					nType = reader.NodeType;
				}
				
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "tree")) {
					JXTerrainTileTreeData tree = new JXTerrainTileTreeData();
					tree.x = float.Parse(reader.GetAttribute("x"));
					tree.y = float.Parse(reader.GetAttribute("y"));
					tree.r = float.Parse(reader.GetAttribute("r"));
					int index = -1;
					string prototypeName = reader.GetAttribute("prototypeName");
					if (prototypeName != null) {
						index = JElements.FindTreePrototype(prototypeName);
					}
					if (index >= 0) {
						// index = int.Parse(reader.GetAttribute("prototype"));
						tree.prototypeIndex = index;
						tree.minHeight = float.Parse(reader.GetAttribute("minHeight"));
						tree.maxHeight = float.Parse(reader.GetAttribute("maxHeight"));
						tree.minWidthVariance = float.Parse(reader.GetAttribute("minWidthVariance"));
						tree.maxWidthVariance = float.Parse(reader.GetAttribute("maxWidthVariance"));
						tree.colorFrom = JStringUtils.StringToColor(reader.GetAttribute("fromColour"));
						tree.colorTo = JStringUtils.StringToColor(reader.GetAttribute("toColour"));
						treeList.Add(tree);
					}
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "object")) {
					JXTerrainTileObjectData obj = new JXTerrainTileObjectData();
					obj.x = float.Parse(reader.GetAttribute("x"));
					obj.y = float.Parse(reader.GetAttribute("y"));
					obj.r = float.Parse(reader.GetAttribute("r"));
					obj.angle = float.Parse(reader.GetAttribute("angle"));
					int index = -1;
					string prototypeName = reader.GetAttribute("name");
					if (prototypeName != null) {
						index = JElements.FindObject(prototypeName);
					}
					if (index >= 0) {
						// index = int.Parse(reader.GetAttribute("objectIndex"));
						obj.index = index;
						obj.minHeight = float.Parse(reader.GetAttribute("minHeight"));
						obj.maxHeight = float.Parse(reader.GetAttribute("maxHeight"));
						obj.minWidthVariance = float.Parse(reader.GetAttribute("minWidthVariance"));
						obj.maxWidthVariance = float.Parse(reader.GetAttribute("maxWidthVariance"));
						objectList.Add(obj);
					}
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "detail")) {
					int index = -1;
					string detailName = reader.GetAttribute("name");
					if (detailName != null) {
						index = JElements.FindDetailPrototype(detailName);
					}
					if (index >= 0) {
						// index = int.Parse(reader.GetAttribute("index"));
						int count = int.Parse(reader.GetAttribute("count"));
						maxDetailIndex = Mathf.Max(maxDetailIndex, index);
						if (!detailDict.ContainsKey(index)) {
							detailDict.Add(index, count);
						}
					}
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "tilelayer")) {
					string name = reader.GetAttribute("name");
					int id = JElements.FindIndexOfExtraTileLayer(name);
					if (id >= 0) {
						// id = int.Parse(reader.GetAttribute("index"));
						tileLayers.Add(id);
					}
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "tile")) {
					break;
				}
			}
			
			tt.trees = treeList.ToArray();
			tt.objects = objectList.ToArray();
			tt.detailCounts = new int[maxDetailIndex + 1];
			foreach (KeyValuePair<int, int> v in detailDict) {
				tt.detailCounts[v.Key] = v.Value;
			}
		}
		else {
			tt.trees = new JXTerrainTileTreeData[0];
			tt.detailCounts = new int[0];
		}
		tt.extraLayers = tileLayers.ToArray();
		return tt;
	}
	
	private static JXVegetation.NewParam LoadNewParam(XmlTextReader reader) {
		JXVegetation.NewParam newParam = new JXVegetation.NewParam();
		string typeStr = reader.GetAttribute("type").ToLower();
		newParam.type = JConfig.GetParamType(typeStr);
		string valStr = reader.GetAttribute("value");
		if (valStr != null) {
			newParam.minRange = byte.Parse(valStr);
			newParam.maxRange = byte.Parse(valStr);
		}
		else {
			newParam.minRange = byte.Parse(reader.GetAttribute("min"));
			newParam.maxRange = byte.Parse(reader.GetAttribute("max"));
		}
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "parameter")) {
					break;
				}
			}
		}
		return newParam;
	}
	
	private static JXVegetation.ParamCondition LoadParamCondition(XmlTextReader reader) {
		JXVegetation.ParamCondition paramCnd = new JXVegetation.ParamCondition();
		string typeStr = reader.GetAttribute("parameter").ToLower();
		paramCnd.type = EnumExtensions.GetParamType(typeStr);

		paramCnd.minRange = byte.Parse(reader.GetAttribute("min"));
		paramCnd.maxRange = byte.Parse(reader.GetAttribute("max"));
		
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "condition")) {
					break;
				}
			}
		}
		return paramCnd;
	}

	
	private static JXVegetation.ParamDelta LoadParamDelta(XmlTextReader reader) {
		JXVegetation.ParamDelta delta = new JXVegetation.ParamDelta();
		delta.type = EnumExtensions.GetParamType(reader.GetAttribute("parameter"));
		delta.delta = int.Parse(reader.GetAttribute("delta"));
		delta.minRange = byte.Parse(reader.GetAttribute("min"));
		delta.maxRange = byte.Parse(reader.GetAttribute("max"));
		
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "delta")) {
					break;
				}
			}
		}
		return delta;
	}
	
	private static JXVegetation.Step LoadStep(XmlTextReader reader) {
		List <JXVegetation.ParamCondition> paramConds = new List<JXVegetation.ParamCondition>();
		JXVegetation.Step step = new JXVegetation.Step();
		step.action = EnumExtensions.GetActionType(reader.GetAttribute("action"));
		if (step.action == EActionTypes.UNDEFINED) {
			step.action = EActionTypes.GeenActie;
		}
		step.chance = float.Parse(reader.GetAttribute("chance"));
		step.newVegetation = int.Parse(reader.GetAttribute("goto"));
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "condition")) {
					JXVegetation.ParamCondition cnd = LoadParamCondition(reader);
					if (cnd.type != EParamTypes.UNDEFINED) paramConds.Add(cnd);
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "step")) {
					break;
				}
			}
		}
		step.conditions = paramConds.ToArray();
		return step;
	}
	
	
	private static JXVegetation.ParamChange LoadChange(XmlTextReader reader) {
		List <JXVegetation.ParamDelta> deltas = new List<JXVegetation.ParamDelta>();
		JXVegetation.ParamChange change = new JXVegetation.ParamChange();
		change.action = EnumExtensions.GetActionType(reader.GetAttribute("action"));
		change.chance = float.Parse(reader.GetAttribute("chance"));
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "delta")) {
					JXVegetation.ParamDelta delta = LoadParamDelta(reader);
					if (delta.type != EParamTypes.UNDEFINED) deltas.Add(delta);
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "change")) {
					break;
				}
			}
		}
		change.deltas = deltas.ToArray();
		return change;
	}
	
	private static JXVegetation LoadVegetation(XmlTextReader reader, FileStream imageStream) {
		JXVegetation veg = new JXVegetation();
		veg.name = reader.GetAttribute("name");
		string colourStr = reader.GetAttribute("colour");
		if (colourStr != null) {
			int colourNr;
			if (int.TryParse(colourStr, System.Globalization.NumberStyles.HexNumber, null, out colourNr)) {
				Color32 c = new Color32((byte) (colourNr >> 16), (byte) (colourNr >> 8), (byte) colourNr, 255);
				veg.colour = c;
			}
		}
		List<JXVegetation.NewParam> newParams = new List<JXVegetation.NewParam>();
		List<JXVegetation.Step> newSteps = new List<JXVegetation.Step>();
		List<JXVegetation.ParamChange> newChanges = new List<JXVegetation.ParamChange>();
		List<EActionTypes> acceptableMeasures = new List<EActionTypes>();
		if (!reader.IsEmptyElement) {
			List<JXTerrainTile> tileList = new List<JXTerrainTile>();
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "parameter")) {
					JXVegetation.NewParam param = LoadNewParam(reader);
					if (param.type != EParamTypes.UNDEFINED) newParams.Add(param);
				}
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "measure")) {
					string measureName = reader.GetAttribute("type").ToLower();
					acceptableMeasures.Add(EnumExtensions.GetActionType(measureName));
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "change")) {
					newChanges.Add(LoadChange(reader));
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "step")) {
					newSteps.Add(LoadStep(reader));
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "tile")) {
					tileList.Add(LoadTerrainTile(reader, imageStream));
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "vegetation")) {
					break;
				}
			}
			veg.tiles = tileList.ToArray();
		}
		else {
			veg.tiles = new JXTerrainTile[0];
		}
		veg.newParams = newParams.ToArray();
		veg.changes = newChanges.ToArray();
		veg.steps = newSteps.ToArray();
		veg.acceptableMeasures = acceptableMeasures.ToArray();
		veg.UpdateStableParam();
		return veg;
	}
	
	private static JXSuccession LoadSuccession(XmlTextReader reader, FileStream imageStream) {
		JXSuccession succession = new JXSuccession();
		succession.name = reader.GetAttribute("name");
		if (!reader.IsEmptyElement) {
			List<JXVegetation> vegetations = new List<JXVegetation>();
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "vegetation")) {
					JXVegetation vegetation = LoadVegetation(reader, imageStream);
					vegetations.Add(vegetation);
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "succession")) {
					break;
				}
			}
			succession.vegetation = vegetations.ToArray();
		}
		else {
			succession.vegetation = new JXVegetation[0];
		}
		foreach (JXVegetation vegetation in succession.vegetation) {
			vegetation.succession = succession;
		}
		return succession;
	}

	public static JXSuccession[] Load(JScenario scenario) {
		string path = scenario.scenarioPath + "elements.xml";
		XmlTextReader reader = new XmlTextReader(new System.IO.StreamReader(path));
		FileStream imageStream = null;
		if (!scenario.IsGameMode && JConfig.LOAD_THUMBNAILS && File.Exists(scenario.scenarioPath + "icons.dat")) {
			imageStream = new FileStream(scenario.scenarioPath + "icons.dat", FileMode.Open);
		}
		
		// List<JXVegetation> typeList = new List<JXVegetation>();
		List<JXSuccession> successions = new List<JXSuccession>();
		
		int index = 0;
		while (reader.Read()) {
			XmlNodeType nType = reader.NodeType;
			if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "succession")) {
				JXSuccession succession = LoadSuccession(reader, imageStream);
				succession.index = index++;
				successions.Add(succession);
			}
		}
		
		
		
		reader.Close();
		if (imageStream != null) {
			imageStream.Close();
		}
		return successions.ToArray();
	}

	public static void Save(string path, JScenario scenario) {
		Save (path, scenario, 1);
	}

	public static void Save(string path, JScenario scenario, int version) {
		string file = path + Path.DirectorySeparatorChar + "elements.xml";
		FileStream stream = null;
		if (JConfig.LOAD_THUMBNAILS) {
			stream = new FileStream(scenario.scenarioPath + "icons.dat", FileMode.Create);
		}
		int imageOffset = 0;
		
		XmlTextWriter write = new XmlTextWriter(file, System.Text.Encoding.UTF8);
		write.WriteStartDocument(true);
		write.WriteStartElement("successions");
		foreach (JXSuccession succession in scenario.successions) {
			write.WriteStartElement("succession");
			
			write.WriteAttributeString("name", succession.name);
			
			foreach (JXVegetation vegetation in succession.vegetation) {
				write.WriteStartElement("vegetation");
				write.WriteAttributeString("name", vegetation.name);
				Color32 c = vegetation.colour;
				write.WriteAttributeString("colour", c.r.ToString("X2") + c.g.ToString("X2") + c.b.ToString("X2"));
				
				foreach (EActionTypes acceptableMeasure in vegetation.acceptableMeasures) {
					switch (version) {
					case 0 :
					case 1 :
						write.WriteStartElement("measure");
						write.WriteAttributeString("type", acceptableMeasure.ToString());
						write.WriteEndElement();
						break;
					case 2 :
						// Get the name of the measure
						if (acceptableMeasure != EActionTypes.GeenActie && acceptableMeasure != EActionTypes.UNDEFINED) {
							JScenario.ActionInfo ai = scenario.GetActionInfo (acceptableMeasure);
							if (ai != null) {
								write.WriteStartElement("action");
								write.WriteAttributeString("name", ai.name);
								write.WriteEndElement(); // ~action
							}
						}
						break;
					}
				}
				foreach (JXVegetation.NewParam newParam in vegetation.newParams) {
					write.WriteStartElement("parameter");
					write.WriteAttributeString("type", newParam.type.ToString());
					write.WriteAttributeString("min", newParam.minRange.ToString());
					write.WriteAttributeString("max", newParam.maxRange.ToString());
					write.WriteEndElement(); // ~parameter
				}
				foreach (JXVegetation.Step step in vegetation.steps) {
					switch (version) {
					case 0 :
					case 1 :
						write.WriteStartElement("step");
						write.WriteAttributeString("action", step.action.ToString());
						write.WriteAttributeString("chance", step.chance.ToString());
						write.WriteAttributeString("goto", step.newVegetation.ToString());
						
						foreach (JXVegetation.ParamCondition cnd in step.conditions) {
							write.WriteStartElement("condition");
							
							write.WriteAttributeString("parameter", cnd.type.ToString());
							write.WriteAttributeString("min", cnd.minRange.ToString());
							write.WriteAttributeString("max", cnd.maxRange.ToString());
							
							write.WriteEndElement(); // ~condition
						}
						
						write.WriteEndElement(); // ~step
						break;
					case 2 :
						// TODO: Must we include the contents (enumTypes) of the Groep_*'s? Or should we just exclude them
						if (!step.action.ToString().StartsWith("Groep_")) {
							write.WriteStartElement("rule");
							if (step.action != EActionTypes.GeenActie && step.action != EActionTypes.UNDEFINED) {
								JScenario.ActionInfo ai = scenario.GetActionInfo (step.action);
								if (ai != null) {
									write.WriteAttributeString("action", ai.name);
								} else {
									write.WriteAttributeString("action", step.action.ToString());
								}
							}
							write.WriteAttributeString("chance", step.chance.ToString());
							write.WriteAttributeString("target", step.newVegetation.ToString());

							foreach (JXVegetation.ParamCondition cnd in step.conditions) {
								write.WriteStartElement("range");
								
								write.WriteAttributeString("parameter", cnd.type.ToString());
								write.WriteAttributeString("low", cnd.minRange.ToString());
								write.WriteAttributeString("high", cnd.maxRange.ToString());
								
								write.WriteEndElement(); // ~range
							}
							write.WriteEndElement(); // ~rule
						}
						break;
					}
				}
				foreach (JXVegetation.ParamChange change in vegetation.changes) {
					switch (version) {
					case 0 :
					case 1 :
						write.WriteStartElement("change");
						write.WriteAttributeString("action", change.action.ToString());
						write.WriteAttributeString("chance", change.chance.ToString());
						
						foreach (JXVegetation.ParamDelta delta in change.deltas) {
							write.WriteStartElement("delta");
							
							write.WriteAttributeString("parameter", delta.type.ToString());
							write.WriteAttributeString("delta", delta.delta.ToString());
							write.WriteAttributeString("min", delta.minRange.ToString());
							write.WriteAttributeString("max", delta.maxRange.ToString());
							
							write.WriteEndElement(); // ~delta
						}
						
						write.WriteEndElement(); // ~change
						break;
					case 2 :
						foreach (JXVegetation.ParamDelta delta in change.deltas) {
							write.WriteStartElement("gradualchange");

							write.WriteAttributeString("instanceid", change.GetHashCode().ToString());
							write.WriteAttributeString("parameter", delta.type.ToString());
							if (change.action != EActionTypes.GeenActie && change.action != EActionTypes.UNDEFINED) {
								JScenario.ActionInfo ai = scenario.GetActionInfo (change.action);
								if (ai != null) {
									write.WriteAttributeString("action", ai.name);
								} else {
									write.WriteAttributeString("action", change.action.ToString());
								}
							}
							write.WriteAttributeString("low", delta.minRange.ToString());
							write.WriteAttributeString("high", delta.maxRange.ToString());
							write.WriteAttributeString("delta", delta.delta.ToString());
							
							write.WriteAttributeString("chance", change.chance.ToString());
							
							write.WriteEndElement(); // ~gradualchange
						}
						break;
					}
				}

				foreach (JXTerrainTile tt in vegetation.tiles) {
					write.WriteStartElement("tile");
					
					write.WriteAttributeString("splat0", tt.am0.ToString());
					write.WriteAttributeString("splat1", tt.am1.ToString());
					write.WriteAttributeString("splat2", tt.am2.ToString());					
					
					if ((tt.icon != null) && (stream != null)) {
						byte[] image = tt.icon.EncodeToPNG();
						write.WriteStartElement("image");
						write.WriteAttributeString("offset", imageOffset.ToString());
						write.WriteAttributeString("length", image.Length.ToString());
						stream.Write(image, 0, image.Length);
						imageOffset += image.Length;
						write.WriteEndElement(); // ~image
					}

					
					foreach (int tileLayerIndex in tt.extraLayers) {
						write.WriteStartElement("tilelayer");
						write.WriteAttributeString("name", JElements.GetTileLayerPrototype(tileLayerIndex).material.name);
						write.WriteAttributeString("index", tileLayerIndex.ToString());
						write.WriteEndElement(); // ~tilelayer
					}
					foreach (JXTerrainTileTreeData td in tt.trees) {						
						write.WriteStartElement("tree");
						
						write.WriteAttributeString("x", td.x.ToString());
						write.WriteAttributeString("y", td.y.ToString());
						write.WriteAttributeString("r", td.r.ToString());
						write.WriteAttributeString("prototypeName", td.GetName());
						write.WriteAttributeString("prototype", td.prototypeIndex.ToString());

						write.WriteAttributeString("minHeight", td.minHeight.ToString());
						write.WriteAttributeString("maxHeight", td.maxHeight.ToString());
						write.WriteAttributeString("minWidthVariance", td.minWidthVariance.ToString());
						write.WriteAttributeString("maxWidthVariance", td.maxWidthVariance.ToString());
						write.WriteAttributeString("fromColour", JStringUtils.ColorToString(td.colorFrom));
						write.WriteAttributeString("toColour", JStringUtils.ColorToString(td.colorTo));
						
						write.WriteEndElement(); // ~tree
					}
					foreach (JXTerrainTileObjectData od in tt.objects) {						
						write.WriteStartElement("object");
						
						write.WriteAttributeString("x", od.x.ToString());
						write.WriteAttributeString("y", od.y.ToString());
						write.WriteAttributeString("r", od.r.ToString());
						write.WriteAttributeString("angle", od.angle.ToString());
						write.WriteAttributeString("name", od.GetName());
						write.WriteAttributeString("objectIndex", od.index.ToString());

						write.WriteAttributeString("minHeight", od.minHeight.ToString());
						write.WriteAttributeString("maxHeight", od.maxHeight.ToString());
						write.WriteAttributeString("minWidthVariance", od.minWidthVariance.ToString());
						write.WriteAttributeString("maxWidthVariance", od.maxWidthVariance.ToString());
						
						write.WriteEndElement(); // ~object
					}
					int index = 0;
					foreach (int count in tt.detailCounts) {
						if (count > 0) {
							write.WriteStartElement("detail");
							DetailPrototype dp = JElements.GetDetailPrototype(index);
							string name = null;
							if (dp.prototype != null) {
								name = dp.prototype.name;
							}
							else {
								name = dp.prototypeTexture.name;
							}
							
							write.WriteAttributeString("name", name);
							write.WriteAttributeString("index", index.ToString());
							write.WriteAttributeString("count", count.ToString());

							write.WriteEndElement(); // ~detail
						}
						index++;
					}
					write.WriteEndElement(); // ~tile
				}
				write.WriteEndElement(); // ~vegetation
			}
			write.WriteEndElement(); // ~succession
		}
		write.WriteEndElement(); // ~successions
		write.WriteEndDocument();
		write.Close();
		if (stream != null) {
			stream.Close();
		}
	}
	
}
