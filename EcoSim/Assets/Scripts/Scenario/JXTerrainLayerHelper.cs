using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JXTerrainLayerHelper {
	public int resolution;
	public const float SCALE = JTerrainData.HORIZONTAL_SCALE;
	
	public int id;
	public List<Vector3> points;
	public List<Vector2> uvs;
	public List<int> indices;
	public int[] usedMap;
	public int index = 0;
	public float verticalOffset;
	public bool isWater;
		
	public JXTerrainLayerHelper(int id, int resolution, float verticalOffset, bool isWater) {
		this.id = id;
		this.resolution = resolution;
		this.isWater = isWater;
		this.verticalOffset = verticalOffset;
		usedMap = new int[(resolution + 1) * (resolution + 1)];
		points = new List<Vector3>();
		uvs = new List<Vector2>();
		indices = new List<int>();
	}

	public void AddTileFakeHeights(int x, int y, float height) {
		int c0, c1, c2, c3;
		int p = x + (resolution + 1) * y;
		if (usedMap[p] > 0) {
			c0 = usedMap[p] - 1;
		}
		else {
			points.Add(new Vector3(x * SCALE, height + verticalOffset, y * SCALE));
			uvs.Add(new Vector2(x, y));
			c0 = index;
			usedMap[p] = ++index;
		}
		if (usedMap[p + 1] > 0) {
			c1 = usedMap[p + 1] - 1;
		}
		else {
			points.Add(new Vector3((x + 1) * SCALE, height + verticalOffset, y * SCALE));
			uvs.Add(new Vector2(x + 1, y));
			c1 = index;
			usedMap[p] = ++index;
		}
		if (usedMap[p + resolution + 1] > 0) {
			c2 = usedMap[p + resolution + 1] - 1;
		}
		else {
			points.Add(new Vector3(x * SCALE, height + verticalOffset, (y + 1) * SCALE));
			uvs.Add(new Vector2(x, y + 1));
			c2 = index;
			usedMap[p] = ++index;
		}
		if (usedMap[p + resolution + 2] > 0) {
			c3 = usedMap[p + resolution + 2] - 1;
		}
		else {
			points.Add(new Vector3((x + 1) * SCALE, height + verticalOffset, (y + 1) * SCALE));
			uvs.Add(new Vector2(x + 1, y + 1));
			c3 = index;
			usedMap[p] = ++index;
		}
		indices.AddRange(new int[] { c1, c0, c2, c2, c3, c1 });
	}
	
	
	public void AddTile(int x, int y, float[,] heightMap) {
		int c0, c1, c2, c3;
		int p = x + (resolution + 1) * y;
		if (usedMap[p] > 0) {
			c0 = usedMap[p] - 1;
		}
		else {
			points.Add(new Vector3(x * SCALE, heightMap[y * 4, x * 4] + verticalOffset, y * SCALE));
			uvs.Add(new Vector2(x, y));
			c0 = index;
			usedMap[p] = ++index;
		}
		if (usedMap[p + 1] > 0) {
			c1 = usedMap[p + 1] - 1;
		}
		else {
			points.Add(new Vector3((x + 1) * SCALE, heightMap[y * 4, x * 4 + 4] + verticalOffset, y * SCALE));
			uvs.Add(new Vector2(x + 1, y));
			c1 = index;
			usedMap[p] = ++index;
		}
		if (usedMap[p + resolution + 1] > 0) {
			c2 = usedMap[p + resolution + 1] - 1;
		}
		else {
			points.Add(new Vector3(x * SCALE, heightMap[y * 4 + 4, x * 4] + verticalOffset, (y + 1) * SCALE));
			uvs.Add(new Vector2(x, y + 1));
			c2 = index;
			usedMap[p] = ++index;
		}
		if (usedMap[p + resolution + 2] > 0) {
			c3 = usedMap[p + resolution + 2] - 1;
		}
		else {
			points.Add(new Vector3((x + 1) * SCALE, heightMap[y * 4 + 4, x * 4 + 4] + verticalOffset, (y + 1) * SCALE));
			uvs.Add(new Vector2(x + 1, y + 1));
			c3 = index;
			usedMap[p] = ++index;
		}
		indices.AddRange(new int[] { c1, c0, c2, c2, c3, c1 });
	}
}
