using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class JExpression {

	private JExpression(string name, string err) {
		this.varName = name;
		this.varType = VarType.UNDEFINED;
		this.subVarType = SubVarType.UNDEFINED;
		this.targetArea = -1;
		this.index = 0;
		errorStr = err;
	}
	
	
	private JExpression(string name, VarType varType, SubVarType subVarType, int targetArea, int index) {
		this.varName = name;
		this.varType = varType;
		this.subVarType = subVarType;
		this.targetArea = targetArea;
		this.index = index;
		errorStr = null;
	}
	
	public enum VarType { UNDEFINED, COUNTER, YEAR, BUDGET, YEAR_COST, MEASURE, RESEARCH, PARAMETER, SPECIES };
	public enum SubVarType { UNDEFINED, MIN, MINCOUNT, MAX, MAXCOUNT, AVG, SUM, COUNT, COUNTNOTZERO };
	
	public readonly string varName;
	public readonly VarType varType;
	public readonly SubVarType subVarType;
	public readonly int targetArea;
	public readonly int index;
	public readonly string errorStr;
	
	public static long ParseCnd(string str, string cnd, JVarContext context, bool checkOnly) {
		int index = str.IndexOf(cnd);
		long v1 = ParseExpression(str.Substring(0, index), context, checkOnly);
		long v2 = 0;
		if (checkOnly) {
			v2 = ParseExpression(str.Substring(index + cnd.Length), context, checkOnly);
			return 0;
		}
		switch (cnd) {
		case "<=" :
			v2 = ParseExpression(str.Substring(index + cnd.Length), context, checkOnly);
			return (v1 <= v2)?1:0;
		case ">=" :
			v2 = ParseExpression(str.Substring(index + cnd.Length), context, checkOnly);
			return (v1 >= v2)?1:0;
		case "==" :
			v2 = ParseExpression(str.Substring(index + cnd.Length), context, checkOnly);
			return (v1 == v2)?1:0;
		case "!=" :
			v2 = ParseExpression(str.Substring(index + cnd.Length), context, checkOnly);
			return (v1 != v2)?1:0;
		case "<" :
			v2 = ParseExpression(str.Substring(index + cnd.Length), context, checkOnly);
			return (v1 < v2)?1:0;
		case ">" :
			v2 = ParseExpression(str.Substring(index + cnd.Length), context, checkOnly);
			return (v1 > v2)?1:0;
		case "&&" :
			if (v1 == 0) return 0;
			v2 = ParseExpression(str.Substring(index + cnd.Length), context, checkOnly);				
			return (v2 != 0)?1:0;
		case "||" :
			if (v1 != 0) return 1;
			v2 = ParseExpression(str.Substring(index + cnd.Length), context, checkOnly);				
			return (v2 != 0)?1:0;
		default : return 0;
		}
	}
	
	public static int FindClosing(string str, int open, char openElt, char closeElt) {
		int openCount = 0;
		for (int i = open; i < str.Length; i++) {
			if (str[i] == openElt) openCount++;
			else if (str[i] == closeElt) {
				openCount--;
				if (openCount == 0) return i;
			}
		}
		throw new System.Exception("Missende '" + closeElt + "'");
	}
	
	public static long ParseExpression(string str, JVarContext context, bool checkOnly) {
		str = str.Trim();
		if (str == "") {
			throw new System.Exception("Lege expressie");
		}
		if (str.StartsWith("(")) {
			int close = FindClosing(str, 0, '(', ')');
			long val = ParseExpression(str.Substring(1, close - 1), context, checkOnly);
			return ParseExpression(val.ToString() + str.Substring(close + 1), context, checkOnly);
		}
		if (str.IndexOf("<=") >= 0) return ParseCnd(str, "<=", context, checkOnly);
		if (str.IndexOf(">=") >= 0) return ParseCnd(str, ">=", context, checkOnly);
		if (str.IndexOf("==") >= 0) return ParseCnd(str, "==", context, checkOnly);
		if (str.IndexOf("!=") >= 0) return ParseCnd(str, "!=", context, checkOnly);
		if (str.IndexOf("<") >= 0) return ParseCnd(str, "<", context, checkOnly);
		if (str.IndexOf(">") >= 0) return ParseCnd(str, ">", context, checkOnly);
		if (str.IndexOf("&&") >= 0) return ParseCnd(str, "&&", context, checkOnly);
		if (str.IndexOf("||") >= 0) return ParseCnd(str, "||", context, checkOnly);
		int index = str.IndexOf('*');
		if (index > 0) {
			long v1 = ParseExpression(str.Substring(0, index), context, checkOnly);
			long v2 = ParseExpression(str.Substring(index + 1), context, checkOnly);
			return (v1 * v2);
		}
		index = str.IndexOf('/');
		if (index > 0) {
			long v1 = ParseExpression(str.Substring(0, index), context, checkOnly);
			long v2 = ParseExpression(str.Substring(index + 1), context, checkOnly);
			if (v2 == 0) return 9999999999L;
			return (v1 / v2);
		}
		index = str.IndexOf('+');
		if (index > 0) {
			long v1 = ParseExpression(str.Substring(0, index), context, checkOnly);
			long v2 = ParseExpression(str.Substring(index + 1), context, checkOnly);
			return (v1 + v2);
		}
		index = str.IndexOf('-');
		if (index > 0) {
			long v1 = ParseExpression(str.Substring(0, index), context, checkOnly);
			long v2 = ParseExpression(str.Substring(index + 1), context, checkOnly);
			return (v1 - v2);
		}
		else if (index == 0) {
			long v1 = ParseExpression(str.Substring(1), context, checkOnly);
			return -v1;
		}
		char c = str[0];
		if (c == '!') {
			long v1 = ParseExpression(str.Substring(1), context, checkOnly);
			return (v1 != 0)?0:1;
		}
		if ((c >= '0') && (c <= '9')) {
			long nr = long.Parse(str);
			return nr;
		}
		else if (str.StartsWith("exists")) {
			str = str.Substring(6).Trim();
			if (str.StartsWith("(")) {
				int close = FindClosing(str, 0, '(', ')');
				string varName = str.Substring(1, close - 1);
				str = (context.HasVar(varName)?"1":"0") + str.Substring(close + 1);
				return ParseExpression(str, context, checkOnly);
			}
			else {
				throw new System.Exception("exists moet gevolgd worden met ( <variablenaam> ).");
			}
			
		}
		else {
			JExpression var = ParseVariable(str, context, checkOnly);
			if (var.errorStr != null) throw new System.Exception(var.errorStr);
			// if (checkOnly) return 0;
			return var.GetValue(context);
		}
	}
	
	public static SubVarType ParseSubVar(string str) {
		switch (str.ToLower()) {
		case "min" : return SubVarType.MIN;
		case "max" : return SubVarType.MAX;
		case "mincount" : return SubVarType.MINCOUNT;
		case "maxcount" : return SubVarType.MAXCOUNT;
		case "avg" : return SubVarType.AVG;
		case "sum" : return SubVarType.SUM;
		case "count" : return SubVarType.COUNT;
		case "countnz" : return SubVarType.COUNTNOTZERO;
		default : return SubVarType.UNDEFINED;
		}
	}

	
	public static JExpression ParseVariable(string str, JVarContext context, bool checkOnly) {
		int leftBrk = str.IndexOf('[');
		int rightBrk = str.IndexOf(']');
		string indexStr = null;
		string postfixStr = null;
		string varName = str;
		if ((leftBrk > 0) || (rightBrk > 0)) {
			if ((leftBrk < 0) || (rightBrk < leftBrk)) {
				return new JExpression(varName, "[ ] verkeerd geplaatst bij '" + str + "'");
			}
			indexStr = str.Substring(leftBrk + 1, rightBrk - leftBrk - 1);
			varName = str.Substring(0, leftBrk);
			
		}
		int periodI = str.IndexOf('.');
		if (periodI > 0) {
			if ((rightBrk > 0) && (periodI != rightBrk + 1)) {
				return new JExpression(varName, ". operatie verkeerd geplaatst bij '" + str + "'");
			}
			postfixStr = str.Substring(periodI + 1);
			if (leftBrk < 0) {
				varName = str.Substring(0, periodI);
			}
		}
		
		if (varName.Equals("jaar")) {
			if ((indexStr != null) || (postfixStr != null)) {
				return new JExpression(varName, "jaar heeft geen [] of . operand bij '" + str + "'");
			}
			return new JExpression(varName, VarType.YEAR, SubVarType.UNDEFINED, -1, 0);
		}
		else if (varName.Equals("budget")) {
			if ((indexStr != null) || (postfixStr != null)) {
				return new JExpression(varName, "budget heeft geen [] of . operand bij '" + str + "'");
			}
			return new JExpression(varName, VarType.BUDGET, SubVarType.UNDEFINED, -1, 0);
		}
		else if (varName.Equals("jaarkosten")) {
			if ((indexStr != null) || (postfixStr != null)) {
				return new JExpression(varName, "jaarkosten heeft geen [] of . operand bij '" + str + "'");
			}
			return new JExpression(varName, VarType.YEAR_COST, SubVarType.UNDEFINED, -1, 0);
		}
		else if ((varName == "maatregel") || (varName == "onderzoek") || (varName == "soorten") || (varName == "waarden")) {
			VarType vt = VarType.UNDEFINED;
			switch (varName) {
			case "maatregel" : vt = VarType.MEASURE; break;
			case "onderzoek" : vt = VarType.RESEARCH; break;
			case "waarden" : vt = VarType.PARAMETER; break;
			case "soorten" : vt = VarType.SPECIES; break;
			}
			if (indexStr == null) {
				return new JExpression(varName,  varName + " vereist [] operand met maatregel type '" + str + "'");
			}
			if (postfixStr == null) {
				return new JExpression(varName, varName + " vereist . operand count, min, mincount, max, maxcount, avg of sum '" + str + "'");
			}
			SubVarType svt = ParseSubVar(postfixStr);
			if (svt == SubVarType.UNDEFINED) {
				return new JExpression(varName, varName + " vereist . operand count, min, mincount, max, maxcount, avg of sum '" + str + "'");
			}
			int comma = indexStr.IndexOf(',');
			if (comma > 0) {
				int t = (int) ParseExpression(indexStr.Substring(0, comma), context, checkOnly);
				int i = (int) ParseExpression(indexStr.Substring(comma + 1), context, checkOnly);
				return new JExpression(varName, vt, svt, t, i);
			}
			else {
				int i = (int) ParseExpression(indexStr.Substring(comma + 1), context, checkOnly);
				return new JExpression(varName, vt, svt, -1, i);
			}
		}
		else if (context.HasVar(varName)) {
			return new JExpression(varName, VarType.COUNTER, SubVarType.UNDEFINED, -1, 0);
		}
		return new JExpression(varName, "variable van onbekend type '" + varName + "' : '" + str + "'");
	}
	
	public class Extra {
		public Extra(int targetId, EActionTypes action) {
			this.targetId = targetId;
			varType = JExpression.VarType.MEASURE;
			actionType = action;
			researchType = EResearchTypes.GeenResearch;
			paramTypes = EParamTypes.UNDEFINED;
			species = -1;
		}

		public Extra(int targetId, EResearchTypes research) {
			this.targetId = targetId;
			varType = JExpression.VarType.RESEARCH;
			actionType = EActionTypes.GeenActie;
			researchType = research;
			paramTypes = EParamTypes.UNDEFINED;
			species = -1;
		}
		
		public Extra(int targetId, EParamTypes param) {
			this.targetId = targetId;
			varType = JExpression.VarType.PARAMETER;
			actionType = EActionTypes.GeenActie;
			researchType = EResearchTypes.GeenResearch;
			paramTypes = param;
			species = -1;
		}
		
		public Extra(int targetId, int species) {
			this.targetId = targetId;
			varType = JExpression.VarType.SPECIES;
			actionType = EActionTypes.GeenActie;
			researchType = EResearchTypes.GeenResearch;
			paramTypes = EParamTypes.UNDEFINED;
			this.species = species;
		}
		
		public readonly int targetId;
		public readonly VarType varType;
		public readonly EActionTypes actionType;
		public readonly EResearchTypes researchType;
		public readonly EParamTypes paramTypes;
		public readonly int species;
		public int total = 0;
		public int notZero = 0;
		public int avgCount = 0;
		public int min = int.MaxValue;
		public int minCount = 0;
		public int max = int.MinValue;
		public int maxCount = 0;
	}
	
	static void DJustCount(JCellData cell, int x, int y, System.Object extra) {
		Extra e = extra as Extra;
		e.total++;
	}
	
	static void DParameters(JCellData cell, int x, int y, System.Object extra) {
		Extra e = extra as Extra;
		e.total++;
		int val = cell.GetData(e.paramTypes, x, y);
		if (val > 0) e.notZero++;
		e.avgCount += val;
		if (val < e.min) {
			e.min = val;
			e.minCount = 1;
		}
		else if (val == e.min) {
			e.minCount++;
		}
		if (val > e.max) {
			e.max = val;
			e.maxCount = 1;
		}
		else if (val == e.max) {
			e.maxCount++;
		}
	}

	static void DSpecies(JCellData cell, int x, int y, System.Object extra) {
		Extra e = extra as Extra;
		e.total++;
		int val = cell.GetSpeciesValue(x, y, e.species) * 100;
		// if (val > 0) Debug.Log("val = " + val + " x = " + x + " y = " + y);
		if (val > 0) e.notZero++;
		e.avgCount += val;
		if (val < e.min) {
			e.min = val;
			e.minCount = 1;
		}
		else if (val == e.min) {
			e.minCount++;
		}
		if (val > e.max) {
			e.max = val;
			e.maxCount = 1;
		}
		else if (val == e.max) {
			e.maxCount++;
		}
	}
		
	private static List<Extra> cache = new List<Extra>();
	
	public static void ClearCache() {
		cache.Clear();
	}
	
	public static Extra GetParameters(JVarContext context, int targetId, EParamTypes param) {
		foreach (Extra e in cache) {
			if ((e.targetId == targetId) && (e.varType == VarType.PARAMETER) && (e.paramTypes == param)) {
				return e;
			}
		}
		Extra extra = new Extra(targetId, param);
		context.scenario.data.ProcessTargetArea(targetId, DParameters, extra);
		return extra;
	}

	public static Extra GetSpecies(JVarContext context, int targetId, int speciesId) {
		foreach (Extra e in cache) {
			if ((e.targetId == targetId) && (e.varType == VarType.SPECIES) && (e.species == speciesId)) {
				return e;
			}
		}
		Extra extra = new Extra(targetId, speciesId);
		context.scenario.data.ProcessTargetArea(targetId, DSpecies, extra);
		return extra;
	}

	public static Extra GetMeasures(JVarContext context, int targetId, EActionTypes action) {
		foreach (Extra e in cache) {
			if ((e.targetId == targetId) && (e.varType == VarType.MEASURE) && (e.actionType == action)) {
				return e;
			}
		}
		Extra extra = new Extra(targetId, action);
		context.scenario.data.ProcessTargetAreaMeasure(targetId, action, DJustCount, extra);
		return extra;
	}
	
	public static Extra GetResearch(JVarContext context, int targetId, EResearchTypes research) {
		foreach (Extra e in cache) {
			if ((e.targetId == targetId) && (e.varType == VarType.RESEARCH) && (e.researchType == research)) {
				return e;
			}
		}
		Extra extra = new Extra(targetId, research);
		
		if ((int) research < 0x20) {
			if (context.scenario.IsGameMode) {
				JScenarioProgress progress = context.scenario.progress;
				JResearchPoint[] points = progress.GetResearchPointsArray();
				JTerrainData data = context.scenario.data;
				foreach (JResearchPoint point in points) {
					bool isRightResearch = false;
					foreach (JResearchPoint.Measurement measure in point.measurements) {
						if (measure.researchType == research) {
							isRightResearch = true;
							break;
						}
					}
					if (isRightResearch) {
						if (data.CheckTargetMapAt(targetId, point.x, point.y)) {
							extra.avgCount++;
						}
					}
				}
			}
		}
		else {
			context.scenario.data.ProcessTargetAreaResearch(targetId, research, DJustCount, extra);
		}
		
		return extra;
	}
	
	public long GetValueFunc(Extra e, SubVarType svt) {
		switch (svt) {
		case SubVarType.AVG : return (e.total == 0)?0:(e.avgCount / e.total);
		case SubVarType.MIN : return e.min;
		case SubVarType.MINCOUNT : return e.minCount;
		case SubVarType.MAX : return e.max;
		case SubVarType.MAXCOUNT : return e.maxCount;
		case SubVarType.SUM : return e.avgCount;
		case SubVarType.COUNT : return e.total;
		case SubVarType.COUNTNOTZERO : return e.notZero;
		default : return 0;
		}
	}
	
	public long GetValue(JVarContext context) {
		JScenario scenario = context.scenario;
		switch (varType) {
		case VarType.UNDEFINED :
			return 0;
		case VarType.COUNTER :
			return context.GetValue(varName);
		case VarType.YEAR :
			if (!scenario.IsGameMode) return scenario.startYear;
			return scenario.progress.year;
		case VarType.BUDGET :
			if (!scenario.IsGameMode) return scenario.startBudget;
			return scenario.progress.budget;
		case VarType.MEASURE :
			return GetValueFunc(JExpression.GetMeasures(context, targetArea, (EActionTypes) index), subVarType);
		case VarType.RESEARCH :
			return GetValueFunc(JExpression.GetResearch(context, targetArea, (EResearchTypes) index), subVarType);
		case VarType.PARAMETER :
			return GetValueFunc(JExpression.GetParameters(context, targetArea, (EParamTypes) index), subVarType);
		case VarType.SPECIES :
			return GetValueFunc(JExpression.GetSpecies(context, targetArea, index), subVarType);
		}
		return 0;
	}
	
	public bool SetValue(JVarContext context, long val) {
		JScenario scenario = context.scenario;
		switch (varType) {
		case VarType.COUNTER :
			context.SetValue(varName, val);
			return true;
		case VarType.YEAR :
			if (scenario.IsGameMode) scenario.progress.year = (int) val;
			return true;
		case VarType.BUDGET :
			if (scenario.IsGameMode) scenario.progress.budget = val;
			return true;
		default :
			return false;
		}
	}
}
