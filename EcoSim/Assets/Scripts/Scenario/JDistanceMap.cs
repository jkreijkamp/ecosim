using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JDistanceMap {
	
	public byte[] map;
	public byte[] occurance;
	private JScenario scenario;
	private JTerrainData data;
	private JAnimalSpecies species;
	private int width;
	private int height;
	
	private int deathsCount = 0;
	private int foodCount = 0;
	private int stepCount = 0;
	
	private static int[] mapping = new  int[] {
		0, 1, 2, 2, 3, 3, 3, 3,
		4, 4, 4, 4, 4, 4, 4, 4,
		5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
		6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
		6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	};
	
	private static int[] reverseMapping = new int[] {
		0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024
	};
	
	public JDistanceMap(JScenario scenario, JAnimalSpecies species) {
		this.scenario = scenario;
		this.data = scenario.data;
		this.species = species;
		width = data.width;
		height = data.height;
		scenario.context.functions.TryGetValue("BerekenDood" + (species.index), out deathFn);
		scenario.context.functions.TryGetValue("BerekenGeboorte" + (species.index), out birthFn);
	}

	EcoBaseFunction deathFn = null;
	EcoBaseFunction birthFn = null;
	
	public void CalcOccurance() {	
		
		int speciesIndex = species.index;
		// first clear species inventarisation
		for (int cy = 0; cy < data.cHeight; cy++) {
			for (int cx = 0; cx < data.cWidth; cx++) {
				JCellData cell = data.GetCell(cx, cy);
				cell.ClearAnimalSpecies(speciesIndex);
			}
		}
		deathsCount = 0;
		foodCount = 0;
		stepCount = 0;
		
		CalculateDistance(species.food, species.foodOverrule);
		// we make a copy to handle changes to the list of nests gracefully.
		JAnimalSpecies.Nest[] nestsArray = species.nests.Clone() as JAnimalSpecies.Nest[];
		List<JAnimalSpecies.Nest> livingNests = new List<JAnimalSpecies.Nest>();
		foreach (JAnimalSpecies.Nest nest in nestsArray) {
			nest.food = 0;
			Coordinate pos = nest.position;
			int females = nest.females;
			int femaledeaths = 0;
			if (females > 0) {
				femaledeaths = MoveRandom(pos.x, pos.y, species, nest, females, species.femaleWandering, species.femaleWalkDistance);
			}
			int males = nest.males;
			int maledeaths = 0;
			if (males > 0) {
				maledeaths = MoveRandom(pos.x, pos.y, species, nest, males, species.maleWandering, species.maleWalkDistance);
			}
			if ((males > 0) || (females > 0)) {
				livingNests.Add(nest);
			}
			if ((birthFn != null) && ((males > 0) || (females > 0))) {
				try {
					EcoNestVar nestV = new EcoNestVar(scenario.context, species, nest, "__internal__");					
					birthFn.Execute(scenario.context, nestV, new EcoNumberValue(maledeaths), new EcoNumberValue(femaledeaths), new EcoNumberValue(nest.food));
				}
				catch (System.Exception e) {
					Debug.LogException(e);
					MyDebug.LogError(e.Message);
				}
			}
		}
		foreach (JAnimalSpecies.Nest nest in livingNests) {
			data.SetAnimalSpecies(nest.position.x, nest.position.y, species.index, 13);
		}
		scenario.context.SetGlobal("dier" + species.index + "nesten", (long) livingNests.Count);
		scenario.context.SetGlobal("dier" + species.index + "cadavers", (long) deathsCount);
		scenario.context.SetGlobal("dier" + species.index + "voedsel", (long) foodCount);
		scenario.context.SetGlobal("dier" + species.index + "sporen", (long) stepCount);
	}
	
	int[] dirx = new int[] { -1, 0, 1, 0 };
	int[] diry = new int[] { 0, -1, 0, 1 };
	
	public int GetMapVal(int x, int y) {
		if ((x < 0) || (y < 0) || (x >= width) || (y >= height)) return 256; // out of bounds
		return map[x + y * width];
	}
	
	public int MoveRandom(int x, int y, JAnimalSpecies species, JAnimalSpecies.Nest nest, int count, float wandering, int distance) {
		if ((distance < 0) || (count <= 0)) return 0;
		int danger = data.GetData(species.danger, x, y);
		int mapval = map[x + width * y];
		if (mapval == 255) return 0;
		int dir = Random.Range(0, 4);
//		Debug.Log("MoveRnd " + x + "," + y + " " + count + " " + distance + " danger " + danger + " mapval " + mapval);
		// we try to make simple function that results in guaranteed deaths for top range of danger
		int deaths = 0;
		if (danger > 0) {
			if (deathFn != null) {
				try {
					EcoStructValue strct = new EcoStructValue(
						"risico", new EcoFloatValue((float) danger / 2.55f),
						"spoor", new EcoFloatValue((float) mapval / 2.55f),
						"aantal", new EcoNumberValue(count),
						"afstand", new EcoNumberValue(distance));
					EcoValue result = deathFn.Execute(scenario.context, new EcoTileVar(scenario.context, x, y, "_internal_"), strct);
					if (result is EcoNumberValue) {
			 			deaths = (int) (((EcoNumberValue) result).v);			
					}
					else if (result is EcoFloatValue) {
			 			deaths = (int) (((EcoFloatValue) result).v);			
					}
					else {
						MyDebug.LogError("Resultaat van BerekenDood" + (species.index + 1) + " moet number of float zijn.");
						return count; // quick termination;
					}
				}
				catch (System.Exception e) {
					Debug.LogException(e);
					MyDebug.LogError(e.Message);
					return count; // quick termination...
				}
			}
			else {
	 			deaths = (int) (count * danger * (0.2f + Random.value) * 0.02f);
			}
		}
		// let these death values have immediate result in spreading
		int newCount = Mathf.Max(0, count - deaths);
		if (mapval > 228) {
			nest.food += newCount;
			distance -= 2; // no need to walk much anymore...
			int newmapval = mapval - newCount;
			if (newmapval < 228) newmapval = 1;
			map[x + width * y] = (byte) newmapval;
		}
		int deathOnCurrentCell = deaths;
		if (newCount > 0) {
			int wanderCount = Mathf.Min((int) (Random.value * wandering * newCount), 4);
			if (mapval == 0) wanderCount += 2;
			bool found = false;
			int maxFoundDeaths = 0;
			// stop when i >= 4 or (found && wanderCount <= 0)
			for (int i = 0; (i < 4) && !(found && (wanderCount <= 0)); i++) {
				int lookval = GetMapVal(x + dirx[dir], y + diry[dir]);
//				Debug.Log("p " + x + "," + y + " np " + (x + dirx[dir]) + "," + (y + diry[dir]) + " i " + i + " mapv " + mapval + " lookv " + lookval);
				if ((lookval > mapval) && (lookval < 255)) {
					// found optimal route
					maxFoundDeaths = Mathf.Max(MoveRandom(x + dirx[dir], y + diry[dir], species, nest, newCount, wandering, distance - 1), maxFoundDeaths);
					found = true;
				}
				else if ((lookval < 256) && (wanderCount > 0)) {
					// wander
					maxFoundDeaths = Mathf.Max(MoveRandom(x + dirx[dir], y + diry[dir], species, nest, (newCount + 1) / 3, wandering, distance - 2), maxFoundDeaths);
					wanderCount--;
				}
				dir = (dir + 1) % 4;
			}
			deaths += maxFoundDeaths;
		}
		if (mapval >= 228) {
			if (data.GetAnimalSpecies(x, y, species.index) != 11) foodCount++;
			data.SetAnimalSpecies(x, y, species.index, 11);
		}
		else {
			if (deathOnCurrentCell > 0) {
				if (data.GetAnimalSpecies(x, y, species.index) != 12) deathsCount++;
				data.SetAnimalSpecies(x, y, species.index, 12);
			}
			else {
				int oldCount = data.GetAnimalSpecies(x, y, species.index);
				if (oldCount < 12) {
					if (oldCount == 0) stepCount++;
					int val = reverseMapping[oldCount] + newCount;
//		Debug.Log("setVal " + x + "," + y + " = " + val + " newCount = " + newCount);
					data.SetAnimalSpecies(x, y, species.index, mapping[Mathf.Min(mapping.Length - 1, val)]);
				}
			}
		}
		return Mathf.Min(deaths, count);
	}
	
	public void CalculateDistance(EParamTypes par, EParamTypes overrule) {
		int w = width;
		int h = height;
		int max = 50;
		
		map = new byte[w * h];
		byte[] cost = new byte[w * h];
		int p = w + 1;
		for (int y = 1; y < h - 1; y++) {
			for (int x = 1; x < w - 1; x++) {
				byte v = data.GetData(overrule, x, y);
				if (v == 0) {
					v = data.GetData(par, x, y);
				}
				if (v <= 178) {
					// v (0% - 69%) is cost of walking through tile
					cost[p] = (byte) (v * 100 / 255 + 1);
				}
				else if (v <= 229) {
					// v (70% - 89%) is inpenetrable
					cost[p] = 255;
					map[p] = 255;
				}
				else {
					// v (90% - 100%) is food...
					cost[p] = 1;
					map[p] = (byte) v;
				}
				p++;
			}
			p += 2;
		}
		for (int i = max; i > 0; i--) {
			p = w + 1;
			for (int y = 1; y < h - 1; y++) {
				for (int x = 1; x < w - 1; x++) {
					byte v = map[p];
					if (v > 0) {
						if (v > 228) v = (byte) (v - 27); // voedsel gebied, neem laagst geldige waarde
						byte costv = cost[p];
						if (costv < v) {
							v -= costv;
							if (v > map[p - 1]) map[p - 1] = v;
							if (v > map[p + 1]) map[p + 1] = v;
							if (v > map[p - w]) map[p - w] = v;
							if (v > map[p + w]) map[p + w] = v;
						}
					}
					p++;
				}
				p += 2;
			}			
		}
	}
}
