using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class JMeasureMap : JAreaMap {
	
	public const string XML_ELEMENT = "measure";
	
	public readonly EActionGroups actionGroup;
	
	private JMeasureMap(EActionGroups actionGroup) {
		this.actionGroup = actionGroup;
	}
	
	/**
	 * makes an empty measuremap
	 */
	public JMeasureMap(JScenario scenario, EActionGroups actionGroup) {
		this.actionGroup = actionGroup;
		int yLen = scenario.data.cHeight;
		int xLen = scenario.data.cWidth;
		maps = new JAreaMapCell16[yLen, xLen];
		for (int cy = 0; cy < yLen; cy++) {
			for (int cx = 0; cx < xLen; cx++) {
				maps[cy, cx] = new JAreaMapCell16(cx, cy);
			}
		}		
	}
	
	/**
	 * Copy constructor
	 */
	public JMeasureMap(JMeasureMap copyFrom) {
		actionGroup = copyFrom.actionGroup;
		maps = copyFrom.CloneMap();
	}
	
	/**
	 * clone method
	 */
	public override JAreaMap Clone() {
		JMeasureMap mm = new JMeasureMap(actionGroup);
		mm.maps = CloneMap();
		return mm;
	}
	
	
	/**
	 * Read in a measure map from xml stream, stream should be at the element defining the map
	 */
	public static JMeasureMap LoadFromXml(JScenario scenario, XmlTextReader reader) {
		string elementName = reader.Name.ToLower();
		EActionGroups actionGroup = EnumExtensions.GetActionGroup(reader.GetAttribute("measuregroup").ToString());
		
		JMeasureMap map = new JMeasureMap(actionGroup);
		
		int yLen = scenario.data.cHeight;
		int xLen = scenario.data.cWidth;
		
		map.maps = new JAreaMapCell[yLen, xLen];
		
		// read in stored cells
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "cell")) {
					JAreaMapCell cell = JAreaMapCell.LoadFromXML(reader);
					map.maps[cell.cy, cell.cx] = cell;
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == elementName)) {
					break;
				}
			}
		}
		
		// fill in empty cells
		for (int cy = 0; cy < yLen; cy++) {
			for (int cx = 0; cx < xLen; cx++) {
				if (map.maps[cy, cx] == null) {
					// for now it's always a 4-bit per tile map
					map.maps[cy, cx] = new JAreaMapCell16(cx, cy);
				}
			}
		}
		return map;
	}
	
	public override void SaveToXml(XmlTextWriter writer) {
		writer.WriteStartElement(XML_ELEMENT);
		writer.WriteAttributeString("measuregroup", actionGroup.ToString());
		foreach (JAreaMapCell cell in maps) {
			cell.SaveToXml(writer);
		}
		writer.WriteEndElement();
	}
	
	/**
	 * See baseclass for explaination
	 */
	public override void CopyInto(JAreaMap targetMap, bool onlyCopyChangedCells) {
		if (!(targetMap is JMeasureMap)) {
			throw new System.Exception("targetMap is not a JMeasureMap but a " + targetMap.GetType().ToString());
		}
		JMeasureMap castedMap = (JMeasureMap) targetMap;
		if (castedMap.actionGroup != actionGroup) {
			throw new System.Exception("targetMap actiongroup = " + castedMap.actionGroup.ToString() + "  but needed " + actionGroup.ToString());
		}
		base.CopyInto(castedMap, onlyCopyChangedCells);
	}
}
