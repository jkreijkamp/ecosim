using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class JXVegetation {
	public string name;
	public Color32 colour = new Color32(255, 255, 255, 255);
	public JXSuccession succession;
	public int index;
	public JXTerrainTile[] tiles;
	public EActionTypes[] acceptableMeasures;
	public bool isStable = true;
	
	public JXVegetation() {
		newParams = new NewParam[0];
		steps = new Step[0];
		changes = new ParamChange[0];
		acceptableMeasures = new EActionTypes[0];
	}
	
	public bool IsValidMeasure(EActionTypes t) {
		// if ((int) t > 0x40) return true;
		foreach (EActionTypes at in acceptableMeasures) {
			if (at == t) return true;
		}
		return false;
	}
	
	public void MakeValidMeasure(EActionTypes t, bool valid) {
		List<EActionTypes> list = new List<EActionTypes>(acceptableMeasures);
		if (list.Contains(t)) {
			if (!valid) list.Remove(t);
		}
		else {
			if (valid) list.Add(t);
		}
		acceptableMeasures = list.ToArray();
	}
	
	public void UpdateStableParam() {
		isStable = true;
		foreach (Step s in steps) {
			if (s.action == EActionTypes.GeenActie) {
				isStable = false;
				// Debug.Log("vegetatie " + name + " is onstabiel");
				return;
			}
		}
		foreach (ParamChange c in changes) {
			if (c.action == EActionTypes.GeenActie) {
				isStable = false;
				// Debug.Log("vegetatie " + name + " is onstabiel");
				return;
			}
		}
	}
	
	public class NewParam {
		public EParamTypes type;
		public byte minRange;
		public byte maxRange;
		
		public void Assign(NewParam source) {
			type = source.type;
			minRange = source.minRange;
			maxRange = source.maxRange;
		}
	}
	
	public class ParamCondition {
		public EParamTypes type;
		public byte minRange;
		public byte maxRange;
		
		public void Assign(ParamCondition source) {
			type = source.type;
			minRange = source.minRange;
			maxRange = source.maxRange;
		}
	}
	
	public class ParamDelta {
		public EParamTypes type;
		public int minRange;
		public int maxRange;
		public int delta;
		public void Assign(ParamDelta source) {
			type = source.type;
			minRange = source.minRange;
			maxRange = source.maxRange;
			delta = source.delta;
		}
	}
	
	public class ParamChange {
		public float chance;
		public EActionTypes action;
		public ParamDelta[] deltas;
		
		public void Assign(ParamChange source) {
			chance = source.chance;
			action = source.action;
			deltas = new ParamDelta[source.deltas.Length];
			for (int i = 0; i < source.deltas.Length; i++) {
				if (source.deltas[i] != null) {
					deltas[i] = new ParamDelta();
					deltas[i].Assign(source.deltas[i]);
				}
			}
		}
	}
	
	public class Step {
		public int newVegetation;
		public float chance;
		public EActionTypes action;
		public ParamCondition[] conditions;

		public void Assign(Step source) {
			newVegetation = source.newVegetation;
			chance = source.chance;
			action = source.action;
			conditions = new ParamCondition[source.conditions.Length];
			for (int i = 0; i < source.conditions.Length; i++) {
				if (source.conditions[i] != null) {
					conditions[i] = new ParamCondition();
					conditions[i].Assign(source.conditions[i]);
				}
			}
		}
	}
	
	public NewParam[] newParams;
	public ParamChange[] changes;
	public Step[] steps;
}
