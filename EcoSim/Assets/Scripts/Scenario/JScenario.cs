using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;

public class JScenario {
	
	public string name;
	public string shortDesc;
	public string description;
	public string scenarioPath;
	public string saveGamePath;
	
	public int startYear;
	public int duration;
	public long startBudget;
	public bool normalSuccessionActive = true;
	
	public JScenarioProgress progress;
	public JTerrainData data;
	public JXSuccession[] successions;
	public JAnimalSpecies[] animalSpecies;
	public JSpecies[] species;

	public string[] articles;
	public JEncyclopediaEntry[] encyclopedia;
	public List<JScenarioGoal> goals;
	public List<JGameRule> gameRules;
	public JVarContext oldContext;
	public EcoContext context;
	public string mainScript;
	
	public Dictionary<string, GameObject> extraPrefabs = new Dictionary<string, GameObject>();
	public Dictionary<string, Texture2D> extraTextures= new Dictionary<string, Texture2D>();
	
	private const int SAVEGAME_SLOTS = 9;
	
	public class ActionInfo {
		public EActionTypes action;
		public string name;
		public string descr;
		public int costPerTile;
		public ActionGroup actionGroup; // used for action objects
	}

	public class ResearchInfo {
		public EResearchTypes research;
		public string name;
		public string descr;
		public int costPerTile;
	}
	
	public class CombinedData {
		public int offset;
		public EParamTypes[] parameters;
		public float[] multipliers;
	}
			
	public class ActionObject {
		public string prefabKey;
		public Vector3 position;
		public float rotation;
		public GameObject instance;
		public bool isChanged;
		public bool isEnabled;
		public InfluenceCoordinate[] influence;
	}
	
	public class ActionGroup {
		public EActionTypes action;
//		public EParamTypes influenceParam;
//		public int activeValue;
//		public int inactiveValue;
		public ActionObject[] objects;
	}
	
	public Dictionary<EActionTypes, ActionInfo> actions = new Dictionary<EActionTypes, ActionInfo>();
	public Dictionary<EResearchTypes, ResearchInfo> research = new Dictionary<EResearchTypes, ResearchInfo>();
	public Dictionary<EParamTypes, CombinedData> combinedData = new Dictionary<EParamTypes, CombinedData>();
	
	private JScenario(string name) {
		this.name = name;
	}
	
	public JScenario(string name, int width, int height) {
		this.name = name;
		scenarioPath = JConfig.scenarioPath + name + Path.DirectorySeparatorChar;
		startYear = 2012;
		startBudget = 1000000;
		duration = 5;
		description = "Omschrijving van " + name;
		shortDesc = "";
		successions = new JXSuccession[1];
		JXSuccession s = new JXSuccession();
		s.name = "Speciaal";
		s.vegetation = new JXVegetation[1];
		JXVegetation veg = new JXVegetation();
		veg.name = "Speciaal";
		veg.tiles = new JXTerrainTile[1];
		JXTerrainTile tile = new JXTerrainTile();
		veg.tiles[0] = tile;
		s.vegetation[0] = veg;
		successions[0] = s;
		
		UpdateSuccessionIndices();		
		
		species = new JSpecies[0];
		animalSpecies = new JAnimalSpecies[0];
		
		data = new JTerrainData(this, width, height);
		goals = new List<JScenarioGoal>();
		gameRules = new List<JGameRule>();
		oldContext = new JVarContext(this);
		articles = new string[0];
		encyclopedia = new JEncyclopediaEntry[0];
		context = new EcoContext(this);
		mainScript = "";
	}
	
	public ActionInfo GetActionInfo(EActionTypes action) {
		if (actions.ContainsKey(action)) return actions[action];
		return null;
	}
	
	public void AddActionInfo(ActionInfo ai) {
		actions.Add(ai.action, ai);
	}
	
	public void RemoveActionInfo(EActionTypes at) {
		actions.Remove(at);
	}

	public ResearchInfo GetResearchInfo(EResearchTypes research) {
		if (this.research.ContainsKey(research)) return this.research[research];
		return null;
	}
	
	public void AddResearchInfo(ResearchInfo ri) {
		research.Add(ri.research, ri);
	}
	
	public void RemoveResearchInfo(EResearchTypes rt) {
		research.Remove(rt);
	}
	public string CompileScript() {
		return CompileScript(false);
	}
	
	public string CompileScript(bool redefineFunctions) {
		context.Reset();
		string errStr = null;
		try {
			EcoScript es = new EcoScript();
			es.Parse(context, mainScript, redefineFunctions);
		}
		catch (EcoException e) {
			errStr = "Script fout: " + e.Message;
		}
		catch (System.Exception e2) {
			errStr = "Systeem fout: " + e2.Message;
		}
		return errStr;
	}
	
	public void SetScript(string script) {
		mainScript = script;
	}
	
	public string GetScript() {
		return mainScript;
	}
	
	/**
	 * Loads scenario and everything with it
	 * Can be used for game and edit mode
	 */
	public static JScenario Load(string name, string saveGamePath, bool isGameMode, bool previewOnly) {
		
		JScenario scenario = new JScenario(name);
		scenario.scenarioPath = JConfig.scenarioPath + name + Path.DirectorySeparatorChar;
		scenario.saveGamePath = saveGamePath;
		
//		UnityEngine.Debug.Log("savegamepath = " + saveGamePath);
		string fileName = scenario.scenarioPath + "scenario.xml";
		
		if (!File.Exists(fileName)) {
			UnityEngine.Debug.Log("no scenario '" + fileName + "' found");
			return null;
		}
		
		if (!previewOnly) {
			// we need to load successions first as species depend on this
			scenario.successions = JVegetationFile.Load(scenario);
			scenario.UpdateSuccessionIndices();
		}

		
		XmlTextReader reader = new XmlTextReader(new System.IO.StreamReader(fileName));
		while (reader.Read()) {
			XmlNodeType nType = reader.NodeType;
			if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "scenario")) {
				scenario.LoadScenario(reader, previewOnly);
			}
		}
		reader.Close();
		if (!previewOnly) {
			scenario.data = JTerrainData.Load(scenario);		
//			UnityEngine.Debug.Log("loaded data " + scenario.data + " " + scenario.data.cells);
		}
		if (isGameMode) {
			scenario.progress = new JScenarioProgress(scenario);
		}
		
		return scenario;
	}
	
	// try free up memory by unloading cells
	public void Unload() {
		int cleanupCount = 0;
		int totalCount = 0;
		foreach (JCellData cell in data.cells) {
			if (cell.isLoaded) totalCount++;
			if (cell.TryCleanup()) cleanupCount++;
		}
		if (cleanupCount > 0) {
			// UnityEngine.Debug.Log("unloaded " + cleanupCount + " out of " + totalCount + " cells.");
			System.GC.Collect();
			UnityEngine.Resources.UnloadUnusedAssets();
		}
	}
	
	/**
	 * Saves a scenario (from editor, not progress)
	 */
	public void SaveScenario(string name) {
		this.name = name;
		string path = JScenario.GetScenarioPath(name);
		Save(path);
	}
	
	/**
	 * Saves scenario and everything with it
	 */
	private void Save(string path) {
		if (!Directory.Exists(path)) {
			Directory.CreateDirectory(path);
		}
		if (!Directory.Exists(path + "/images")) {
			Directory.CreateDirectory(path + "/images");
		}
		
		if (progress != null) {
			// in game so we need to save game progress
			progress.Save(path);
			data.Save(path, false);
		}
		else {
			// scenario data is static so we only save it if in editor mode
			
			string fileName = path + "scenario.xml";
			XmlTextWriter writer = new XmlTextWriter(fileName, System.Text.Encoding.UTF8);
			writer.WriteStartDocument(true);
			writer.WriteStartElement("scenario");
			writer.WriteAttributeString("name", name);
			writer.WriteAttributeString("shortdescription", shortDesc);
			writer.WriteAttributeString("version", JConfig.FILE_VERSION.ToString());
			writer.WriteAttributeString("description", description);
			writer.WriteAttributeString("budget", startBudget.ToString());
			writer.WriteAttributeString("year", startYear.ToString());
			writer.WriteAttributeString("duration", duration.ToString());
			writer.WriteAttributeString("normalsuccessionactive", normalSuccessionActive?"true":"false");
			foreach (ActionInfo ai in actions.Values) {
				writer.WriteStartElement("action");
				writer.WriteAttributeString("type", ai.action.ToString().ToLower());
				writer.WriteAttributeString("name", ai.name);
				writer.WriteAttributeString("description", ai.descr);
				writer.WriteAttributeString("cost", ai.costPerTile.ToString());
				if (ai.actionGroup != null) {
					ActionGroup actionGroup = ai.actionGroup;
//					writer.WriteAttributeString("parameter", actionGroup.influenceParam.ToString());
//					writer.WriteAttributeString("active", actionGroup.activeValue.ToString());
//					writer.WriteAttributeString("inactive", actionGroup.inactiveValue.ToString());
					foreach (ActionObject obj in actionGroup.objects) {
						writer.WriteStartElement("object");
						writer.WriteAttributeString("prefab", obj.prefabKey);
						writer.WriteAttributeString("position", JUtil.V3ToStr(obj.position));
						writer.WriteAttributeString("rotation", obj.rotation.ToString());
						writer.WriteAttributeString("influence", JStringUtils.InflCoordAToString(obj.influence));
						writer.WriteEndElement();
					}
				}
				writer.WriteEndElement();
			}
			foreach (ResearchInfo ri in research.Values) {
				writer.WriteStartElement("research");
				writer.WriteAttributeString("type", ri.research.ToString().ToLower());
				writer.WriteAttributeString("name", ri.name);
				writer.WriteAttributeString("description", ri.descr);
				writer.WriteAttributeString("cost", ri.costPerTile.ToString());
				writer.WriteEndElement();
			}
			foreach (JSpecies s in species) {
				writer.WriteStartElement("species");
				writer.WriteAttributeString("name", s.name);
				writer.WriteAttributeString("spawncount", s.spawnCount.ToString());
				writer.WriteAttributeString("spawnmultiplier", s.spawnMultiplier.ToString());
				writer.WriteAttributeString("spawnradius", s.spawnRadius.ToString());
				foreach (JSpecies.Rule r in s.rules) {
					writer.WriteStartElement("rule");
					writer.WriteAttributeString("canspawn", r.canSpawn?"true":"false");
					writer.WriteAttributeString("delta", r.delta.ToString());
					writer.WriteAttributeString("chance", r.chance.ToString());
					foreach (JSpecies.Vegetation v in r.vegetations) {
						writer.WriteStartElement("vegetation");
						writer.WriteAttributeString("successionindex", v.successionIndex.ToString());
						writer.WriteAttributeString("vegetationindex", v.vegetationIndex.ToString());
						writer.WriteEndElement();
					}
					foreach (JSpecies.Parameter p in r.conditions) {
						writer.WriteStartElement("condition");
						writer.WriteAttributeString("type", p.type.ToString());
						writer.WriteAttributeString("min", p.min.ToString());
						writer.WriteAttributeString("max", p.max.ToString());
						writer.WriteEndElement();
					}
					writer.WriteEndElement();
				}
				writer.WriteEndElement();
			}
			foreach (JAnimalSpecies s in animalSpecies) {
				writer.WriteStartElement("animalspecies");
				writer.WriteAttributeString("name", s.name);
				writer.WriteAttributeString("danger", s.danger.ToString());
				writer.WriteAttributeString("food", s.food.ToString());
				if (s.foodOverrule != s.food) {
				writer.WriteAttributeString("foodoverrule", s.foodOverrule.ToString());
				}
				writer.WriteAttributeString("walkdistancef", s.femaleWalkDistance.ToString());
				writer.WriteAttributeString("walkdistancem", s.maleWalkDistance.ToString());
				writer.WriteAttributeString("wanderf", s.femaleWandering.ToString());
				writer.WriteAttributeString("wanderm", s.maleWandering.ToString());
				foreach (JAnimalSpecies.Nest n in s.nests) {
					writer.WriteStartElement("nest");
					writer.WriteAttributeString("x", n.position.x.ToString());
					writer.WriteAttributeString("y", n.position.y.ToString());
					writer.WriteAttributeString("f", n.females.ToString());
					writer.WriteAttributeString("m", n.males.ToString());
					writer.WriteEndElement();
				}
				writer.WriteEndElement();
			}
			foreach (KeyValuePair<EParamTypes, CombinedData> kvp in combinedData) {
				writer.WriteStartElement("combineddata");
				writer.WriteAttributeString("parameter", kvp.Key.ToString().ToLower());
				CombinedData cd = kvp.Value;
				writer.WriteAttributeString("offset", cd.offset.ToString());
				for (int i = 0; i < cd.parameters.Length; i++) {
					writer.WriteStartElement("item");
					writer.WriteAttributeString("parameter", cd.parameters[i].ToString().ToLower());
					writer.WriteAttributeString("multiplier", cd.multipliers[i].ToString());
					writer.WriteEndElement();
				}
				writer.WriteEndElement();
			}
			foreach (string s in articles) {
				writer.WriteStartElement("article");
				writer.WriteAttributeString("text", s);
				writer.WriteEndElement();
			}
			foreach (JEncyclopediaEntry e in encyclopedia) {
				writer.WriteStartElement("definition");
				writer.WriteAttributeString("item", e.item);
				writer.WriteAttributeString("text", e.text);
				if (e.externalURL != null) {
					writer.WriteAttributeString("url", e.externalURL);
				}
				writer.WriteEndElement();
			}
			foreach (JScenarioGoal g in goals) {
				g.Save(writer);
			}
			oldContext.Save(writer);
			foreach (JGameRule rule in gameRules) {
				rule.Save(writer);
			}
			writer.WriteStartElement("script");
			writer.WriteAttributeString("text", mainScript);
			writer.WriteEndElement();
			writer.WriteEndElement();
			writer.WriteEndDocument();
			writer.Close();
			data.Save(path, true);
			JVegetationFile.Save(path, this);
			JRenderMap.SaveTiles(path);
			
			// copying of image files and extraassets is now done in ScenarioEditor.
//			if (System.String.Compare(
//				Path.GetFullPath(path).TrimEnd('\\'),
//				Path.GetFullPath(scenarioPath).TrimEnd('\\'),
//				System.StringComparison.InvariantCultureIgnoreCase) != 0) {
//				try {
//					string imgPath = scenarioPath + "/images/";
//					string newImgPath = path + "/images/";
//					if (!Directory.Exists(newImgPath)) {
//						Directory.CreateDirectory(newImgPath);
//					}
//					string[] files = Directory.GetFiles(scenarioPath + "/images");
//					foreach (string f in files) {
//						Debug.Log("f = '" + f + "'");
//						if (File.Exists(imgPath)) {
//							File.Copy(imgPath + f, newImgPath + f, true);
//						}
//					}
//				}
//				catch (System.Exception e) {
//					UnityEngine.Debug.LogError(e.Message);
//				}
//			}
			
			scenarioPath = path;			
		}
	}
	
	/**
	 * Loads a savegame
	 */
	public static JScenario LoadGame(int slot, bool previewOnly) {
		string path = GetSaveGamePath(slot);
		JScenarioProgress progress = JScenarioProgress.Load(path, previewOnly);
		if (progress == null) return null;
		// JScenario scenario =  Load(progress.scenarioName, true, previewOnly);
		JScenario scenario = progress.scenario;
		return scenario;
	}
	
	/**
	 * Saves to savegame slot
	 */
	public void SaveGame(int slot) {
		string path = GetSaveGamePath(slot);
		Save(path);
		saveGamePath = path;
	}
	
	/**
	 * return true if playing, false if in editor mode
	 */
	public bool IsGameMode {
		get { return (progress != null); }
	}
	
	public static bool IsScenario(string name) {
		return (File.Exists(GetScenarioPath(name) + "scenario.xml"));
	}

	public static bool IsObsoleteScenario(string name) {
		UnityEngine.Debug.Log(GetScenarioPath(name) + "info.xml");
		return (File.Exists(GetScenarioPath(name) + "info.xml"));
	}
	
	public static string GetScenarioPath(string name) {
		return JConfig.scenarioPath + name + System.IO.Path.DirectorySeparatorChar;
	}
	
	
	public static string GetSaveGamePath(int slotNr) {
		return JConfig.saveGamesPath + "Voortgang_" + slotNr.ToString() + System.IO.Path.DirectorySeparatorChar;;
	}	
	
	public static JScenario[] GetSaveGamePreviews() {
		JScenario[] result = new JScenario[SAVEGAME_SLOTS];
		for (int i = 0; i < SAVEGAME_SLOTS; i++) {
			try {
				result[i] = LoadGame(i, true);
			}
			catch (System.Exception e) {
				MyDebug.LogError("For slot " + i + " : " + e);
				UnityEngine.Debug.LogError(e.StackTrace);
			}
		}
		return result;
	}

	public static JScenario[] GetScenarioPreviews() {
		List<JScenario> scenarios = new List<JScenario>();
		DirectoryInfo di = new DirectoryInfo(JConfig.scenarioPath);
		DirectoryInfo[] scenarioDirs = di.GetDirectories();
		foreach (DirectoryInfo scenarioDir in scenarioDirs) {
			try {
				JScenario scenario = Load(scenarioDir.Name, null, true, true);
				if (scenario != null) {
					scenarios.Add(scenario);
				}
			}
			catch (System.Exception e) {
				MyDebug.LogError("for directory " + scenarioDir.ToString() + " : " + e);
				UnityEngine.Debug.LogError(e.StackTrace);
			}
		}
		return scenarios.ToArray();
	}

	private static ActionObject LoadActionObject(XmlTextReader reader) {
		ActionObject obj = new ActionObject();
		obj.position = JStringUtils.StringToVector3(reader.GetAttribute("position"));
		obj.rotation = float.Parse(reader.GetAttribute("rotation"));
		obj.prefabKey = reader.GetAttribute("prefab");
		obj.influence = JStringUtils.StringToInflCoordA(reader.GetAttribute("influence"));
		JIOUtils.ReadUntilEndElement(reader, "object");
		return obj;
	}
	
	private static ActionInfo LoadActionInfo(XmlTextReader reader) {
		ActionInfo ai = new ActionInfo();
		ai.action = EnumExtensions.GetActionType(reader.GetAttribute("type"));
		ai.name = reader.GetAttribute("name");
		ai.descr = reader.GetAttribute("description");
		ai.costPerTile = int.Parse(reader.GetAttribute("cost"));
		if ((ai.action >= EActionTypes.Object1) && (ai.action <= EActionTypes.Object8)) {
			// we have a action group
			ActionGroup actionGroup = new ActionGroup();
			actionGroup.action = ai.action;
//			actionGroup.influenceParam = EnumExtensions.GetParamType(reader.GetAttribute("parameter"));
//			actionGroup.inactiveValue = int.Parse(reader.GetAttribute("inactive"));
//			actionGroup.activeValue = int.Parse(reader.GetAttribute("active"));
			ai.actionGroup = actionGroup;
			List<ActionObject> objList = new List<ActionObject>();
			if (!reader.IsEmptyElement) {
				while (reader.Read()) {
					XmlNodeType nType = reader.NodeType;
					if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "object")) {
						objList.Add(LoadActionObject(reader));
					}
					else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "action")) {
						break;
					}
				}
			}
			actionGroup.objects = objList.ToArray();

		}
		else {
			JIOUtils.ReadUntilEndElement(reader, "action");
		}
		return ai;
	}
	
	private static ResearchInfo LoadResearchInfo(XmlTextReader reader) {
		ResearchInfo ri = new ResearchInfo();
		ri.research = EnumExtensions.GetResearchType(reader.GetAttribute("type"));
		ri.name = reader.GetAttribute("name");
		ri.descr = reader.GetAttribute("description");
		ri.costPerTile = int.Parse(reader.GetAttribute("cost"));
		JIOUtils.ReadUntilEndElement(reader, "research");
		return ri;
	}
	
	
	JSpecies.Rule LoadSpeciesRule(XmlTextReader reader) {
		JSpecies.Rule rule = new JSpecies.Rule();
		rule.canSpawn = reader.GetAttribute("canspawn").ToLower() == "true";
		rule.delta = int.Parse(reader.GetAttribute("delta"));
		rule.chance = float.Parse(reader.GetAttribute("chance"));
		List<JSpecies.Vegetation> vegetationList = new List<JSpecies.Vegetation>();
		List<JSpecies.Parameter> parameterList = new List<JSpecies.Parameter>();
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "vegetation")) {
					int successionIndex = int.Parse(reader.GetAttribute("successionindex"));
					int vegetationIndex = int.Parse(reader.GetAttribute("vegetationindex"));
					vegetationList.Add(new JSpecies.Vegetation(successionIndex, vegetationIndex));
					JIOUtils.ReadUntilEndElement(reader, "vegetation");

				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "condition")) {
					JSpecies.Parameter p = new JSpecies.Parameter();
					p.type = EnumExtensions.GetParamType(reader.GetAttribute("type"));
					p.min = float.Parse(reader.GetAttribute("min"));
					p.max = float.Parse(reader.GetAttribute("max"));
					if (p.type != EParamTypes.UNDEFINED) parameterList.Add(p);
					JIOUtils.ReadUntilEndElement(reader, "condition");
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "rule")) {
					break;
				}
			}
		}
		rule.vegetations = vegetationList.ToArray();
		rule.conditions = parameterList.ToArray();
		return rule;
	}
	
	JSpecies LoadSpecies(XmlTextReader reader) {
		JSpecies s = new JSpecies();
		s.name = reader.GetAttribute("name").ToString();
		s.spawnCount = int.Parse(reader.GetAttribute("spawncount"));
		s.spawnRadius = int.Parse(reader.GetAttribute("spawnradius"));
		s.spawnMultiplier = int.Parse(reader.GetAttribute("spawnmultiplier"));
		
		List<JSpecies.Rule> rules = new List<JSpecies.Rule>();
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "rule")) {
					rules.Add(LoadSpeciesRule(reader));
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "species")) {
					break;
				}
			}
		}
		s.rules = rules.ToArray();
		return s;
	}

	JAnimalSpecies.Nest LoadAnimalSpeciesNest(XmlTextReader reader) {
		JAnimalSpecies.Nest nest = new JAnimalSpecies.Nest();
		int x = int.Parse(reader.GetAttribute("x"));
		int y = int.Parse(reader.GetAttribute("y"));
		Coordinate pos = new Coordinate(x, y);
		nest.position = pos;
		nest.females = int.Parse(reader.GetAttribute("f"));
		nest.males = int.Parse(reader.GetAttribute("m"));
		JIOUtils.ReadUntilEndElement(reader, "nest");
		return nest;
	}
	
	
	JAnimalSpecies LoadAnimalSpecies(XmlTextReader reader) {
		JAnimalSpecies s = new JAnimalSpecies();
		s.name = reader.GetAttribute("name").ToString();
		s.danger = EnumExtensions.GetParamType(reader.GetAttribute("danger"));
		s.food = EnumExtensions.GetParamType(reader.GetAttribute("food"));
		string foodOverruleStr = reader.GetAttribute("foodoverrule");
		if (foodOverruleStr != null) {
			s.foodOverrule = EnumExtensions.GetParamType(foodOverruleStr);
		}
		else {
			s.foodOverrule = s.food;
		}
		s.femaleWalkDistance = int.Parse(reader.GetAttribute("walkdistancef"));
		s.maleWalkDistance = int.Parse(reader.GetAttribute("walkdistancem"));
		s.femaleWandering = float.Parse(reader.GetAttribute("wanderf"));
		s.maleWandering = float.Parse(reader.GetAttribute("wanderm"));
		List<JAnimalSpecies.Nest> nests = new List<JAnimalSpecies.Nest>();
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "nest")) {
					nests.Add(LoadAnimalSpeciesNest(reader));
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "animalspecies")) {
					break;
				}
			}
		}
		s.nests = nests.ToArray();
		return s;
	}
	
	CombinedData LoadCombinedData(XmlTextReader reader) {
		CombinedData cd = new CombinedData();
		cd.offset = int.Parse(reader.GetAttribute("offset"));
		List<EParamTypes> paramList = new List<EParamTypes>();
		List<float> multiplierList = new List<float>();
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "item")) {
					EParamTypes pt = EnumExtensions.GetParamType(reader.GetAttribute("parameter"));
					float multiplier = 1.0f;
					float.TryParse(reader.GetAttribute("multiplier"), out multiplier);
					paramList.Add(pt);
					multiplierList.Add(multiplier);
					JIOUtils.ReadUntilEndElement(reader, "item");
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "combineddata")) {
					break;
				}
			}
		}
		cd.parameters = paramList.ToArray();
		cd.multipliers = multiplierList.ToArray();
		return cd;
	}
	
	void LoadScenario(XmlTextReader reader, bool previewOnly) {
		bool emptyElt = reader.IsEmptyElement;
		name = reader.GetAttribute("name");
		shortDesc = reader.GetAttribute("shortdescription");
		if (shortDesc == null) shortDesc = "";
		description = reader.GetAttribute("description");
		long version = long.Parse(reader.GetAttribute("version"));
		if (version != JConfig.FILE_VERSION) {
			throw new System.Exception("Unsupported version " + version);
		}
		
		List<string> articleList = new List<string>();
		List<JEncyclopediaEntry> encList = new List<JEncyclopediaEntry>();
		List<JSpecies> speciesList = new List<JSpecies>();
		List<JAnimalSpecies> animalSpeciesList = new List<JAnimalSpecies>();
		combinedData.Clear();
		goals = new List<JScenarioGoal>();
		gameRules = new List<JGameRule>();
		oldContext = new JVarContext(this);
		mainScript = "";
		
		
		startBudget = long.Parse(reader.GetAttribute("budget"));
		startYear = int.Parse(reader.GetAttribute("year"));
		duration = int.Parse(reader.GetAttribute("duration"));
		if (reader.GetAttribute("normalsuccessionactive") != null) {
			normalSuccessionActive = (reader.GetAttribute("normalsuccessionactive").ToLower() == "true")?true:false;
		}
		else {
			normalSuccessionActive = true;
		}
		if (!emptyElt) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "action")) {
					ActionInfo ai = LoadActionInfo(reader);
					actions.Add(ai.action, ai);
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "research")) {
					ResearchInfo ri = LoadResearchInfo(reader);
					research.Add(ri.research, ri);
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "species")) {
					JSpecies s = LoadSpecies(reader);
					s.index = speciesList.Count;
					speciesList.Add(s);
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "animalspecies")) {
					JAnimalSpecies s = LoadAnimalSpecies(reader);
					s.index = animalSpeciesList.Count;
					animalSpeciesList.Add(s);
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "article")) {
					string s = reader.GetAttribute("text");
					articleList.Add(s);
					JIOUtils.ReadUntilEndElement(reader, "article");
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "definition")) {
					JEncyclopediaEntry entry = new JEncyclopediaEntry();
					entry.item = reader.GetAttribute("item");
					entry.text = reader.GetAttribute("text");
					entry.externalURL = reader.GetAttribute("url");
					encList.Add(entry);
					JIOUtils.ReadUntilEndElement(reader, "definition");
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "combineddata")) {
					EParamTypes paramType = EnumExtensions.GetParamType(reader.GetAttribute("parameter"));
					CombinedData cd = LoadCombinedData(reader);
					combinedData.Add(paramType, cd);
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == JScenarioGoal.XML_ELEMENT)) {
					JScenarioGoal g = JScenarioGoal.Load(reader);
					goals.Add(g);
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == JGameRule.XML_ELEMENT)) {
					JGameRule r = JGameRule.Load(reader);
					gameRules.Add(r);
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == JVarContext.XML_ELEMENT)) {
					oldContext.Load(reader);
				}
				else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "script")) {
					mainScript = reader.GetAttribute("text");
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "scenario")) {
					break;
				}
			}
		}
		context = new EcoContext(this);
		CompileScript();
		foreach (JGameRule rule in gameRules) {
			rule.CompileScript(context);
		}
		
		species = speciesList.ToArray();
		animalSpecies = animalSpeciesList.ToArray();
		articles = articleList.ToArray();
		encyclopedia = encList.ToArray();	
	}
	
	void AddPrefab(string assetsFile, string name, GameObject go) {
		if (extraPrefabs.ContainsKey(name)) {
			MyDebug.LogWarning(assetsFile + ": Prefab with '" + name + "' already loaded.");
		}
		else {
			extraPrefabs.Add(name, go);
		}
	}

	void AddTexture(string assetsFile, string name, Texture2D tex) {
		if (extraTextures.ContainsKey(name)) {
			MyDebug.LogWarning(assetsFile + ": Texture with '" + name + "' already loaded.");
		}
		else {
			extraTextures.Add(name, tex);
		}
	}
	
	public IEnumerator COLoadExtraAssets() {
		extraPrefabs.Clear();
		extraTextures.Clear();
		string[] files = Directory.GetFiles(scenarioPath, "*.unity3d");
		foreach (string file in files) {
			Debug.Log("Loading assets file '" + file + "'");
			
			IEnumerator e = COLoadExtraAsset(file);
			while (e.MoveNext()) yield return e.Current;
		}
	}
	
	public IEnumerator COLoadExtraAsset(string path) {
//		string assetsPath = scenarioPath + path;
		if (File.Exists(path)) {
			byte[] data = File.ReadAllBytes(path);
			AssetBundleCreateRequest assetBCR = AssetBundle.CreateFromMemory(data);
			yield return assetBCR;
			AssetBundle assetB = assetBCR.assetBundle;
			if (assetB != null) {
				Object[] gos = assetB.LoadAll();
				foreach (Object o in gos) {
					if ((o is GameObject) && (o.name.EndsWith("Prefab"))) {
						AddPrefab(path, o.name.Substring(0, o.name.Length - 6), (GameObject) o);
					}
					else if (o is Texture2D) {
						AddTexture(path, o.name, (Texture2D) o);
					}
					else {
//						Debug.LogWarning("Unknown '" + o.name + "' type '" + o.GetType().ToString() + "'");
					}
				}				
			}
			assetB.Unload(false);
		}
	}
	
	
	public void ProcessGoals(int mode) {
//		JExpression.ClearCache();
//		for (int i = 0; i < goals.Count; i++) {
//			goals[i].TryGoal(i, oldContext, mode);
//		}
		
		foreach (JGameRule rule in gameRules) {
			if (((mode & rule.ruleMask) != 0) && (!rule.disabled)) {
				rule.Execute(context);
			}
		}
	}
	
	public void UpdateSuccessionIndices() {
		for (int i = 0; i < successions.Length; i++) {
			successions[i].index = i;
			JXVegetation[] vegetation = successions[i].vegetation;
			for (int j = 0; j < vegetation.Length; j++) {
				vegetation[j].succession = successions[i];
				vegetation[j].index = j;
				JXTerrainTile[] tiles = vegetation[j].tiles;
				for (int k = 0; k < tiles.Length; k++) {
					tiles[k].index = k;
					tiles[k].vegetation = vegetation[j];
				}
			}
		}
	}
	
	public Texture2D ReplaceTexture(Texture2D tex) {
		if (extraTextures.ContainsKey(tex.name)) {
			return extraTextures[tex.name];
		}
		return tex;
	}
	
	public Texture2D LoadTexure(string resPath, string name) {
		if (extraTextures.ContainsKey(name)) {
//			Debug.Log("Using extra texture '" + name + "'");
			return extraTextures[name];
		}
		else {
//			Debug.Log("Using resource texture '" + name + "'");
			return (Texture2D) Resources.Load(resPath + "/" + name);
		}
	}
}