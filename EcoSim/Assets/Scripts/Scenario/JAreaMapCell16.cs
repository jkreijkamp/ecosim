using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

/**
 * a JCellData sized map for containing a 4-bit number per tile
 */
public class JAreaMapCell16 : JAreaMapCell {
			
	/**
	 * return true if all map values are 0
	 */
	public override bool IsEmpty() {
		return (bitmap == null);
	}
	
	/**
	 * Creates a map with all values set to 0
	 */
	public JAreaMapCell16(int cx, int cy) : base(cx, cy) {
		isChanged = false;
		countMap = new int[16];
		countMap[0] = SQUARE_CELL_SIZE;
	}
	
	public JAreaMapCell16(int cx, int cy, int[] countMap, byte[] data) : base(cx, cy) {
		this.countMap = countMap;
		bitmap = data;
		isChanged = false;
	}
	
	/**
	 * copy constructor
	 */
	public JAreaMapCell16(JAreaMapCell16 copyFrom) : base(copyFrom.cx, copyFrom.cy) {
		isChanged = copyFrom.isChanged;
		if (copyFrom.bitmap != null) bitmap = (byte[]) copyFrom.bitmap.Clone();
		countMap = (int[]) copyFrom.countMap.Clone();
	}
	
	/**
	 * makes a copy of this JAreaMapCell into target, the cx and cy coordinates of target must match 
	 * the cx and cy coordinates of this instance, otherwise an exception is thrown
	 */
	public override void CopyInto(JAreaMapCell target) {
		if ((target.cx != cx) || (target.cy != cy)) {
			throw new System.Exception("CopyInto: target [" + target.cx + ", " + target.cy + "] doesn't match [" + cx + ", " + cy + "].");
		}
		if (target is JAreaMapCell16) {
			JAreaMapCell16 c = (JAreaMapCell16) target;
			if (bitmap != null) c.bitmap = (byte[]) bitmap.Clone();
			c.countMap = (int[]) countMap.Clone();
			c.isChanged = isChanged;
		}
		throw new System.Exception("Target type (" + target.GetType().ToString() + ") doesn't match source type (JAreaMapCell16)");
	}
	
	/**
	 * clone operator
	 */
	public override JAreaMapCell Clone() {
		return new JAreaMapCell16(this);
	}

	/**
	 * saves map to xml
	 */
	public override bool SaveToXml(XmlTextWriter writer) {
		isChanged = false;
		if (bitmap == null) return false;
		writer.WriteStartElement("cell");
		writer.WriteAttributeString("x", cx.ToString());
		writer.WriteAttributeString("y", cy.ToString());
		writer.WriteAttributeString("count", JStringUtils.IAToString(countMap));
		writer.WriteBase64(bitmap, 0, bitmap.Length);
		writer.WriteEndElement();
		return true;
	}
	
	/**
	 * sets value v (0..15) at position x, y
	 */
	public override void Set(int x, int y, int v) {
		if (v == 0) {
			Clear(x, y);
			return;
		}
		if (bitmap == null) {
			bitmap = new byte[SQUARE_CELL_SIZE / 2];
		}
		int i = x + CELL_SIZE * y;
		int p = i >> 1;
		int val = bitmap[p];
		if ((i & 0x01) == 0) {
			countMap[val & 0x0f]--;
			countMap[v]++;
			bitmap[p] = (byte) ((val & 0xf0) | v);
		}
		else {
			countMap[(val >> 4) & 0x0f]--;
			countMap[v]++;
			bitmap[p] = (byte) ((val & 0x0f) | (v << 4));
		}
		isChanged = true;
	}
	
	/**
	 * return value (0..15) from position x, y
	 */
	public override int Get(int x, int y) {
		if (bitmap == null) return 0;
		int i = x + CELL_SIZE * y;
		int p = i >> 1;
		int val = bitmap[p];
		if ((i & 0x01) == 0) return val & 0x0f;
		else return (val >> 4) & 0x0f;
	}
	
	/**
	 * sets value to 0 at position x, y
	 */
	public override void Clear(int x, int y) {
		if (bitmap == null) return;
		
		int i = x + CELL_SIZE * y;
		int p = i >> 1;
		int val = bitmap[p];
		if ((i & 0x01) == 0) {
			if ((val & 0x0f) != 0) {
				bitmap[p] = (byte) (val & 0xf0);
				countMap[val & 0x0f]--;
				countMap[0]++;
				isChanged = true;
				if (countMap[0] == SQUARE_CELL_SIZE) {
					bitmap = null;
				}
			}
		}
		else {
			if ((val & 0xf0) != 0) {
				bitmap[p] = (byte) (val & 0x0f);
				countMap[(val >> 4) & 0x0f]--;
				countMap[0]++;
				isChanged = true;
				if (countMap[0] == SQUARE_CELL_SIZE) {
					bitmap = null;
				}
			}
		}
	}
	
	
	/**
	 * Generates mesh, using height data from cell.
	 * values are only shown for tiles supporting succession
	 * mesh is not scaled and origin is corner of cell
	 */
	public override void GenerateMesh(JCellData cell, Mesh mesh) {
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> indices = new List<int>();
		float uvStep = 1f / 4f;
		byte[] successionBitmap = cell.GetSuccessionBitmap();
		int count = 0;
		int p = 0;
		int val = 0;
		int v = 0;
		
		int sp = 0;
		int spp = 0x80;
		int sval = 0;
		for (int y = 0; y < CELL_SIZE; y++) {
			for (int x = 0; x < CELL_SIZE; x++) {
				if (spp == 0x80) {
					sval = successionBitmap[sp++];
					spp = 0x01;
				}
				else {
					spp = spp << 1;
				}
				if (bitmap != null) {
					if ((x & 0x01) == 0) {
						val = bitmap[p++];
						v = val & 0x0f;
					}
					else {
						v = (val >> 4) & 0x0f;
					}
				}
				
				
				
				if ((sval & spp) != 0) {
					int uvX = v % 4;
					int uvY = v / 4;
					uv.Add(new Vector2(uvStep * uvX, uvStep * uvY));
					uv.Add(new Vector2(uvStep * (uvX + 1), uvStep * uvY));
					uv.Add(new Vector2(uvStep * uvX, uvStep * (uvY + 1)));
					uv.Add(new Vector2(uvStep * (uvX + 1), uvStep * (uvY + 1)));
					vertices.Add(new Vector3(x, cell.GetCornerHeight(x, y), y));
					vertices.Add(new Vector3(x + 1, cell.GetCornerHeight(x + 1, y), y));
					vertices.Add(new Vector3(x, cell.GetCornerHeight(x, y + 1), y + 1));
					vertices.Add(new Vector3(x + 1, cell.GetCornerHeight(x + 1, y + 1), y + 1));
					indices.Add(count + 1);
					indices.Add(count);
					indices.Add(count + 2);
					indices.Add(count + 1);
					indices.Add(count + 2);
					indices.Add(count + 3);
					count += 4;
				}
			}
		}
		mesh.Clear();
		mesh.vertices = vertices.ToArray();
		mesh.uv = uv.ToArray();
		mesh.triangles = indices.ToArray();
		mesh.Optimize();
		mesh.RecalculateNormals();
	}

	/**
	 * Generates mesh, using height data from cell.
	 * tiles are only shown for values 1 or higher
	 * mesh is not scaled and origin is corner of cell
	 */
	public override void GenerateMeshShowOnePlus(JCellData cell, Mesh mesh) {
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> indices = new List<int>();
		float uvStep = 1f / 4f;
		int count = 0;
		int p = 0;
		int val = 0;
		int v = 0;
		
		for (int y = 0; y < CELL_SIZE; y++) {
			for (int x = 0; x < CELL_SIZE; x++) {
				if ((x & 0x01) == 0) {
					val = bitmap[p++];
					v = val & 0x0f;
				}
				else {
					v = (val >> 4) & 0x0f;
				}
				
				if (v > 0) {
					int uvX = v % 4;
					int uvY = v / 4;
					uv.Add(new Vector2(uvStep * uvX, uvStep * uvY));
					uv.Add(new Vector2(uvStep * (uvX + 1), uvStep * uvY));
					uv.Add(new Vector2(uvStep * uvX, uvStep * (uvY + 1)));
					uv.Add(new Vector2(uvStep * (uvX + 1), uvStep * (uvY + 1)));
					vertices.Add(new Vector3(x, cell.GetCornerHeight(x, y), y));
					vertices.Add(new Vector3(x + 1, cell.GetCornerHeight(x + 1, y), y));
					vertices.Add(new Vector3(x, cell.GetCornerHeight(x, y + 1), y + 1));
					vertices.Add(new Vector3(x + 1, cell.GetCornerHeight(x + 1, y + 1), y + 1));
					indices.Add(count + 1);
					indices.Add(count);
					indices.Add(count + 2);
					indices.Add(count + 1);
					indices.Add(count + 2);
					indices.Add(count + 3);
					count += 4;
				}
			}
		}
		mesh.Clear();
		mesh.vertices = vertices.ToArray();
		mesh.uv = uv.ToArray();
		mesh.triangles = indices.ToArray();
		mesh.Optimize();
		mesh.RecalculateNormals();
	}

}
