using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;

/**
 * a JCellData sized map for containing a number per tile
 */

public abstract class JAreaMapCell {

	protected const int CELL_SIZE = JTerrainData.CELL_SIZE;
	protected const int SQUARE_CELL_SIZE = CELL_SIZE * CELL_SIZE;

	protected bool isChanged;
	protected int[] countMap;
	protected byte[] bitmap;
	public readonly int cx;
	public readonly int cy;
	
	protected JAreaMapCell(int cx, int cy) {
		this.cx = cx;
		this.cy = cy;
	}
	
	/**
	 * expects reader has just encountered 'cell' element
	 * returns an areamap cell or throws an exception if things go wrong
	 */
	public static JAreaMapCell LoadFromXML(XmlTextReader reader) {
		int cx = int.Parse(reader.GetAttribute("x"));
		int cy = int.Parse(reader.GetAttribute("y"));
		int[] countMap = JStringUtils.StringToIA(reader.GetAttribute("count"));
		int len = 0;
		int nrValues = countMap.Length;
		if (nrValues == 16) {
			len = SQUARE_CELL_SIZE / 2;
		}
		else {
			throw new System.Exception("unsupported bitmap size " + countMap.Length);
		}
		byte[] data = new byte[len];
				
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Text)) {
					int readLen = reader.ReadContentAsBase64(data, 0, data.Length);
					if (readLen != data.Length) {
						throw new System.Exception("Error in input data");
					}
					break; // hack but somehow I don't see the </cell> end element after ReadContentAsBase64
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "cell")) {
					break;
				}
			}
		}
		
		if (nrValues == 16) {
			return new JAreaMapCell16(cx, cy, countMap, data);
		}
		return null; // shouldn't happen as exception is already thrown when an unsupported size is encountered
	}
	
	/**
	 * writes map to writer (element name is 'cell'), if empty nothing is actually written
	 * map is marked as not changed after calling method (property IsChanged is false).
	 */
	public abstract bool SaveToXml(XmlTextWriter writer);
	
	/**
	 * return true if all map values are 0
	 */
	public abstract bool IsEmpty();
	
	/**
	 * returns number of occurances of v (0..n) in map
	 */
	public int GetCount(int v) { return countMap[v]; }
	
	/**
	 * true <=> data structure has changed since last save (or manual clearing of property)
	 * Note: originally this was a read-only property, but changed it as it was useful to have
	 * more control over it
	 */
	public bool IsChanged { get { return isChanged; }  set { isChanged = value; } }
	

	/**
	 * sets value v (0..n) at position x, y
	 */
	public abstract void Set(int x, int y, int v);

	/**
	 * return value (0..n) from position x, y
	 */
	public abstract int Get(int x, int y);

	/**
	 * sets value to 0 at position x, y
	 */
	public abstract void Clear(int x, int y);

	/**
	 * Generates mesh, using height data from cell.
	 * values are only shown for tiles supporting succession
	 * mesh is not scaled and origin is corner of cell
	 */
	public abstract void GenerateMesh(JCellData cell, Mesh mesh);

	/**
	 * Generates mesh, using height data from cell.
	 * tiles are only shown for values 1 or higher
	 * mesh is not scaled and origin is corner of cell
	 */
	public abstract void GenerateMeshShowOnePlus(JCellData cell, Mesh mesh);
	
	/**
	 * makes a copy of this JAreaMapCell into target (target type must match source type)
	 */
	public abstract void CopyInto(JAreaMapCell target);
	
	/**
	 * clone method
	 */
	public abstract JAreaMapCell Clone();
	
}
