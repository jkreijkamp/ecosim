using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

/**
 * indicates locations of where inventarisations of species are done
 * other types of research aren't done through a researchmap but
 * by markers
 */
public class JInventarisationResultMap : JAreaMap {
	
	public const string XML_ELEMENT = "inventarisationresult";

	public readonly EResearchTypes researchType;
	public readonly int year;
	
	private JInventarisationResultMap(EResearchTypes research, int year) {
		researchType = research;
		this.year = year;
	}
	
	/**
	 * makes an empty research map
	 */
	public JInventarisationResultMap(JScenario scenario, EResearchTypes research, int year) {
		researchType = research;
		this.year = year;
		int yLen = scenario.data.cHeight;
		int xLen = scenario.data.cWidth;
		maps = new JAreaMapCell16[yLen, xLen];
		for (int cy = 0; cy < yLen; cy++) {
			for (int cx = 0; cx < xLen; cx++) {
				maps[cy, cx] = new JAreaMapCell16(cx, cy);
			}
		}		
	}
	
	/**
	 * Copy constructor
	 */
	public JInventarisationResultMap(JInventarisationResultMap copyFrom) {
		researchType = copyFrom.researchType;
		year = copyFrom.year;
		maps = copyFrom.CloneMap();
	}
	
	/**
	 * clone method
	 */
	public override JAreaMap Clone() {
		JInventarisationResultMap mm = new JInventarisationResultMap(researchType, year);
		mm.maps = CloneMap();
		return mm;
	}
	
	/**
	 * used for EcoScript ProductOfTargetMap function
	 */
	public JInventarisationResultMap ProductOfTargetMap(JScenario scenario, int targetId) {
		JInventarisationResultMap newMap = new JInventarisationResultMap(scenario, researchType, year);
		JTerrainData data = scenario.data;
		int yLen = data.cHeight;
		int xLen = data.cWidth;
		// newMap.maps = new JAreaMapCell16[yLen, xLen];
		for (int cy = 0; cy < yLen; cy++) {
			for (int cx = 0; cx < xLen; cx++) {
				JCellData cell = data.GetCell(cx, cy);
				byte[] targetMap = cell.GetTargetMap(targetId);
				JAreaMapCell16 mc = maps[cy, cx] as JAreaMapCell16;
				JAreaMapCell16 nmc = newMap.maps[cy, cx] as JAreaMapCell16;
				if (targetMap != null) {
					int p = 0;
					int pp = 0x80; // bit mask (start with 0x80 instead of 0x01 to force reading in new byte)
					int val = 0; // current byte of target mask
					for (int y = 0; y < JTerrainData.CELL_SIZE; y++) {
						for (int x = 0; x < JTerrainData.CELL_SIZE; x++) {
							if (pp == 0x80) {
								// we need to read in new byte from target bitmap
								val = targetMap[p++];
								// and set mask to first bit
								pp = 0x01;
							}
							else {
								// shift bit mask to look at next bit
								pp = pp << 1;
							}
							if ((val & pp) != 0) {
								nmc.Set(x, y, mc.Get(x, y));
							}
						}
					}					
				}
			}
		}
		// scenario.progress.AddInventarisationResultMap(newMap);
		return newMap;
	}
	
	/**
	 * Read in a research map from xml stream, stream should be at the element defining the map
	 */
	public static JInventarisationResultMap LoadFromXml(JScenario scenario, XmlTextReader reader) {
		string elementName = reader.Name.ToLower();
		EResearchTypes research = EnumExtensions.GetResearchType(reader.GetAttribute("research"));
		int year = int.Parse(reader.GetAttribute("year"));
		JInventarisationResultMap map = new JInventarisationResultMap(research, year);
		
		int yLen = scenario.data.cHeight;
		int xLen = scenario.data.cWidth;
		
		map.maps = new JAreaMapCell[yLen, xLen];
		
		// read in stored cells
		if (!reader.IsEmptyElement) {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "cell")) {
					JAreaMapCell cell = JAreaMapCell.LoadFromXML(reader);
					map.maps[cell.cy, cell.cx] = cell;
				}
				else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == elementName)) {
					break;
				}
			}
		}
		
		// fill in empty cells
		for (int cy = 0; cy < yLen; cy++) {
			for (int cx = 0; cx < xLen; cx++) {
				if (map.maps[cy, cx] == null) {
					// for now it's always a 4-bit per tile map
					map.maps[cy, cx] = new JAreaMapCell16(cx, cy);
				}
			}
		}
		return map;
	}
	
	public override void SaveToXml(XmlTextWriter writer) {
		writer.WriteStartElement(XML_ELEMENT);
		writer.WriteAttributeString("research", researchType.ToString());
		writer.WriteAttributeString("year", year.ToString());
		foreach (JAreaMapCell cell in maps) {
			cell.SaveToXml(writer);
		}
		writer.WriteEndElement();
	}
	
	/**
	 * See baseclass for explaination
	 */
	public override void CopyInto(JAreaMap targetMap, bool onlyCopyChangedCells) {
		if (!(targetMap is JInventarisationResultMap)) {
			throw new System.Exception("targetMap is not a JInventarisationResultMap but a " + targetMap.GetType().ToString());
		}
		JInventarisationResultMap castedMap = (JInventarisationResultMap) targetMap;
		if ((castedMap.researchType != researchType) || (castedMap.year != year)) {
			throw new System.Exception("type or year mismatch");
		}
		base.CopyInto(castedMap, onlyCopyChangedCells);
	}
}
