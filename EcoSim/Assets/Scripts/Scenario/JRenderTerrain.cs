using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public class JRenderTerrain : MonoBehaviour {
	
	public Terrain terrain;
	public GameObject objectHolder;
	public GameObject roadsHolder;
	public List<GameObject> waterGOs = null;
	public GameObject waterPrefab;
	
	public static JRenderTerrain self;

	private const int CELL_SIZE = JTerrainData.CELL_SIZE;
	private const int CELL_SIZE2EXP = JTerrainData.CELL_SIZE2EXP;
	private const int CELL_MASK = JTerrainData.CELL_MASK;
	
	public class DetailMap {
		public bool isEmpty = true;
		public int[,] map;
	}
	
	public delegate void DNotifyDrawnCells(int xMin, int yMin, int xMax, int yMax);
	
	private List<DNotifyDrawnCells> notifyCallbacks;
	private bool neverRendered = false;
	
	void Awake() {
		self = this;
		SetTerrainQuality(null, QualitySettings.GetQualityLevel());
		JRenderTerrain.SetSize(512);
		UpdateSize();
		notifyCallbacks = new List<DNotifyDrawnCells>();
	}
	
	public void AddNotifyCallback(DNotifyDrawnCells cb) {
		notifyCallbacks.Add(cb);
	}
	
	public void RemoveNotifyCallback(DNotifyDrawnCells cb) {
		if (notifyCallbacks.Contains(cb)) {
			notifyCallbacks.Remove(cb);
		}
		else {
			Debug.LogWarning("can't find callback " + cb.ToString());
		}
	}
	
	private int lastOffsetX = -1;
	private int lastOffsetY = -1;
	private int currentSize = -1;
	private int wantedSize = -1;
	
	public static bool isRendering = false;
	
	private static int lastQualityLevel;
	private static int maxTerrainSize = 512;
	
	public static void SetupForScenario(JScenario scenario) {
		if (scenario == null) return;
		self.SetTerrainQuality(scenario, lastQualityLevel);
		if ((scenario.data.width < 512) || (scenario.data.height < 512)) {
//			Debug.Log("Max size forced to 256");
			maxTerrainSize = 256;
			SetSize(maxTerrainSize);
			self.UpdateSize();
		}
		self.neverRendered = true;
	}
	
	public static void SetGenerateMapQuality() {
		self.terrain.basemapDistance = 2000;
		self.terrain.treeBillboardDistance = 0;
		self.terrain.treeMaximumFullLODCount = 0;
		self.terrain.heightmapPixelError = 1.0f;
		self.terrain.detailObjectDensity = 1f;
		self.terrain.detailObjectDistance = 0f;
		self.terrain.treeDistance = 100000f;
//		RenderSettings.fog = false;
	}
	
	private int SetTerrainQuality(JScenario scenario, int ql) {
		int oldQuality = lastQualityLevel;
		lastQualityLevel = ql;
//		RenderSettings.fog = true;
		terrain.treeDistance = 20000f;
		switch (ql) {
		case 0 : // Fastest
			terrain.basemapDistance = 250;
			terrain.treeBillboardDistance = 0;
			terrain.treeMaximumFullLODCount = 0;
			terrain.heightmapPixelError = 5.0f;
			terrain.detailObjectDensity = 0.5f;
			terrain.detailObjectDistance = 100f;
			RenderSettings.fogDensity = 0.001f;
			maxTerrainSize = 256;
			break;
		case 1 : // Fast
			terrain.basemapDistance = 500;
			terrain.treeBillboardDistance = 200;
			terrain.treeMaximumFullLODCount = 4;
			terrain.heightmapPixelError = 2.0f;
			terrain.detailObjectDensity = 0.75f;
			terrain.detailObjectDistance = 200f;
			RenderSettings.fogDensity = 0.0005f;
			maxTerrainSize = 256;
			break;
		case 2 : // Simple
			terrain.basemapDistance = 1000;
			terrain.treeBillboardDistance = 250;
			terrain.treeMaximumFullLODCount = 20;
			terrain.heightmapPixelError = 1.0f;
			terrain.detailObjectDensity = 1f;
			terrain.detailObjectDistance = 250f;
			RenderSettings.fogDensity = 0.00015f;
			maxTerrainSize = 256;
			break;
		case 3 : // Good
			terrain.basemapDistance = 2000;
			terrain.treeBillboardDistance = 300;
			terrain.treeMaximumFullLODCount = 100;
			terrain.heightmapPixelError = 1.0f;
			terrain.detailObjectDensity = 1f;
			terrain.detailObjectDistance = 250f;
			RenderSettings.fogDensity = 0.00015f;
			maxTerrainSize = 512;
			break;
		case 4 : // Beautiful
			terrain.basemapDistance = 2000;
			terrain.treeBillboardDistance = 500;
			terrain.treeMaximumFullLODCount = 300;
			terrain.heightmapPixelError = 1.0f;
			terrain.detailObjectDensity = 1f;
			terrain.detailObjectDistance = 250f;
			RenderSettings.fogDensity = 0.00015f;
			maxTerrainSize = 512;
			break;
		default : //.Fantastic
			terrain.basemapDistance = 2000;
			terrain.treeBillboardDistance = 500;
			terrain.treeMaximumFullLODCount = 300;
			terrain.heightmapPixelError = 1.0f;
			terrain.detailObjectDensity = 1f;
			terrain.detailObjectDistance = 250f;
			RenderSettings.fogDensity = 0.00015f;
			maxTerrainSize = 512;
			break;
		}
//		Debug.Log("Quality set to " + ql);
		if ((scenario != null) && ((scenario.data.width < 512) || (scenario.data.height < 512))) {
//			Debug.Log("Max size forced to 256");
			maxTerrainSize = 256;
		}
		return oldQuality;
	}
	
	public static void TryCleanup(JScenario scenario) {
		if (isRendering) return;
		if (scenario != null) scenario.Unload();
		int currentQL = QualitySettings.GetQualityLevel();
		if (currentQL != lastQualityLevel) {
			self.SetTerrainQuality(scenario, currentQL);
		}
	}
	
	public static void ForceQualityCheck(JScenario scenario) {
		int currentQL = QualitySettings.GetQualityLevel();
		self.SetTerrainQuality(scenario, currentQL);
	}
	
	public static void SetSize(int size) {
		size = Mathf.Min(size, maxTerrainSize);
		if (size != self.wantedSize) {
			self.wantedSize = size;
		}
	}
	
	public void UpdateSize() {
		wantedSize = Mathf.Min(wantedSize, maxTerrainSize);
		if (wantedSize != currentSize) {
			TerrainData data = terrain.terrainData;
			data.heightmapResolution = wantedSize * 4;
			data.baseMapResolution = wantedSize;
			data.alphamapResolution = wantedSize;
			data.SetDetailResolution(wantedSize, 16);
			data.size = new Vector3(JTerrainData.HORIZONTAL_SCALE * wantedSize, JTerrainData.VERTICAL_SCALE, JTerrainData.HORIZONTAL_SCALE * wantedSize);
			terrain.Flush();
			currentSize = wantedSize;
			lastOffsetX = -1000; // force render
			
		}
	}
	
	public static void RemoveAll(JScenario scenario) {
		self.RemoveAllObjects(scenario);
		scenario.data.roads.RemoveAllRoadInstances();
	}
	
	public void RemoveAllObjects(JScenario scenario) {
		JTerrainData terrainData = scenario.data;
		for (int yc = 0; yc < terrainData.cHeight; yc++) {
			for (int xc = 0; xc < terrainData.cWidth; xc++) {
				JCellData cell = terrainData.GetCell(xc, yc);
				cell.BecameInvisible();
			}
		}
		if (waterGOs != null) {
			foreach (GameObject go in waterGOs) {
				MeshFilter filter = go.GetComponent<MeshFilter>();
				DestroyImmediate(filter.mesh);
				DestroyImmediate(go);
			}
		}
		waterGOs = null;
	}
	
	public static void Render(JScenario scenario, float offsetX, float offsetY, bool forceRender) {
		if (scenario == null) return;
		if (isRendering) return;
		TerrainData data = self.terrain.terrainData;
		float scale = data.heightmapScale.x * 4.0f;

		int x = (int) (offsetX / scale);
		int y = (int) (offsetY / scale);
		self.StartCoroutine(self.CORender(scenario, x, y, forceRender));
	}
	
	public static bool RenderIfNeeded(JScenario scenario, float x, float y, float border) {
		if (scenario == null) return false;
		if (isRendering) return false;
		
		TerrainData data = self.terrain.terrainData;
		int resolution = data.baseMapResolution;
		float scale = data.heightmapScale.x * 4.0f;
		
		x = x / scale;
		y = y / scale;
		
		int width = scenario.data.width;
		int height = scenario.data.height;
		
		float borderDistance = border * resolution;
		
		x = Mathf.Clamp(x, borderDistance, width - borderDistance - 1);
		y = Mathf.Clamp(y, borderDistance, height - borderDistance - 1);
		
		Vector3 terrainPos = self.terrain.transform.localPosition / scale;
		float minX = terrainPos.x + borderDistance;
		float minY = terrainPos.z + borderDistance;
		float maxX = terrainPos.x + resolution - borderDistance;
		float maxY = terrainPos.z + resolution - borderDistance;
//		Debug.Log("pos = " + x + "," + y + " box " + minX +"," + minY +" - " + maxX + "," + maxY);
		if ((x < minX) || (y < minY) || (x > maxX) || (y > maxY) || (self.wantedSize > self.currentSize) || self.neverRendered) {
			if (self.wantedSize != self.currentSize) {
				self.UpdateSize();
			}
			
			self.StartCoroutine(self.CORender(scenario, (int) x, (int) y, false));
			return true;
		}
		return false;
	}
	
	public static void LowLevelStartRender(JScenario scenario, int x, int y) {
		self.UpdateSize();
		x += self.currentSize / 2;
		y += self.currentSize / 2;
		self.StartCoroutine(self.CORender(scenario, x, y, true));
	}
	
	private void AddWaterGO(List<CombineInstance> waterInstances) {
		if (JConfig.TEST_ACTIVE) return;
		if (waterInstances == null) return;
		GameObject go = (GameObject) GameObject.Instantiate(self.waterPrefab);
		Transform t = go.transform;
		t.parent = self.objectHolder.transform;
		t.localPosition = Vector3.zero;
		t.localRotation = Quaternion.identity;
		t.localScale = Vector3.one;
		go.layer = JLayers.L_WATER;
		if (waterGOs == null) {
			waterGOs = new List<GameObject>();
		}
		waterGOs.Add(go);
		Mesh mesh = new Mesh();
		MeshFilter filter = go.GetComponent<MeshFilter>();
		mesh.CombineMeshes(waterInstances.ToArray(), true, true);
		foreach (CombineInstance ci in waterInstances) {
			DestroyImmediate(ci.mesh);
		}
		// mesh.Optimize();
		mesh.RecalculateBounds();
		filter.sharedMesh = mesh;
		mesh.RecalculateNormals();
	}
	
	public class THeightmapData {
		public static int waitForCompletion = 0;
		public JCellData cell;
		public float[,] heightMap;
		public float[,,] alpha;
		public List<TreeInstance> trees;
		public int xc;
		public int yc;
		public int resolution;
		public int prototypeCount;
	}
	
	public class TDetailMapReturnData {
		public int xc;
		public int yc;
		public DetailMap[] detailMaps;
	}
	
	private static Stack<TDetailMapReturnData> returnDetailmaps;
	
	private void DoWorkHeightmap(System.Object infoO) {
		THeightmapData info = (THeightmapData) infoO;
		int xc = info.xc;
		int yc = info.yc;
		try {
			List<TreeInstance> trees = new List<TreeInstance>();
			int prototypeCount = info.prototypeCount;
			
			DetailMap[] detailMaps = new DetailMap[prototypeCount];
			for (int i = 0; i < prototypeCount; i++) detailMaps[i] = new JRenderTerrain.DetailMap();
			
			info.cell.CopyHeightmap(info.heightMap, xc << (CELL_SIZE2EXP + 2), yc << (CELL_SIZE2EXP + 2));
			info.cell.AddElementsAsyncPart(info.alpha, detailMaps, xc << CELL_SIZE2EXP, yc << CELL_SIZE2EXP);
			info.cell.AddTrees(trees, xc << CELL_SIZE2EXP, yc << CELL_SIZE2EXP, info.resolution);
			
			
			lock (info.trees) {
				info.trees.AddRange(trees);
			}
			TDetailMapReturnData returnData = new TDetailMapReturnData();
			returnData.xc = xc;
			returnData.yc = yc;
			returnData.detailMaps = detailMaps;
			lock (returnDetailmaps) {
				returnDetailmaps.Push(returnData);
			}
		} catch (System.Exception e) {
			Debug.LogError("in " + xc + "," + yc + ": " + e.ToString());
		}
		THeightmapData.waitForCompletion--;
			
	}
	
	private IEnumerator CORender(JScenario scenario, int offsetX, int offsetY, bool forceRender) {
//		Debug.LogWarning("We start CORender...");
		isRendering = true;
		neverRendered = false;
		if (forceRender) {
			RemoveAllObjects(scenario);
			yield return 0;
			// make sure old objects are destroyed, could be of another scenario
			foreach (Transform t in objectHolder.transform) {
				Debug.LogWarning("Object forcefully removed: " + t.gameObject.name);
				Destroy(t.gameObject);
			}
		}
		
		TerrainData data = terrain.terrainData;
		int resolution = data.baseMapResolution;
		
		float scale = data.heightmapScale.x * 4.0f;
		
		offsetX = (offsetX + CELL_SIZE / 2) & (~CELL_MASK);
		offsetY = (offsetY + CELL_SIZE / 2) & (~CELL_MASK);
		
		offsetX -= resolution / 2;
		offsetY -= resolution / 2;
		if (offsetX < 0) offsetX = 0;
		if (offsetY < 0) offsetY = 0;
		if (offsetX + resolution >= scenario.data.width) offsetX = scenario.data.width - resolution;
		if (offsetY + resolution >= scenario.data.height) offsetY = scenario.data.height - resolution;
		

		offsetX = offsetX & (~CELL_MASK);
		offsetY = offsetY & (~CELL_MASK);
		
		if ((offsetX == lastOffsetX) && (offsetY == lastOffsetY) && !forceRender) {
			isRendering = false;
			yield break;
		}
		JWaitSpinner.StartSpin();

//		Debug.Log("resolution " + resolution + " alphamap " + data.alphamapResolution + " width " + data.heightmapScale + " pos = " + offsetX + "," + offsetY);
		
		lastOffsetX = offsetX;
		lastOffsetY = offsetY;
		
		int cellX = offsetX >> CELL_SIZE2EXP;
		int cellY = offsetY >> CELL_SIZE2EXP;
		int cellC = resolution >> CELL_SIZE2EXP;
		
		foreach (DNotifyDrawnCells cb in notifyCallbacks) {
			cb(cellX, cellY, cellX + cellC - 1, cellY + cellC - 1);
		}
		
		float[,] heightMap = new float[resolution * 4 + 1, resolution * 4 + 1];
		float[,,] alpha = new float[resolution, resolution, 4];
		
		int[,] emptyMap = new int[CELL_SIZE, CELL_SIZE];
		int prototypeCount = data.detailPrototypes.Length;
		
		JTerrainData terrainData = scenario.data;
		returnDetailmaps = new Stack<TDetailMapReturnData>();
		for (int yc = 0; yc < terrainData.cHeight; yc++) {
			for (int xc = 0; xc < terrainData.cWidth; xc++) {
				if ((xc < cellX) || (yc < cellY) || (xc >= cellX + cellC) || (yc >= cellY + cellC)) {
					JCellData cell = terrainData.GetCell(xc, yc);
					cell.BecameInvisible();
				}
			}
		}

		Vector3 worldPos = new Vector3(offsetX * scale, 0f, offsetY * scale);
		
		List<TreeInstance> trees = new List<TreeInstance>();
		List<CombineInstance> waterInstances = null;
		int vertexCount = 0;
		
//		float testTime = Time.realtimeSinceStartup;
		for (int yc = 0; yc < cellC; yc++) {
			for (int xc = 0; xc < cellC; xc++) {
				// float xPos = (xc + cellX) * CELL_SIZE * scale;
				// float yPos = (yc + cellY) * CELL_SIZE * scale;
				JCellData cell = terrainData.GetCell(cellX + xc, cellY + yc);
				cell.PrepareAsyncPart();
				THeightmapData info = new THeightmapData();
				info.prototypeCount = prototypeCount;
				info.cell = cell;
				info.heightMap = heightMap;
				info.alpha = alpha;
				info.xc = xc;
				info.yc = yc;
				info.resolution = resolution;
				info.trees = trees;
				THeightmapData.waitForCompletion++;
				ThreadPool.QueueUserWorkItem(new WaitCallback(self.DoWorkHeightmap), info);
			}
		}
		bool ready = false;
		
		while (!ready) {
			TDetailMapReturnData returnData = null;
			lock (returnDetailmaps) {
				if (returnDetailmaps.Count > 0) {
					returnData = returnDetailmaps.Pop();
				}
			}

			
			if (returnData != null) {
				for (int i = 0; i < prototypeCount; i++) {
					data.SetDetailLayer(returnData.xc << CELL_SIZE2EXP, returnData.yc << CELL_SIZE2EXP,
						i, returnData.detailMaps[i].isEmpty?emptyMap:(returnData.detailMaps[i].map));
				}
			}
			else {
				ready = (THeightmapData.waitForCompletion <= 0);
				yield return 0;
			}		
		}
		
//		Debug.Log("Time " + (Time.realtimeSinceStartup - testTime));
		
		List<GameObject> oldWaterGOs = waterGOs;
		waterGOs = null;
		
		for (int yc = 0; yc < cellC; yc++) {
			for (int xc = 0; xc < cellC; xc++) {
				yield return 0;
				float xPos = (xc + cellX) * CELL_SIZE * scale;
				float yPos = (yc + cellY) * CELL_SIZE * scale;
				JCellData cell = terrainData.GetCell(cellX + xc, cellY + yc);
				
				// cell.AddElements(alpha, detailMaps, xc << CELL_SIZE2EXP, yc << CELL_SIZE2EXP);
				// cell.AddTrees(trees, xc << CELL_SIZE2EXP, yc << CELL_SIZE2EXP, resolution);
				
				cell.AddObjects(self.objectHolder, xPos, yPos, scale);
				// cell.MakeWaterGO(self.objectHolder, xc << CELL_SIZE2EXP, yc << CELL_SIZE2EXP, scale);
				Mesh m = cell.MakeWaterMesh();
				if (m != null) {
					int vertexCountMesh = m.vertexCount;
					if (vertexCount + vertexCountMesh >= 65000) {
						AddWaterGO(waterInstances);
						waterInstances = null;
						vertexCount = 0;
					}
					// prevent water mesh to become too big
					if (vertexCount + vertexCountMesh < 65000) {
						if (waterInstances == null) {
						    waterInstances = new List<CombineInstance>();
						}
						CombineInstance ci = new CombineInstance();
						ci.mesh = m;
						ci.subMeshIndex = 0;
						ci.transform = Matrix4x4.TRS(new Vector3(xPos, 0f, yPos), Quaternion.identity, new Vector3(scale, 1f, scale));
						waterInstances.Add(ci);
						vertexCount += vertexCountMesh;
					}
					else {
						Debug.Log("Too many vertices " + vertexCount + " + " + vertexCountMesh + " = " + (vertexCount + vertexCountMesh));
					}
				}
			}
		}
		yield return 0;
		emptyMap = null;
		data.SetHeights(0, 0, heightMap);
		data.SetAlphamaps(0, 0, alpha);
		data.treeInstances = trees.ToArray();
		
		AddWaterGO(waterInstances);
		// remove old water planes...
		if (oldWaterGOs != null) {
			foreach (GameObject go in oldWaterGOs) {
				MeshFilter filter = go.GetComponent<MeshFilter>();
				DestroyImmediate(filter.mesh);
				DestroyImmediate(go);
			}
		}
		
		terrain.transform.localPosition = worldPos;
		JWaitSpinner.StopSpin();
		isRendering = false;
//		Debug.LogWarning("We end rendering...");
	}
}
