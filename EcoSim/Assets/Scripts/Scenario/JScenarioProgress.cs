using System.Collections.Generic;
using System.IO;
using System.Xml;

public class JScenarioProgress {
	public string playerFirstName = "Jan";
	public string playerFamilyName = "de Vries";
	public bool isMale = true;
	
	public long budget = 0;
	public int year = 0;
	public bool allowResearch = true;
	public bool allowMeasures = true;
	
	public string scenarioName;
	public JScenario scenario; // note that scenario isn't always set!
	
	private Dictionary<EActionGroups, JMeasureMap> actionAreas;
	private JInventarisationMap inventarisationMap;
	private List<JInventarisationResultMap> inventarisationResultMaps;
	private List<JResearchPoint> researchPoints;
//	private Dictionary<EResearchTypes, JAreaMap> researchAreas;
	public List<string> encyclopediaViewed;
	
	public void MarkEncyclopediaEntryViewed(string name) {
		name = name.ToLower();
		if (!encyclopediaViewed.Contains(name)) encyclopediaViewed.Add(name);
	}
	
	public bool IsEncyclopediaEntryViewed(string name) {
		name = name.ToLower();
		return encyclopediaViewed.Contains(name);
	}
	
	public int CountEncyclopediaViewer() {
		return encyclopediaViewed.Count;
	}
	
	public long CalculateYearlyCosts() {
		long total = 0;
		foreach (JResearchPoint rp in researchPoints) {
			foreach (JResearchPoint.Measurement m in rp.measurements) {
				if (m.msg == null) {
					total += scenario.research[m.researchType].costPerTile;
				}
			}
		}
		foreach (JMeasureMap map in actionAreas.Values) {
			foreach (EActionTypes at in EnumExtensions.GetActionTypesForGroup(map.actionGroup)) {
				int tileCount = map.Count(((int) at) & 0x0f);
				if (tileCount > 0) {
					JScenario.ActionInfo ai = scenario.GetActionInfo(at);
					if (ai != null) {
						total += ai.costPerTile * tileCount;
					}
					else {
						UnityEngine.Debug.LogError("Actiontype " + at.ToString() + " has no action info tile count = " + tileCount);
					}
				}
			}
		}
		foreach (JScenario.ActionInfo info in scenario.actions.Values) {
			JScenario.ActionGroup grp = info.actionGroup;
			int cost = info.costPerTile;
			if ((grp != null) && (cost != 0)) {
				foreach (JScenario.ActionObject obj in grp.objects) {
					if ((obj.isChanged) && (!obj.isEnabled)) total += cost;
				}
			}
		}
		if (inventarisationMap != null) {
			for (int i = 1; i < 16; i++) {
				int invCount = inventarisationMap.Count(i);
				if (invCount > 0) {
					total += scenario.research[(EResearchTypes)(i + 0x20)].costPerTile * invCount;
				}
			}	
		}
//		foreach (JAreaMap map in researchAreas.Values) {
//			total += scenario.GetResearchInfo(map.researchType).costPerTile * map.count;
//		}
		return total;
	}
	
	/**
	 * Get all Research points that have data
	 */
	public JResearchPoint[] GetResearchPointsArray() {
		return researchPoints.ToArray();
	}
	
	public void AddResearchPoint(JResearchPoint rp) {
		researchPoints.Add(rp);
	}

	public void RemoveResearchPoint(JResearchPoint rp) {
		researchPoints.Remove(rp);
	}
	
	
	
	/**
	 * returns measuremap for actiongroup actiontype is part of.
	 */
	public JMeasureMap GetMeasureMap(EActionTypes actionType) {
		EActionGroups ag = (EActionGroups) (((int) actionType) & 0xf0);
		return GetMeasureMap(ag);
	}
		
	/**
	 * returns measuremap (can be an empty measuremap)
	 */
	public JMeasureMap GetMeasureMap(EActionGroups actionGroup) {
		if (!actionAreas.ContainsKey(actionGroup)) {
			JMeasureMap map = new JMeasureMap(scenario, actionGroup);
			actionAreas.Add(map.actionGroup, map);
			return map;
		}
		else {
			return actionAreas[actionGroup];
		}
	}
	
	
	
	public JInventarisationMap GetInventarisationMap() {
		if (inventarisationMap == null) {
			inventarisationMap = new JInventarisationMap(scenario);
		}
		return inventarisationMap;
	}
	
	
	/*
	 * returns true if theres a measuremap for action with at least one data point
	 */
	public bool HasMeasureMap(EActionGroups actionGroup) {
		return actionAreas.ContainsKey(actionGroup);
	}

	public bool HasInventarisationMap() {
		return (inventarisationMap != null);
	}
	
	public void ClearInventarisationMap() {
		inventarisationMap = null;
	}
	
	public void AddInventarisationResultMap(JInventarisationResultMap resultMap) {
		inventarisationResultMaps.Add(resultMap);
	}
	
	public JInventarisationResultMap[] GetInventarisationResultMapArray() {
		return inventarisationResultMaps.ToArray();
	}
	
	JScenarioProgress() {
		scenarioName = null;
		actionAreas = new Dictionary<EActionGroups, JMeasureMap>();
		inventarisationResultMaps = new List<JInventarisationResultMap>();
		researchPoints = new List<JResearchPoint>();
		encyclopediaViewed = new List<string>();
	}
	
	public JScenarioProgress(JScenario scenario) {
		budget = scenario.startBudget;
		year = scenario.startYear;
		scenarioName = scenario.name;
		this.scenario = scenario;
		actionAreas = new Dictionary<EActionGroups, JMeasureMap>();
		inventarisationResultMaps = new List<JInventarisationResultMap>();
		researchPoints = new List<JResearchPoint>();
//		researchAreas = new Dictionary<EResearchTypes, JAreaMap>();
		encyclopediaViewed = new List<string>();
	}

	public static JScenarioProgress Load(string path, bool previewOnly) {
		string fileName = path + "savegame.xml";
		if (!File.Exists(fileName)) return null;
		JScenarioProgress sp = new JScenarioProgress();
		XmlTextReader reader = new XmlTextReader(new System.IO.StreamReader(fileName));
		try {
			while (reader.Read()) {
				XmlNodeType nType = reader.NodeType;
				if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "savegame")) {
					sp.LoadSaveGame(path, reader, previewOnly);
				}
			}
		}
		finally {
			reader.Close();
		}
		return sp;
	}
	

	public void Save(string path) {
		if (!Directory.Exists(path)) {
			Directory.CreateDirectory(path);
		}
		string fileName = path + "savegame.xml";
		XmlTextWriter writer = new XmlTextWriter(fileName, System.Text.Encoding.UTF8);
		writer.WriteStartDocument(true);
		writer.WriteStartElement("savegame");
		writer.WriteAttributeString("budget", budget.ToString());
		writer.WriteAttributeString("year", year.ToString());
		writer.WriteAttributeString("scenario", scenarioName);
		writer.WriteAttributeString("allowresearch", allowResearch?"true":"false");
		writer.WriteAttributeString("allowmeasures", allowMeasures?"true":"false");
		writer.WriteAttributeString("firstname", playerFirstName);
		writer.WriteAttributeString("familyname", playerFamilyName);
		writer.WriteAttributeString("gender", isMale?"m":"v");
		
		foreach (JScenario.ActionInfo info in scenario.actions.Values) {
			JScenario.ActionGroup grp = info.actionGroup;
			if (grp != null) {
				int objindex = 0;
				foreach (JScenario.ActionObject obj in grp.objects) {
					if ((obj.isEnabled) || (obj.isChanged)) {
						writer.WriteStartElement("actionobject");
						writer.WriteAttributeString("type", grp.action.ToString().ToLower());
						writer.WriteAttributeString("index", objindex.ToString());
						writer.WriteAttributeString("enabled", obj.isEnabled?"true":"false");
						writer.WriteAttributeString("changed", obj.isChanged?"true":"false");
						writer.WriteEndElement();
					}
					objindex++;
				}
			}
		}
		
		foreach (JMeasureMap map in actionAreas.Values) {
			map.SaveToXml(writer);
		}
		if (inventarisationMap != null) {
			inventarisationMap.SaveToXml(writer);
		}
		foreach (JInventarisationResultMap map in inventarisationResultMaps) {
			map.SaveToXml(writer);
		}
		foreach (JResearchPoint rp in researchPoints) {
			rp.SaveToXml(writer);
		}
		foreach (JScenario.ActionInfo info in scenario.actions.Values) {
			if (info.actionGroup != null) {
				JScenario.ActionGroup actionGroup = info.actionGroup;
				List<int> list = new List<int>();
				for (int i = 0; i < actionGroup.objects.Length; i++) {
					JScenario.ActionObject obj = actionGroup.objects[i];
					if (obj.isEnabled) {
						list.Add(i);
					}
				}
				if (list.Count > 0) {
					writer.WriteStartElement("actiongroup");
					writer.WriteAttributeString("action", info.action.ToString());
					writer.WriteAttributeString("enabled", JStringUtils.IAToString(list.ToArray()));
					writer.WriteEndElement();
				}
			}
		}
		foreach (JAnimalSpecies a in scenario.animalSpecies) {
			foreach (JAnimalSpecies.Nest n in a.nests) {
				writer.WriteStartElement("nest");
				writer.WriteAttributeString("id", a.index.ToString());
				writer.WriteAttributeString("x", n.position.x.ToString());
				writer.WriteAttributeString("y", n.position.y.ToString());
				writer.WriteAttributeString("f", n.females.ToString());
				writer.WriteAttributeString("m", n.males.ToString());
				writer.WriteEndElement();
			}
		}
		foreach (string entry in encyclopediaViewed) {
			writer.WriteStartElement("viewedentry");
			writer.WriteAttributeString("name", entry);
			writer.WriteEndElement();
		}
		scenario.oldContext.Save(writer);
		scenario.context.SaveVars(writer);
		int count = 0;
		foreach (JGameRule rule in scenario.gameRules) {
			if (rule.disabled) {
				writer.WriteStartElement("disablerule");
				writer.WriteAttributeString("index", count.ToString());
				writer.WriteEndElement();
			}
			count++;
		}
		writer.WriteEndElement();
		writer.WriteEndDocument();
		writer.Close();
	}


	void LoadAnimalSpeciesNest(XmlTextReader reader) {
		JAnimalSpecies.Nest nest = new JAnimalSpecies.Nest();
		int id = int.Parse(reader.GetAttribute("id"));
		int x = int.Parse(reader.GetAttribute("x"));
		int y = int.Parse(reader.GetAttribute("y"));
		Coordinate pos = new Coordinate(x, y);
		nest.position = pos;
		nest.females = int.Parse(reader.GetAttribute("f"));
		nest.males = int.Parse(reader.GetAttribute("m"));
		JIOUtils.ReadUntilEndElement(reader, "nest");
		List<JAnimalSpecies.Nest> nests = new List<JAnimalSpecies.Nest>(scenario.animalSpecies[id].nests);
		nests.Add(nest);
		scenario.animalSpecies[id].nests = nests.ToArray();
	}
	
	void LoadSaveGame(string path, XmlTextReader reader, bool previewOnly) {
		bool emptyElt = reader.IsEmptyElement;
		budget = long.Parse(reader.GetAttribute("budget"));
		year = int.Parse(reader.GetAttribute("year"));
		scenarioName = reader.GetAttribute("scenario");
		playerFirstName = reader.GetAttribute("firstname");
		playerFamilyName = reader.GetAttribute("familyname");
		isMale = (reader.GetAttribute("gender").ToLower().StartsWith("m"));
		if (reader.GetAttribute("allowresearch") == "false") allowResearch = false;
		if (reader.GetAttribute("allowmeasures") == "false") allowMeasures = false;
		
		if (!previewOnly) {
			actionAreas = new Dictionary<EActionGroups, JMeasureMap>();
		}

		scenario = JScenario.Load(scenarioName, path, true, previewOnly);
		scenario.progress = this;
		foreach (JScenario.ActionInfo info in scenario.actions.Values) {
			if (info.actionGroup != null) {
				foreach (JScenario.ActionObject obj in info.actionGroup.objects) {
					obj.isEnabled = false; // we get enabled/disabled from progress xml below
				}
			}
		}
		
		// we gonna delete all nest instances for animal species as we are loading them again from progress...
		if (!previewOnly) {
			foreach (JAnimalSpecies s in scenario.animalSpecies) {
				s.nests = new JAnimalSpecies.Nest[0];
			}
		}
		if (emptyElt) return;
		while (reader.Read()) {
			XmlNodeType nType = reader.NodeType;
			if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "actiongroup") && !previewOnly) {
				EActionTypes actionType = EnumExtensions.GetActionType(reader.GetAttribute("action"));
				int[] enabled = JStringUtils.StringToIA(reader.GetAttribute("enabled"));
				JScenario.ActionGroup actionGroup = scenario.actions[actionType].actionGroup;
				foreach (int i in enabled) {
					actionGroup.objects[i].isEnabled = true;
				}
				JIOUtils.ReadUntilEndElement(reader, "actiongroup");
			}
			if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == JMeasureMap.XML_ELEMENT) && !previewOnly) {
				JMeasureMap map = JMeasureMap.LoadFromXml(scenario, reader);
				actionAreas.Add(map.actionGroup, map);
			}
			else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == JInventarisationMap.XML_ELEMENT) && !previewOnly) {
				inventarisationMap = JInventarisationMap.LoadFromXml(scenario, reader);
			}
			else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == JInventarisationResultMap.XML_ELEMENT) && !previewOnly) {
				JInventarisationResultMap irmap = JInventarisationResultMap.LoadFromXml(scenario, reader);
				inventarisationResultMaps.Add(irmap);
			}
			else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == JResearchPoint.XML_ELEMENT) && !previewOnly) {
				JResearchPoint rp = JResearchPoint.LoadFromXml(reader);
				researchPoints.Add(rp);
			}
			else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == JVarContext.XML_ELEMENT) && !previewOnly) {
				scenario.oldContext.Load(reader);
			}
			else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == EcoContext.XML_ELEMENT) && !previewOnly) {
				scenario.context.LoadVar(reader);
			}
			else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "nest") && !previewOnly) {
				LoadAnimalSpeciesNest(reader);
			}
			else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "viewedentry") && !previewOnly) {
				encyclopediaViewed.Add(reader.GetAttribute("name").ToLower());
				JIOUtils.ReadUntilEndElement(reader, "viewedentry");
			}
			else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "disablerule") && !previewOnly) {
				scenario.gameRules[int.Parse(reader.GetAttribute("index"))].Disable();
				JIOUtils.ReadUntilEndElement(reader, "disablerule");
			}
			else if ((nType == XmlNodeType.Element) && (reader.Name.ToLower() == "actionobject") && !previewOnly) {
				EActionTypes at = EnumExtensions.GetActionType(reader.GetAttribute("type"));
				int index = int.Parse(reader.GetAttribute("index"));
				bool ischanged = reader.GetAttribute("changed").ToLower() == "true";
				bool isenabled = reader.GetAttribute("enabled").ToLower() == "true";
				JScenario.ActionInfo info = scenario.actions[at];
				JScenario.ActionGroup grp = info.actionGroup;
				grp.objects[index].isEnabled = isenabled;
				grp.objects[index].isChanged = ischanged;
				JIOUtils.ReadUntilEndElement(reader, "actionobject");
			}
			else if ((nType == XmlNodeType.EndElement) && (reader.Name.ToLower() == "savegame")) {
				break;
			}
		}
	}
}
