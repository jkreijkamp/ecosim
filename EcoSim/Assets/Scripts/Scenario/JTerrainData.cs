using System.IO;
using UnityEngine;

/**
 * Class that stores the terrain data (heights, water, vegetation, ...)
 * Uses JCellData to store smaller segments of the level, to prevent
 * the need of keeping huge amounts of dats active in memory.
 */
public class JTerrainData {
	public JCellData[] cells;
	public JRoadMgr roads;
	
	public int width; // width of map in tiles
	public int height; // height of map in tiles
	
	public int cWidth; // number of cells in width
	public int cHeight; // number of cells in height
	
	public JScenario scenario;
	
	private JTerrainData(JScenario scenario) {
		this.scenario = scenario;
	}
	
	public const int CELL_SIZE2EXP = 6;
	public const int CELL_SIZE = 1 << CELL_SIZE2EXP;
	public const int CELL_MASK = CELL_SIZE - 1;
	public const int MIN_SIZE = 128;
	
	public const float VERTICAL_SCALE = 400f;
	public const float HORIZONTAL_SCALE = 20f;
	

	public delegate void DProcessCellData(JCellData cell, int x, int y, System.Object extra);
	
	/**
	 * used for creating new terrain in new scenario
	 */
	public JTerrainData(JScenario scenario, int width, int height) {
		if ((width % CELL_SIZE != 0) && ((height % CELL_SIZE) != 0)) {
			throw new System.Exception("dimensions should be dividable by " + CELL_SIZE);
		}
		if ((width < MIN_SIZE) || (height < MIN_SIZE)) {
			throw new System.Exception("dimensions should be at least " + MIN_SIZE);
		}
		this.scenario = scenario;
		this.width = width;
		this.height = height;
		
		cWidth = width / CELL_SIZE;
		cHeight = height / CELL_SIZE;
		
		cells = new JCellData[cWidth * cHeight];
		for (int y = 0; y < cHeight; y++) {
			for (int x = 0; x < cWidth; x++) {
				cells[x + y * cWidth] = new JCellData(scenario, x, y);
			}
		}
		roads = new JRoadMgr(scenario);
	}
	
	public void Resize(int cx, int cy, int countX, int countY) {
		JCellData[] newCells = new JCellData[countX * countY];
		for (int y = 0; y < countY; y++) {
			for (int x = 0; x < countX; x++) {
				JCellData cell = cells[(x + cx) + (y + cy) * cWidth];
				cell.Load();
				cell.xIndex = x;
				cell.yIndex = y;
				newCells[x + y * countX] = cell;
			}
		}
		cells = newCells;
		cWidth = countX;
		cHeight = countY;
		width = cWidth * CELL_SIZE;
		height = cHeight * CELL_SIZE;
//		roads.RemoveAllRoadInstances();
		roads.MoveRoads(cx * CELL_SIZE, cy * CELL_SIZE, countX * CELL_SIZE, countY * CELL_SIZE);
	}
	
	public static JTerrainData Load(JScenario scenario) {
		JTerrainData data = new JTerrainData(scenario);
		string fileName = scenario.saveGamePath;
		if (fileName == null) {
			fileName = scenario.scenarioPath;
		}
		fileName += "terrain.dat";
		FileStream stream = new FileStream(fileName, FileMode.Open);
		BinaryReader reader = new BinaryReader(stream);
				
		if (reader.ReadInt64() != JConfig.FILE_VERSION) {
			reader.Close();
			throw new System.Exception("Wrong version for " + fileName);
		}
		data.width = reader.ReadInt32();
		data.height = reader.ReadInt32();
		data.cWidth = data.width / CELL_SIZE;
		data.cHeight = data.height / CELL_SIZE;
		
		data.cells = new JCellData[data.cWidth * data.cHeight];
//		UnityEngine.Debug.Log("Cells = " + data.cWidth + "x" + data.cHeight + " " +data.width + "x" + data.height + " path = " + fileName);
		for (int y = 0; y < data.cHeight; y++) {
			for (int x = 0; x < data.cWidth; x++) {
				JCellData cell = new JCellData(scenario, x, y);
				data.cells[x + y * data.cWidth] = cell;
				byte modus = reader.ReadByte();
				cell.SetModus(modus);			
			}
		}
		reader.Close();
		data.roads = new JRoadMgr(scenario);
		data.roads.Load(scenario);
		
		return data;
	}
	
	
	public void Save(string path, bool saveAllCells) {
//		UnityEngine.Debug.Log("Cells = " + cWidth + "x" + cHeight + " " + width + "x" + height + " path = " + path);
		FileStream stream = new FileStream(path + Path.DirectorySeparatorChar + "terrain.dat", FileMode.Create);
		BinaryWriter writer = new BinaryWriter(stream);
		writer.Write(JConfig.FILE_VERSION);
		writer.Write(width);
		writer.Write(height);
		bool isGameMode = scenario.IsGameMode;
		
		// if true then we must copy saved cells from previous savegame to new savegame slot
		bool mustCopySavedCells = isGameMode && (path != scenario.saveGamePath) && (scenario.saveGamePath != null);
		for (int y = 0; y < cHeight; y++) {
			for (int x = 0; x < cWidth; x++) {
				JCellData cell = cells[x + cWidth * y];
				cell.SaveIfNeeded(path, mustCopySavedCells, saveAllCells);
				writer.Write(cell.GetModus());
			}
		}
		writer.Close();
		stream.Close();
		if (!isGameMode) {
			roads.Save(path);
		}
	}
	
	public void ClearParamType(EParamTypes t) {
		foreach (JCellData cell in cells) {
			cell.ClearParamType(t);
		}
	}

	public void ProcessTargetArea(int targetId, DProcessCellData processFunc, System.Object extra) {
		foreach (JCellData cell in cells) {
			byte[] targetMap = cell.GetTargetMap(targetId);
			if (targetMap != null) {
				int count = 0;
				int p = 0;
				int pp = 0x80; // bit mask (start with 0x80 instead of 0x01 to force reading in new byte)
				int val = 0; // current byte of target mask
				for (int y = 0; y < CELL_SIZE; y++) {
					for (int x = 0; x < CELL_SIZE; x++) {
						if (pp == 0x80) {
							// we need to read in new byte from target bitmap
							val = targetMap[p++];
							// and set mask to first bit
							pp = 0x01;
						}
						else {
							// shift bit mask to look at next bit
							pp = pp << 1;
						}
						if ((val & pp) != 0) {
							processFunc(cell, x, y, extra);
							count++;
						}
					}
				}
//				Debug.Log("target map exists for " + cell.GetCellName() + " count = " + count + " target id = " + targetId);
			}
		}
	}

	public void ProcessTargetAreaMeasure(int targetId, EActionTypes action, DProcessCellData processFunc, System.Object extra) {
		if (!scenario.IsGameMode) return;
		JMeasureMap map = scenario.progress.GetMeasureMap(action);
		if (map == null) return;
		
		int atInt = ((int) action) & 0x0f;
		
		foreach (JCellData cell in cells) {
			byte[] targetMap = cell.GetTargetMap(targetId);
			if (targetMap != null) {
				JAreaMapCell mapCell = map.GetCell(cell.xIndex, cell.yIndex);
				if ((mapCell != null) && (mapCell.GetCount(atInt) > 0)) {
					int p = 0;
					int pp = 0x80; // bit mask (start with 0x80 instead of 0x01 to force reading in new byte)
					int val = 0; // current byte of target mask
					for (int y = 0; y < CELL_SIZE; y++) {
						for (int x = 0; x < CELL_SIZE; x++) {
							if (pp == 0x80) {
								// we need to read in new byte from target bitmap
								val = targetMap[p++];
								// and set mask to first bit
								pp = 0x01;
							}
							else {
								// shift bit mask to look at next bit
								pp = pp << 1;
							}
							if (((val & pp) != 0) && (mapCell.Get(x, y) == atInt)) {
								processFunc(cell, x, y, extra);
							}
						}
					}
				}
			}
		}
	}

	public void ProcessTargetAreaResearch(int targetId, EResearchTypes research, DProcessCellData processFunc, System.Object extra) {
		if (!scenario.IsGameMode) return;
		
		int resInt = ((int) research) & 0x0f;
		JInventarisationMap map = scenario.progress.GetInventarisationMap();
		if (map == null) return;
		
		foreach (JCellData cell in cells) {
			byte[] targetMap = cell.GetTargetMap(targetId);
			if (targetMap != null) {
				JAreaMapCell mapCell = map.GetCell(cell.xIndex, cell.yIndex);
				if ((mapCell != null) && (mapCell.GetCount(resInt) > 0)) {
					int p = 0;
					int pp = 0x80; // bit mask (start with 0x80 instead of 0x01 to force reading in new byte)
					int val = 0; // current byte of target mask
					for (int y = 0; y < CELL_SIZE; y++) {
						for (int x = 0; x < CELL_SIZE; x++) {
							if (pp == 0x80) {
								// we need to read in new byte from target bitmap
								val = targetMap[p++];
								// and set mask to first bit
								pp = 0x01;
							}
							else {
								// shift bit mask to look at next bit
								pp = pp << 1;
							}
							if (((val & pp) != 0) && (mapCell.Get(x, y) == resInt)) {
								processFunc(cell, x, y, extra);
							}
						}
					}
				}
			}
		}
	}	

	public bool CheckTargetMapAt(int targetId, int x, int y) {
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		byte[] targetMap = cells[cell].GetTargetMap(targetId);
		if (targetMap == null) return false;
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		int i = (y << CELL_SIZE2EXP) | x;
		int val = targetMap[i >> 3];
		int offset = 1 << (i & 0x7);
		return (val & offset) != 0;
		
	}
	
	public void RemoveUnusedParameters() {
		foreach (JCellData cell in cells) {
			cell.RemoveUnusedParameters();
		}
	}
	
	public string GetDebugHeight(int x, int y) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return "";
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		return cells[cell].GetDebugHeight(x, y);
	}
		
	public float GetHeight(int x, int y) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return 0f;
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		return cells[cell].GetHeight(x, y);
	}
	
	public float GetWaterHeight(int x, int y) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return 0f;
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		return cells[cell].GetWaterHeight(x, y);
	}
	
	public float GetAdjustedWaterHeight(int x, int y) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return 0f;
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		return cells[cell].GetAdjustedWaterHeight(x, y);
	}

	public JXSuccession GetSuccession(int x, int y) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return null;
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		return cells[cell].GetSuccession(x, y);
	}
	
	public JXVegetation GetVegetation(int x, int y) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return null;
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		return cells[cell].GetVegetation(x, y);
	}
	public JXVegetation GetVegetation(int x, int y, out int variant) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) {
			variant = 0;
			return null;
		}
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		return cells[cell].GetVegetation(x, y, out variant);
	}
	
	public byte GetData(EParamTypes t, int x, int y) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) {
			return 0;
		}
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		return cells[cell].GetData(t, x, y);
	}
	
	public byte GetSpecial(int x, int y) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) {
			return 0;
		}
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		return cells[cell].GetSpecial(x, y);
	}
	
	public void SetHeight(int x, int y, float v) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return;
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		cells[cell].SetHeight(x, y, v);
	}
	
	public void SetWaterHeight(int x, int y, float v) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return;
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		cells[cell].SetWaterHeight(x, y, v);		
	}
	
	public void SetAdjustedWaterHeight(int x, int y, float v) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return;
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		cells[cell].SetAdjustedWaterHeight(x, y, v);		
	}
	
	public void SetVegetation(int x, int y, JXVegetation vegetation, int variant) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return;
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		cells[cell].SetVegetation(x, y, vegetation, variant);		
	}
	
	public void SetVegetation(int x, int y, JXTerrainTile tile) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return;
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		cells[cell].SetVegetation(x, y, tile);		
	}
	
	public void SetData(EParamTypes t, int x, int y, byte v) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return;
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		cells[cell].SetData(t, x, y, v);		
	}

	public void SetSpecial(int x, int y, byte v, byte mask) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return;
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		cells[cell].SetSpecial(x, y, v, mask);		
	}
	
	public void SetSpecial(int x, int y, byte v) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) return;
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		cells[cell].SetSpecial(x, y, v);		
	}
	
	public float GetCornerHeight(int x, int y) {
		if ((x <  0) || (y < 0) || (x >= width + 1) || (y >= height + 1)) return 0f;
		int cx = x >> CELL_SIZE2EXP;
		int cy = y >> CELL_SIZE2EXP;
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		if (cx == cWidth) { cx--; x += CELL_SIZE; }
		if (cy == cHeight) { cy--; y += CELL_SIZE; }
		return cells[cx + cWidth * cy].GetCornerHeight(x, y);
	}
	
	public JCellData.BuildingData AddBuilding(out JCellData cell, Vector3 pos, Quaternion rot, Vector3 scale, JElements.BuildingWrapper building) {
		int cx = Mathf.Clamp((int) (pos.x / (HORIZONTAL_SCALE * CELL_SIZE)), 0, cWidth - 1);
		int cy = Mathf.Clamp((int) (pos.z / (HORIZONTAL_SCALE * CELL_SIZE)), 0, cHeight - 1);
		pos.x = (pos.x / HORIZONTAL_SCALE) - cx  * CELL_SIZE;
		pos.z = (pos.z / HORIZONTAL_SCALE) - cy * CELL_SIZE;
		cell = cells[cx + cWidth * cy];
		return cell.AddBuilding(pos, rot, scale, building);
	}
	
	public void SetNeedCalculateSuccession(int x, int y) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) {
			return;
		}
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		cells[cell].SetNeedCalculateSuccession(x, y);
	}

	public bool NeedCalculateSuccession(int x, int y) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) {
			return false;
		}
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		return cells[cell].NeedCalculateSuccession(x, y);
	}
	
	public bool CellNeedsCalculateSuccession(int cx, int cy) {
		return cells[cx + cWidth * cy].NeedCalculateSuccession();
	}

	public void SetTarget(int index, int x, int y) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) {
			return;
		}
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		cells[cell].SetTarget(index, x, y);
	}

	
	public int GetAnimalSpecies(int x, int y, int index) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) {
			return 0;
		}
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		return cells[cell].GetAnimalSpeciesValue(x, y, index);
	}

	public void SetAnimalSpecies(int x, int y, int index, int v) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) {
			return;
		}
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		cells[cell].SetAnimalSpeciesValue(x, y, index, v);
	}
	
	public int GetSpecies(int x, int y, int index) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) {
			return 0;
		}
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		return cells[cell].GetSpeciesValue(x, y, index);
	}

	public void SetSpecies(int x, int y, int index, int v) {
		if ((x <  0) || (y < 0) || (x >= width) || (y >= height)) {
			return;
		}
		int cell = (x >> CELL_SIZE2EXP) + cWidth * (y >> CELL_SIZE2EXP);
		x = x & CELL_MASK;
		y = y & CELL_MASK;
		cells[cell].SetSpeciesValue(x, y, index, v);
	}
	
	
	
	public JCellData GetCell(int cx, int cy) {
		return cells[cx + cWidth * cy];
	}
	
	const int RANGE = 6;

	public void UpdateAdjustedWaterMap() {
		UpdateAdjustedWaterMap(0, 0, width - 1, height - 1);
	}

	
	public void UpdateAdjustedWaterMap(int xLow, int yLow, int xHigh, int yHigh) {
		xLow = Mathf.Max(0, xLow);
		yLow = Mathf.Max(0, yLow);
		xHigh = Mathf.Min(width - 1, xHigh);
		yHigh = Mathf.Min(height - 1, yHigh);
		for (int y = yLow; y <= yHigh; y++) {
			for (int x = xLow; x <= xHigh; x++) {
				UpdateAdjustedWaterMap(x, y);
			}
		}
	}

	public void UpdateAdjustedWaterMap(int xc, int yc) {
		// float verticalScale = JXTerrain.VERTICAL_SCALE;
		int xLow = Mathf.Max(0, xc - RANGE);
		int yLow = Mathf.Max(0, yc - RANGE);
		int xHigh = Mathf.Min(width - 1, xc + RANGE);
		int yHigh = Mathf.Min(height - 1, yc + RANGE);
		float sloot = 0f;
		float waterHeight = GetWaterHeight(xc, yc);
		float landHeight = GetHeight(xc, yc);
		if (waterHeight < landHeight) {
			for (int y = yLow; y <= yHigh; y++) {
				for (int x = xLow; x <= xHigh; x++) {
					int special = GetSpecial(x, y) & 0x07;
					if (special != 0) {
						int dist = (x - xc) * (x - xc) + (y - yc) * (y - yc);
						if (dist <= 2) {
							sloot = Mathf.Max(sloot, 0.25f + special * 0.107f);
						}
						else if (dist < (RANGE * RANGE)) {
							float d = Mathf.Sqrt(dist - 2);
							sloot = Mathf.Max(sloot, (0.25f + special * 0.107f) / Mathf.Pow(3.0f, d));
						}
					}
				}
			}
		}
		sloot = Mathf.Clamp01(sloot);
		
		SetData(EParamTypes.Sloot, xc, yc, (byte) (255 * sloot));
		if (sloot > 0f) {
//			float limitedRange = 0.75f + 0.25f * sloot;
			float maxHeight = Mathf.Min(waterHeight, landHeight - 1.25f - 0.2f * sloot);
//			SetAdjustedWaterHeight(xc, yc, Mathf.Lerp(waterHeight, maxHeight, limitedRange));
			SetAdjustedWaterHeight(xc, yc, maxHeight);
		}
		else {
			SetAdjustedWaterHeight(xc, yc, waterHeight);
		}
		//SetAdjustedWaterHeight(xc, yc, Mathf.Lerp(waterHeight, landHeight, sloot));
	}
	
	
	public void MakeEmptyCells() {
		foreach (JCellData cell in cells) cell.InitNoData();
	}
}