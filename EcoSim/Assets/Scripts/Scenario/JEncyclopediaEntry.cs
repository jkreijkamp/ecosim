using UnityEngine;
using System.Collections;

public class JEncyclopediaEntry : System.IComparable {
	public string item;
	public string text = "";
	public string externalURL;
	
	public int CompareTo(System.Object o) {
		JEncyclopediaEntry other = o as JEncyclopediaEntry;
		if (other == null) return -1;
		return item.CompareTo(other.item);
	}
}
